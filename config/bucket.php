<?php

return [

    /*
    |--------------------------------------------------------------------------
    | S3 BUCKET
    |--------------------------------------------------------------------------
    |
    | This value is the name of S3 BUCKET.
    |
    */

    's3' => env('AWS_BUCKET'),
    'APP_URL' => env('APP_URL'),
    'AES_KEY' => env('AES_KEY'),
    'MISSING_CONFIGURATION' => 'Configuration is missing.',
    'GAMING_ACCOUNT_PROBLEM' => 'Sorry, we are having technical issues creating your account. Please contact our staff for further assistance.',
    'GAMING_SYSTEM_PROBLEM' => 'Gaming System have problem right now.',
    'SIGNIN_DETAILS_ALREADY_USE' => 'Sign in details already in use. Please use this email to reset your password and sign in.',
    'FORGOT_PASSWORD_EMAIL_NOT_FOUND' => 'Email not found',
    'EMAIL_NOT_FOUND' => 'Email not found, please see our team in venue to update your details.',
    'MOBILE_NOT_FOUND' => 'Mobile not found, please see our team in venue to update your details.',
    'INVALID_ACCOUNT_NUMBER_ID' => 'Invalid account number and/or account ID. Please see our team in venue.',
    'DUPLICATED_EMAIL' => 'Email provided already exists in our system. Please just login.',
    'DUPLICATED_MOBILE' => 'Mobile provided already exists in our system. Please see staff in venue to update your details.',
    'UNABLE_CONNECT_SSO' => 'We are having a few problems connecting, please try again.',
    'UNABLE_CONNECT_BEPOZ' => 'We are having a few problems connecting, please try again.',
    'EMAIL_PASSWORD_INCORRECT' => 'Email or password incorrect. Please try again.',
    'WRONG_CONFIGURATION' => 'Something wrong with configuration, please contact Administrator'

];

cd /var/www2/api/ws
git stash
git pull
docker exec -w /var/www/api/ws laradock_workspace_1 php artisan migrate
docker exec -w /var/www/api/ws laradock_workspace_1 php artisan db:seed --class=SettingsTableSeeder
docker exec -w /var/www/api/ws laradock_workspace_1 php artisan db:seed --class=EmailsTableSeeder
docker exec -w /var/www/api/ws laradock_workspace_1 php artisan db:seed --class=ListingTypesTableSeeder
docker exec -w /var/www/api/ws laradock_workspace_1 php artisan db:seed --class=ProductTypesTableSeeder
exit

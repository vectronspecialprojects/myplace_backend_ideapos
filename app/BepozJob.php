<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BepozJob extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bepoz_jobs';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Lock the current job.
     */
    public function reserve() {
        $this->reserved = 1;
        $this->reserved_at = time();
        $this->save();
    }

    public function free() {
        $this->reserved = 0;
        $this->freed_at = time();
        $this->job_uid = null;
        $this->save();
    }

    public function setJobUID($uid) {
        $this->job_uid = $uid;
        $this->save();
    }
    
    /**
     * Check how many attempts this bepoz has done
     * 
     * @return mixed
     */
    public function attempts() {
        return $this->attempts;
    }

    /**
     * Destroy this job
     * 
     * @throws \Exception
     */
    public function dispatch() {
        $this->delete();
    }
}

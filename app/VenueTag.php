<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VenueTag extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'venue_tags';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $casts = [

    ];

    /**
     * Defines The tier of VenueTag
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */

    /**
     * Get venues
     * >> Define relationship
     */
    public function venues()
    {
        return $this->belongsToMany('App\Venue', 'venue_pivot_tags', 'venue_tag_id', 'venue_id')->wherePivot('deleted_at', null)->withPivot('display_order')->withTimestamps();
    }


    /**
     * Get the related Venue Pivot Tag
     * >> Define relationship
     *
     */
    public function venue_pivot_tags()
    {
        return $this->hasMany('App\VenuePivotTag');
    }

}

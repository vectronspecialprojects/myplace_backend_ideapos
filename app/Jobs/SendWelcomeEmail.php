<?php

namespace App\Jobs;

use App\Email;
use App\Helpers\MandrillExpress;
use App\Helpers\MailjetExpress;
use App\PendingJob;
use App\Platform;
use App\Setting;
use App\SystemLog;
use App\User;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendWelcomeEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $user;

    /**
     * Create a new job instance.
     *
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MandrillExpress $mx, MailjetExpress $mjx)
    {
        try {
            DB::beginTransaction();

            $user = $this->user;
            $company_name = Setting::where('key', 'company_name')->first()->value;

            $platform = Platform::where('name', 'web')->first();

            $url = config('bucket.APP_URL') . '/api/unsubscribeMail/' . $user->member->token . '?app_token=' . $platform->app_token;

            // Send Welcome Email to member
            $data = array(
                'LAST_NAME' => $user->member->last_name,
                'FIRST_NAME' => $user->member->first_name,
                'UNSUB' => $url,
            );

            $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;

            if ($email_server === "mandrill") {

                if ($mx->init()) {
                    $mx->send('welcome', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                } else {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $user->email,
                        'category' => 'welcome',
                        'name' => $user->member->first_name . ' ' . $user->member->last_name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }

            } else if ($email_server === "mailjet") {

                if ($mjx->init()) {
                    $mjx->send('welcome', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                } else {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $user->email,
                        'category' => 'welcome',
                        'name' => $user->member->first_name . ' ' . $user->member->last_name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }

            }

            // send to admin
            $admin_email = Setting::where('key', 'signup_email')->first()->extended_value;
            $admin_name = Setting::where('key', 'admin_full_name')->first()->value;

            if (!is_null($admin_email)) {
                $emails = \GuzzleHttp\json_decode($admin_email, true);
                if (!empty($emails)) {
                    $created_at = Carbon::parse($user->created_at);

                    $data = array(
                        'FIRST_NAME' => $user->member->first_name,
                        'LAST_NAME' => $user->member->last_name,
                        'CURRENT_YEAR' => date("Y"),
                        'ADMIN_NAME' => $admin_name,
                        'EMAIL' => $user->email,
                        'PHONE' => is_null($user->mobile) ? 'Not Provided' : $user->mobile,
                        'JOINED_AT' => $created_at->format('l jS \\of F Y h:i:s A'),
                        'COMPANY' => $company_name
                    );

                    foreach ($emails as $email) {
                        if ($email_server === "mandrill") {

                            if ($mx->init()) {
                                $mx->send('signup_admin', $admin_name, $email, $data);
                            } else {
                                $pj = new PendingJob();
                                $pj->payload = \GuzzleHttp\json_encode($data);
                                $pj->extra_payload = \GuzzleHttp\json_encode(array(
                                    'email' => $email,
                                    'category' => 'signup_admin',
                                    'name' => $admin_name
                                ));
                                $pj->queue = "email";
                                $pj->save();
                            }

                        } else if ($email_server === "mailjet") {

                            if ($mjx->init()) {
                                $mjx->send('signup_admin', $admin_name, $email, $data);
                            } else {
                                $pj = new PendingJob();
                                $pj->payload = \GuzzleHttp\json_encode($data);
                                $pj->extra_payload = \GuzzleHttp\json_encode(array(
                                    'email' => $email,
                                    'category' => 'signup_admin',
                                    'name' => $admin_name
                                ));
                                $pj->queue = "email";
                                $pj->save();
                            }
            
                        }
                    }

                }
            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            $log = new SystemLog();
            $log->type = 'job-error';
            $log->humanized_message = 'Sending welcome email is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'SendWelcomeEmail.php';
            $log->save();
        }
    }
}

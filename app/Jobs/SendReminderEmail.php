<?php

namespace App\Jobs;

use App\Email;
use App\FriendReferral;
use App\Helpers\MandrillExpress;
use App\Helpers\MailjetExpress;
use App\Jobs\Job;
use App\PendingJob;
use App\Platform;
use App\Setting;
use App\SystemLog;
use App\User;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SendReminderEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $user;
    protected $friend;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, FriendReferral $friend)
    {
        $this->user = $user;
        $this->friend = $friend;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MandrillExpress $mx, MailjetExpress $mjx)
    {
        try {
            DB::beginTransaction();

            $user = $this->user;
            $friend = $this->friend;

            $company_name = Setting::where('key', 'company_name')->first()->value;

            $platform = Platform::where('name', 'web')->first();

            $accepted_url = config('bucket.APP_URL') . '/api/acceptReferral/' . $friend->token . '?app_token=' . $platform->app_token;
            $url = config('bucket.APP_URL') . '/api/unsubscribe/' . $friend->token . '?app_token=' . $platform->app_token;

            // send to member
            $data = array(
                'INVITER_FIRST_NAME' => $user->member->first_name,
                'INVITER_LAST_NAME' => $user->member->last_name,
                'INVITEE_FIRST_NAME' => $friend->full_name,
                'CURRENT_YEAR' => date("Y"),
                'COMPANY' => $company_name,
                'TOKEN' => $friend->token,
                'UNSUB' => $url,
                'ACCEPT_URL' => $accepted_url
            );

            $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;

            if ($email_server === "mandrill") {

                if ($mx->init()) {
                    $mx->send('reminder_referral', $friend->name, $friend->email, $data);
                } else {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $friend->email,
                        'category' => 'reminder_referral',
                        'name' => $friend->name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }

            } else if ($email_server === "mailjet") {

                if ($mjx->init()) {
                    $mjx->send('reminder_referral', $friend->name, $friend->email, $data);
                } else {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $friend->email,
                        'category' => 'reminder_referral',
                        'name' => $friend->name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }

            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            $log = new SystemLog();
            $log->type = 'job-error';
            $log->humanized_message = 'Sending reminder email is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'SendReminderEmail.php';
            $log->save();
        }

    }
}

<?php

namespace App\Jobs;

use App\Helpers\Bepoz;
use App\Jobs\Job;
use App\Member;
use App\MemberVouchers;
use App\VoucherSetups;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RetrieveVouchers extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $member;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Member $member)
    {
        $this->member = $member;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Bepoz $bepoz)
    {
        $voc = $bepoz->IDListGet(14, $this->member->bepoz_account_id, null, 1);

        if ($voc) {
            if (intval($voc['IDList']['Count']) > 0) {
                for ($i = 0; $i < count($voc['IDList']['ID']['int']); $i++) {
                    $result = $bepoz->VoucherGet($voc['IDList']['ID']['int'][$i]);

                    if ($result) {
                        $memberVoucher = MemberVouchers::where('voucher_id', $result['Voucher']['VoucherID'])->first();
                        if (is_null($memberVoucher)) {
                            $memberVoucher = new MemberVouchers();
                            $memberVoucher->member_id = $this->member->id;
                            $memberVoucher->category = 'voucher';

                            $vs = VoucherSetups::where('bepoz_voucher_setup_id', $result['Voucher']['VoucherSetupID'])->first();
                            if (is_null($vs)) {
                                $voucherSetupInformation = $bepoz->VoucherSetupGet($result['Voucher']['VoucherSetupID']);
                                if (!$voucherSetupInformation) {
                                    continue;
                                }

                                $vs = new VoucherSetups;
                                $vs->bepoz_voucher_setup_id = $result['Voucher']['VoucherSetupID'];
                                $vs->name = $voucherSetupInformation['VoucherSetup']['Name'];
                                $vs->inactive = boolval($voucherSetupInformation['VoucherSetup']['Inactive']);
                                $vs->expiry_type = intval($voucherSetupInformation['VoucherSetup']['ExpiryType']);
                                $vs->expiry_number = intval($voucherSetupInformation['VoucherSetup']['ExpiryNumber']);
                                $vs->expiry_date = Carbon::parse($voucherSetupInformation['VoucherSetup']['ExpiryDate']);
                                $vs->voucher_type = intval($voucherSetupInformation['VoucherSetup']['VoucherType']);
                                $vs->save();
                            }

                            $memberVoucher->voucher_setup_id = $vs->id;
                            $memberVoucher->name = $vs->name;
                            $memberVoucher->category = 'bepoz';

                        }

                        $memberVoucher->voucher_id = $result['Voucher']['VoucherID'];
                        $memberVoucher->lookup = $result['Voucher']['Lookup'];
                        $memberVoucher->voucher_type = $result['Voucher']['VoucherType'];
                        $memberVoucher->unlimited_use = filter_var($result['Voucher']['UnlimitedUse'], FILTER_VALIDATE_BOOLEAN);
                        $memberVoucher->maximum_discount = $result['Voucher']['MaximumDiscount'];
                        $memberVoucher->claim_venue_id = $result['Voucher']['ClaimVenueID'];
                        $memberVoucher->claim_store_id = $result['Voucher']['ClaimStoreID'];
                        $memberVoucher->used_count = $result['Voucher']['UsedCount'];
                        $memberVoucher->used_value = $result['Voucher']['UsedValue'];
                        $memberVoucher->used_trans_id = $result['Voucher']['UsedTransID'];
                        $memberVoucher->amount_left = $result['Voucher']['AmountLeft'];
                        $memberVoucher->amount_issued = $result['Voucher']['AmountIssued'];
                        $memberVoucher->status = 'successful';

                        $raw_barcode = "99" . str_repeat("0", 10 - strlen($result['Voucher']['Lookup'])) . $result['Voucher']['Lookup'];
                        $memberVoucher->barcode = $raw_barcode . $this->check_digit($raw_barcode);
                        $memberVoucher->issue_date = Carbon::parse($result['Voucher']['IssuedDate']);
                        $memberVoucher->expire_date = strtotime($result['Voucher']['DateExpiry']) && !is_null($result['Voucher']['DateExpiry']) ? Carbon::parse($result['Voucher']['DateExpiry']) : null;
                        $memberVoucher->save();
                    }

                }
            }

        }

    }

    /**
     * Return check digit of the barcode
     *
     * @param $lookup
     * @return int
     */
    protected function check_digit($lookup)
    {
        $sum = 0;
        $codeString = str_split($lookup);

        for ($i = 0; $i < 12; $i++) {
            if (($i % 2) == 0)
                $sum += $codeString[$i];
            else
                $sum += (3 * $codeString[$i]);
        }

        $sum = $sum % 10;

        if ($sum > 0)
            $sum = 10 - $sum;
        else
            $sum = 0;

        return $sum;
    }
}

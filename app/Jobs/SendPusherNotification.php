<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Notification;
use App\Role;
use App\SystemLog;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\App;

class SendPusherNotification extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $data;
    protected $event;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($event, $data)
    {
        $this->event = $event;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $pusher = App::make('pusher');

            $pusher->trigger(env('PUSHER_CHANNEL'),
                $this->event,
                $this->data
            );

        } catch (\Exception $e) {
            $log = new SystemLog();
            $log->humanized_message = 'Failed sending push notification. Please check the error message';
            $log->type = 'pusher-error';
            $log->message = $e;
            $log->source = 'SendPusherNotification.php';
            $log->save();
        }
    }
}

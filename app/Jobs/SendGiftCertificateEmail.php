<?php

namespace App\Jobs;

use App\Email;
use App\GiftCertificate;
use App\Helpers\MandrillExpress;
use App\Helpers\MailjetExpress;
use App\Jobs\Job;
use App\Member;
use App\Order;
use App\PendingJob;
use App\Platform;
use App\Setting;
use App\SystemLog;
use App\User;
use App\VoucherSetups;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendGiftCertificateEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $gf;
    protected $order;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(GiftCertificate $gf, Order $order)
    {
        $this->gf = $gf;
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MandrillExpress $mx, MailjetExpress $mjx)
    {
        try {
            DB::beginTransaction();

            $gf = $this->gf;
            $order = $this->order;

            $company_name = Setting::where('key', 'company_name')->first()->value;
            $platform = Platform::where('name', 'web')->first();

            $member = Member::find($gf->purchaser_id);

            $payload = \GuzzleHttp\json_decode($gf->payload);
            $vs = VoucherSetups::find($payload->voucher_setups_id);


            // send to receiver (who receive a gif voucher)

            $url = config('bucket.APP_URL') . '/api/giftCertificate/' . $gf->token . '?app_token=' . $platform->app_token;

            $data = array(
                'VENUE_NAME' => $company_name,
                'PURCHASER_NAME' => $member->first_name . ' ' . $member->last_name,
                'VOUCHER_NUMBER' => $gf->lookup,
                'VOUCHER_TYPE' => 'Gift Certificate (' . $vs->name . ')',
                'BALANCE' => '$' . $gf->value,
                'EXPIRY_DATE' => $gf->expiry_date,
                'FULL_NAME' => $gf->full_name,
                'EMAIL' => $gf->email,
                'CURRENT_YEAR' => date("Y"),
                'NOTE' => is_null($gf->note) ? '' : $gf->note,
                'URL' => $url
            );

            $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;

            if ($email_server === "mandrill") {

                if ($mx->init()) {
                    $mx->send('gift_certificate_receiver', $gf->full_name, $gf->email, $data);
                } else {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $gf->email,
                        'category' => 'gift_certificate_receiver',
                        'name' => $gf->full_name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }

            } else if ($email_server === "mailjet") {

                if ($mjx->init()) {
                    $mjx->send('gift_certificate_receiver', $gf->full_name, $gf->email, $data);
                } else {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $gf->email,
                        'category' => 'gift_certificate_receiver',
                        'name' => $gf->full_name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }

            }

            // send to purchaser (who will receive a proof of purchase

            $url = config('bucket.APP_URL') . '/api/receipt/' . $gf->token . '?app_token=' . $platform->app_token;

            $data = array(
                'VENUE_NAME' => $company_name,
                'PURCHASER_NAME' => $member->first_name . ' ' . $member->last_name,
                'VOUCHER_NUMBER' => $gf->lookup,
                'RECEIPT_ID' => $order->id,
                'VOUCHER_TYPE' => 'Gift Certificate (' . $vs->name . ')',
                'BALANCE' => '$' . $gf->value,
                'EXPIRY_DATE' => $gf->expiry_date,
                'RECEIVER_NAME' => $gf->full_name,
                'EMAIL' => $gf->email,
                'CURRENT_YEAR' => date("Y"),
                'NOTE' => is_null($gf->note) ? '' : $gf->note,
                'URL' => $url
            );

            if ($email_server === "mandrill") {

                if ($mx->init()) {
                    $mx->send('gift_certificate_sender', $gf->full_name, $member->user->email, $data);
                } else {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $member->user->email,
                        'category' => 'gift_certificate_sender',
                        'name' => $gf->full_name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }

            } else if ($email_server === "mailjet") {

                if ($mjx->init()) {
                    $mjx->send('gift_certificate_sender', $gf->full_name, $member->user->email, $data);
                } else {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $member->user->email,
                        'category' => 'gift_certificate_sender',
                        'name' => $gf->full_name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }
                
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            $log = new SystemLog();
            $log->type = 'job-error';
            $log->humanized_message = 'Sending gift certificate email is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'SendGiftCertificateEmail.php';
            $log->save();
        }

    }
}

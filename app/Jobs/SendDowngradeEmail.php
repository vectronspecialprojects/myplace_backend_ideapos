<?php

namespace App\Jobs;

use App\Email;
use App\Helpers\MandrillExpress;
use App\Helpers\MailjetExpress;
use App\PendingJob;
use App\Platform;
use App\Setting;
use App\SystemLog;
use App\User;
use App\Tier;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendDowngradeEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $user;

    /**
     * Create a new job instance.
     *
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MandrillExpress $mx, MailjetExpress $mjx)
    {
        try {
            DB::beginTransaction();

            $user = $this->user;
            $company_name = Setting::where('key', 'company_name')->first()->value;

            $platform = Platform::where('name', 'web')->first();

            $tier_default = Tier::find(1);
            // Send Welcome Email to member
            $data = array(
                'NAME' => $user->member->first_name." ".$user->member->last_name,
                'EXPIRED_TIER' => $user->member->member_tier->tier->name,
                'EXPIRY_DATE' => (string) $user->member->member_tier->date_expiry,
                'CURRENT_TIER' => $tier_default->name,
                'VENUE_NAME' => $user->member->current_preferred_venue_name ? $user->member->current_preferred_venue_name : $company_name,
            );

            // Log::info($data);

            $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;

            if ($email_server === "mandrill") {

                if ($mx->init()) {
                    $mx->send('membership_downgrade_notification', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                } else {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $user->email,
                        'category' => 'membership_downgrade_notification',
                        'name' => $user->member->first_name . ' ' . $user->member->last_name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }

            } else if ($email_server === "mailjet") {

                if ($mjx->init()) {
                    $mjx->send('membership_downgrade_notification', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                } else {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $user->email,
                        'category' => 'membership_downgrade_notification',
                        'name' => $user->member->first_name . ' ' . $user->member->last_name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }

            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            $log = new SystemLog();
            $log->type = 'job-error';
            $log->humanized_message = 'Sending welcome email is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'SendWelcomeEmail.php';
            $log->save();
        }
    }
}

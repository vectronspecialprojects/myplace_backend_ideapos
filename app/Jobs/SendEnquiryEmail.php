<?php

namespace App\Jobs;

use App\Helpers\MandrillExpress;
use App\Helpers\MailjetExpress;
use App\Jobs\Job;
use App\ListingEnquiry;
use App\Member;
use App\Email;
use App\Listing;
use App\PendingJob;
use App\Setting;
use App\SystemLog;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEnquiryEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $enquiry;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(ListingEnquiry $enquiry)
    {
        //
        $this->enquiry = $enquiry;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MandrillExpress $mx, MailjetExpress $mjx)
    {
        try {
            // Log::error("SendEnquiryEmail");
            DB::beginTransaction();

            $enquiry = $this->enquiry;

            $member = Member::find($enquiry->member_id);
            $user = $member->user;
            $company_name = Setting::where('key', 'company_name')->first()->value;

            $data = array(
                'LAST_NAME' => $user->member->last_name,
                'FIRST_NAME' => $user->member->first_name,
                'CURRENT_YEAR' => date("Y"),
                'COMPANY' => $company_name
            );

            $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;

            if ($email_server === "mandrill") {

                if ($mx->init()) {
                    $mx->send('enquiry', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                } else {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $user->email,
                        'category' => 'enquiry',
                        'name' => $user->member->first_name . ' ' . $user->member->last_name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }

            } else if ($email_server === "mailjet") {

                if ($mjx->init()) {
                    $mjx->send('enquiry', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                } else {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $user->email,
                        'category' => 'enquiry',
                        'name' => $user->member->first_name . ' ' . $user->member->last_name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }

            }

            /*
             * Send enquiry detail to admin...
             */
            $admin_email = Setting::where('key', 'enquiry_email')->first()->extended_value;
            $admin_name = Setting::where('key', 'admin_full_name')->first()->value;

            if (!is_null($admin_email)) {
                $emails = \GuzzleHttp\json_decode($admin_email, true);
                if (!empty($emails)) {
                    $listing = Listing::find($enquiry->listing_id);

                    $data = array(
                        'ADMIN_NAME' => $admin_name,
                        'FIRST_NAME' => $user->member->first_name,
                        'LAST_NAME' => $user->member->last_name,
                        'CURRENT_YEAR' => date("Y"),
                        'COMPANY' => $company_name,
                        'EMAIL' => $user->email,
                        'SUBJECT' => $enquiry->subject,
                        'ENQUIRY' => $enquiry->message,
                        'LISTING' => $listing->name
                    );

                    foreach ($emails as $email) {
                        if ($email_server === "mandrill") {

                            if ($mx->init()) {
                                $mx->send('enquiry_admin', $admin_name, $email, $data);
                            } else {
                                $pj = new PendingJob();
                                $pj->payload = \GuzzleHttp\json_encode($data);
                                $pj->extra_payload = \GuzzleHttp\json_encode(array(
                                    'email' => $email,
                                    'category' => 'enquiry_admin',
                                    'name' => $admin_name
                                ));
                                $pj->queue = "email";
                                $pj->save();
                            }

                        } else if ($email_server === "mailjet") {

                            if ($mjx->init()) {
                                $mjx->send('enquiry_admin', $admin_name, $email, $data);
                            } else {
                                $pj = new PendingJob();
                                $pj->payload = \GuzzleHttp\json_encode($data);
                                $pj->extra_payload = \GuzzleHttp\json_encode(array(
                                    'email' => $email,
                                    'category' => 'enquiry_admin',
                                    'name' => $admin_name
                                ));
                                $pj->queue = "email";
                                $pj->save();
                            }
            
                        }
        
                    }
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            $log = new SystemLog();
            $log->type = 'job-error';
            $log->humanized_message = 'Sending enquiry email is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'SendEnquiryEmail.php';
            $log->save();
        }

    }
}

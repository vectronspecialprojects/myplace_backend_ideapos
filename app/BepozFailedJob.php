<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BepozFailedJob extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bepoz_failed_jobs';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Destroy this job
     *
     * @throws \Exception
     */
    public function dispatch() {
        $this->delete();
    }
}

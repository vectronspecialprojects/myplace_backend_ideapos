<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Listing extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'listings';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at', 'listing_type_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $casts = [
        'payload' => 'object',
        'extra_settings' => 'object'
    ];

    /**
     * Get the type.
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('App\ListingType', 'listing_type_id');
    }

    public function venue()
    {
        return $this->belongsTo('App\Venue', 'venue_id');
    }

    /**
     * Get the schedules.
     * >> Define relationship
     *
     */
    public function schedules()
    {
        return $this->hasMany('App\ListingSchedule', 'listing_id');
    }

    /**
     * Get the products.
     * >> Define relationship
     *
     */
    public function claimed_promotions()
    {
        return $this->hasMany('App\ClaimedPromotion', 'listing_id');
    }

    /**
     * Get the products.
     * >> Define relationship
     *
     */
    public function products()
    {
        return $this->hasMany('App\ListingProduct', 'listing_id');
    }

    /**
     * Get the prize promotion.
     * >> Define relationship
     *
     */
    public function prize_promotion()
    {
        return $this->belongsTo('App\PrizePromotion', 'prize_promotion_id');
    }

    public function enquiries()
    {
        return $this->hasMany('App\ListingEnquiry');
    }

    public function order()
    {
        return $this->hasMany('App\Order');
    }
    
    public function order_payment_success()
    {
        $status = ['pending', 'in_progress', 'successful'];
        return $this->hasMany('App\Order')->whereIn('payment_status', $status);
    }
    
    public function order_details()
    {
        return $this->hasMany('App\OrderDetail');
    }
    
    public function order_details_success()
    {
      $status = ['pending', 'in_progress', 'successful'];
      return $this->hasMany('App\OrderDetail')
        ->whereIn('status', $status);
    }

    /**
     * Get listing Tags
     * >> Define relationship
     */
    public function listing_tags()
    {
        return $this->belongsToMany('App\ListingTag', 'listing_pivot_tags', 'listing_id', 'listing_tag_id')->wherePivot('deleted_at', null)->withPivot('display_order')->withTimestamps();
    }

    /**
     * Get the related listing
     * >> Define relationship
     *
     */
    public function listing_pivot_tags()
    {
        return $this->hasMany('App\ListingPivotTag');
    }

    public function getExtraSettingsAttribute($obj)
    {
        return is_null($obj) ? null : \GuzzleHttp\json_decode($obj);
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($listing) {

            if (is_null($listing->image_square)) {
                $default_setting = Setting::where('key', 'use_invoice_logo_as_default_image')->first();
                if (!is_null($default_setting)) {
                    if ($default_setting->value === 'true') {
                        $invoice_logo = Setting::where('key', 'invoice_logo')->first();
                        $listing->image_square = $invoice_logo->value;

                    } else {
                        $setting = Setting::where('key', 'listing_image_square')->first();
                        if (!is_null($setting)) {
                            $listing->image_square = $setting->value;
                        } else {
                            $listing->image_square = "https://via.placeholder.com/500x500";
                        }
                    }


                } else {
                    $setting = Setting::where('key', 'listing_image_square')->first();
                    if (!is_null($setting)) {
                        $listing->image_square = $setting->value;
                    } else {
                        $listing->image_square = "https://via.placeholder.com/500x500";
                    }
                }


            }

            if (is_null($listing->image_banner)) {
                $setting = Setting::where('key', 'listing_image_banner')->first();
                if (!is_null($setting)) {
                    $listing->image_banner = $setting->value;
                } else {
                    $listing->image_banner = "https://via.placeholder.com/500x160";
                }

            }


        });

        static::deleting(function ($listing) { // before delete() method call this

            foreach ($listing->schedules as $schedule) {

                foreach ($schedule->tags as $tag) {
                    $tag->delete();
                }

                foreach ($schedule->types as $type) {
                    $type->delete();
                }

                foreach ($schedule->venues as $venue) {
                    $venue->delete();
                }

                $schedule->delete();
            }

            foreach ($listing->products as $product) {
                $product->delete();
            }

            foreach ($listing->enquiries as $enquiry) {
                $enquiry->delete();
            }

            ListingFavorite::where('listing_id', $listing->id)->delete();

        });

    }
}

<?php

namespace App\Http\Middleware;

use Closure;

class AfterRoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if ($response->headers->get('content-type') == 'application/json' && $request->offsetExists('decrypted_token')) {
            $original = $response->getData();

            $temp = $request->input('decrypted_token')->roles()->first();
            $temp->control;

            // $original->signature = cryptoJsAesEncrypt(env('AES_KEY','BEPOZ'), $temp);
            $original->signature = cryptoJsAesEncrypt(config('bucket.AES_KEY'), $temp);
            $response->setData($original);

            return $response;
        }

        return $response;
    }
}

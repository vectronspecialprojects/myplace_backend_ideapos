<?php

namespace App\Http\Middleware;

use App\Tier;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class TierValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!is_null($request->input('decrypted_token')->member->tiers()->first())) {

            $current = $request->input('decrypted_token')->member->member_tier;

            // manual checking
            $tier = Tier::find(3);

            if ($current->tier_id != $tier->id) {

                // bypass student tier
                return $next($request);
            } else {

                if (is_null($current->date_expiry)) {

                    return response()->json(['status' => 'error', 'message' => 'unpaid_tier', 'suggestion' => 'Please purchase the tier.'], Response::HTTP_PAYMENT_REQUIRED);
                } else {

                    $expiry_date = Carbon::parse($current->date_expiry);
                    if ($expiry_date->isPast()) {

                        return response()->json(['status' => 'error', 'message' => 'expired_tier', 'suggestion' => 'Please renew the tier.'], Response::HTTP_PAYMENT_REQUIRED);
                    } else {

                        // bypass, vip tier is valid
                        return $next($request);
                    }

                }
            }

        }

        return response()->json(['status' => 'error', 'message' => 'unpaid_tier', 'suggestion' => 'Please purchase the tier.'], Response::HTTP_PAYMENT_REQUIRED);
    }
}

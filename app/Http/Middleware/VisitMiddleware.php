<?php

namespace App\Http\Middleware;

use App\VisitLog;
use Carbon\Carbon;
use Closure;

class VisitMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $now = Carbon::now(config('app.timezone'));

        $visit = VisitLog::whereDate('visit_time', '=', $now->toDateString())
            ->first();

        if (is_null($visit)) {
            $visit = new VisitLog();
            $visit->visit_time = $now->toDateString();
            $visit->total_visits = 1;
            $visit->save();
        }
        else {
            $visit->total_visits++;
            $visit->save();
        }

        return $next($request);
    }
}

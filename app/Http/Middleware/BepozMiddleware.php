<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class BepozMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!is_null($request->decrypted_token->member->bepoz_account_id)) {
            return $next($request);
        }

        return response()->json(['status' => 'error', 'message' => 'Missing Barcode (AccountID), please wait until you receive your barcode.'], Response::HTTP_NOT_ACCEPTABLE);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Response;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     * >> Checking the role
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $this->getAuthenticatedUser();

        // Get role data from routes
        $roles = $request->route()->getAction()['roles'];

        if (!is_array($roles)) {
            $roles = [$roles];
        }

        foreach ($roles as $role) {
            if ($role === '*') {

                if ($user->is('member')) {
                    if (!$user->roles()->first()->control->frontend_access) {
                        return response()->json(['status' => 'error', 'message' => 'Sorry, you have no access to this portal.'], Response::HTTP_UNAUTHORIZED);
                    }

                    if ($user->member->status == 'inactive') {
                        return response()->json(['status' => 'error', 'message' => 'Sorry, your account is locked. Please contact the venue'], Response::HTTP_UNAUTHORIZED);
                    }
                }

                $request->offsetSet('decrypted_token', $user);
                return $next($request);
            } else {
                if ($user->is($role)) {

                    if ($role === 'member') {
                        if (!$user->roles()->first()->control->frontend_access) {
                            return response()->json(['status' => 'error', 'message' => 'Sorry, you have no access to this portal.'], Response::HTTP_UNAUTHORIZED);
                        }

                        if ($user->member->status == 'inactive') {
                            return response()->json(['status' => 'error', 'message' => 'Sorry, your account is locked. Please contact the venue'], Response::HTTP_UNAUTHORIZED);
                        }
                    }

                    $request->offsetSet('decrypted_token', $user);
                    return $next($request);
                }
            }

        }

        return response()->json(['status' => 'error', 'message' => 'insufficient_role'], Response::HTTP_UNAUTHORIZED);

    }

    /**
     * Get the token and authenticate it.
     * >> return the user object
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAuthenticatedUser()
    {
        try {

            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['status' => 'error', 'message' => 'user_not_found'], Response::HTTP_UNAUTHORIZED);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['status' => 'error', 'message' => $e->getStatusCode()], Response::HTTP_UNAUTHORIZED);

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['status' => 'error', 'message' => $e->getStatusCode()], Response::HTTP_UNAUTHORIZED);

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['status' => 'error', 'message' => $e->getStatusCode()], Response::HTTP_UNAUTHORIZED);

        }

        // the token is valid and we have found the user via the sub claim
        return $user->makeVisible('id');
    }
}

<?php

namespace App\Http\Controllers;

use App\Email;
use App\Helpers\MandrillExpress;
use App\Helpers\MailjetExpress;
use App\Helpers\SMSProvider;
use App\Member;
use App\PendingJob;
use App\Platform;
use App\Setting;
use App\Staff;
use App\Tier;
use App\User;
use App\UserRoles;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Carbon\Carbon;

class ForgotPasswordController extends Controller
{
    /**
     * Show the reset-password form for the given user.
     * >> Check the token first.
     *
     * @param Request $request
     * @param $token
     * @return mixed
     */
    protected function showResetPasswordForm(Request $request, $token)
    {

        try {
            $data = DB::table('password_resets')->select('email', 'expired', 'valid')
                ->where('token', $token)->first();

            $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

            if (is_null($data)) {

                return View::make('errors.error', array('company_logo' => $invoice_logo, 
                    'message' => 'Your reset-password request is no longer valid. Please request a new one.'));
            } else {
                if ($data->valid) {

                    // Valid
                    if (time() <= $data->expired) {

                        // Get web app token
                        $platform = Platform::where('name', 'web')->first();
                        $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

                        return View::make('form.reset-temp', array('company_logo' => $invoice_logo,
                            'url' => config('bucket.APP_URL') . '/api/reset/' . $token, 'token' => $token, 
                            'app_token' => $platform->app_token, 'app_secret' => $platform->app_secret));

                    } else {

                        // Expired
                        return View::make('errors.error', array('company_logo' => $invoice_logo, 
                            'message' => 'Your reset-password request has been expired.'));
                    }

                } else {
                    // Invalid
                    return View::make('errors.error', array('company_logo' => $invoice_logo, 
                        'message' => 'Your reset-password request is no longer valid.'));
                }
            }


        } catch (\Exception $e) {
            Log::error($e);
            $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

            return View::make('errors.error', array('company_logo' => $invoice_logo, 'message' => $e->getMessage()));
        }

    }

    /**
     * Send a verification code for resetting password via sms
     *
     * @param Request $request
     * @param SMSProvider $sms
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendForgotPasswordVerificationCode(Request $request, SMSProvider $sms)
    {
        try {

            $validator = Validator::make($request->all(), [
                'mobile' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], HttpResponse::HTTP_BAD_REQUEST);
            }

            $mobile = $request->input('mobile');

            if (User::where('mobile', '=', $mobile)->exists()) {

                // Generate 6 random number
                $code = mt_rand(100000, 999999);

                $user = User::where('mobile', '=', $mobile)->first();

                DB::table('password_resets')->where('email', '=', $user->email)->update(['valid' => 0]);

                $expire_time = time() + (2 * 24 * 60 * 60); // set token expire time: 2 days
                $token = md5(implode("|", ['email' => $user->email, 'expired' => $expire_time, 'mobile' => $mobile]));

                DB::table('password_resets')->insert(
                    ['mobile' => $mobile, 'email' => $user->email, 'token' => $token, 'code' => $code, 'expired' => $expire_time, 'type' => 'sms']
                );

                $sms->sendSMS($mobile, "Your verification code: " . $code);

                return response()->json(['status' => 'ok', 'message' => 'Verification code sent successful.', 'code' => $code, 'token' => $token]);
            } else {
                return response()->json(['status' => 'error', 'message' => 'Mobile not found or required!'], HttpResponse::HTTP_BAD_REQUEST);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Verify sms code for resetting password.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function verifyCode(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'code' => 'required',
                'token' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], HttpResponse::HTTP_BAD_REQUEST);
            }

            $code = $request->input('code');
            $token = $request->input('token');

            $data = DB::table('password_resets')->select('email', 'expired', 'valid')
                ->where('token', $token)->where('code', $code)->first();

            if (is_null($data)) {
                return response()->json(['status' => 'error', 'message' => 'invalid_code'], HttpResponse::HTTP_BAD_REQUEST);
            } else {
                if ($data->valid) {

                    // Valid
                    if (time() <= $data->expired) {

                        return response()->json(['status' => 'ok', 'message' => 'Code is valid.', 'token' => $token]);

                    } else {

                        // Expired
                        return response()->json(['status' => 'error', 'message' => 'code_expired'], HttpResponse::HTTP_BAD_REQUEST);
                    }

                } else {
                    // Invalid
                    return response()->json(['status' => 'error', 'message' => 'invalid_code'], HttpResponse::HTTP_UNAUTHORIZED);
                }
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Send reset-password request to user by email.
     * >> with validation
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function resetPasswordRequestOriginal(Request $request, MandrillExpress $mx, MailjetExpress $mjx)
    {

        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|max:155'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], HttpResponse::HTTP_BAD_REQUEST);
            }

            $email = preg_replace('/\s+/', '+', $request->input('email'));

            if (User::where('email', '=', $email)->exists()) {

                // Create reset password token

                $expire_time = time() + (2 * 24 * 60 * 60); // set token expire time: 2 days
                $token = md5(implode("|", ['email' => $email, 'expired' => $expire_time]));


                // Set all existed tokens to invalid
                DB::table('password_resets')->where('email', '=', $email)->update(['valid' => 0]);

                // Store the token
                DB::table('password_resets')->insert(
                    ['email' => $email, 'token' => $token, 'expired' => $expire_time]
                );

                $user = User::where('email', '=', $email)->first();
                $url = config('bucket.APP_URL') . '/api/reset/' . $token;

                // Send Reset Email + url

                $data = array(
                    'FIRST_NAME' => $user->member->first_name,
                    'LAST_NAME' => $user->member->last_name,
                    'RESET_URL' => $url
                );

                $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;

                if ($email_server === "mandrill") {

                    if ($mx->init()) {
                        $mx->send('reset', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                    } else {
                        $pj = new PendingJob();
                        $pj->payload = \GuzzleHttp\json_encode($data);
                        $pj->extra_payload = \GuzzleHttp\json_encode(array(
                            'email' => $user->email,
                            'category' => 'reset',
                            'name' => $user->member->first_name . ' ' . $user->member->last_name
                        ));
                        $pj->queue = "email";
                        $pj->save();
                    }

                } else if ($email_server === "mailjet") {

                    if ($mjx->init()) {
                        $mjx->send('reset', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                    } else {
                        $pj = new PendingJob();
                        $pj->payload = \GuzzleHttp\json_encode($data);
                        $pj->extra_payload = \GuzzleHttp\json_encode(array(
                            'email' => $user->email,
                            'category' => 'reset',
                            'name' => $user->member->first_name . ' ' . $user->member->last_name
                        ));
                        $pj->queue = "email";
                        $pj->save();
                    }

                }

                return response()->json(['status' => 'ok', 'message' => 'Reset Password Request sent successful.', 'token' => $token]);
            } else {
                return response()->json(['status' => 'error', 'message' => 'The email has not been found. Please try again.'], HttpResponse::HTTP_BAD_REQUEST);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Send reset-password request to user by email with Temp Password.
     * >> with validation
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function resetPasswordRequestTempPassword(Request $request, MandrillExpress $mx, MailjetExpress $mjx)
    {

        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|max:155'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], HttpResponse::HTTP_BAD_REQUEST);
            }

            $email = preg_replace('/\s+/', '+', $request->input('email'));

            if (User::where('email', '=', $email)->exists()) {

                // Create reset password token

                $expire_time = time() + (2 * 24 * 60 * 60); // set token expire time: 2 days
                $token = md5(implode("|", ['email' => $email, 'expired' => $expire_time]));

                // Set all existed tokens to invalid
                DB::table('password_resets')->where('email', '=', $email)->update(['valid' => 0]);

                // Store the token
                DB::table('password_resets')->insert(
                    ['email' => $email, 'token' => $token, 'expired' => $expire_time]
                );

                $user = User::where('email', '=', $email)->first();
                $url = config('bucket.APP_URL') . '/api/resetform/' . $token;

                $password = $this->generateRandomString(6);
                $user->password = Hash::make($password);
                $user->save();
                
                // Send Reset Email + url
                $data = array(
                    'FIRST_NAME' => $user->member->first_name,
                    'LAST_NAME' => $user->member->last_name,
                    'TEMP_PASSWORD' => $password,
                    'RESET_URL' => $url
                );
                // Log::error($data);
                
                //integrate SSO
                    if ($user->member->share_info == 1) {
                        try {
                            //NEW LOGIC SEND INTEGRATION TO SERVICE APP
                            $your_order_api = Setting::where('key', 'your_order_api')->first()->value."/changePassword";
                            $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

                            $datasend = array(
                                'email' => $email,
                                'password' => $password,
                                "license_key" => $your_order_key
                            );

                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_URL, $your_order_api);
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
                            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                            curl_setopt($ch, CURLOPT_ENCODING, "gzip");
                            $content = curl_exec($ch);
                            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                            // Log::warning($content);
                            // Log::warning($httpCode);
                        } catch (\Exception $e) {
                            Log::error($e);
                
                            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
                        }
                    }
                

                $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;

                if ($email_server === "mandrill") {

                    if ($mx->init()) {
                        $mx->send('reset', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                    } else {
                        $pj = new PendingJob();
                        $pj->payload = \GuzzleHttp\json_encode($data);
                        $pj->extra_payload = \GuzzleHttp\json_encode(array(
                            'email' => $user->email,
                            'category' => 'reset',
                            'name' => $user->member->first_name . ' ' . $user->member->last_name
                        ));
                        $pj->queue = "email";
                        $pj->save();
                    }

                } else if ($email_server === "mailjet") {

                    if ($mjx->init()) {
                        $mjx->send('reset', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                    } else {
                        $pj = new PendingJob();
                        $pj->payload = \GuzzleHttp\json_encode($data);
                        $pj->extra_payload = \GuzzleHttp\json_encode(array(
                            'email' => $user->email,
                            'category' => 'reset',
                            'name' => $user->member->first_name . ' ' . $user->member->last_name
                        ));
                        $pj->queue = "email";
                        $pj->save();
                    }

                }

                return response()->json(['status' => 'ok', 'message' => 'Reset Password Request sent successful.']);
            } else {
                return response()->json(['status' => 'error', 'message' => config('bucket.FORGOT_PASSWORD_EMAIL_NOT_FOUND')], HttpResponse::HTTP_BAD_REQUEST);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Send reset-password request to user by email with Temp Password and SSO check
     * >> with validation
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function resetPasswordRequest(Request $request, MandrillExpress $mx, MailjetExpress $mjx)
    {

        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|max:155'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], HttpResponse::HTTP_BAD_REQUEST);
            }

            $email = preg_replace('/\s+/', '+', $request->input('email'));

            $password = $this->generateRandomString(6);

            $user_found = User::where('email', '=', $email)->exists();

            // CHECK SSO
            if ($sso_response = $this->checkSSOAccount($email)) {
                // successful sso login, user found in SSO and myplace
                if ($user_found){
                    // CONTINUE TO SEND RESET EMAIL
                } else {
                    $profile = $this->getProfile($sso_response);
                    // Log::info("profile");
                    // Log::info($profile);

                    // NEED TO CREATE MYPLACE ACCOUNT
                    $credentials = array(
                        'email' => $email,
                        'password' => Hash::make($password),
                        'mobile' => !isset($profile->phone_number) ? '' : $profile->phone_number
                    );
                    $user = User::create($credentials);

                    $member = new Member;
                    $member->user_id = $user->id;
                    $member->first_name = $profile->given_name;
                    $member->last_name = $profile->family_name;
                    $member->share_info = 1;
                    $member->current_preferred_venue_name = "";
                    $member->original_preferred_venue_name = "";
                    $member->dob = null;
                    $member->postcode = null;
                    $member->login_token = $sso_response;

                    if (isset($profile->claims)){
                        if (!is_null($profile->claims)){
                            $member->bepoz_account_id = $profile->claims->bepoz_id;
                            $member->bepoz_account_number = $profile->claims->bepoz_account_number;
                            $member->bepoz_account_card_number = $profile->claims->bepoz_account_number;
                        }
                    }
                    
                    $member->save();

                    $user_obj = User::find($user->id); // refresh...
                    $user_obj->member->save();

                    $date_now = Carbon::now(config('app.timezone'));
                    if ($profile->email_verified){
                        $user->email_confirmation = $profile->email_verified;
                        $user->email_confirmation_sent = $date_now;
                        $user->email_confirmed_at = $date_now;
                        $user->save();
                    }

                    if ($profile->phone_number_verified){
                        $user->sms_confirmation = $profile->phone_number_verified;
                        $user->sms_confirmation_sent = $date_now;
                        $user->sms_confirmed_at = $date_now;
                        $user->save();
                    }

                    $user_role = new UserRoles;
                    $user_role->role_id = 3; // Manually add
                    $user_role->user_id = $user->id;
                    $user_role->save();

                    $default_tier = Setting::where('key', 'default_tier')->first()->value;

                    $tier = Tier::find($default_tier);
                    // QUEENSBERRY FEATURE
                    // $default_member_expiry_date = Setting::where('key', 'default_member_expiry_date')->first()->value;
                                
                    // if ($default_member_expiry_date == "true"){
                    //     $setting = Setting::where('key', 'cut_off_date')->first();
                    //     $date_expiry = Carbon::parse($setting->value);
                    //     $date_expiry->setTimezone(config('app.timezone'));
                    //     $date_expiry->setTime(0, 0, 0);
                    // } else {
                    //     $date_expiry = null;
                    // }

                    $date_expiry = null;
                    $user->member->tiers()->sync([$tier->id => ['date_expiry' => $date_expiry]]);
                    
                    $user_found = true;
                }

            } else {
                if ($user_found){
                    // SEND BACK TO SSO REGISTER

                } else {
                    return response()->json(['status' => 'error', 'message' => config('bucket.FORGOT_PASSWORD_EMAIL_NOT_FOUND')], HttpResponse::HTTP_BAD_REQUEST);
                }
            }

            
            if ($user_found) {

                // Create reset password token
                $expire_time = time() + (2 * 24 * 60 * 60); // set token expire time: 2 days
                $token = md5(implode("|", ['email' => $email, 'expired' => $expire_time]));

                // Set all existed tokens to invalid
                DB::table('password_resets')->where('email', '=', $email)->update(['valid' => 0]);

                // Store the token
                DB::table('password_resets')->insert(
                    ['email' => $email, 'token' => $token, 'expired' => $expire_time]
                );

                $user = User::with('member')->where('email', '=', $email)->first();
                $url = config('bucket.APP_URL') . '/api/resetform/' . $token;

                $user->password = Hash::make($password);
                $user->save();
                
                // Send Reset Email + url
                $data = array(
                    'FIRST_NAME' => $user->member->first_name,
                    'LAST_NAME' => $user->member->last_name,
                    'TEMP_PASSWORD' => $password,
                    'RESET_URL' => $url
                );
                // Log::error($data);
                
                //integrate SSO
                    if ($user->member->share_info == 1) {
                        try {
                            //NEW LOGIC SEND INTEGRATION TO SERVICE APP
                            $your_order_api = Setting::where('key', 'your_order_api')->first()->value."/changePassword";
                            $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

                            $datasend = array(
                                'email' => $email,
                                'password' => $password,
                                "license_key" => $your_order_key
                            );

                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_URL, $your_order_api);
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
                            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                            curl_setopt($ch, CURLOPT_ENCODING, "gzip");
                            $content = curl_exec($ch);
                            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                            // Log::warning($content);
                            // Log::warning($httpCode);
                        } catch (\Exception $e) {
                            Log::error($e);
                
                            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
                        }
                    }


                $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;

                if ($email_server === "mandrill") {

                    if ($mx->init()) {
                        $mx->send('reset', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                    } else {
                        $pj = new PendingJob();
                        $pj->payload = \GuzzleHttp\json_encode($data);
                        $pj->extra_payload = \GuzzleHttp\json_encode(array(
                            'email' => $user->email,
                            'category' => 'reset',
                            'name' => $user->member->first_name . ' ' . $user->member->last_name
                        ));
                        $pj->queue = "email";
                        $pj->save();
                    }

                } else if ($email_server === "mailjet") {

                    if ($mjx->init()) {
                        $mjx->send('reset', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                    } else {
                        $pj = new PendingJob();
                        $pj->payload = \GuzzleHttp\json_encode($data);
                        $pj->extra_payload = \GuzzleHttp\json_encode(array(
                            'email' => $user->email,
                            'category' => 'reset',
                            'name' => $user->member->first_name . ' ' . $user->member->last_name
                        ));
                        $pj->queue = "email";
                        $pj->save();
                    }

                }

                return response()->json(['status' => 'ok', 'message' => 'Reset Password Request sent successful.']);
            } else {
                return response()->json(['status' => 'error', 'message' => config('bucket.FORGOT_PASSWORD_EMAIL_NOT_FOUND')], HttpResponse::HTTP_BAD_REQUEST);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }

    }

    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Send reset-password request to admin by email.
     * >> with validation
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function resetAdminPasswordRequestOriginal(Request $request, MandrillExpress $mx, MailjetExpress $mjx)
    {

        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|max:155'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], HttpResponse::HTTP_BAD_REQUEST);
            }

            $email = preg_replace('/\s+/', '+', $request->input('email'));

            if ($email === "admin@vectron.com.au") {
                return response()->json(['status' => 'ok', 'message' => 'Reset Password Request sent successful.']);
            }

            $userEmail = User::where('email', '=', $email);
            $userExist = $userEmail->exists();
            $staffExist = !is_null(Staff::where('user_id', $userEmail->first()->id)->first());
        
            $accessBackpanel = 0;

            if (!is_null($userEmail->first()->roles)){
                foreach ($userEmail->first()->roles as $role ){
                    if ($role->name == "admin" ){
                        $accessBackpanel = 1;
                    }
        
                    if ($role->name == "staff" ){
                        $accessBackpanel = 1;
                    }
                }
            }

            if ($userExist && $staffExist && $accessBackpanel) {

                // Create reset password token

                $expire_time = time() + (2 * 24 * 60 * 60); // set token expire time: 2 days
                $token = md5(implode("|", ['email' => $email, 'expired' => $expire_time]));


                // Set all existed tokens to invalid
                DB::table('password_resets')->where('email', '=', $email)->update(['valid' => 0]);

                // Store the token
                DB::table('password_resets')->insert(
                    ['email' => $email, 'token' => $token, 'expired' => $expire_time]
                );

                $user = User::where('email', '=', $email)->first();

                $url = config('bucket.APP_URL') . '/api/resetform/' . $token;
                // Send Reset Email + url

                $password = $this->generateRandomString(6);
                $user->password = Hash::make($password);
                $user->save();

                $data = array(
                    'FIRST_NAME' => $user->member->first_name,
                    'LAST_NAME' => $user->member->last_name,
                    'TEMP_PASSWORD' => $password,
                    'RESET_URL' => $url
                );

                //integrate SSO
                    if ($user->member->share_info == 1) {
                        try {
                            //NEW LOGIC SEND INTEGRATION TO SERVICE APP
                            $your_order_api = Setting::where('key', 'your_order_api')->first()->value."/changePassword";
                            $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

                            $datasend = array(
                                'email' => $email,
                                'password' => $password,
                                "license_key" => $your_order_key
                            );

                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_URL, $your_order_api);
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
                            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                            curl_setopt($ch, CURLOPT_ENCODING, "gzip");
                            $content = curl_exec($ch);
                            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                            // Log::warning($content);
                            // Log::warning($httpCode);
                        } catch (\Exception $e) {
                            Log::error($e);
                
                            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
                        }
                    }
                

                $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;

                if ($email_server === "mandrill") {

                    if ($mx->init()) {
                        $mx->send('reset', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                    } else {
                        $pj = new PendingJob();
                        $pj->payload = \GuzzleHttp\json_encode($data);
                        $pj->extra_payload = \GuzzleHttp\json_encode(array(
                            'email' => $user->email,
                            'category' => 'reset',
                            'name' => $user->member->first_name . ' ' . $user->member->last_name
                        ));
                        $pj->queue = "email";
                        $pj->save();
                    }

                } else if ($email_server === "mailjet") {

                    if ($mjx->init()) {
                        $mjx->send('reset', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                    } else {
                        $pj = new PendingJob();
                        $pj->payload = \GuzzleHttp\json_encode($data);
                        $pj->extra_payload = \GuzzleHttp\json_encode(array(
                            'email' => $user->email,
                            'category' => 'reset',
                            'name' => $user->member->first_name . ' ' . $user->member->last_name
                        ));
                        $pj->queue = "email";
                        $pj->save();
                    }

                }

                return response()->json(['status' => 'ok', 'message' => 'Reset Password Request sent successful.', 'token' => $token]);
            } else {
                return response()->json(['status' => 'error', 'message' => 'The email has not been found. Please try again.'], HttpResponse::HTTP_BAD_REQUEST);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }

    }

     /**
     * Send reset-password request to admin by email.
     * >> with validation, checking staff and user role
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function resetAdminPasswordRequest(Request $request, MandrillExpress $mx, MailjetExpress $mjx)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|max:155'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], HttpResponse::HTTP_BAD_REQUEST);
            }

            $email = preg_replace('/\s+/', '+', $request->input('email'));

            // $user_found = User::where('email', '=', $email)->exists();

            
            if ($email === "admin@vectron.com.au") {
                return response()->json(['status' => 'ok', 'message' => 'Reset Password Request sent successful.']);
            }

            $userEmail = User::where('email', '=', $email);
            $userExist = $userEmail->exists();
            $staffExist = !is_null(Staff::where('user_id', $userEmail->first()->id)->first());
        
            $accessBackpanel = 0;

            if (!is_null($userEmail->first()->roles)){
                foreach ($userEmail->first()->roles as $role ){
                    if ($role->name == "admin" ){
                        $accessBackpanel = 1;
                    }
        
                    if ($role->name == "staff" ){
                        $accessBackpanel = 1;
                    }
                }
            }

            // CHECK USER AND ROLE
            if ($userExist && $staffExist && $accessBackpanel) {
                
                // Create reset password token
                $expire_time = time() + (2 * 24 * 60 * 60); // set token expire time: 2 days
                $token = md5(implode("|", ['email' => $email, 'expired' => $expire_time]));

                // Set all existed tokens to invalid
                DB::table('password_resets')->where('email', '=', $email)->update(['valid' => 0]);

                // Store the token
                DB::table('password_resets')->insert(
                    ['email' => $email, 'token' => $token, 'expired' => $expire_time]
                );

                $user = User::with('member')->where('email', '=', $email)->first();
                $url = config('bucket.APP_URL') . '/api/resetform/' . $token;

                $password = $this->generateRandomString(6);
                $user->password = Hash::make($password);
                $user->save();
                
                // Send Reset Email + url
                $data = array(
                    'FIRST_NAME' => $user->member->first_name,
                    'LAST_NAME' => $user->member->last_name,
                    'TEMP_PASSWORD' => $password,
                    'RESET_URL' => $url
                );
                // Log::error($data);
                
                //integrate SSO
                    if ($user->member->share_info == 1) {
                        try {
                            //NEW LOGIC SEND INTEGRATION TO SERVICE APP
                            $your_order_api = Setting::where('key', 'your_order_api')->first()->value."/changePassword";
                            $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

                            $datasend = array(
                                'email' => $email,
                                'password' => $password,
                                "license_key" => $your_order_key
                            );

                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_URL, $your_order_api);
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
                            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                            curl_setopt($ch, CURLOPT_ENCODING, "gzip");
                            $content = curl_exec($ch);
                            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                            // Log::warning($content);
                            // Log::warning($httpCode);
                        } catch (\Exception $e) {
                            Log::error($e);
                
                            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
                        }
                    }
                

                $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;

                if ($email_server === "mandrill") {

                    if ($mx->init()) {
                        $mx->send('reset', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                    } else {
                        $pj = new PendingJob();
                        $pj->payload = \GuzzleHttp\json_encode($data);
                        $pj->extra_payload = \GuzzleHttp\json_encode(array(
                            'email' => $user->email,
                            'category' => 'reset',
                            'name' => $user->member->first_name . ' ' . $user->member->last_name
                        ));
                        $pj->queue = "email";
                        $pj->save();
                    }

                } else if ($email_server === "mailjet") {

                    if ($mjx->init()) {
                        $mjx->send('reset', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                    } else {
                        $pj = new PendingJob();
                        $pj->payload = \GuzzleHttp\json_encode($data);
                        $pj->extra_payload = \GuzzleHttp\json_encode(array(
                            'email' => $user->email,
                            'category' => 'reset',
                            'name' => $user->member->first_name . ' ' . $user->member->last_name
                        ));
                        $pj->queue = "email";
                        $pj->save();
                    }

                }

                return response()->json(['status' => 'ok', 'message' => 'Reset Password Request sent successful.']);
            } else {
                return response()->json(['status' => 'error', 'message' => config('bucket.EMAIL_NOT_FOUND') ], HttpResponse::HTTP_BAD_REQUEST);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Reset password using token.
     * >> Inform the user by email if the resetting password is successful.
     * >> with validation
     *
     * @param Request $request
     * @param $token
     * @return \Illuminate\Http\JsonResponse
     */
    protected function resetPassword(Request $request, $token, MandrillExpress $mx, MailjetExpress $mjx)
    {
        try {

            $validator = Validator::make($request->all(), [
                'temporary_password' => 'required',
                'password' => 'required'
            ]);

            if ($validator->fails()) {
                $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

                return View::make('errors.error', array('company_logo' => $invoice_logo, 'message' => $validator->messages()));
            }

            $password = $request->input('password');
            $temporary_password = trim($request->input('temporary_password'));

            $data = DB::table('password_resets')->select('email', 'expired')
                ->where('token', $token)->first();

            if (is_null($data)) {
                $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;
                return View::make('errors.error', array('company_logo' => $invoice_logo, 'message' => 'token_invalid'));
            } else {
                // Mark the token as invalid
                DB::table('password_resets')
                    ->where('token', $token)
                    ->update(['valid' => 0]);
                
                $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

                if (time() <= $data->expired) {
                    
                    $credentials = ['email' => $data->email, 'password' => $temporary_password, 'deleted_at' => null];
                    $token = JWTAuth::attempt($credentials);

                    if ($token) {
                        // Not Yet
                        User::where('email', $data->email)->update(['password' => Hash::make($password)]);

                        $user = User::where('email', '=', $data->email)->first();

                        //integrate SSO
                            if ($user->member->share_info == 1) {
                                try {
                                    //NEW LOGIC SEND INTEGRATION TO SERVICE APP
                                    $your_order_api = Setting::where('key', 'your_order_api')->first()->value."/changePassword";
                                    $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

                                    $datasend = array(
                                        'email' => $data->email,
                                        'password' => $request->input('password'),
                                        "license_key" => $your_order_key
                                    );

                                    $ch = curl_init();
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                    curl_setopt($ch, CURLOPT_URL, $your_order_api);
                                    curl_setopt($ch, CURLOPT_POST, 1);
                                    curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
                                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                                    curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                                    curl_setopt($ch, CURLOPT_ENCODING, "gzip");
                                    $content = curl_exec($ch);
                                    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                                    // Log::warning($content);
                                    // Log::warning($httpCode);
                                } catch (\Exception $e) {
                                    Log::error($e);
                        
                                    return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
                                }
                            }
                        

                        $data = array(
                            'FIRST_NAME' => $user->member->first_name,
                            'LAST_NAME' => $user->member->last_name,
                        );

                        $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;

                        if ($email_server === "mandrill") {

                            if ($mx->init()) {
                                $mx->send('reset_successful', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                            } else {
                                $pj = new PendingJob();
                                $pj->payload = \GuzzleHttp\json_encode($data);
                                $pj->extra_payload = \GuzzleHttp\json_encode(array(
                                    'email' => $user->email,
                                    'category' => 'reset_successful',
                                    'name' => $user->member->first_name . ' ' . $user->member->last_name
                                ));
                                $pj->queue = "email";
                                $pj->save();
                            }
                            
                        } else if ($email_server === "mailjet") {

                            if ($mjx->init()) {
                                $mjx->send('reset_successful', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                            } else {
                                $pj = new PendingJob();
                                $pj->payload = \GuzzleHttp\json_encode($data);
                                $pj->extra_payload = \GuzzleHttp\json_encode(array(
                                    'email' => $user->email,
                                    'category' => 'reset_successful',
                                    'name' => $user->member->first_name . ' ' . $user->member->last_name
                                ));
                                $pj->queue = "email";
                                $pj->save();
                            }

                        }
                        
                        // return View::make('email.reset-successful', array('company_logo' => $invoice_logo, 
                        return View::make('errors.reset-successful', array('company_logo' => $invoice_logo, 
                            'heading' => 'RESET PASSWORD SUCCESSFUL', 
                            'msg' => 'Your temporary password has been replaced.</ br>Please log in with your new password.'));

                    } else {
                        return View::make('errors.error', array('company_logo' => $invoice_logo, 
                            'message' => 'Your temporary password not same'));
                    }

                } else {
                    $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

                    // Expired
                    return View::make('errors.error', array('company_logo' => $invoice_logo, 'message' => 'Your token is expired'));

                }
            }

        } catch (\Exception $e) {
            Log::error($e);
            $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

            return View::make('errors.error', array('company_logo' => $invoice_logo, 'message' => $e->getMessage()));

        }

    }
    
    /**
     * checkSSOAccount
     * internal function only
     *
     */
    protected function checkSSOAccount($email) {
        $your_order_api = Setting::where('key', 'your_order_api')->first()->value;
        $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

        $datacheck = array(
            'email' => $email,
            "license_key" => $your_order_key
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $your_order_api."/checkSubAccount");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datacheck);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_ENCODING, "gzip");
        $content_check = curl_exec($ch);
        $httpCode_check = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        // Log::info("542");
        // Log::info($content_check);
        // Log::info($httpCode_check);
        
        if ($content_check){
            $payload_check = \GuzzleHttp\json_decode($content_check);
            if ($payload_check->status == "ok") {
                return $payload_check->guid;
            } else {
                return false;
            }
        } else {
            return false;
        }

        return false;
    }

    /**
     * getProfile
     * internal function only
     *
     */
    protected function getProfile($guid) {
        $your_order_api = Setting::where('key', 'your_order_api')->first()->value;
        $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

        $datacheck = array(
            'guid' => $guid,
            "license_key" => $your_order_key
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $your_order_api."/getProfileViaGuid");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datacheck);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_ENCODING, "gzip");
        $content_check = curl_exec($ch);
        $httpCode_check = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        // Log::info("542");
        // Log::info($content_check);
        // Log::info($httpCode_check);
        
        if ($content_check){
            $payload_check = \GuzzleHttp\json_decode($content_check);
            if ($payload_check->status == "ok") {
                return $payload_check->data;
            } else {
                return false;
            }
        } else {
            return false;
        }

        return false;
    }

}

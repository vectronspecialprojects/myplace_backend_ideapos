<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    /**
     * Just return status ok
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function validateJWTToken(Request $request)
    {
        try {
            return response()->json(['status' => 'ok']);
        } catch (\Exception $e) {
            Log::error($e);

            return response()
                ->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }
    }


    /**
     * Return all users
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index()
    {
        try {
            $users = User::all();

            return response()->json(['status' => 'ok', 'data' => $users]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Change Admin Password
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function changePassword(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'old_password' => 'required',
                'new_password' => 'required|confirmed',
                'new_password_confirmation' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], HttpResponse::HTTP_BAD_REQUEST);
            }

            $user = JWTAuth::parseToken()->toUser();

            $old_password = $request->input('old_password');

            if (Hash::check($old_password, $user->password)) {

                if ($request->has('new_password')) {
                    $user->password = Hash::make($request->input('new_password'));
                    $user->save();

                    // return jwt token
                    $token = JWTAuth::refresh(JWTAuth::getToken());
                    return response()->json(['status' => 'ok', 'message' => 'Changing Password Successful', 'token' => $token]);
                } else {
                    return response()->json(['status' => 'error', 'message' => 'New password is not matched.'], HttpResponse::HTTP_BAD_REQUEST);
                }
            } else {
                return response()->json(['status' => 'error', 'message' => 'Old password is invalid.'], HttpResponse::HTTP_BAD_REQUEST);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }

    }
}

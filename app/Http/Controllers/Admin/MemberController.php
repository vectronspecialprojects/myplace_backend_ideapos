<?php

namespace App\Http\Controllers\Admin;

use App\Address;
use App\BepozJob;
use App\Email;
use App\Helpers\MandrillExpress;
use App\Helpers\MailjetExpress;
use App\Jobs\BroadcastSystemNotifications;
use App\Jobs\BroadcastGroupSystemNotifications;
use App\Jobs\SendReminderNotification;
use App\Member;
use App\MemberLog;
use App\MemberSystemNotification;
use App\MemberTiers;
use App\MemberVouchers;
use App\Order;
use App\PendingJob;
use App\Platform;
use App\PrizePromotion;
use App\PromoAccount;
use App\Setting;
use App\Staff;
use App\SystemNotification;
use App\Tier;
use App\User;
use App\Venue;
use App\SystemLog;
use App\UserRoles;
use Approached\LaravelImageOptimizer\ImageOptimizer;
use Carbon\Carbon;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

// use jmrieger\OneSignal\OneSignalFacade;
// use jmrieger\OneSignal\OneSignalClient;
use OneSignal;
use berkayk\OneSignal\OneSignalFacade;
use berkayk\OneSignal\OneSignalClient;
use Maatwebsite\Excel\Facades\Excel;

class MemberController extends Controller
{
    /**
     * Return all members
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(Member $member, Request $request)
    {
        try {
            $member = $member->newQuery();

            if ($request->has('keyword')) {

                if (strtotime(($request->input('keyword')) === false)) {
                    // ignore

                } elseif (Carbon::hasRelativeKeywords($request->input('keyword'))) {

                    if ($this->isDate($request->input('keyword'))) {
                        $c = Carbon::parse($request->input('keyword'), config('app.timezone'));

                        if (strpos($request->input('keyword'), ' ') !== false) {
                            // contain 2 words or more
                            $member->orWhereRaw("(day(dob) = ? AND month(dob) = ?)", [$c->day, $c->month]);
                        } else {
                            $member->orWhereRaw("(month(dob) = ?)", [$c->month]);
                        }

                    }


                } else {
                    if ($this->isDate($request->input('keyword'))) {
                        $c = Carbon::parse($request->input('keyword'), config('app.timezone'));

                        if (strpos($request->input('keyword'), ' ') !== false) {
                            // contain 2 words or more
                            $member->orWhereRaw("(day(dob) = ? AND month(dob) = ?)", [$c->day, $c->month]);
                        } else {
                            $member->orWhereRaw("(month(dob) = ?)", [$c->month]);
                        }
                    }

                }

                $member->orWhere(function ($query) use ($request) {
                    $query->orWhereHas('user', function ($q) use ($request) {
                        $q->where('email', 'like', '%' . $request->input('keyword') . '%');
                    });

                    $query->orWhere(function ($query) use ($request) {
                        if (strpos($request->input('keyword'), ' ') !== false) {
                            $array = explode(" ", $request->input('keyword'));
                            $keyword = implode("%", $array);
                            $query->where('first_name', 'like', '%' . $keyword . '%');
                        } else {
                            $query->where('first_name', 'like', '%' . $request->input('keyword') . '%');
                        }
                    });

                    $query->orWhere(function ($query) use ($request) {
                        if (strpos($request->input('keyword'), ' ') !== false) {
                            $array = explode(" ", $request->input('keyword'));
                            $keyword = implode("%", $array);
                            $query->where('last_name', 'like', '%' . $keyword . '%');
                        } else {
                            $query->where('last_name', 'like', '%' . $request->input('keyword') . '%');
                        }
                    });

                    $query->orWhereHas('user', function ($q) use ($request) {
                        $q->where('mobile', 'like', '%' . $request->input('keyword') . '%');
                    });

                });

                $member->orWhere(function ($query) use ($request) {
                    if (strpos($request->input('keyword'), ' ') !== false) {
                        $array = explode(" ", $request->input('keyword'));

                        foreach ($array as $element) {
                            $query->orWhereHas('user', function ($q) use ($request, $element) {
                                $q->where('email', 'like', '%' . $element . '%');
                            });

                            $query->orWhere(function ($query) use ($request, $element) {
                                $query->where('first_name', 'like', '%' . $element . '%');
                            });

                            $query->orWhere(function ($query) use ($request, $element) {
                                $query->where('last_name', 'like', '%' . $element . '%');
                            });

                            $query->orWhereHas('user', function ($q) use ($request, $element) {
                                $q->where('mobile', 'like', '%' . $element . '%');
                            });

                        }
                    }
                });
            }

            if ($request->has('orderBy') && $request->has('sortBy')) {
                if ($request->input('orderBy') == 'email') {
                    $member->select(
                        'members.*','users.id as users_id'
                    );
                    $member->join('users', 'members.user_id', '=', 'users.id');
                    $member->orderBy('users.email', $request->input('sortBy'));
                } elseif ($request->input('orderBy') == 'mobile') {
                    $member->select(
                        'members.*','users.id as users_id'
                    );
                    $member->join('users', 'members.user_id', '=', 'users.id');
                    $member->orderBy('users.mobile', $request->input('sortBy'));
                } elseif ($request->input('orderBy') == 'tier') {
                    //original
                    //$member->join('member_tier', 'members.id', '=', 'member_tier.member_id');
                    //$member->orderBy('member_tier.tier_id', $request->input('sortBy'));

                    $member->select(
                        'members.*', 'member_tier.id as member_tier_id', 'tiers.id as tiers_id'
                    );
                    $member->join('member_tier', 'members.id', '=', 'member_tier.member_id')
                    ->join('tiers', 'member_tier.tier_id', '=', 'tiers.id')
                    ->select('members.*', 'member_tier.id as member_tier_id', 'tiers.name')
                    ->orderBy('tiers.name', $request->input('sortBy'));
                } elseif ($request->input('orderBy') == 'dob') {
                    $member->orderByRaw('MONTH(dob) ' . $request->input('sortBy') . ', DAYOFMONTH(dob) ' . $request->input('sortBy'));
                } elseif ($request->input('orderBy') == 'date_expiry') {
                    $member->select(
                        'members.*', 'member_tier.id as member_tier_id'
                    );
                    $member->join('member_tier', 'members.id', '=', 'member_tier.member_id');
                    $member->orderBy('member_tier.date_expiry', $request->input('sortBy'));
                } else {
                    $member->orderBy($request->input('orderBy'), $request->input('sortBy'));
                }
            }

            if ($request->has('showAll')) {
                if (intval($request->input('showAll')) === 0) {
                    $member->where('members.status', 'active');
                }
            }

            if ($request->has('filter')) {
                if ($request->input('filter') !== 'none') {
                    $member->whereHas('tiers', function ($query) use ($request) {
                        $query->where('tier_id', $request->input('filter'));
                    });
                }
            }

            if ($request->has('venue_id')) {
                if ($request->input('venue_id') !== '0') {
                    $member->whereHas('tiers', function ($query) use ($request) {
                        $query->where('venue_id', $request->input('venue_id'));
                    });
                }
            }

            $paginate_number = 10;
            if ($request->has('paginate_number')) {
                $paginate_number = $request->input('paginate_number');
            }

            $members = $member->with('tiers')
                ->paginate($paginate_number);

            foreach ($members as $member){
                $temp = User::find($member->user_id);
                // $temp->setHidden(['id'])->all();
                // $temp->makeHidden('id')->toArray();
                $tempuser = collect(
                    [
                        'email' => $temp->email,
                        'email_confirmation' => $temp->email_confirmation,
                        'email_confirmation_sent' => $temp->email_confirmation_sent,
                        'email_confirmed_at' => $temp->email_confirmed_at,
                        'mobile' => $temp->mobile,
                        'otp_verified' => $temp->otp_verified,
                        'show_qr_code' => $temp->show_qr_code,
                        'sms_confirmation' => $temp->sms_confirmation,
                        'sms_confirmation_sent' => $temp->sms_confirmation_sent,
                        'sms_confirmed_at' => $temp->sms_confirmed_at
                    ]);

                $member->user = $tempuser;
            }

            return response()->json(['status' => 'ok', 'data' => $members]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update member
     * >> for admin
     *
     * @param Request $request
     * @param ImageOptimizer $imageOptimizer
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(Request $request, $id, ImageOptimizer $imageOptimizer)
    {
        try {
            DB::beginTransaction();

            $member = Member::with('user', 'tiers')->find($id);
            $cloned = $member->replicate();

            if ($request->has('first_name')) {
                $member->first_name = $request->input('first_name');
            } else {
                $member->first_name = '';
            }

            if ($request->has('last_name')) {
                $member->last_name = $request->input('last_name');
            } else {
                $member->last_name = '';
            }

            $member->points = $request->input('points');
            $member->balance = $request->input('balance');
            $member->bepoz_account_number = $request->input('bepoz_account_number');
            $member->bepoz_account_card_number = $request->input('bepoz_account_card_number');

            if ($request->has('status')) {
                $member->status = $request->input('status');
                $member->user->status = $request->input('status');

            //    $staff = Staff::where('user_id', $member->user->id)->first();
            //    if (!is_null($staff)) {
            //        $staff->active = $request->input('status') === 'active' ? true : false;
            //        $staff->save();
            //    }
            }

            if ($request->has('phone')) {
                $member->phone = $request->input('phone');
            }

            if ($request->has('birthday')) {
                $member->dob = date("Y-m-d H:i:s", strtotime($request->input('birthday')));
            }

            if ($request->has('email')) {
                $member->user->email = preg_replace('/\s+/', '+', $request->input('email'));
            }

            if ($request->has('mailing_preference')) {
                $member->mailing_preference = $request->input('mailing_preference');
            }

            if ($request->has('password')) {
                $member->user->password = Hash::make($request->input('password'));
            }

            if ($request->has('mobile')) {
                $member->user->mobile = $request->input('mobile');
            }

            $tier = Tier::where('key', $request->input('tier_key'))->first();
            $date_expiry = $request->has('date_expiry') ? date("Y-m-d H:i:s", strtotime($request->input('date_expiry'))) : null;
            $member->tiers()->sync([$tier->id => ['date_expiry' => $date_expiry]]);

            // if ($request->has('addresses')) {
            //     $addresses = \GuzzleHttp\json_decode($request->input('addresses'));

            //     foreach ($addresses as $address) {
            //         if (isset($address->id)) {
            //             if ($address->deleted) {
            //                 Address::destroy($address->id);
            //             } else {
            //                 $member_address = Address::find($address->id);

            //                 $member_address->category = strtolower($address->category);

            //                 $member_address->type = strtolower($address->type);

            //                 $member_address->unit_number = isset($address->unit_number) ? $address->unit_number : NULL;

            //                 $member_address->recipient_name = isset($address->recipient_name) ? $address->recipient_name : $member->first_name;

            //                 $member_address->save();
            //             }
            //         } else {
            //             $member_address = new Address;

            //             $member_address->member_id = $id;

            //             $member_address->state = $address->components->state;

            //             $member_address->country = $address->components->country;

            //             $member_address->postcode = $address->components->postCode;

            //             $member_address->suburb = $address->components->city;

            //             $member_address->category = strtolower($address->category);

            //             $member_address->type = strtolower($address->type);

            //             $member_address->street = $address->components->fullStreet;

            //             $member_address->unit_number = isset($address->unit_number) ? $address->unit_number : NULL;

            //             $member_address->recipient_name = isset($address->recipient_name) ? $address->recipient_name : $member->first_name;

            //             $member_address->save();

            //         }

            //     }
            // }

            if ($request->hasFile('file')) {

                if (config('bucket.s3')){

                    $picture = $request->file('file');

                    $mime = explode('/', $picture->getMimeType());
                    $type = end($mime);

                    $imageOptimizer->optimizeUploadedImageFile($picture);

                    Storage::disk('s3')->put('member/' . $member->id . '/profile' . '.' . $type, file_get_contents($picture), 'public');

                    $member->profile_img = "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/member/" . $member->id . "/profile." . $type . "?" . time();

                } else {

                    $log = new SystemLog();
                    $log->type = 'setting-error';
                    $log->humanized_message = 'Setting of AWS S3 bucket not found.';
                    $log->payload = 'MemberController.php/update';
                    $log->message = 'Setting of S3 bucket not found.';
                    $log->source = 'MemberController.php';
                    $log->save();

                    return response()->json(['status' => 'error', 'message' => 'Please contact Administrator.'], Response::HTTP_BAD_REQUEST);
                }

            }

            $member->user->save();
            $member->save();

            if (!is_null($member->bepoz_account_id)) {

                $data = array();
                $data['member_id'] = $member->id;

                $job = new BepozJob();
                $job->queue = "update-account";
                $job->payload = \GuzzleHttp\json_encode($data);
                $job->available_at = time();
                $job->created_at = time();
                $job->save();
            }

            if ($request->input('points') !== $request->input('original_points') || $request->input('balance') !== $request->input('original_balance')) {
                $data = array();
                $data['member_id'] = $member->id;
                $data['original_points'] = $request->input('original_points');
                $data['points'] = $request->input('points');
                $data['original_balance'] = $request->input('original_balance');
                $data['balance'] = $request->input('balance');

                $job = new BepozJob();
                $job->queue = "update-balance";
                $job->payload = \GuzzleHttp\json_encode($data);
                $job->available_at = time();
                $job->created_at = time();
                $job->save();
            }

            // log
            $ml = new MemberLog();
            $ml->before = $cloned->toJson();
            $ml->member_id = $member->id;
            $ml->message = $request->input('decrypted_token')->staff->first_name . '(' . $request->input('decrypted_token')->staff->id . ') has updated this member details. ';
            $ml->after = $member->toJson();
            $ml->save();

            DB::commit();

            return response()->json(['status' => 'ok', 'message' => 'Updating member is successful.']);

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Create a new member
     * >> for admin
     *
     * @param Request $request
     * @param ImageOptimizer $imageOptimizer
     * @return \Illuminate\Http\JsonResponse
     */
    protected function create(Request $request, ImageOptimizer $imageOptimizer)
    {
        try {
            $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                // 'password' => 'required',
                // 'metas' => 'required',
                'status' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            // $create_member = Setting::select('value')->where('key', 'create_member')->first()->value;

            // if ($create_member == 'true') {
                // Log::error("create_member");

                DB::beginTransaction();

                $user = new User;

                $user->email = preg_replace('/\s+/', '+', $request->input('email'));

                $user->password = Hash::make($request->input('password'));

                $user->status = $request->input('status');

                if ($request->has('mobile')) {
                    $user->mobile = $request->input('mobile');
                }

                $user->save();


                $member = new Member;

                $member->user_id = $user->id;

                $member->first_name = $request->input('first_name');

                $member->last_name = $request->input('last_name');

                $member->status = $request->input('status');

                if ($request->has('phone')) {
                    $member->phone = $request->input('phone');
                }

                if ($request->has('birthday')) {
                    $member->dob = date("Y-m-d H:i:s", strtotime($request->input('birthday')));
                }

                if ($request->has('mailing_preference')) {
                    $member->mailing_preference = $request->input('mailing_preference');
                }

                $member->save();

                if ($request->has('addresses')) {
                    $addresses = \GuzzleHttp\json_decode($request->input('addresses'));

                    foreach ($addresses as $address) {
                        $member_address = new Address;

                        $member_address->member_id = $member->id;

                        $member_address->state = $address->components->state;

                        $member_address->country = $address->components->country;

                        $member_address->postcode = $address->components->postCode;

                        $member_address->suburb = $address->components->city;

                        $member_address->category = strtolower($address->category);

                        $member_address->type = strtolower($address->type);

                        $member_address->street = $address->components->fullStreet;

                    //    $member_address->street = (isset($address->components->streetNumber) ? $address->components->streetNumber . ' ' : '') . $address->components->street;

                        $member_address->unit_number = isset($address->unit_number) ? $address->unit_number : NULL;

                        $member_address->recipient_name = isset($address->recipient_name) ? $address->recipient_name : $member->first_name;

                        $member_address->save();

                    }
                }

                if ($request->hasFile('file')) {

                    if (config('bucket.s3')){

                        $picture = $request->file('file');

                        $mime = explode('/', $picture->getMimeType());
                        $type = end($mime);

                        $imageOptimizer->optimizeUploadedImageFile($picture);

                        Storage::disk('s3')->put('member/' . $member->id . '/profile' . '.' . $type, file_get_contents($picture), 'public');

                        $member->profile_img = "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/member/" . $user->member->id . "/profile." . $type . "?" . time();

                        $member->save();

                    } else {

                        $log = new SystemLog();
                        $log->type = 'setting-error';
                        $log->humanized_message = 'Setting of AWS S3 bucket not found.';
                        $log->payload = 'MemberController.php/create';
                        $log->message = 'Setting of S3 bucket not found.';
                        $log->source = 'MemberController.php';
                        $log->save();

                        return response()->json(['status' => 'error', 'message' => 'Please contact Administrator.'], Response::HTTP_BAD_REQUEST);
                    }

                }

                DB::commit();

            // } else {
            //     return response()->json(['status' => 'error', 'message' => 'Sorry, create member is disabled. Please check setting.'], Response::HTTP_BAD_REQUEST);
            // }

            return response()->json(['status' => 'ok', 'request' => $request->all(), 'message' => 'Creating member is successful.']);

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return information for specific member
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show(Request $request, $id)
    {
        try {
            $member = Member::with('user', 'tiers', 'promo_accounts',
                'member_orders.order_details.voucher_setup', 'member_issued_vouchers.order_detail.voucher_setup',
                'member_tier.tier', 'claimed_vouchers.voucher_setup')->find($id);

            if (is_null($member)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                call_in_background('acc-balance-get-id ' . $member->id);
                call_in_background('accounts-updated-individual ' . $member->id);
                call_in_background('voucher-updated-individual ' . $member->id);
                call_in_background('accpromo-updated-individual ' . $member->id);

                return response()->json(['status' => 'ok', 'data' => $member->makeVisible(['created_at', 'updated_at'])]);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return all transactions of selected member
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function transaction($id)
    {
        try {
            $orders = Order::where('member_id', $id)->orderBy('id', 'desc')->get();

            if (is_null($orders)) {
                return response()->json(['status' => 'ok', 'data' => []]);
            } else {
                foreach ($orders as $order) {
                    $order->order_details;
                }

                return response()->json(['status' => 'ok', 'data' => $orders]);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return all issued vouchers of selected member
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function issuedVoucher($id)
    {
        try {
            $vouchers = MemberVouchers::where('member_id', '=', $id)->get();
            foreach ($vouchers as $voucher) {
                $voucher->order_detail->voucher_setup;
            }

            return response()->json(['status' => 'ok', 'data' => $vouchers]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Bulk process of member status
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function bulk(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'original_ids' => 'array',
                'new_ids' => 'array',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $originals = $request->input("original_ids");
            $new_ids = $request->input("new_ids");

            if (!empty(array_filter($originals))) {

                foreach ($new_ids as $new_id) {

                    if (DB::table('members')->where('id', $new_id)->first()->status == 'inactive') {

                        DB::table('members')->where('id', $new_id)->update(['status' => 'active']);

                    } else {

                        DB::table('members')->where('id', $new_id)->update(['status' => 'inactive']);
                    }

                }

            } else {

                foreach ($new_ids as $new_id) {
                    if (DB::table('members')->where('id', $new_id)->first()->status == 'inactive') {

                        DB::table('members')->where('id', $new_id)->update(['status' => 'active']);

                    } else {

                        DB::table('members')->where('id', $new_id)->update(['status' => 'inactive']);
                    }
                }

            }


            return response()->json(['status' => 'ok', 'message' => "Member changed successfully"]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Resend confirmation email
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function resendConfirmations(Request $request, MandrillExpress $mx, MailjetExpress $mjx)
    {
        try {
            $users = User::where('email_confirmation', 0)
                ->get();

            // Get web app token
            $platform = Platform::where('name', 'web')->first();

            foreach ($users as $user) {

                $confirmationToken = $user->generateEmailConfirmationToken();

                // LOAD GAMING SETTING
                $gaming_system_enable = Setting::where('key', 'gaming_system_enable')->first()->value;

                if ( $gaming_system_enable == "true" ) {
                    $gaming_system = Setting::where('key', 'gaming_system')->first()->value;

                    if ( $gaming_system == "igt" ){
                        $url = config('bucket.APP_URL') . '/api/confirmIGT/' . $confirmationToken . '?app_token=' . $platform->app_token;
                    } else if ( $gaming_system == "odyssey" ){
                        $url = config('bucket.APP_URL') . '/api/confirmOdyssey/' . $confirmationToken . '?app_token=' . $platform->app_token;
                    }

                } else {
                    $url = config('bucket.APP_URL') . '/api/confirm/' . $confirmationToken . '?app_token=' . $platform->app_token;
                }

                // Send Confirmation Email

                $data = array(
                    'URL' => $url
                );

                $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;

                if ($email_server === "mandrill") {

                    if ($mx->init()) {
                        $mx->send('confirmation', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                    } else {
                        $pj = new PendingJob();
                        $pj->payload = \GuzzleHttp\json_encode($data);
                        $pj->extra_payload = \GuzzleHttp\json_encode(array(
                            'email' => $user->email,
                            'category' => 'confirmation',
                            'name' => $user->member->first_name . ' ' . $user->member->last_name
                        ));
                        $pj->queue = "email";
                        $pj->save();
                    }

                } else if ($email_server === "mailjet") {

                    if ($mjx->init()) {
                        $mjx->send('confirmation', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                    } else {
                        $pj = new PendingJob();
                        $pj->payload = \GuzzleHttp\json_encode($data);
                        $pj->extra_payload = \GuzzleHttp\json_encode(array(
                            'email' => $user->email,
                            'category' => 'confirmation',
                            'name' => $user->member->first_name . ' ' . $user->member->last_name
                        ));
                        $pj->queue = "email";
                        $pj->save();
                    }

                }


                $job = (new SendReminderNotification($user))->delay(30);
                dispatch($job);
            }

            return response()->json(['status' => 'ok', 'message' => 'Confirmation emails have been successfully sent.']);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Resend confirmation member
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function resendConfirmationMember(Request $request, MandrillExpress $mx, MailjetExpress $mjx)
    {
        try {
            if ($request->has('member_id')) {

                $member = Member::where('id', $request->input('member_id'))->first();
                $user = User::where('id', $member->user_id)->first();


                // Get web app token
                $platform = Platform::where('name', 'web')->first();
                $confirmationToken = $user->generateEmailConfirmationToken();

                // LOAD GAMING SETTING
                $gaming_system_enable = Setting::where('key', 'gaming_system_enable')->first()->value;

                if ( $gaming_system_enable == "true" ) {
                    $gaming_system = Setting::where('key', 'gaming_system')->first()->value;

                    if ( $gaming_system == "igt" ){
                        $url = config('bucket.APP_URL') . '/api/confirmIGT/' . $confirmationToken . '?app_token=' . $platform->app_token;
                    } else if ( $gaming_system == "odyssey" ){
                        $url = config('bucket.APP_URL') . '/api/confirmOdyssey/' . $confirmationToken . '?app_token=' . $platform->app_token;
                    }

                } else {
                    $url = config('bucket.APP_URL') . '/api/confirm/' . $confirmationToken . '?app_token=' . $platform->app_token;
                }

                // Send Confirmation Email

                $data = array(
                    'URL' => $url
                );

                $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;

                if ($email_server === "mandrill") {

                    if ($mx->init()) {
                        $mx->send('confirmation', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                    } else {
                        $pj = new PendingJob();
                        $pj->payload = \GuzzleHttp\json_encode($data);
                        $pj->extra_payload = \GuzzleHttp\json_encode(array(
                            'email' => $user->email,
                            'category' => 'confirmation',
                            'name' => $user->member->first_name . ' ' . $user->member->last_name
                        ));
                        $pj->queue = "email";
                        $pj->save();
                    }

                } else if ($email_server === "mailjet") {

                    if ($mjx->init()) {
                        $mjx->send('confirmation', $user->member->first_name . ' ' . $user->member->last_name, $user->email, $data);
                    } else {
                        $pj = new PendingJob();
                        $pj->payload = \GuzzleHttp\json_encode($data);
                        $pj->extra_payload = \GuzzleHttp\json_encode(array(
                            'email' => $user->email,
                            'category' => 'confirmation',
                            'name' => $user->member->first_name . ' ' . $user->member->last_name
                        ));
                        $pj->queue = "email";
                        $pj->save();
                    }

                }

                $job = (new SendReminderNotification($user))->delay(30);
                dispatch($job);

            }

            return response()->json(['status' => 'ok', 'message' => 'Confirmation emails for this member have been successfully sent.']);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Search member
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function search(Request $request, Member $member)
    {
        try {
            $member = $member->newQuery();

            if ($request->has('email')) {
                $member->whereHas('user', function ($q) use ($request) {
                    $q->where('email', 'like', '%' . $request->input('email') . '%');
                });
            }

            if ($request->has('first_name')) {

                $member->where(function ($query) use ($request) {
                    if (strpos($request->input('first_name'), ' ') !== false) {
                        $array = explode(" ", $request->input('first_name'));
                        $keyword = implode("%", $array);
                        $query->where('first_name', 'like', '%' . $keyword . '%');
                    } else {
                        $query->where('first_name', 'like', '%' . $request->input('first_name') . '%');
                    }
                });
            }

            if ($request->has('last_name')) {
                $member->where(function ($query) use ($request) {
                    if (strpos($request->input('last_name'), ' ') !== false) {
                        $array = explode(" ", $request->input('last_name'));
                        $keyword = implode("%", $array);
                        $query->where('last_name', 'like', '%' . $keyword . '%');
                    } else {
                        $query->where('last_name', 'like', '%' . $request->input('last_name') . '%');
                    }
                });
            }

            if ($request->has('mobile')) {
                $member->whereHas('user', function ($q) use ($request) {
                    $q->where('mobile', 'like', '%' . $request->input('mobile') . '%');
                });
            }

            $members = $member->with('user')->get();

            return response()->json(['status' => 'ok', 'data' => $members, 'request' => $request->all()]);

        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Deactivate member
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function inactiveMember(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'member_id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $member = Member::find($request->input('member_id'));
            if (is_null($member)) {
                return response()->json(['status' => 'error', 'message' => 'Member not found'], Response::HTTP_BAD_REQUEST);
            }

            $cloned = $member->replicate();

            DB::beginTransaction();

            $member->status = 'inactive';
            $member->save();

            if (!is_null($member->bepoz_account_id)) {

                $data = array();
                $data['member_id'] = $member->id;

                $job = new BepozJob();
                $job->queue = "update-account";
                $job->payload = \GuzzleHttp\json_encode($data);
                $job->available_at = time();
                $job->created_at = time();
                $job->save();
            }

            $ml = new MemberLog();
            $ml->before = $cloned->toJson();
            $ml->member_id = $member->id;
            $ml->message = $request->input('decrypted_token')->staff->first_name . '(' . $request->input('decrypted_token')->staff->id . ') has updated this member details. ';
            $ml->after = $member->toJson();
            $ml->save();

            // $staff = Staff::where('user_id', $member->user->id)->first();
            // if (!is_null($staff)) {
            //     $staff->active = false;
            //     $staff->save();
            // }

            DB::commit();

            return response()->json(['status' => 'ok', 'message' => 'Member is inactive now.']);

        } catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Send System Notification to all
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendBroadcastSystemNotification(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'notification_id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $notification = SystemNotification::find($request->input('notification_id'));
            if (is_null($notification)) {
                return response()->json(['status' => 'error', 'message' => 'Notification not found'], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();

            $job = (new BroadcastSystemNotifications($notification->id))->delay(30);
            dispatch($job);

            DB::commit();

            return response()->json(['status' => 'ok', 'message' => 'Sending Notification is successful.']);

        } catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Send System Notification to group
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendBroadcastGroupSystemNotification(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'tier_id' => 'required',
                'notification_id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $tier_id = $request->input('tier_id');
            $notification = SystemNotification::find($request->input('notification_id'));
            if (is_null($notification)) {
                return response()->json(['status' => 'error', 'message' => 'Notification not found'], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();

            $job = (new BroadcastGroupSystemNotifications($tier_id, $notification->id))->delay(30);
            dispatch($job);

            DB::commit();

            return response()->json(['status' => 'ok', 'message' => 'Sending Group System Notification is successful.']);

        } catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Send System Notification to a member
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendSystemNotification(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'member_id' => 'required',
                'notification_id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }


            $member = Member::find($request->input('member_id'));
            if (is_null($member)) {
                return response()->json(['status' => 'error', 'message' => 'Member not found'], Response::HTTP_BAD_REQUEST);
            }

            $notification = SystemNotification::find($request->input('notification_id'));
            if (is_null($notification)) {
                return response()->json(['status' => 'error', 'message' => 'Notification not found'], Response::HTTP_BAD_REQUEST);
            }

            $company_name = Setting::where('key', 'company_name')->first()->value;

            DB::beginTransaction();

            $msn = new MemberSystemNotification();
            $msn->member_id = $member->id;
            $msn->system_notification_id = $notification->id;
            $msn->status = 'unread';
            $msn->sent_at = Carbon::now(config('app.timezone'));
            $msn->save();

            $data = [
                // "tags" => [["key" => "email", "relation" => "=", "value" => $member->user->email]],
                "filters" => [["field" => "tag", "key" => "email", "relation" => "=", "value" => $member->user->email]],
                "contents" => ["en" => $notification->message],
                "headings" => ["en" => $company_name],
                "data" => ["category" => "member"],
                "ios_badgeType" => "Increase",
                "ios_badgeCount" => 1
            ];

            // OneSignalFacade::postNotification($data);

            DB::commit();

            OneSignal::sendNotificationCustom($data);

            return response()->json(['status' => 'ok', 'message' => 'Sending Notification is successful.']);

        } catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Paginate member logs
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function paginateMemberLogs(Request $request)
    {
        try {
            $logs = MemberLog::where('member_id', $request->input('member_id'))
                ->orderBy('created_at', 'desc')
                ->paginate(10);

            return response()->json(['status' => 'ok', 'data' => $logs]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }


    /**
     * Get Prize Promotion
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function prizePromotion(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $id = \GuzzleHttp\json_decode($request->input('id'));

            $prize_promotion = PrizePromotion::whereIn('id', $id)->get();

            if (is_null($prize_promotion)) {
                return response()->json(['status' => 'error', 'message' => 'Prize Promotion not found'], Response::HTTP_BAD_REQUEST);
            }

            return response()->json(['status' => 'ok', 'prize_promotion' => $prize_promotion]);

        } catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Import members data in CSV
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function importExcel()
    {
        try {
            if (Input::hasFile('import_file')) {
                $path = Input::file('import_file')->getRealPath();
                $data = Excel::load($path, function ($reader) {

                })->get();
                if (!empty($data) && $data->count()) {
                    foreach ($data as $key => $value) {
                        if (!is_null($value->email_primary)) {


                            $user = User::with('member')->where('email', $value->email_primary)->first();
                            if (is_null($user)) {
                                $user = new User();
                                $user->email = $value->email_primary;
                                $user->email_confirmation = true;
                                $user->save();

                                $member = new Member();
                                $member->user_id = $user->id;
                                $member->save();

                            }
                            $user->password = Hash::make($value->first_name . '123');
                            $user->mobile = $value->mobile;
                            $user->save();

                            $user->member->first_name = $value->first_name;
                            $user->member->last_name = $value->last_name;
                            $user->member->points = intval($value->points_balance) / 100;
                            $user->member->balance = intval($value->account_balance) / 100;
                            $user->member->bepoz_account_id = intval($value->account_id);
                            $user->member->bepoz_account_number = $value->account_number;
                            $user->member->bepoz_account_card_number = $value->card_number;
                            $user->member->bepoz_existing_account = true;
                            $user->member->bepoz_account_status = $value->status;
                            $user->member->save();

                            $tier = Tier::where('bepoz_group_id', intval($value->parent_id))
                                ->where('is_active', true)
                                ->first();

                            $member_tier = MemberTiers::where('member_id', $user->member->id)->first();
                            if (is_null($member_tier)) {
                                $member_tier = new MemberTiers();
                            }
                            if (!is_null($tier)) {
                                $member_tier->tier_id = $tier->id;

                            }

                            $member_tier->date_expiry = $value->date_expiry;
                            $member_tier->save();
                        }

                    }
                }
            }
            return back();
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }


    }

    function isDate($value)
    {
        if (!$value) {
            return false;
        }

        try {
            return Carbon::parse($value, config('app.timezone'));
        } catch (\Exception $e) {
            return false;
        }
    }
    
    public function importUsers(Request $request, MandrillExpress $mx, MailjetExpress $mjx)
    {
        $ctr = 0;
        $properties = [];
        $rowerror = [];
        try {
            $file = $request->file('csv_file');

            $name = time() . '.csv';
            $file_path = public_path() . '/' . $name;
            file_put_contents($file_path, file_get_contents($file));
            $handle = fopen($file_path, "r");

            $counterCreated = 0;
            $counterUpdated = 0;

            $keys = [];
            $emailsend = [];
            if ($handle) {

                $total_import = 0;
                while (($line = fgets($handle)) !== false) {
                    // process the line read.

                    if ($ctr < 1) {
                        $keys = explode(',', $line);
                        foreach ($keys as $key => &$value) {
                            $value = preg_replace('/[^A-Za-z0-9\-]/', '', strtolower(trim(preg_replace('/\s/', '', $value))));
                            $keys[$key] = $value;
                        }

                    } else {
                        $columns = explode(',', $line);
                        $debug = false;
                        foreach ($columns as $key => $column) {                            
                            if (isset($column)){
                                $properties[$keys[$key]] = trim($column);
                            } else {
                                $debug = true;
                                // Log::warning($keys[$key]);
                                // Log::warning($column);
                            }
                        }

                        if ($debug){
                            $rowerror[] = $properties;
                            Log::warning($properties);
                            continue;
                        }

                        if (!isset($properties['email'])) {
                            return response()->json(['status' => 'error', 'message' => 'missing email'], Response::HTTP_BAD_REQUEST);
                        }

                        if (!isset($properties['bepozaccountid'])) {
                            return response()->json(['status' => 'error', 'message' => 'missing bepozaccountid'], Response::HTTP_BAD_REQUEST);
                        }

                        if (!isset($properties['bepozaccountnumber'])) {
                            return response()->json(['status' => 'error', 'message' => 'missing bepozaccountnumber'], Response::HTTP_BAD_REQUEST);
                        }

                        if (!isset($properties['bepozcardnumber'])) {
                            return response()->json(['status' => 'error', 'message' => 'missing bepozcardnumber'], Response::HTTP_BAD_REQUEST);
                        }

                        if (!isset($properties['fname'])) {
                            return response()->json(['status' => 'error', 'message' => 'missing fname'], Response::HTTP_BAD_REQUEST);
                        }

                        if (!isset($properties['lname'])) {
                            return response()->json(['status' => 'error', 'message' => 'missing lname'], Response::HTTP_BAD_REQUEST);
                        }

                        if (!isset($properties['bepozgroupid'])) {
                            return response()->json(['status' => 'error', 'message' => 'missing bepozgroupid'], Response::HTTP_BAD_REQUEST);
                        }

                        $user = User::where('email', strtolower($properties['email']))->first();

                        try {
                            DB::beginTransaction();
       
                            $password = $this->generateRandomString(6);

                            if (is_null($user)) {
                                
                                $user = User::create([
                                    'email' => strtolower($properties['email']),
                                    'mobile' => empty($properties['mobile']) ? null : $properties['mobile'],
                                    'password' => Hash::make($password)
                                ]);

                                $member = new Member();
                                $member->user_id = $user->id;
                                $member->first_name = $properties['fname'];
                                $member->last_name = $properties['lname'];
                                $member->status = 'active';
                                $member->bepoz_account_id = $properties['bepozaccountid'];
                                $member->bepoz_account_number = $properties['bepozaccountnumber'];
                                $member->bepoz_account_card_number = $properties['bepozcardnumber'];
                                $member->bepoz_account_status = "OK";
                                $member->save();

                                $date_now = Carbon::now(config('app.timezone'));
                                $user->email_confirmation = true;
                                $user->email_confirmation_sent = $date_now;
                                $user->email_confirmed_at = $date_now;
                                $user->save();

                                $user_role = new UserRoles();
                                $user_role->role_id = 3; // Manually add
                                $user_role->user_id = $user->id;
                                $user_role->save();

                                $tier = Tier::where('bepoz_group_id', $properties['bepozgroupid'])
                                    ->orWhere('name', $properties['bepozgroupid'])
                                    ->where('is_active', true)
                                    ->first();

                                if (is_null($tier)) {
                                    return response()->json(['status' => 'error', 'message' => 'Tier not Found for BepozGroupId: ' . $properties['bepozgroupid']], Response::HTTP_BAD_REQUEST);
                                }

                                $member_tier = new MemberTiers();
                                $member_tier->member_id = $user->member->id;
                                $member_tier->tier_id = $tier->id;
                                $member_tier->date_expiry = null;
                                $member_tier->save();
                                
                                try {                                    
                                    $checkSSOAccount = $this->checkSSOAccount(strtolower($properties['email']));
                                
                                    if ($checkSSOAccount['status'] == 'ok') {

                                    } else {
                                        $this->createSSOAccount(strtolower($properties['email']), $password);
                                    }
                                } catch (\Exception $e) {
                                    Log::info($e);
                                }

                                $counterCreated++;


                            } else {

                                $user->password = Hash::make($password);
                                $user->mobile = empty($properties['mobile']) ? null : $properties['mobile'];
                                $user->save();

                                $tier = Tier::where('bepoz_group_id', $properties['bepozgroupid'])
                                    ->where('is_active', true)
                                    ->first();

                                if (is_null($tier)) {
                                    return response()->json(['status' => 'error', 'message' => 'Tier not Found for BepozGroupId: ' . $properties['bepozgroupid']], Response::HTTP_BAD_REQUEST);
                                }

                                $member = Member::find($user->member->id);
                                $member->first_name = $properties['fname'];
                                $member->last_name = $properties['lname'];
                                $member->status = 'active';
                                $member->bepoz_account_id = $properties['bepozaccountid'];
                                $member->bepoz_account_number = $properties['bepozaccountnumber'];
                                $member->bepoz_account_card_number = $properties['bepozcardnumber'];
                                $member->bepoz_account_status = "OK";
                                $member->save();

                                $member_tier = MemberTiers::where('member_id', $user->member->id)->first();
                                $member_tier->tier_id = $tier->id;
                                $member_tier->date_expiry = null;
                                $member_tier->save();

                                $counterUpdated++;
                            }

                            // Create reset password token
                            $expire_time = time() + (2 * 24 * 60 * 60); // set token expire time: 2 days
                            $token = md5(implode("|", ['email' => strtolower($properties['email']), 'expired' => $expire_time]));

                            // Set all existed tokens to invalid
                            DB::table('password_resets')->where('email', '=', strtolower($properties['email']))->update(['valid' => 0]);

                            // Store the token
                            DB::table('password_resets')->insert(
                                ['email' => strtolower($properties['email']), 'token' => $token, 'expired' => $expire_time]
                            );

                            $reset_url = config('bucket.APP_URL') . '/api/resetform/' . $token;
                            $ios_link = Setting::where('key', 'ios_link')->first()->extended_value;
                            $android_link = Setting::where('key', 'android_link')->first()->extended_value;

                            // Send Email + url
                            $data = array(
                                'EMAIL' => strtolower($properties['email']),
                                'BEPOZ_ID' => strtolower($properties['bepozaccountid']),
                                'BEPOZ_ACC_NUMBER' => strtolower($properties['bepozaccountnumber']),
                                'FIRST_NAME' => $user->member->first_name,
                                'LAST_NAME' => $user->member->last_name,
                                'TEMP_PASSWORD' => $password,
                                'RESET_URL' => $reset_url,
                                'URL_IOS' => $ios_link,
                                'URL_ANDROID' => $android_link                            
                            );

                            $emailsend[] = $data;
                            DB::commit();

                        } catch (\Exception $e) {
                            DB::rollBack();
                            Log::info($e);
                        }


                        $total_import++;
                    }

                    $ctr++;
                }

                fclose($handle);

                if ($counterCreated == 0 && $counterUpdated == 0) {
                    return response()->json(['status' => 'ok', 'result' => 0, 'data' => 'Nothing to update/create. Possibly wrong template.', 'rowerror' => $rowerror]);
                }

                 
                $email_server = Setting::select('value')->where('key', 'email_server')->first()->value;

                if ($email_server === "mandrill") {

                    if ($mx->init()) {
                        foreach ($emailsend as $data){
                            $mx->send('import_member', $data['FIRST_NAME']. ' ' .$data['LAST_NAME'], $data['EMAIL'], $data);
                        }
                    } else {
                        foreach ($emailsend as $data){
                            $pj = new PendingJob();
                            $pj->payload = \GuzzleHttp\json_encode($data);
                            $pj->extra_payload = \GuzzleHttp\json_encode(array(
                                'email' => $data['EMAIL'],
                                'category' => 'import_member',
                                'name' => $data['FIRST_NAME']. ' ' .$data['LAST_NAME']
                            ));
                            $pj->queue = "email";
                            $pj->save();
                        }
                    }

                }
                
                return response()->json(['status' => 'ok', 'data' => $total_import . ' members are imported, ' . $counterCreated . ' are created, ' . $counterUpdated . ' are updated.', 'rowerror' => $rowerror]);
            } else {
                // error opening the file.
                Log::warning("error opening the file");
            }

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'trace' => $e->getTraceAsString(), 'rowerror' => $rowerror], Response::HTTP_BAD_REQUEST);
        }

        // $test = explode('/images/', $image->path);
        // $name = array_last($test);
        // $exists = file_get_contents($image->path);
        // Storage::disk('s3')->put('images/' . $name, $exists, 'public');
    }
    
    public function importUsersOnesignal(Request $request, MandrillExpress $mx, MailjetExpress $mjx)
    {
        $ctr = 0;
        $delimiter = ';';
        $properties = [];
        $rowerror = [];
        try {
            $file = $request->file('csv_file');

            $name = time() . '.csv';
            $file_path = public_path() . '/' . $name;
            file_put_contents($file_path, file_get_contents($file));
            $handle = fopen($file_path, "r");

            $counterCreated = 0;
            $counterUpdated = 0;

            $keys = [];
            $emailsend = [];
            if ($handle) {

                $total_import = 0;
                while (($line = fgets($handle)) !== false) {
                    // process the line read.

                    if ($ctr < 1) {
                        $keys = explode($delimiter, $line);
                        foreach ($keys as $key => &$value) {
                            $value = preg_replace('/[^A-Za-z0-9\-]/', '', strtolower(trim(preg_replace('/\s/', '', $value))));
                            $keys[$key] = $value;

                            // Log::warning($value);
                        }

                        // subscribed
                        // lastactive
                        // firstsession
                        // channel
                        // device
                        // sessions
                        // playerid
                        // tags
                        // appversion
                        // country
                        // rooted
                        // lat
                        // lon
                        // usageduration
                        // languagecode

                    } else {
                        $columns = explode($delimiter, $line);
                        $debug = false;
                        foreach ($columns as $key => $column) { 
                            if (isset($column)){
                                $properties[$keys[$key]] = trim($column);
                            } else {
                                $debug = true;
                                // Log::warning($keys[$key]);
                                // Log::warning($column);
                            }
                        }

                        if ($debug){
                            $rowerror[] = $properties;
                            // Log::warning($properties);
                            continue;
                        }

                        // foreach ($properties as $key => $column) {
                        //     if ($key == "tags") {
                        //         Log::warning($column);
                        //     }
                        // }

                        if (isset($properties['tags'])) {
                            // Log::warning($properties['tags']);
                            
                                $tags = str_replace('"{', '{', $properties['tags']);
                                $tags = str_replace('}"', '}', $tags);
                                $tags = str_replace('""', '"', $tags);
                                $tags = json_decode($tags, true);
                                // Log::critical($tags);

                                if ( isset($tags['email']) ) {
                                    // Log::error($tags['email']);

                                    $email = strtolower(trim($tags['email']));
                                    $user = User::where('email', $email)->first();
                                    // Log::critical($user);

                                    if ( isset($user->member) ) {
                                        // Log::warning($user->member->onesignal);

                                        if ( empty($user->member->onesignal) ) {
                                            
                                            try {
                                                DB::beginTransaction();
                                                $user->member->onesignal = $properties['playerid'];
                                                $user->member->save();
                                                $counterUpdated++;
                                                DB::commit();
                                            } catch (\Exception $e) {
                                                DB::rollBack();
                                                Log::info($e);
                                            }

                                        }
                                    }
                                }
                        }

                        // Log::info("END");

                        $total_import++;
                    }

                    $ctr++;
                }

                fclose($handle);
                 
                return response()->json(['status' => 'ok', 'data' => $total_import . ' onesignal members are imported, ' . $counterUpdated . ' are updated.', 'rowerror' => $rowerror]);
            } else {
                // error opening the file.
                Log::warning("error opening the file");
            }

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'trace' => $e->getTraceAsString(), 'rowerror' => $rowerror], Response::HTTP_BAD_REQUEST);
        }
    }

    public function dummyMember(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [                
                'max' => 'required',
                'min' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            if ( intval($request->input('min')) < 1) {
                return response()->json(['status' => 'error', 'message' => 'should fill number'], Response::HTTP_BAD_REQUEST);
            }

            if ( intval($request->input('max')) < 1) {
                return response()->json(['status' => 'error', 'message' => 'should fill number'], Response::HTTP_BAD_REQUEST);
            }

            for ($i = $request->input('min'); $i <= $request->input('max'); $i++)
            {
                
                    $email = "steve+100".$i."@vectron.com.au";
                    $password = "test123";
                    $mobile = "0400000000";
                    $first_name = "steve";
                    $last_name = "test".$i;
                    $mobile = "0400000000";
                    $mobile = "0400000000";

                    $credentials = array(
                        'email' => $email,
                        'password' => Hash::make($password),
                        'mobile' => $mobile
                    );

                    $user = User::create($credentials);                    

                    $member = new Member;
                    $member->user_id = $user->id;
                    $member->first_name = $first_name;
                    $member->last_name = $last_name;
                    $member->share_info = 1;
                    $member->current_preferred_venue_name = "";
                    $member->original_preferred_venue_name = "";

                    // if ( $gaming_system_enable == "true" ) {
                    //     $member->bepoz_account_number = $bepoz_account_number;
                    // }

                    if ($request->has('dob')) {
                        if ($request->input('dob') != '') {
                            $member->dob = Carbon::createFromFormat('d/m/Y', $request->input('dob'));
                        } else {
                            $member->dob = null;
                        }
                    } else {
                        $member->dob = null;
                    }
                    // Log::info("signup 2500 ");
                    $member->postcode = $request->has('postcode') ? $request->input('postcode') : null;
                    $member->save();

                    $confirmationToken = $user->generateEmailConfirmationToken();

                    $user_obj = User::find($user->id); // refresh...
                    $user_obj->member->save();

                    $user_role = new UserRoles;
                    $user_role->role_id = 3; // Manually add
                    $user_role->user_id = $user->id;
                    $user_role->save();

                    $default_tier = Setting::where('key', 'default_tier')->first()->value;
                    $tier = Tier::find($default_tier);
                    $date_expiry = null;
                    $user->member->tiers()->sync([$tier->id => ['date_expiry' => $date_expiry]]);

                    $venue = Venue::with('venue_tags')->find($request->input('venue_id'));
                    $member->current_preferred_venue = is_null($venue) ? '' : $venue->id;
                    $member->current_preferred_venue_name = is_null($venue) ? '' : $venue->name;
                    $member->original_preferred_venue = is_null($venue) ? '' : $venue->id;
                    $member->original_preferred_venue_name = is_null($venue) ? '' : $venue->name;

                    // $member->payload = $customfieldpayload;
                    $member->share_info = 1;
                    $member->save();

            }

            return response()->json(['status' => 'ok', 'data' => $request->input('min'). ' until ' .$request->input('max')]);
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage() ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function confirmDummyMember(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [                
                'max' => 'required',
                'min' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            if ( intval($request->input('min')) < 1) {
                return response()->json(['status' => 'error', 'message' => 'should fill number'], Response::HTTP_BAD_REQUEST);
            }

            if ( intval($request->input('max')) < 1) {
                return response()->json(['status' => 'error', 'message' => 'should fill number'], Response::HTTP_BAD_REQUEST);
            }

            for ($i = $request->input('min'); $i <= $request->input('max'); $i++)
            {
                    $member = Member::find($i);
                    $confirmationToken = $member->user->email_confirmation_token;

                    $platform = Platform::where('name', 'web')->first();
                    $url = config('bucket.APP_URL') . '/api/confirm/' . $confirmationToken . '?app_token=' . $platform->app_token;

                    Log::warning($url);

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                    curl_setopt($ch, CURLOPT_ENCODING, "gzip");
                    $content = curl_exec($ch);
                    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    
                    Log::warning($content);
                    Log::warning($httpCode);

                    if ($content) {
                        // Log::info("signup 2710 ");
                        // $payload = \GuzzleHttp\json_decode($content);                        
                    } else {                        
                        Log::warning("failed ".$url);
                    }
            }

            return response()->json(['status' => 'ok', 'data' => $request->input('min'). ' until ' .$request->input('max')]);
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage() ], Response::HTTP_BAD_REQUEST);
        }
    }
    
    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    protected function createSSOAccount($email, $password)
    {
        $your_order_api = Setting::where('key', 'your_order_api')->first()->value;
        $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

        $user = User::where('email', strtolower($email))->first();

        $datasend = array(
            'license_key' => $your_order_key,
            'email' => $user->email,
            'password' => $password,
            'family_name' => $user->member->last_name,
            'given_name' => $user->member->first_name,
            'phone_number' => $user->mobile,
            'name' => $user->member->first_name
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $your_order_api . "/register");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_ENCODING, "gzip");
        $content_check = curl_exec($ch);
        $httpCode_check = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        // Log::info("1639 content_check");
        $payload_check = \GuzzleHttp\json_decode($content_check);
        if ($payload_check->status == "ok") {
            return true;
        }

        if ($payload_check->status == "error") {
            return false;
        }

        return false;
    }

    protected function checkSSOAccount($email)
    {
        $your_order_api = Setting::where('key', 'your_order_api')->first()->value;
        $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

        $datacheck = array(
            'email' => $email,
            "license_key" => $your_order_key,
            "request_token" => 1
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $your_order_api . "/checkSubAccount");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datacheck);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_ENCODING, "gzip");
        $content_check = curl_exec($ch);
        $httpCode_check = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // Log::info("542");
        // Log::info($content_check);
        // Log::info($httpCode_check);

        // if ($payload_check->status == "ok") {
        //     return true;
        // }

        // if ($payload_check->status == "error") {
        //     return false;
        // }

        // return false;

        // $formatted_response = [
        //     'status' => 'error',
        //     'error' => 'SSO problem'
        // ];

        // $formatted_response = array(
        //     'status' => 'error',
        //     'error' => 'SSO problem'
        // );

        $formatted_response = array();
        $formatted_response['status'] = 'error';
        $formatted_response['error'] = 'SSO problem';
        // Log::info("1701 content_check");
        // $array_formatted_response = json_decode(json_encode($formatted_response), true);
        $array_formatted_response = \GuzzleHttp\json_decode(json_encode($formatted_response));

        if ($content_check) {
            // Log::info("1706 content_check");
            $payload_check = \GuzzleHttp\json_decode($content_check, true);
            return $payload_check;
        } else {
            // Log::info("823");
            return $array_formatted_response;
        }

        // Log::info("827");
        return $array_formatted_response;
    }

    /**
     * receive members data from websignup (called by 3rd party system APi)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function websignup(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'member_group' => 'required',
                'expiry' => 'required',
                'member_number' => 'required',
                'mobile' => 'required',
                'email' => 'required',
                'firstname' => 'required',
                'lastname' => 'required',
                'barcode' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            // bepoz group id
            $member_group = $request->input('member_group');

            // format mm/dd/yyyy
            $expiry = $request->input('expiry');

            // bepoz account number or bepoz account card number
            $member_number = $request->input('member_number');


            $mobile = $request->input('mobile');
            $email = preg_replace('/\s+/', '+', $request->input('email'));

            if (!$this->validEmail($email)){
                return response()->json(['status' => 'error', 'message' => 'Wrong email format'], Response::HTTP_BAD_REQUEST);
            }

            $firstname = $request->input('firstname');
            $lastname = $request->input('lastname');
            $barcode = $request->input('barcode');

            $message = 'A member has been updated';
            if (!is_null($member_number)) {

                $member = Member::where('bepoz_account_card_number', $member_number)->first();

                $bepoz = new Bepoz();
                $checkBepozConnection = $bepoz->SystemCheck();

                $selectedID = 0;
                if ($checkBepozConnection) {
                    $searchData = $member_number;
                    $searchResult = $bepoz->AccountSearch(4, $searchData);                    
                    // Log::info("AccountSearch");
                    // Log::info($searchResult);

                    if ($searchResult) {
                        // $dateUpdated = "2018-12-18T16:24:33";

                        //$payload = \GuzzleHttp\json_decode($searchResult);

                        foreach ($searchResult as $key => $value) {
                            foreach ($value as $key1 => $value1) {
                                if ($key1 == 'ID') {
                                    $param = ['AccountID' => $value1];
                                    $detail = $bepoz->AccountGet($param);
                                    // Log::info("AccountGet");
                                    // Log::info($detail);
                                    // Log::info( $detail['Account']['AccountID']);
                                    if ($detail) {
                                        $selectedID = $detail['Account']['AccountID'];
                                    //    foreach ($detail as $detailkey1 => $detailvalue1) {
                                    //        foreach ($detailvalue1 as $detailkey2 => $detailvalue2) {
                                    //            if ($detailkey2 == 'DateUpdated') {
                                    //                if ($detailvalue2 > $dateUpdated) {
                                    //                    foreach ($value1 as $key2 => $value2) {
                                    //                        $selectedID = $value2;
                                    //                    }
                                    //                    $dateUpdated = $detailvalue2;
                                    //                }
                                    //            }
                                    //        }
                                    //    }
                                    } else {
                                        return response()->json(['status' => 'error', 'message' => 'Please make sure that your Bepoz is online'], Response::HTTP_BAD_REQUEST);
                                    }

                                }

                            }

                        }
                    } else {
                        return response()->json(['status' => 'error', 'message' => 'Please make sure that your Bepoz is online'], Response::HTTP_BAD_REQUEST);
                    }

                } else {
                    return response()->json(['status' => 'error', 'message' => 'Please make sure that your Bepoz is online'], Response::HTTP_BAD_REQUEST);
                }

                if (is_null($member)) {
                    $message = 'A new member has been created';

                    DB::beginTransaction();

                    $user = new User();
                    $user->email = $email;
                    $user->email_confirmation = true;
                    $user->save();

                    $member = new Member();
                    $member->user_id = $user->id;
                    $member->first_name = $firstname;
                    $member->last_name = $lastname;
                    $member->bepoz_account_id = $selectedID;
                    $member->bepoz_account_card_number = $member_number;
                    $member->bepoz_account_number = $member_number;
                    $member->bepoz_existing_account = true;
                    $member->bepoz_account_status = "active";
                    $member->member_group = $member_group;
                    $member->barcode = $barcode;

                    if (!is_null($request->input('sid'))) {
                        $member->sid = $request->input('sid');
                    }

                    if ( $request->has('is_photo_uploaded') ) {
                        $is_photo_uploaded = $request->input('is_photo_uploaded');
                        if ($is_photo_uploaded == 'no' || $is_photo_uploaded == 'false' || $is_photo_uploaded== '0' ) {
                            $member->is_photo_uploaded = 0;
                        }
                        if ($is_photo_uploaded == 'yes' || $is_photo_uploaded == 'true' || $is_photo_uploaded== '1' ) {
                            $member->is_photo_uploaded = 1;
                        }
                    }

                    if ($request->has('reset_udid')){
                        $reset_udid = $request->input('reset_udid');

                        if ($reset_udid == 'yes' || $reset_udid == 'true' || $reset_udid== '1' ) {
                            $member->udid = null;
                        }
                    }
                    $member->save();

                    $user->password = Hash::make('123');
                    $user->mobile = $mobile;
                    $user->email_confirmation = true;
                    $user->save();

                    $user_role = new UserRoles;
                    $user_role->role_id = 3; // Manually add
                    $user_role->user_id = $user->id;
                    $user_role->save();

                    $member_tier = MemberTiers::where('member_id', $member->id)->first();
                    if (is_null($member_tier)) {

                        $tier = Tier::where('bepoz_group_id', $member_group)
                            ->first();

                        if (is_null($tier)) {
                            return response()->json(['status' => 'error', 'message' => 'Bepoz Group ID does not exist in our cloud system, please contact our service center'], Response::HTTP_BAD_REQUEST);
                        } else {
                            $tier->is_active = true;
                        }

                        $tier->save();


                        $member_tier = new MemberTiers();
                        $member_tier->tier_id = $tier->id;
                    }

                    $member_tier->member_id = $member->id;
                    $member_tier->date_expiry = Carbon::parse($expiry);
                    $member_tier->save();
                    
                    
                    $date_expiry = Carbon::parse($user->member->member_tier->date_expiry);
                    $date_now = Carbon::now();

                    $length = $date_now->diffInDays($date_expiry, 0);

                    if ( $length < 0 ) {
                        //Log::info($length);
                        $user->member->status = 'inactive';            
                        $user->member->save();
                        $user->status = 'inactive';            
                        $user->save();

                    } else {
                        $user->member->status = 'active';            
                        $user->member->save();
                        $user->status = 'active';            
                        $user->save();

                    }

                    DB::commit();

                } else {
                    // Log::info("update");

                    DB::beginTransaction();
                    //update data when user exist
                    $member->first_name = $firstname;
                    $member->last_name = $lastname;
                    $member->bepoz_account_id = $selectedID;
                    $member->bepoz_account_card_number = $member_number;
                    $member->bepoz_account_number = $member_number;
                    $member->bepoz_existing_account = true;
                    $member->bepoz_account_status = "active";
                    $member->member_group = $member_group;
                    $member->barcode = $barcode;

                    if ($request->has('udid')) {
                        if ($request->input('udid') == 'NULL' || is_null($request->input('udid'))) {
                            $member->udid = null;
                        }
                    }

                    if (!is_null($request->input('sid'))) {
                        $member->sid = $request->input('sid');
                    }
                    
                    if ( $request->has('is_photo_uploaded') ) {
                        $is_photo_uploaded = $request->input('is_photo_uploaded');
                        if ($is_photo_uploaded == 'no' || $is_photo_uploaded == 'false' || $is_photo_uploaded== '0' ) {
                            $member->is_photo_uploaded = 0;
                        }
                        if ($is_photo_uploaded == 'yes' || $is_photo_uploaded == 'true' || $is_photo_uploaded== '1' ) {
                            $member->is_photo_uploaded = 1;
                        }
                    }

                    if ($request->has('reset_udid')){
                        $reset_udid = $request->input('reset_udid');

                        if ($reset_udid == 'yes' || $reset_udid == 'true' || $reset_udid== '1' ) {
                            $member->udid = null;
                        }
                    }
                    $member->save();

                    $user = $member->user;
                    $user->email = $email;
                    $user->mobile = $mobile;
                    $user->email_confirmation = true;
                    $user->save();

                    $user_role = UserRoles::where('user_id', $user->id)->get();

                    if ($user_role->isEmpty()) {
                        $user_role = new UserRoles;
                        $user_role->role_id = 3; // Manually add
                        $user_role->user_id = $user->id;
                        $user_role->save();
                    }

                    $member_tier = MemberTiers::where('member_id', $member->id)->first();
                    $member_tier_id_old = 0;
                    if (is_null($member_tier)) {

                        $tier = Tier::where('bepoz_group_id', $member_group)
                            ->first();

                        if (is_null($tier)) {
                            return response()->json(['status' => 'error', 'message' => 'Bepoz Group ID does not exist in our cloud system, please contact our service center'], Response::HTTP_BAD_REQUEST);
                        } else {
                            $tier->is_active = true;
                        }

                        $tier->save();

                        $member_tier = new MemberTiers();
                        $member_tier->tier_id = $tier->id;
                        $member_tier->member_id = $member->id;

                        if (intval($member_tier->tier_id) === 1) {
                            $member->is_photo_uploaded = 0;
                        }

                        if (intval($member_tier->tier_id) === 5) {
                            $member->is_photo_uploaded = 0;
                        }

                        $member->save();
                    } else {
                        $tier = Tier::where('bepoz_group_id', $member_group)
                            ->first();

                        if (is_null($tier)) {
                            return response()->json(['status' => 'error', 'message' => 'Bepoz Group ID does not exist in our cloud system, please contact our service center'], Response::HTTP_BAD_REQUEST);
                        } else {
                            $tier->is_active = true;
                        }

                        $tier->save();

                        $member_tier_id_old = intval($member_tier->tier_id);
                        $member_tier->tier_id = $tier->id;
                    }

                    $member_tier->date_expiry = Carbon::parse($expiry);
                    $member_tier->save();
                    
                    $date_expiry = Carbon::parse($user->member->member_tier->date_expiry);
                    $date_now = Carbon::now();

                    $length = $date_now->diffInDays($date_expiry, 0);

                    if ( $length < 0 ) {
                        //Log::info($length);
                        $user->member->status = 'inactive';            
                        $user->member->save();
                        $user->status = 'inactive';            
                        $user->save();

                    } else {
                        $user->member->status = 'active';            
                        $user->member->save();
                        $user->status = 'active';            
                        $user->save();

                    }
                    

                    DB::commit();

                }

                // Call SSO
                if ($this->checkSSOAccount($email) == "no_account"){
                    $this->createSSOAccount($email, $mobile);
                } else if ($this->checkSSOAccount($email) == "ok"){
                    $this->updateSSOAccount($member);
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Please make sure that your SSO is online'], Response::HTTP_BAD_REQUEST);
                }

            } else {
                return response()->json(['status' => 'error', 'message' => 'Member number missing'], Response::HTTP_BAD_REQUEST);
            }

            return response()->json(['status' => 'ok', 'message' => 'Data received, ' . $message, 'request' => $request->all()]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }


    }
    
    /**
     * trigger cron for 1 member custom field
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function triggerMemberCustomField(Request $request)
    {
        
    }
    
    /**
     * trigger cron for all member custom field
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function triggerAllMemberCustomField(Request $request)
    {
        $members = Member::whereNotNull('bepoz_account_id')
            ->chunk(100, function ($members) {

                $system_send_all = array();
                $now = Carbon::now(config('app.timezone'));

                foreach ($members as $member) {

                    $data = array(
                        "member_id" => $member->id
                    );
            
                    $job = new BepozJob();
                    $job->queue = "update-custom-field";
                    $job->payload = \GuzzleHttp\json_encode($data);
                    $job->available_at = time();
                    $job->created_at = time();
                    $job->save();
            
                }

            }
        );

        return response()->json(['status' => 'ok']);
    }

    /**
     * Suspend member
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function memberSuspend(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'member_id' => 'required',
            ]);

            $member_id = $request->input('member_id');

            // Log::warning('Suspend Successful '. $member_id);

            // DB::beginTransaction();

            $member = Member::with('user', 'tiers')->find($member_id);
            $user = $member->user;
            $cloned = $user->replicate();
            // Log::warning($member);

            $inactiverandom = $this->generateRandomString(6);

            $end = "@";
            $e = explode($end, $user->email);
            $emailModified = $e[0]."+inactive".$inactiverandom.$end.$e[1];
            $user->email = $emailModified;
            $user->status = "inactive";
            $user->mobile = "XXXXXXXXXX";
            $user->save();

            $member->status = "inactive";
            $member->bepoz_account_status = "Inactive";
            $member->bepoz_account_id = "";
            $member->bepoz_account_number = "";
            $member->bepoz_account_card_number = "";
            $member->save();

            // log
            $ml = new MemberLog();
            $ml->before = "user active";
            $ml->member_id = $member->id;
            $ml->message = $request->input('decrypted_token')->staff->first_name . '(' . $request->input('decrypted_token')->staff->id . ') has suspended this member.';
            $ml->after = "user inactive";
            $ml->save();

            // $SSOAccountGUID = $this->SSOAccountGUID($cloned->email);

            // if ($SSOAccountGUID == $user->member->login_token) {
            //     // UPDATE SSO
            //     $this->updateSSO($user);
            // }

            // DB::commit();

            return response()->json(['status' => 'ok', 'message' => 'Suspending member is successful.']);

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
    
    /**
     * SSO Account GUID
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function SSOAccountGUID($email)
    {
        $your_order_api = Setting::where('key', 'your_order_api')->first()->value;
        $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

        $datacheck = array(
            'email' => $email,
            "license_key" => $your_order_key
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $your_order_api . "/checkSubAccount");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datacheck);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_ENCODING, "gzip");
        $content_check = curl_exec($ch);
        $httpCode_check = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // Log::info("542");
        // Log::info($content_check);
        // Log::info($httpCode_check);

        $payload_check = \GuzzleHttp\json_decode($content_check);
        if ($payload_check->status == "ok") {
            return $payload_check->guid;
        }

        if ($payload_check->status == "error") {
            return false;
        }

        return false;
    }

    /**
     * update SSO
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function updateSSO($user)
    {
        try {
            $your_order_api = Setting::where('key', 'your_order_api')->first()->value;
            $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

            $datasend = array(
                'given_name' => $user->member->first_name,
                'family_name' => $user->member->last_name,
                'phone_number' => $user->mobile,
                'email' => $user->email,
                'bepoz_id' => $user->member->bepoz_account_id,
                'bepoz_account_number' => $user->member->bepoz_account_number,
                'bepoz_account_card_number' => $user->member->bepoz_account_card_number,
                'guid' => $user->member->login_token,
                'license_key' => $your_order_key
            );

            // $payload = json_encode( $datasend );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $your_order_api . "/updateAccount");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            curl_setopt($ch, CURLOPT_ENCODING, "gzip");
            $content = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            // Log::warning("AuthenticationController updateSSO");
            // Log::warning($your_order_api);
            // Log::warning($payload);
            // Log::warning($content);
            // Log::warning($httpCode);

            if ($content) {
                $payload = \GuzzleHttp\json_decode($content);
                if ($payload->status == "error" && $payload->error == "user_not_found") {
                    // return response()->json(['status' => 'error', 'message' => 'Email not found'], Response::HTTP_BAD_REQUEST);
                    return false;
                }

                return true;
            } else {
                // return response()->json(['status' => 'error', 'message' => 'Unable to connect'], Response::HTTP_BAD_REQUEST);
                return false;
            }

            return false;

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

}

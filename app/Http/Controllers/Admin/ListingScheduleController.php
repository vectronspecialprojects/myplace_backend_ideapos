<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Listing;
use App\ListingSchedule;
use App\ListingScheduleTag;
use App\ListingScheduleType;
use App\ListingScheduleVenue;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ListingScheduleController extends Controller
{
    /**
     * Return all Listing schedules
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index()
    {
        try {
            $schedules = ListingSchedule::all();

            return response()->json(['status' => 'ok', 'data' => $schedules]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Create a listing type
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function store(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'date_start' => 'required|date',
                'date_end' => 'required|date',
                'date_timezone' => 'required|timezone',
                'listing_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $listing = Listing::find($request->input('listing_id'));
            if(is_null($listing)) {
                return response()->json(['status' => 'error', 'message' => 'listing_not_found'], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();

            $schedule = new ListingSchedule;
            $schedule->listing_id = $listing->id;
            $schedule->date_start = new Carbon($request->input('date_start'), $request->input('date_timezone'));
            $schedule->date_start->setTimezone(config('app.timezone'));

            if ($request->has('date_end')) {
                $schedule->date_end = new Carbon($request->input('date_end'), $request->input('date_timezone'));
                $schedule->date_end->setTimezone(config('app.timezone'));
            }
            
            if ($request->has('status')) {
                $schedule->status = $request->input('status');
            }

            if ($request->has('never_expired')) {
                $schedule->never_expired = $request->input('never_expired');
            }

            $schedule->save();

            if($request->has('venues')) {
                $venues = \GuzzleHttp\json_decode($request->input('venues'));

                foreach ($venues as $venue) {
                    if((bool)$venue->checked) {
                        $obj = new ListingScheduleVenue();
                        $obj->listing_schedule_id = $schedule->id;
                        $obj->venue_id = $venue->id;
                        $obj->save();
                    }
                }
            }

            if($request->has('tags')) {
                $tags = \GuzzleHttp\json_decode($request->input('tags'));

                foreach ($tags as $tag) {
                    if((bool)$tag->checked) {
                        $obj = new ListingScheduleTag();
                        $obj->listing_schedule_id = $schedule->id;
                        $obj->tag_id = $tag->id;
                        $obj->save();
                    }
                }
            }

            if($request->has('types')) {
                $types = \GuzzleHttp\json_decode($request->input('types'));

                foreach ($types as $type) {
                    if((bool)$type->checked) {
                        $obj = new ListingScheduleType();
                        $obj->listing_schedule_id = $schedule->id;
                        $obj->listing_type_id = $type->id;
                        $obj->save();
                    }
                }
            }



            DB::commit();

            return response()->json(['status' => 'ok', 'message' => 'Creating listing type is successful.']);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Delete a specific listing type
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function destroy($id)
    {
        try {
            $schedule = ListingSchedule::find($id);

            if (is_null($schedule)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                ListingSchedule::destroy($id);
                return response()->json(['status' => 'ok', 'message' => 'Deleting schedule is successful.']);
            }

        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update a listing type
     * >> for Admin
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(Request $request, $id)
    {
        try {

            $validator = Validator::make($request->all(), [
                'date_start' => 'required|date',
                'date_end' => 'required|date',
                'date_timezone' => 'required|timezone'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $schedule = ListingSchedule::find($id);

            if (is_null($schedule)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            }

            $schedule->date_start = new Carbon($request->input('date_start'), $request->input('date_timezone'));
            $schedule->date_start->setTimezone(config('app.timezone'));
            
            if ($request->has('date_end')) {
                $schedule->date_end = new Carbon($request->input('date_end'), $request->input('date_timezone'));
                $schedule->date_end->setTimezone(config('app.timezone'));
            }

            if ($request->has('status')) {
                $schedule->status = $request->input('status');
            }

            if ($request->has('never_expired')) {
                $schedule->never_expired = $request->input('never_expired');
            }
            
            $schedule->save();

            return response()->json(['status' => 'ok', 'message' => 'Updating schedule is successful.']);

        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return information for a specific listing type
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show($id)
    {
        try {
            $schedule = ListingSchedule::find($id);

            if (is_null($schedule)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            }

            $schedule->listing;

            return response()->json(['status' => 'ok', 'data' => $schedule]);

        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return information for editing a specific listing type
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function edit($id)
    {
        try {
            $schedule = ListingSchedule::find($id);

            if (is_null($schedule)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            }

            return response()->json(['status' => 'ok', 'data' => $schedule]);

        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}

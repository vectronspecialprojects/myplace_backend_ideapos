<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\SystemLog;
use App\Listing;
use App\ListingProduct;
use App\ListingSchedule;
use App\ListingScheduleTag;
use App\ListingScheduleType;
use App\ListingScheduleVenue;
use App\ListingType;
use App\ListingPivotTag;
use App\ListingTag;
use App\Venue;
use App\VenuePivotTag;
use App\VenueTag;
use Approached\LaravelImageOptimizer\ImageOptimizer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class VenueController extends Controller
{
    /**
     * Return all venues
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index()
    {
        try {
            $venues = Venue::with('tier')->get();

            return response()->json(['status' => 'ok', 'data' => $venues]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return all venues (pagination)
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function paginateVenue(Request $request, Venue $venue)
    {
        try {

            $obj = $venue->newQuery();

            if ($request->has('keyword')) {
                $obj->where(function ($query) use ($request) {

                    if (strpos($request->input('keyword'), ' ') !== false) {
                        $array = explode(" ", $request->input('keyword'));
                        $keyword = implode("%", $array);
                        $query->where('name', 'like', '%' . $keyword . '%');
                    } else {
                        $query->where('name', 'like', '%' . $request->input('keyword') . '%');
                    }

                });
            }

            if ($request->has('orderBy') && $request->has('sortBy')) {
                $obj->orderBy($request->input('orderBy'), $request->input('sortBy'));
            }

            if ($request->has('showAll')) {
                if (intval($request->input('showAll')) === 0) {
                    $obj->where('venues.active', true);
                }
            }

            $paginate_number = 10;
            if ($request->has('paginate_number')) {
                $paginate_number = $request->input('paginate_number');
            }

            $listings = $obj->paginate($paginate_number);

            return response()->json(['status' => 'ok', 'data' => $listings]);

        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Search through all venue
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function search(Request $request, Venue $venue)
    {
        try {
            $venue = $venue->newQuery();

            if ($request->has('keyword')) {
                $venue->where(function ($query) use ($request) {

                    if (strpos($request->input('keyword'), ' ') !== false) {
                        $array = explode(" ", $request->input('keyword'));
                        $keyword = implode("%", $array);
                        $query->where('name', 'like', '%' . $keyword . '%');
                    } else {
                        $query->where('name', 'like', '%' . $request->input('keyword') . '%');
                    }

                });
            }

            $venue = $venue->get();

            return response()->json(['status' => 'ok', 'data' => $venue, 'request' => $request->all()]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Create a venue
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function store(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'name' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $venue = new Venue;

            $venue->name = $request->input('name');
            $venue->active = $request->input('active');
            $venue->gaming_system_point = $request->input('gaming_system_point') == true ? 1 : 0;
            $venue->bepoz_venue_id = $request->has('bepoz_venue_id') ? $request->input('bepoz_venue_id') : null;
            $venue->bepoz_api = $request->has('bepoz_api') ? $request->input('bepoz_api') : null;
            $venue->bepoz_mac_key = $request->has('bepoz_mac_key') ? $request->input('bepoz_mac_key') : null;
            $venue->bepoz_secondary_api = $request->has('bepoz_secondary_api') ? $request->input('bepoz_secondary_api') : null;
            $venue->bepoz_till_id = $request->has('bepoz_till_id') ? $request->input('bepoz_till_id') : null;
            $venue->bepoz_operator_id = $request->has('bepoz_operator_id') ? $request->input('bepoz_operator_id') : null;
            $venue->bepoz_training_mode = $request->has('bepoz_training_mode') ? $request->input('bepoz_training_mode') : null;
            $venue->bepoz_payment_name = $request->has('bepoz_payment_name') ? $request->input('bepoz_payment_name') : null;
            $venue->default_card_number_prefix = $request->has('default_card_number_prefix') ? $request->input('default_card_number_prefix') : null;
            $venue->default_card_track = $request->has('default_card_track') ? $request->input('default_card_track') : null;
            $venue->image = $request->has('image') ? $request->input('image') : null;
            $venue->social_links = $request->has('social_links') ? $request->input('social_links') : null;
            $venue->menu_links = $request->has('menu_links') ? $request->input('menu_links') : null;

            $venue->open_and_close_hours = $request->has('open_and_close_hours') ? $request->input('open_and_close_hours') : null;
            $venue->pickup_and_delivery_hours = $request->has('pickup_and_delivery_hours') ? $request->input('pickup_and_delivery_hours') : null;
            $venue->address = $request->has('address') ? $request->input('address') : null;
            $venue->color = $request->has('color') ? $request->input('color') : null;
            $venue->email = $request->has('email') ? $request->input('email') : null;
            $venue->telp = $request->has('telp') ? $request->input('telp') : null;
            $venue->fax = $request->has('fax') ? $request->input('fax') : null;
            $venue->hide_delivery_info = $request->input('hide_delivery_info');
            $venue->hide_opening_hours_info = $request->input('hide_opening_hours_info');
            $venue->android_link = $request->has('android_link') ? $request->input('android_link') : null;
            $venue->ios_link = $request->has('ios_link') ? $request->input('ios_link') : null;
            $venue->your_order_link = $request->has('your_order_link') ? $request->input('your_order_link') : null;
            $venue->your_order_integration = $request->input('your_order_integration') == true ? 1 : 0;
            $venue->your_order_api = $request->has('your_order_api') ? $request->input('your_order_api') : null;
            $venue->your_order_key = $request->has('your_order_key') ? $request->input('your_order_key') : null;
            $venue->odyssey_custom_field_id = $request->has('odyssey_custom_field_id') ? $request->input('odyssey_custom_field_id') : null;

            $venue->link1 = $request->has('link1') ? $request->input('link1') : null;
            $venue->link2 = $request->has('link2') ? $request->input('link2') : null;
            $venue->link3 = $request->has('link3') ? $request->input('link3') : null;
            
            $venue->location = $request->has('location') ? $request->input('location') : null;

            $venue->save();
            
            $venue->display_order = $venue->id;
            $venue->save();
            
            // Log::error($request->input('your_order_integration'));
            return response()->json(['status' => 'ok', 'message' => 'Creating venue is successful.']);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Delete a specific venue
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function destroy($id)
    {
        try {
            $venue = Venue::find($id);

            if (is_null($venue)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                Venue::destroy($id);

                return response()->json(['status' => 'ok', 'message' => 'Deleting venue is successful.']);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update a venue
     * >> for Admin
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(Request $request, $id)
    {
        try {

            $venue = Venue::find($id);

            if (is_null($venue)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            }

            $venue->active = $request->input('active');
            $venue->hide_delivery_info = $request->input('hide_delivery_info');
            $venue->hide_opening_hours_info = $request->input('hide_opening_hours_info');

            if ($request->has('gaming_system_point')) {
                $venue->gaming_system_point = $request->input('gaming_system_point') == true ? 1 : 0;
            }

            if ($request->has('gaming_system_point')) {
                $venue->gaming_system_point = $request->input('gaming_system_point') == true ? 1 : 0;
            }

            if ($request->has('bepoz_venue_id')) {
                $venue->bepoz_venue_id = $request->input('bepoz_venue_id');
            }

            if ($request->has('bepoz_api')) {
                $venue->bepoz_api = $request->input('bepoz_api');
            }

            if ($request->has('bepoz_mac_key')) {
                $venue->bepoz_mac_key = $request->input('bepoz_mac_key');
            }

            if ($request->has('bepoz_secondary_api')) {
                $venue->bepoz_secondary_api = $request->input('bepoz_secondary_api');
            }

            if ($request->has('bepoz_till_id')) {
                $venue->bepoz_till_id = $request->input('bepoz_till_id');
            }

            if ($request->has('bepoz_operator_id')) {
                $venue->bepoz_operator_id = $request->input('bepoz_operator_id');
            }

            if ($request->has('bepoz_training_mode')) {
                $venue->bepoz_training_mode = $request->input('bepoz_training_mode');
            }

            if ($request->has('bepoz_payment_name')) {
                $venue->bepoz_payment_name = $request->input('bepoz_payment_name');
            }

            if ($request->has('default_card_number_prefix')) {
                $venue->default_card_number_prefix = $request->input('default_card_number_prefix');
            }

            if ($request->has('default_card_track')) {
                $venue->default_card_track = $request->input('default_card_track');
            }

            if ($request->has('open_and_close_hours')) {
                $venue->open_and_close_hours = $request->input('open_and_close_hours');
            }

            if ($request->has('pickup_and_delivery_hours')) {
                $venue->pickup_and_delivery_hours = $request->input('pickup_and_delivery_hours');
            }

            if ($request->has('image')) {
                $venue->image = $request->input('image');
            }

            if ($request->has('social_links')) {
                $venue->social_links = $request->input('social_links');
            }

            if ($request->has('menu_links')) {
                $venue->menu_links = $request->input('menu_links');
            }

            if ($request->has('address')) {
                $venue->address = $request->input('address');
            }

            if ($request->has('color')) {
                $venue->color = $request->input('color');
            }

            if ($request->has('email')) {
                $venue->email = $request->input('email');
            }

            if ($request->has('telp')) {
                $venue->telp = $request->input('telp');
            }

            if ($request->has('fax')) {
                $venue->fax = $request->input('fax');
            }

            if ($request->has('name')) {
                $venue->name = $request->input('name');
            }

            if ($request->has('android_link')) {
                $venue->android_link = $request->input('android_link');
            }

            if ($request->has('ios_link')) {
                $venue->ios_link = $request->input('ios_link');
            }

            if ($request->has('your_order_link')) {
                $venue->your_order_link = $request->input('your_order_link');
            }

            if ($request->has('your_order_integration')) {
                $venue->your_order_integration = $request->input('your_order_integration');
            }

            if ($request->has('your_order_api')) {
                $venue->your_order_api = $request->input('your_order_api');
            }

            if ($request->has('your_order_key')) {
                $venue->your_order_key = $request->input('your_order_key');
            }

            if ($request->has('link1')) {
                $venue->link1 = $request->input('link1');
            }

            if ($request->has('link2')) {
                $venue->link2 = $request->input('link2');
            }

            if ($request->has('link3')) {
                $venue->link3 = $request->input('link3');
            }

            if ($request->has('odyssey_custom_field_id')) {
                $venue->odyssey_custom_field_id = $request->input('odyssey_custom_field_id');
            }
            
            if ($request->has('allow_signup_for_existing_customer_only')) {
                $venue->allow_signup_for_existing_customer_only = $request->input('allow_signup_for_existing_customer_only');
            }
            
            if ($request->has('latitude')) {
                $venue->latitude = $request->input('latitude');
            }
            
            if ($request->has('longitude')) {
                $venue->longitude = $request->input('longitude');
            }
            
            if ($request->has('your_order_location')) {
                $venue->your_order_location = $request->input('your_order_location');
            }
            
            if ($request->has('display_order')) {
                if (intval($request->input('display_order')) < 1) {
                    $venue->display_order = $venue->id;
                }
            } else {
                if (intval($venue->display_order) < 1) {
                    $venue->display_order = $venue->id;
                }
            }

            if ($request->has('display_order')) {
                if (intval($request->input('display_order')) < 1) {
                    $venue->display_order = $venue->id;
                }
            } else {
                if (intval($venue->display_order) < 1) {
                    $venue->display_order = $venue->id;
                }
            }

            $venue->save();

            return response()->json(['status' => 'ok', 'message' => 'Updating venue is successful.']);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return information for specific venue
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show($id)
    {
        try {
            $venue = Venue::find($id);

            if (is_null($venue)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                return response()->json(['status' => 'ok', 'data' => $venue]);
            }
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Change status
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function changeStatus(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'active' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            // DB::beginTransaction();

            $venue = Venue::find($request->input('id'));
            $venue->active = $request->input('active');
            $venue->save();

            // Log::info($request->input('active'));

            // IF disabled, need disable things that related
            if (!$request->input('active')) {
                    
                $venuePivotTags = VenuePivotTag::where('venue_id', $venue->id)->get();
                foreach ($venuePivotTags as $venuePivotTag) {
                    VenuePivotTag::destroy($venuePivotTag->id);
                }

                $listings = Listing::where('venue_id', $venue->id)->get();
                foreach ($listings as $listing) {
                    Listing::destroy($listing->id);
                }
            }

            // DB::commit();
            return response()->json(['status' => 'ok', 'message' => 'Changing venue status is successful.', 'data' => ['id' => $venue->id], 'requests' => $request->all()]);

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'requests' => $request->all()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Upload venue image to s3
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function uploadVenueImage(Request $request, ImageOptimizer $imageOptimizer)
    {
        try {
            $validator = Validator::make($request->all(), [
                'image' => 'required|file',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            if ($request->hasFile('image')) {

                if (config('bucket.s3')){
                    $timestamp = Carbon::now(config('app.timezone'))->timestamp;
                    $picture = $request->file('image');
                    $mime = explode('/', $picture->getMimeType());
                    $type = end($mime);

                    // optimize size
                    $imageOptimizer->optimizeUploadedImageFile($picture);

                    // upload to s3
                    Storage::disk('s3')->put('settings/venueImage/image_' . $timestamp . '.' . $type, file_get_contents($picture), 'public');
                    $url = "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/settings/venueImage/image_" . $timestamp . '.' . $type . "?" . time();

                    return response()->json(['status' => 'ok', 'message' => 'Upload successful', 'data' => $url]);

                } else {
                    $log = new SystemLog();
                    $log->type = 'setting-error';
                    $log->humanized_message = 'Setting of AWS S3 bucket not found.';
                    $log->payload = 'VenueController.php/uploadVenueImage';
                    $log->message = 'Setting of S3 bucket not found.';
                    $log->source = 'VenueController.php';
                    $log->save();

                    return response()->json(['status' => 'error', 'message' => 'Please contact Administrator.'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                return response()->json(['status' => 'error', 'message' => 'no image file'], Response::HTTP_BAD_REQUEST);

            }
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'request' => $request->all()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return all venues with display order
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function allVenueDisplayOrder()
    {
        try {
            $venues = Venue::select('id','name', 'display_order')
                ->orderBy('display_order')
                ->get();

            return response()->json(['status' => 'ok', 'data' => $venues]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Set all venue display order
     *
     * @param Request $request
     * @return mixed
     */
    protected function displayOrder(Request $request)
    {
        try {
            if ($request->has('display_order')) {
                
                //Log::info($request->input('display_order'));
                $display_orders = \GuzzleHttp\json_decode($request->input('display_order'));

                $ctr = 1;
                foreach ($display_orders as $display_order) {
                    //Log::info($ctr." ".$display_order->id." ".$display_order->name);
                    $listing = Venue::find($display_order->id);
                    $listing->display_order = $ctr;
                    $listing->save();

                    $ctr++;
                }

            } else {
                // Log::info('no display_order');
            }

            if (is_null($request)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {
                return response()->json(['status' => 'ok', 'data' => $request]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

}

<?php

namespace App\Http\Controllers\Admin;

use App\Answer;
use App\Http\Controllers\Controller;
use App\MemberAnswer;
use App\Question;
use App\QuestionAnswer;
use Illuminate\Http\Request;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class QuestionController extends Controller
{
    /**
     * Return all questions
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index()
    {
        try {
            $questions = Question::with('answers')->get();

            return response()->json(['status' => 'ok', 'data' => $questions]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Create a question
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function store(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'question' => 'required',
                'multiple_choice' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $question = new Question;
            $question->question = $request->input('question');
            $question->multiple_choice = (bool)$request->input('multiple_choice');
            $question->save();

            if ($request->has('answers')) {

                $answers = \GuzzleHttp\json_decode($request->input('answers'));
                foreach ($answers as $answer) {

                    if ($answer->id == "new") {
                        $obj = new Answer();
                        $obj->answer = $answer->answer;
                        $obj->save();
                    } else {
                        $obj = Answer::find($answer->id);
                    }

                    $question->answers()->save($obj);

                }

            }

            $question->answers;

            return response()->json(['status' => 'ok', 'message' => 'Creating question is successful.', 'data' => $question]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Delete a specific question
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function destroy(Request $request, $id)
    {
        try {
            $question = Question::find($id);

            if (is_null($question)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                $ma = MemberAnswer::where('question_id', $id)->first();
                if (is_null($ma)) {
                    Question::destroy($id);

                    return response()->json(['status' => 'ok', 'message' => 'Deleting question is successful.']);
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Not safe. Some objects used this value'], Response::HTTP_BAD_REQUEST);

                }
            }

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update a question
     * >> for Admin
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(Request $request, $id)
    {
        try {

            $validator = Validator::make($request->all(), [
                'question' => 'required',
                'multiple_choice' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $question = Question::find($id);

            if (is_null($question)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            }

            $question->question = $request->input('question');
            $question->multiple_choice = (bool)$request->input('multiple_choice');
            $question->save();

            // reload answers
            if ($request->has('answers')) {
                $answers = \GuzzleHttp\json_decode($request->input('answers'));
                $ids = [];
                if (!empty($answers)) {

                    foreach ($answers as $answer) {

                        if ($answer->id == "new") {
                            $obj = new Answer();
                            $obj->answer = $answer->answer;
                            $obj->save();

                            $ids[] = $obj->id;

                            $question->answers()->save($obj);

                        } else {
                            $ids[] = $answer->id;

                            $qa = QuestionAnswer::where('question_id', $question->id)->where('answer_id', $answer->id)->first();

                            if (is_null($qa)) {
                                $obj = Answer::find($answer->id);
                                $question->answers()->save($obj);
                            }
                        }


                    }

                } else {
                    QuestionAnswer::where('question_id', $question->id)->delete();
                }

                if (!empty($ids)) {
                    $qas = QuestionAnswer::whereNotIn('answer_id', $ids)->where('question_id', $question->id)->get();
                    foreach ($qas as $qa) {
                        $qa->delete();
                    }
                }
            }

            if ($request->has('sequence')) {
                $answers = \GuzzleHttp\json_decode($request->input('sequence'));

                $id = 1;
                foreach ($answers as $answer) {
                    if ($answer->id == "new"){
                        $answer_id = Answer::select('id')->where('answer', $answer->answer)->first()->id;
                    } else {
                        $answer_id = $answer->id;
                    }
                    
                    $qa = QuestionAnswer::where('question_id', $question->id)->where('answer_id', $answer_id)->first();
                    
                    if (is_null($qa)) {
                        $qa->answer_order = $id;
                        $qa->save();

                        $id++;
                    }

                }
            }


            return response()->json(['status' => 'ok', 'message' => 'Updating question is successful.', 'data' => $question]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return information for specific question
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show(Request $request, $id)
    {
        try {
            $question = Question::with('answers')->find($id);

            if (is_null($question)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                return response()->json(['status' => 'ok', 'data' => $question]);
            }
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Check Question status
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function checkQuestion(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'question_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            // check pivot
            $ma = MemberAnswer::where('question_id', $request->input('question_id'))
                ->first();

            if (is_null($ma)) {
                return response()->json(['status' => 'ok', 'message' => 'Checking completed.', 'data' => 'safe']);
            } else {
                return response()->json(['status' => 'ok', 'message' => 'Checking completed.', 'data' => 'unsafe']);
            }

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Listing;
use App\ListingProduct;
use App\ListingSchedule;
use App\ListingScheduleTag;
use App\ListingScheduleType;
use App\ListingScheduleVenue;
use App\ListingType;
use App\ListingPivotTag;
use App\ListingTag;
use App\MemberVouchers;
use App\Order;
use App\OrderDetail;
use App\Product;
use App\ProductType;
use App\Setting;
use App\SystemLog;
use App\Venue;
use Approached\LaravelImageOptimizer\ImageOptimizer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use ImageOptimizer\Exception\Exception;
use Milon\Barcode\DNS1D;

class ListingController extends Controller
{
    /**
     * Return all listings
     *
     * @param Request $request
     * @return mixed
     */
    protected function index(Request $request)
    {
        try {
            if ($request->has('type_id')) {
                $type = ListingType::find($request->input('type_id'));

                if (is_null($type)) {
                    return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
                }

                // $listings = Listing::with('type', 'schedules.types.type', 'reservations', 'schedules.venues.venue', 'schedules.tags.tag')->where('listing_type_id', $type->id)->get();
                $listings = Listing::with('type', 'schedules.types.type', 'schedules.venues.venue', 'schedules.tags.tag')->where('listing_type_id', $type->id)->get();
            } else {
                // $listings = Listing::with('type', 'schedules.types.type', 'reservations', 'schedules.venues.venue', 'schedules.tags.tag')->get();
                $listings = Listing::with('type', 'schedules.types.type', 'schedules.venues.venue', 'schedules.tags.tag')->get();
            }

            if (is_null($listings)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {
                return response()->json(['status' => 'ok', 'data' => $listings]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return all active listings
     *
     * @param Request $request
     * @return mixed
     */
    protected function activeListing(Request $request)
    {
        try {
            if ($request->has('type_id')) {
                $type = ListingType::find($request->input('type_id'));

                if (is_null($type)) {
                    return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
                }

                $listings = Listing::with('type')
                ->where('listing_type_id', $type->id)->where('status', 'active')
                ->orderBy('display_order' ,'asc')->get();
                
            } else {
                $listings = Listing::with('type')->where('status', 'active')->orderBy('display_order' ,'asc')->get();
            }

            if (is_null($listings)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {
                return response()->json(['status' => 'ok', 'data' => $listings, 'type_id' => $request->has('type_id')]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Set all listings display order
     *
     * @param Request $request
     * @return mixed
     */
    protected function displayOrder(Request $request)
    {
        try {
            if ($request->has('display_order')) {
                
                //Log::info($request->input('display_order'));
                $display_orders = \GuzzleHttp\json_decode($request->input('display_order'));

                $ctr = 1;
                foreach ($display_orders as $display_order) {
                    //Log::info($ctr." ".$display_order->id." ".$display_order->name);
                    $listing = Listing::find($display_order->id);
                    $listing->display_order = $ctr;
                    $listing->save();

                    $ctr++;
                }

            } else {
                // Log::info('no display_order');
            }

            if (is_null($request)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {
                return response()->json(['status' => 'ok', 'data' => $request]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return all listings
     *
     * @param Request $request
     * @return mixed
     */
    protected function paginateListing(Request $request, Listing $listing)
    {
        try {

            // Log::warning($request);

            $obj = $listing->newQuery();


            if ($request->has('type_id')) {
                $type = ListingType::find($request->input('type_id'));

                if (is_null($type)) {
                    return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
                }

                $obj->where('listing_type_id', $type->id);
            }

            if ($request->has('keyword')) {
                $obj->where(function ($query) use ($request) {

                    if (strpos($request->input('keyword'), ' ') !== false) {
                        $array = explode(" ", $request->input('keyword'));
                        $keyword = implode("%", $array);
                        $query->where('heading', 'like', '%' . $keyword . '%');
                        $query->orWhere('name', 'like', '%' . $keyword . '%');
                    } else {
                        $query->where('heading', 'like', '%' . $request->input('keyword') . '%');
                        $query->orWhere('name', 'like', '%' . $request->input('keyword') . '%');
                    }

                });
            }

            // if ($request->has('expired')) {
            //     $type = ListingType::find($request->input('type_id'));
            //     if (!$type->key->option_regular) {
            //         if (intval($request->input('expired')) === 0) {
            //             $obj->where(function ($obj1) {
            //                 $obj1->where(function ($query) {
            //                     $query->whereDate('datetime_end', '>=', Carbon::now(config('app.timezone'))->toDateString());
            //                 });
            //                 $obj1->orWhere('datetime_start', null);
            //             });
            //         }
            //     }

            // }

            if ($request->has('venue_id')) {
                if (intval($request->input('venue_id')) !== 0) {
                    $obj->where('listings.venue_id', $request->input('venue_id'));
                }
            }

            if ($request->has('orderBy') && $request->has('sortBy')) {
                $obj->orderBy($request->input('orderBy'), $request->input('sortBy'));
            }

            if ($request->has('showAll')) {
                if (intval($request->input('showAll')) === 0) {
                    $obj->where('listings.status', 'active');
                }
            }

            $paginate_number = 10;
            if ($request->has('paginate_number')) {
                $paginate_number = $request->input('paginate_number');
            }

            $listings = $obj->with('type', 'venue', 'products.product')
                ->withCount(['order_details' => function ($query) {
                    $query->where('status', 'successful');
                }])
                ->withCount(['claimed_promotions' => function ($query) {
                    $query->where('status', 'successful');
                }])
                ->paginate($paginate_number);
            
            foreach ($listings as $listing) {
                //Log::info($listing->listing_type_id);
                if ( $listing->listing_type_id == "1" ){
                    $sum = OrderDetail::
                            //->where('status', 'successful')->where('status', 'successful')->sum('qty');
                            join('orders', 'order_details.order_id', '=', 'orders.id')
                            ->where('order_details.listing_id', $listing->id)
                            ->where('order_details.product_type_id', '2')
                            ->where('orders.payment_status', 'successful')->sum('qty');
                    $listing->order_details_count = is_null($sum) ? 0 : $sum;
                }
            }
            
            // Log::warning($listings);
            // Log::warning($listings[0]);
            // Log::warning($listings[0]->per_page);

            if ( !is_null($listings[0]) ) {
                $yes_per_page = $listings[0]->per_page;
            } else {
                $yes_per_page = $paginate_number;
            }

            return response()->json(['status' => 'ok', 'data' => $listings, 'yes_per_page' => $yes_per_page]);

        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return information for specific listing
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show($id)
    {
        try {
            /*
            $listing = Listing::with('type', 'products.product', 
            'schedules.types.type', 'schedules.venues.venue', 'schedules.tags.tag', 
            'enquiries.member', 'enquiries.listing',
            'order_details.member_vouchers', 'order_details.order.member.user')
                ->with(['order_details' => function ($query) {
                    $query->where('status', 'successful');
                }])
                ->find($id);
                */

            //SUM FROM ORDER WITH SUCCESSFUL PAYMENT
            $listing = Listing::with('type', 'products.product', 'listing_pivot_tags.listing_tags',
            'schedules.types.type', 'schedules.venues.venue', 'schedules.tags.tag', 
            'enquiries.member', 'enquiries.listing', 
            'order_details.member_vouchers', 'order_details.order.member.user')
                ->with(['order_details' => function ($query) {
                    $query->whereHas('order', function($q) {
                        $q->where('payment_status', 'successful');
                    });
                }])
                ->find($id);


            //$sql = $listing->toSql();
            //Log::info($sql);

            if (is_null($listing)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                $listing->desc_long = $listing->desc_long = str_replace("<br>", "\r\n", $listing->desc_long);

                $total_qty = 0;
                $total_gross = 0;
                $total_net = 0;
                $total_order = 0;
                $total_order_detail = 0;

                /*
                foreach ($listing->order_details as $row) {
                    //add quantity sum for each order
                    $total_qty += $row->qty;
                    $total_gross += $row->subtotal_unit_price;
                    $total_net += $row->paid_subtotal_unit_price;
                    $total_order_detail += 1;

                }
                */
                
                $sumQty = OrderDetail::
                        //->where('status', 'successful')->where('status', 'successful')->sum('qty');
                        join('orders', 'order_details.order_id', '=', 'orders.id')
                        ->where('order_details.listing_id', $listing->id)
                        ->where('order_details.product_type_id', '2')
                        ->where('orders.payment_status', 'successful');
                $listing->total_qty = is_null($sumQty->sum('qty')) ? 0 : $sumQty->sum('qty');

                //$listing->sql = $sql ;
                $listing->total_order = $total_qty ;
                $listing->total_order_detail = $total_order_detail ;
                //$listing->total_qty = $total_qty ;

                $sum = OrderDetail::
                        //->where('status', 'successful')->where('status', 'successful')->sum('qty');
                        join('orders', 'order_details.order_id', '=', 'orders.id')
                        ->where('order_details.listing_id', $listing->id)
                        //->where('order_details.product_type_id', '2')
                        ->where('orders.payment_status', 'successful');

                $sumOrder = Order::where('listing_id', $listing->id)
                        //->where('order_details.product_type_id', '2')
                        ->where('payment_status', '<>', 'expired');
 

                //$listing->total_gross = is_null($sum->sum('subtotal_unit_price')) ? 0 : $sum->sum('subtotal_unit_price');
                $listing->total_gst = is_null($sumOrder->sum('gst')) ? 0 : $sumOrder->sum('gst');
                $listing->total_gross = is_null($sumOrder->sum('total_price')) ? 0 : $sumOrder->sum('total_price');
                $listing->total_net = is_null($sumOrder->sum('paid_total_price_before_gst')) ? 0 : $sumOrder->sum('paid_total_price_before_gst');
                $total_discount = $listing->total_gross - $listing->total_net;
                //$listing->total_discount = $total_discount - ($total_discount/11) ;
                $listing->total_discount = $total_discount;
                //$listing->total_after_gst = is_null($sumOrder->sum('paid_total_price')) ? 0 : $sumOrder->sum('paid_total_price');

                return response()->json(['status' => 'ok', 'data' => $listing]);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Duplicate specific listing
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function duplicateListing(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'listing_id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $obj = Listing::with('type', 'products.product', 'schedules.types.type', 'schedules.venues.venue', 'schedules.tags.tag', 'enquiries.member', 'enquiries.listing')
                ->find($request->input('listing_id'));
            if (is_null($obj)) {
                return response()->json(['status' => 'error', 'message' => 'listing_not_found'], Response::HTTP_BAD_REQUEST);
            }


            DB::beginTransaction();
            $listing = new Listing();
            $listing->listing_type_id = $obj->listing_type_id;
            $listing->name = $obj->name . ' - copy';
            $listing->heading = $obj->heading . ' - copy';
            $listing->desc_short = $obj->desc_short;
            $listing->comment = $obj->comment;
            $listing->desc_long = $obj->desc_long;
            $listing->status = $obj->status;
            $listing->max_limit = $obj->max_limit;
            $listing->max_limit_per_day = $obj->max_limit_per_day;
            $listing->max_limit_per_account = $obj->max_limit_per_account;
            $listing->max_limit_per_day_per_account = $obj->max_limit_per_day_per_account;
            $listing->is_hidden = $obj->is_hidden;
            $listing->date_start = $obj->date_start;
            $listing->date_end = $obj->date_end;
            $listing->datetime_start = $obj->datetime_start;
            $listing->datetime_end = $obj->datetime_end;
            $listing->time_start = $obj->time_start;
            $listing->time_end = $obj->time_end;
            $listing->payload = $obj->payload;
            $listing->extra_settings = $obj->extra_settings;
            $listing->image_banner = $obj->image_banner;
            $listing->image_square = $obj->image_square;
            $listing->venue_id = $obj->venue_id;
            $listing->display_order = $obj->display_order;
            $listing->save();

            // listing - product
            $lps = ListingProduct::where('listing_id', $obj->id)->get();
            foreach ($lps as $lp) {
                $new_lp = new ListingProduct();
                $new_lp->product_id = $lp->product_id;
                $new_lp->listing_id = $listing->id;
                $new_lp->save();
            }

            // listing - schedule
            $lss = ListingSchedule::where('listing_id', $obj->id)->get();
            foreach ($lss as $ls) {
                $new_ls = new ListingSchedule();
                $new_ls->listing_id = $listing->id;
                $new_ls->date_start = $ls->date_start;
                $new_ls->date_end = $ls->date_end;
                $new_ls->status = $ls->status;
                $new_ls->save();

                $lslts = ListingScheduleType::where('listing_schedule_id', $ls->id)->get();
                foreach ($lslts as $lslt) {
                    $new_lslt = new ListingScheduleType();
                    $new_lslt->listing_schedule_id = $new_ls->id;
                    $new_lslt->listing_type_id = $lslt->listing_type_id;
                    $new_lslt->tier_id = $lslt->tier_id;
                    $new_lslt->save();
                }

                $lsts = ListingScheduleTag::where('listing_schedule_id', $ls->id)->get();
                foreach ($lsts as $lst) {
                    $new_lst = new ListingScheduleTag();
                    $new_lst->listing_schedule_id = $new_ls->id;
                    $new_lst->tier_id = $lst->tier_id;
                    $new_lst->tag_id = $lst->tag_id;
                    $new_lst->save();
                }

                $lsvs = ListingScheduleVenue::where('listing_schedule_id', $ls->id)->get();
                foreach ($lsvs as $lsv) {
                    $new_lsv = new ListingScheduleVenue();
                    $new_lsv->listing_schedule_id = $new_ls->id;
                    $new_lsv->tier_id = $lsv->tier_id;
                    $new_lsv->venue_id = $lsv->venue_id;
                    $new_lsv->save();
                }
            }

            DB::commit();

            return response()->json(['status' => 'ok', 'message' => 'Duplicating listing is successful.', 'data' => ['id' => $listing->id]]);

        } catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return information for specific listing
     * >> for editing
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function edit($id)
    {
        try {
            $listing = Listing::find($id);

            if (is_null($listing)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                return response()->json(['status' => 'ok', 'data' => $listing]);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Create a listing
     *
     * @param Request $request
     * @param ImageOptimizer $imageOptimizer
     * @return \Illuminate\Http\JsonResponse
     */
    protected function store(Request $request, ImageOptimizer $imageOptimizer)
    {
        try {

            $validator = Validator::make($request->all(), [
                'listing_type_id' => 'required',
                'name' => 'required',
                'heading' => 'required',
                'desc_short' => 'required',
                'date_timezone' => 'timezone',
                'status' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $listingType = ListingType::find($request->input('listing_type_id'));
            if (is_null($listingType)) {
                return response()->json(['status' => 'error', 'message' => 'listing_type_not_found'], Response::HTTP_BAD_REQUEST);
            }
            // Log::info($request);

            DB::beginTransaction();

            $listing = new Listing;
            $listing->listing_type_id = $listingType->id;
            $listing->name = $request->input('name');
            $listing->heading = $request->input('heading');
            $listing->desc_short = $request->input('desc_short');

            if ($request->has('comment')) {
                $listing->comment = $request->input('comment');
            }

            if ($request->has('desc_long')) {
                $listing->desc_long = str_replace("\r\n", "<br>", $request->input('desc_long'));
            }

            if ($request->has('status')) {
                $listing->status = $request->input('status');
            }

            if ($request->has('venue_id')) {
                $listing->venue_id = $request->input('venue_id');
            }

            // if ($request->has('venue_name')) {
            //     $listing->venue_name = $request->input('venue_name');

            //     if ( $request->input('venue_id') == '0' ){
            //         //Log::info("create new venue");
            //         $venue = new Venue();
            //         $venue->name = $listing->venue_name;
            //         $venue->active = 0;
            //         $venue->open_and_close_hours = '[]';
            //         $venue->pickup_and_delivery_hours = '[]';
            //         $venue->social_links = '[{"platform":"facebook","url":""},{"platform":"twitter","url":""},{"platform":"instagram","url":""},{"platform":"googleplus","url":""},{"platform":"website","url":""}]';
            //         $venue->menu_links = '[{"platform":"food","url":""},{"platform":"drink","url":""}]';
            //         $tc = Setting::where('key', 'invoice_logo')->first();
            //         $venue->image = $tc->value;
            //         $venue->save();

            //         $listing->venue_id = $venue->id;
            //     }
            // } else {
            //     $listing->venue_name = "";
            // }
            
            // if ($request->has('non_bepoz_offer')) {
            //     $listing->non_bepoz_offer = $request->input('non_bepoz_offer');
            // }

            if ($request->has('display_order')) {
                $listing->display_order = $request->input('display_order');
            }

            if ($request->has('max_limit')) {
                if ( (int) $request->input('max_limit') ) {
                    $listing->max_limit = $request->input('max_limit');
                }  else {
                    $listing->max_limit = 0;
                }
            } else {
                $listing->max_limit = 0;
            }

            if ($request->has('max_limit_per_day')) {
                $listing->max_limit_per_day = $request->input('max_limit_per_day');
            }

            if ($request->has('max_limit_per_account')) {
                $listing->max_limit_per_account = $request->input('max_limit_per_account');
            }

            if ($request->has('max_limit_per_day_per_account')) {
                $listing->max_limit_per_day_per_account = $request->input('max_limit_per_day_per_account');
            }

            if ($request->has('is_hidden')) {
                $listing->is_hidden = (bool)$request->input('is_hidden');
            }

            if ($request->has('date_start')) {

                if ($this->checkIsAValidDate($request->input('date_start'))) {
                    if ($request->input('date_start') != 'null' || !is_null($request->input('date_start'))) {
                        if (!$request->has('date_timezone')) {
                            return response()->json(['status' => 'error', 'message' => 'not_timezone'], Response::HTTP_BAD_REQUEST);
                        }

                        $listing->date_start = new Carbon($request->input('date_start'), config('app.timezone'));
                        $listing->date_start->setTimezone(config('app.timezone'));
                    }
                }

            }

            if ($request->has('date_end')) {

                if ($this->checkIsAValidDate($request->input('date_end'))) {
                    if ($request->input('date_end') != 'null' || !is_null($request->input('date_end'))) {
                        if (!$request->has('date_timezone')) {
                            return response()->json(['status' => 'error', 'message' => 'not_timezone'], Response::HTTP_BAD_REQUEST);
                        }

                        $listing->date_end = new Carbon($request->input('date_end'), config('app.timezone'));
                        $listing->date_end->setTimezone(config('app.timezone'));
                    }
                }

            }

            if ($request->has('datetime_start')) {

                if ($this->checkIsAValidDate($request->input('datetime_start'))) {
                    if ($request->input('datetime_start') != 'null' || !is_null($request->input('datetime_start'))) {
                        if (!$request->has('date_timezone')) {
                            return response()->json(['status' => 'error', 'message' => 'not_timezone'], Response::HTTP_BAD_REQUEST);
                        }

                        $listing->datetime_start = new Carbon($request->input('datetime_start'), config('app.timezone'));
                        $listing->datetime_start->setTimezone(config('app.timezone'));
                    }
                }

            }

            if ($request->has('datetime_end')) {

                if ($this->checkIsAValidDate($request->input('datetime_end'))) {
                    if ($request->input('datetime_end') != 'null' || !is_null($request->input('datetime_end'))) {
                        if (!$request->has('date_timezone')) {
                            return response()->json(['status' => 'error', 'message' => 'not_timezone'], Response::HTTP_BAD_REQUEST);
                        }

                        $listing->datetime_end = new Carbon($request->input('datetime_end'), config('app.timezone'));
                        $listing->datetime_end->setTimezone(config('app.timezone'));
                    }
                }

            }

            if ($request->has('sell_datetime_start')) {

                if ($this->checkIsAValidDate($request->input('sell_datetime_start'))) {
                    if ($request->input('sell_datetime_start') != 'null' || !is_null($request->input('sell_datetime_start'))) {
                        if (!$request->has('date_timezone')) {
                            return response()->json(['status' => 'error', 'message' => 'not_timezone'], Response::HTTP_BAD_REQUEST);
                        }

                        $listing->sell_datetime_start = new Carbon($request->input('sell_datetime_start'), config('app.timezone'));
                        $listing->sell_datetime_start->setTimezone(config('app.timezone'));
                    }
                }

            }

            if ($request->has('sell_datetime_end')) {

                if ($this->checkIsAValidDate($request->input('sell_datetime_end'))) {
                    if ($request->input('sell_datetime_end') != 'null' || !is_null($request->input('sell_datetime_end'))) {
                        if (!$request->has('date_timezone')) {
                            return response()->json(['status' => 'error', 'message' => 'not_timezone'], Response::HTTP_BAD_REQUEST);
                        }

                        $listing->sell_datetime_end = new Carbon($request->input('sell_datetime_end'), config('app.timezone'));
                        $listing->sell_datetime_end->setTimezone(config('app.timezone'));
                    }
                }

            }

            /**** HACK IN TIME_start/end
             * Fix if wrong
             */
            if ($request->has('time_start')) {

                if ($this->checkIsAValidDate($request->input('time_start'))) {
                    if ($request->input('time_start') != 'null' || !is_null($request->input('time_start'))) {
                        // $listing->time_start = new Carbon($request->input('time_start'), config('app.timezone'));
                        // $listing->time_start->setTimezone(config('app.timezone'));
                        $listing->time_start = new Carbon($request->input('time_start'));
                    }
                }
            }

            if ($request->has('time_end')) {

                if ($this->checkIsAValidDate($request->input('time_end'))) {
                    if ($request->input('time_end') != 'null' || !is_null($request->input('time_end'))) {
                        // $listing->time_end = new Carbon($request->input('time_end'), config('app.timezone'));
                        // $listing->time_end->setTimezone(config('app.timezone'));
                        $listing->time_end = new Carbon($request->input('time_end'));
                    }
                }
            }

            if ($request->has('extra_settings')) {
                $listing->extra_settings = $request->input('extra_settings');
            }

            if ($request->has('prize_promotion_id')) {
                $listing->prize_promotion_id = $request->input('prize_promotion_id');
            }

            if ($request->has('prize_promotion_enable')) {
                $listing->prize_promotion_enable = $request->input('prize_promotion_enable');
            }

            if ($request->has('payload')) {
                $listing->payload = $request->input('payload');
            }

            if ($request->has('membership_tier')) {
                $listing->membership_tier = $request->input('membership_tier');
            }

            if ($request->has('bepoz_account_group')) {
                $listing->bepoz_account_group = $request->input('bepoz_account_group');
            }

            if ($request->has('membership_expiry_date')) {
                $listing->membership_expiry_date = $request->input('membership_expiry_date');
            }

            if ($request->has('membership_downgrade_tier')) {
                $listing->membership_downgrade_tier = $request->input('membership_downgrade_tier');
            }

            if ($request->has('product_price_option')) {
                $listing->product_price_option = $request->input('product_price_option');
            }

            if ($request->has('dollar_price')) {
                $listing->dollar_price = $request->input('dollar_price');
            }

            if ($request->has('point_price')) {
                $listing->point_price = $request->input('point_price');
            }

            $listing->save();

            // if ($request->has('product_create')) {
            //     $product_create = $request->input('product_create');

            //     $productSearch = Product::where('bepoz_product_number', $product_create['bepoz_product_number'])->first();

            //     if (is_null($productSearch)){
            //         $productSearch = new Product;
            //     }

            //     $productSearch->name = $product_create['name'];
            //     $productSearch->desc_short = $product_create['name'];
            //     $productSearch->point_price = $product_create['point_price'];
            //     $productSearch->unit_price = $product_create['unit_price'];                    
            //     $productSearch->status = 'active';
            //     $productSearch->product_type_id = 4;        
            //     $productSearch->bepoz_product_id = $product_create['bepoz_product_id'];
            //     $productSearch->bepoz_product_number = $product_create['bepoz_product_number'];
            //     $productSearch->bepoz_voucher_setup_id = $product_create['bepoz_voucher_setup_id'];
            //     $productSearch->is_free = $product_create['is_free'];
            //     $productSearch->category = 'shop';
            //     $productSearch->payload = $product_create['payload'];

            //     $productSearch->save();

            //     $listingProduct = ListingProduct::where('product_id', $productSearch->id)->where('listing_id', $listing->id)->first();
                
            //     if (is_null($listingProduct)){
            //         $listingProduct = new ListingProduct();
            //     }

            //     $listingProduct->product_id = $productSearch->id;
            //     $listingProduct->listing_id = $listing->id;
            //     $listingProduct->save();
            // }

            if ($request->hasFile('image_banner')) {
                if (config('bucket.s3')){
                    $picture = $request->file('image_banner');
                    $mime = explode('/', $picture->getMimeType());
                    $type = end($mime);
                    $imageOptimizer->optimizeUploadedImageFile($picture);
                    Storage::disk('s3')->put('listing/' . $listing->id . '/image_banner' . '.' . $type, file_get_contents($picture), 'public');
                    $listing->image_banner = "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/listing/" . $listing->id . "/image_banner." . $type . "?" . time();
                    $listing->save();

                } else {
                    $log = new SystemLog();
                    $log->type = 'setting-error';
                    $log->humanized_message = 'Setting of AWS S3 bucket not found.';
                    $log->payload = 'ListingController.php/store image_banner';
                    $log->message = 'Setting of S3 bucket not found.';
                    $log->source = 'ListingController.php';
                    $log->save();

                    return response()->json(['status' => 'error', 'message' => 'Please contact Administrator.'], Response::HTTP_BAD_REQUEST);
                }
            }

            if ($request->hasFile('image_square')) {
                if (config('bucket.s3')){
                    $picture = $request->file('image_square');
                    $mime = explode('/', $picture->getMimeType());
                    $type = end($mime);
                    $imageOptimizer->optimizeUploadedImageFile($picture);
                    Storage::disk('s3')->put('listing/' . $listing->id . '/image_square' . '.' . $type, file_get_contents($picture), 'public');
                    $listing->image_square = "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/listing/" . $listing->id . "/image_square." . $type . "?" . time();
                    $listing->save();

                } else {
                    $log = new SystemLog();
                    $log->type = 'setting-error';
                    $log->humanized_message = 'Setting of AWS S3 bucket not found.';
                    $log->payload = 'ListingController.php/store image_square';
                    $log->message = 'Setting of S3 bucket not found.';
                    $log->source = 'ListingController.php';
                    $log->save();

                    return response()->json(['status' => 'error', 'message' => 'Please contact Administrator.'], Response::HTTP_BAD_REQUEST);
                }
            }

            if ($request->has('products')) {
                $products = \GuzzleHttp\json_decode($request->input('products'));
                // Log::info($products);
                if (!is_array($products)) {
                    $products = [$products];
                }

                foreach ($products as $product) {
                    // Log::info("845");
                    $obj = new ListingProduct();
                    $obj->product_id = $product->id;
                    $obj->listing_id = $listing->id;
                    $obj->save();
                    
                    if ($listing->listing_type_id == 9) {
                        $productPrice = Product::find($product->id);
                        $listing->dollar_price = $productPrice->unit_price;
                        $listing->point_price = $productPrice->point_price;
                        $listing->product_price_option = $productPrice->product_price_option;
                        $listing->save();
                    }
                }
            }

            if ($request->has('schedules')) {
                $schedules = \GuzzleHttp\json_decode($request->input('schedules'));

                foreach ($schedules as $schedule) {
                    if ($schedule->mode == 'new') {
                        $obj = new ListingSchedule();
                        $obj->listing_id = $listing->id;
                        $obj->date_start = new Carbon($schedule->date_start, config('app.timezone'));
                        $obj->date_start->setTimezone(config('app.timezone'));
                        $obj->date_end = new Carbon($schedule->date_end, config('app.timezone'));
                        $obj->date_end->setTimezone(config('app.timezone'));
                        $obj->status = $schedule->status;
                        $obj->never_expired = $schedule->never_expired;
                        
                        $obj->save();

                        if (isset($schedule->venues)) {

                            foreach ($schedule->venues as $venue) {
                                $lsv = new ListingScheduleVenue();
                                $lsv->listing_schedule_id = $obj->id;
                                $lsv->venue_id = $venue->venue->id;
                                $lsv->tier_id = $venue->tier_id;
                                $lsv->save();

                            }
                        }

                        if (isset($schedule->tags)) {

                            foreach ($schedule->tags as $tag) {
                                $lst = new ListingScheduleTag();
                                $lst->listing_schedule_id = $obj->id;
                                $lst->tag_id = $tag->tag->id;
                                $lst->tier_id = $tag->tier_id;
                                $lst->save();
                            }
                        }

                        if (isset($schedule->types)) {

                            foreach ($schedule->types as $type) {
                                $lst = new ListingScheduleType();
                                $lst->listing_schedule_id = $obj->id;
                                $lst->listing_type_id = $type->type->id;
                                $lst->tier_id = $type->tier_id;
                                $lst->save();

                            }
                        }
                    }

                }
            }

            if ($request->has('listingTagsPivot')) {
                // $listingTagsPivot = \GuzzleHttp\json_decode($request->input('listingTagsPivot'));
                $listingTagsPivot = $request->input('listingTagsPivot');

                foreach ($listingTagsPivot as $key=>$listingTag) {
                                        
                    $listingPivotTag = ListingPivotTag::where('listing_id', $listing->id)
                        ->where('listing_tag_id', $listingTag['id'])->first();

                    if ($listingTag['mode'] == 'new'){
                        if (is_null($listingPivotTag)){
                            $obj = new ListingPivotTag();

                            $obj->listing_id = $listing->id;
                            $obj->listing_name = $listing->name;
                            $obj->listing_tag_id = $listingTag['id'];
                            $obj->listing_tag_name = $listingTag['name'];
                            $obj->display_order = $key+1;
                            $obj->status = 1;
                            $obj->save();
                        }
                    }
                }
            }

            DB::commit();

            return response()->json(['status' => 'ok', 'message' => 'Creating listing is successful.', 'data' => ['id' => $listing->id]]);

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Update a listing
     *
     * @param Request $request
     * @param ImageOptimizer $imageOptimizer
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(Request $request, $id, ImageOptimizer $imageOptimizer)
    {
        try {

            $validator = Validator::make($request->all(), [
                'listing_type_id' => 'required',
                'name' => 'required',
                'heading' => 'required',
                'desc_short' => 'required',
                'date_timezone' => 'timezone',
                'status' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $listingType = ListingType::find($request->input('listing_type_id'));
            if (is_null($listingType)) {
                return response()->json(['status' => 'error', 'message' => 'listing_type_not_found'], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();

            $listing = Listing::find($id);
            $listing->listing_type_id = $listingType->id;
            $listing->name = $request->input('name');
            $listing->heading = $request->input('heading');
            $listing->desc_short = $request->input('desc_short');

            if ($request->has('comment')) {
                $listing->comment = $request->input('comment');
            } else {
                $listing->comment = null;
            }

            if ($request->has('desc_long')) {
                $listing->desc_long = str_replace("\r\n", "<br>", $request->input('desc_long'));
            } else {
                $listing->desc_long = null;
            }

            if ($request->has('status')) {
                $listing->status = $request->input('status');
            }

            if ($request->has('venue_id')) {
                $listing->venue_id = $request->input('venue_id');
            } else {
                $listing->venue_id = 0;
            }

            // if ($request->has('venue_name')) {
            //     $listing->venue_name = $request->input('venue_name');

            //     if ( $request->input('venue_id') == '0' ){
            //         //Log::info("create new venue");
            //         $venue = new Venue();
            //         $venue->name = $listing->venue_name;
            //         $venue->active = 0;
            //         $venue->open_and_close_hours = '[]';
            //         $venue->pickup_and_delivery_hours = '[]';
            //         $venue->social_links = '[{"platform":"facebook","url":""},{"platform":"twitter","url":""},{"platform":"instagram","url":""},{"platform":"googleplus","url":""},{"platform":"website","url":""}]';
            //         $venue->menu_links = '[{"platform":"food","url":""},{"platform":"drink","url":""}]';
            //         $tc = Setting::where('key', 'invoice_logo')->first();
            //         $venue->image = $tc->value;
            //         $venue->save();

            //         $listing->venue_id = $venue->id;
            //     }
            // } else {
            //     $listing->venue_name = "";
            // }
            
            // if ($request->has('non_bepoz_offer')) {
            //     $listing->non_bepoz_offer = $request->input('non_bepoz_offer');
            // }

            if ($request->has('display_order')) {
                $listing->display_order = $request->input('display_order');
            } else {
                $listing->display_order = 0;
            }
            
            if ($request->has('max_limit')) {
                if ( (int) $request->input('max_limit') ) {
                    $listing->max_limit = $request->input('max_limit');
                }  else {
                    $listing->max_limit = 0;
                }
            } else {
                $listing->max_limit = 0;
            }

            if ($request->has('max_limit_per_day')) {
                $listing->max_limit_per_day = $request->input('max_limit_per_day');
            } else {
                $listing->max_limit_per_day = 0;
            }

            if ($request->has('max_limit_per_account')) {
                $listing->max_limit_per_account = $request->input('max_limit_per_account');
            } else {
                $listing->max_limit_per_account = 0;
            }

            if ($request->has('max_limit_per_day_per_account')) {
                $listing->max_limit_per_day_per_account = $request->input('max_limit_per_day_per_account');
            } else {
                $listing->max_limit_per_day_per_account = 0;
            }

            if ($request->has('date_start')) {

                if ($this->checkIsAValidDate($request->input('date_start'))) {
                    if ($request->input('date_start') != 'null' || !is_null($request->input('date_start'))) {
                        if (!$request->has('date_timezone')) {
                            return response()->json(['status' => 'error', 'message' => 'not_timezone'], Response::HTTP_BAD_REQUEST);
                        }

                        $listing->date_start = new Carbon($request->input('date_start'), config('app.timezone'));
                        $listing->date_start->setTimezone(config('app.timezone'));
                    }
                }
            } else {
                $listing->date_start = null;
            }

            if ($request->has('date_end')) {

                if ($this->checkIsAValidDate($request->input('date_end'))) {
                    if ($request->input('date_end') != 'null' || !is_null($request->input('date_end'))) {
                        if (!$request->has('date_timezone')) {
                            return response()->json(['status' => 'error', 'message' => 'not_timezone'], Response::HTTP_BAD_REQUEST);
                        }

                        $listing->date_end = new Carbon($request->input('date_end'), config('app.timezone'));
                        $listing->date_end->setTimezone(config('app.timezone'));
                    }
                }
            } else {
                $listing->date_end = null;
            }

            if ($request->has('datetime_start')) {

                if ($this->checkIsAValidDate($request->input('datetime_start'))) {
                    if ($request->input('datetime_start') != 'null' || !is_null($request->input('datetime_start'))) {
                        if (!$request->has('date_timezone')) {
                            return response()->json(['status' => 'error', 'message' => 'not_timezone'], Response::HTTP_BAD_REQUEST);
                        }

                        $listing->datetime_start = new Carbon($request->input('datetime_start'), config('app.timezone'));
                        $listing->datetime_start->setTimezone(config('app.timezone'));
                    }
                }
            } else {
                $listing->datetime_start = null;
            }

            if ($request->has('datetime_end')) {

                if ($this->checkIsAValidDate($request->input('datetime_end'))) {
                    if ($request->input('datetime_end') != 'null' || !is_null($request->input('datetime_end'))) {
                        if (!$request->has('date_timezone')) {
                            return response()->json(['status' => 'error', 'message' => 'not_timezone'], Response::HTTP_BAD_REQUEST);
                        }

                        $listing->datetime_end = new Carbon($request->input('datetime_end'), config('app.timezone'));
                        $listing->datetime_end->setTimezone(config('app.timezone'));
                    }
                }
            } else {
                $listing->datetime_end = null;
            }

            if ($request->has('sell_datetime_start')) {

                if ($this->checkIsAValidDate($request->input('sell_datetime_start'))) {
                    if ($request->input('sell_datetime_start') != 'null' || !is_null($request->input('sell_datetime_start'))) {
                        if (!$request->has('date_timezone')) {
                            return response()->json(['status' => 'error', 'message' => 'not_timezone'], Response::HTTP_BAD_REQUEST);
                        }

                        $listing->sell_datetime_start = new Carbon($request->input('sell_datetime_start'), config('app.timezone'));
                        $listing->sell_datetime_start->setTimezone(config('app.timezone'));
                    }
                }
            } else {
                $listing->sell_datetime_start = null;
            }

            if ($request->has('sell_datetime_end')) {

                if ($this->checkIsAValidDate($request->input('sell_datetime_end'))) {
                    if ($request->input('sell_datetime_end') != 'null' || !is_null($request->input('sell_datetime_end'))) {
                        if (!$request->has('date_timezone')) {
                            return response()->json(['status' => 'error', 'message' => 'not_timezone'], Response::HTTP_BAD_REQUEST);
                        }

                        $listing->sell_datetime_end = new Carbon($request->input('sell_datetime_end'), config('app.timezone'));
                        $listing->sell_datetime_end->setTimezone(config('app.timezone'));
                    }
                }
            } else {
                $listing->sell_datetime_end = null;
            }

            /**** HACK IN TIME_start/end
             * Fix if wrong
             */
            if ($request->has('time_start')) {
                if ($this->checkIsAValidDate($request->input('time_start'))) {
                    if ($request->input('time_start') != 'null' || !is_null($request->input('time_start'))) {
                        // $listing->time_start = new Carbon($request->input('time_start'), config('app.timezone'));
                        // $listing->time_start->setTimezone(config('app.timezone'));
                        $listing->time_start = new Carbon($request->input('time_start'));
                    }
                }
            } else {
                $listing->time_start = null;
            }

            if ($request->has('time_end')) {
                // Log::info("input raw");
                // Log::info($request->input('time_end'));
                if ($this->checkIsAValidDate($request->input('time_end'))) {
                    if ($request->input('time_end') != 'null' || !is_null($request->input('time_end'))) {
                        // $listing->time_end = new Carbon($request->input('time_end'), config('app.timezone'));
                        // $listing->time_end->setTimezone(config('app.timezone'));
                        $listing->time_end = new Carbon($request->input('time_end'));
                    }
                }
            } else {
                $listing->time_end = null;
            }

            if ($request->has('is_hidden')) {
                $listing->is_hidden = (bool)$request->input('is_hidden');
            }

            if ($request->hasFile('image_banner')) {
                if (config('bucket.s3')){
                    $picture = $request->file('image_banner');
                    $mime = explode('/', $picture->getMimeType());
                    $type = end($mime);
                    $imageOptimizer->optimizeUploadedImageFile($picture);
                    Storage::disk('s3')->put('listing/' . $listing->id . '/image_banner' . '.' . $type, file_get_contents($picture), 'public');
                    $listing->image_banner = "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/listing/" . $listing->id . "/image_banner." . $type . "?" . time();
                    
                } else {
                    $log = new SystemLog();
                    $log->type = 'setting-error';
                    $log->humanized_message = 'Setting of AWS S3 bucket not found.';
                    $log->payload = 'ListingController.php/update image_banner';
                    $log->message = 'Setting of S3 bucket not found.';
                    $log->source = 'ListingController.php';
                    $log->save();

                    return response()->json(['status' => 'error', 'message' => 'Please contact Administrator.'], Response::HTTP_BAD_REQUEST);
                }
            }

            if ($request->hasFile('image_square')) {
                if (config('bucket.s3')){
                    $picture = $request->file('image_square');
                    $mime = explode('/', $picture->getMimeType());
                    $type = end($mime);
                    $imageOptimizer->optimizeUploadedImageFile($picture);
                    Storage::disk('s3')->put('listing/' . $listing->id . '/image_square' . '.' . $type, file_get_contents($picture), 'public');
                    $listing->image_square = "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/listing/" . $listing->id . "/image_square." . $type . "?" . time();
    
                } else {
                    $log = new SystemLog();
                    $log->type = 'setting-error';
                    $log->humanized_message = 'Setting of AWS S3 bucket not found.';
                    $log->payload = 'ListingController.php/update image_square';
                    $log->message = 'Setting of S3 bucket not found.';
                    $log->source = 'ListingController.php';
                    $log->save();

                    return response()->json(['status' => 'error', 'message' => 'Please contact Administrator.'], Response::HTTP_BAD_REQUEST);
                }
            }

            if ($request->has('payload')) {
                $listing->payload = $request->input('payload');
            } else {
                $listing->payload = null;
            }

            if ($request->has('extra_settings')) {
                $listing->extra_settings = $request->input('extra_settings');
            } else {
                $listing->extra_settings = null;
            }

            if ($request->has('prize_promotion_id')) {
                $listing->prize_promotion_id = $request->input('prize_promotion_id');
            }

            if ($request->has('prize_promotion_enable')) {
                $listing->prize_promotion_enable = $request->input('prize_promotion_enable');
            }

            if ($request->has('membership_tier')) {
                $listing->membership_tier = $request->input('membership_tier');
            }

            if ($request->has('bepoz_account_group')) {
                $listing->bepoz_account_group = $request->input('bepoz_account_group');
            }

            if ($request->has('membership_expiry_date')) {
                $listing->membership_expiry_date = $request->input('membership_expiry_date');
            }

            if ($request->has('membership_downgrade_tier')) {
                $listing->membership_downgrade_tier = $request->input('membership_downgrade_tier');
            }

            if ($request->has('product_price_option')) {
                $listing->product_price_option = $request->input('product_price_option');
            }

            if ($request->has('dollar_price')) {
                $listing->dollar_price = $request->input('dollar_price');
            }

            if ($request->has('point_price')) {
                $listing->point_price = $request->input('point_price');
            }

            $listing->save();

            if ($request->has('product_create')) {
                $product_create = $request->input('product_create');

                $productSearch = Product::where('bepoz_product_number', $product_create['bepoz_product_number'])->first();

                if (is_null($productSearch)){
                    $productSearch = new Product;
                }

                $productSearch->name = $product_create['name'];
                $productSearch->desc_short = $product_create['name'];
                $productSearch->point_price = $product_create['point_price'];
                $productSearch->unit_price = $product_create['unit_price'];                    
                $productSearch->status = 'active';
                $productSearch->product_type_id = 4;        
                $productSearch->bepoz_product_id = $product_create['bepoz_product_id'];
                $productSearch->bepoz_product_number = $product_create['bepoz_product_number'];
                $productSearch->bepoz_voucher_setup_id = $product_create['bepoz_voucher_setup_id'];
                $productSearch->is_free = $product_create['is_free'];
                $productSearch->category = 'shop';
                $productSearch->payload = $product_create['payload'];

                $productSearch->save();

                $listingProduct = ListingProduct::where('listing_id', $listing->id)->first();
                
                if (is_null($listingProduct)){
                    $listingProduct = new ListingProduct();
                    $listingProduct->listing_id = $listing->id;
                }

                $listingProduct->product_id = $productSearch->id;
                $listingProduct->save();
            }

            // Log::info($listing->listing_type_id);
            if ($request->has('products')) {
                $products = \GuzzleHttp\json_decode($request->input('products'));
                // Log::info($products);
                if (is_array($products)) {
                    // Log::info("1331");
                    foreach ($products as $product) {
                        // Log::info("1333");
                        if ($listing->listing_type_id == 9) {
                            // Log::info("1335");
                            $productPrice = Product::find($product->id);
                            $listing->dollar_price = $productPrice->unit_price;
                            $listing->point_price = $productPrice->point_price;
                            $listing->product_price_option = $productPrice->product_price_option;
                            $listing->save();
                        }

                        if ($product->mode == 'not_modified') {
                            continue;
                        } elseif ($product->mode == 'deleted') {
                            $obj = ListingProduct::where('product_id', $product->id)->where('listing_id', $listing->id);
                            if (!is_null($obj->first())) {
                                $obj->delete();
                            }
                        } elseif ($product->mode == 'new') {
                            $obj = new ListingProduct();
                            $obj->product_id = $product->id;
                            $obj->listing_id = $listing->id;
                            $obj->save();
                        } else {
                            if (isset($product->listing_product_id)) {
                                $obj = ListingProduct::find($product->listing_product_id);
                                $obj->save();
                            }
                        }
                        
                    }
                } else {
                    $obj = new ListingProduct();
                    $obj->product_id = $products->id;
                    $obj->listing_id = $listing->id;
                    $obj->save();

                    if ($listing->listing_type_id == 9) {
                        $productPrice = Product::find($products->id);
                        $listing->dollar_price = $productPrice->unit_price;
                        $listing->point_price = $productPrice->point_price;
                        $listing->product_price_option = $productPrice->product_price_option;
                    }
                }

            }

            if ($request->has('schedules')) {
                $schedules = \GuzzleHttp\json_decode($request->input('schedules'));
                foreach ($schedules as $schedule) {
                    if ($schedule->mode == 'not_modified') {
                        continue;
                    } elseif ($schedule->mode == 'deleted') {
                        $obj = ListingSchedule::find($schedule->id);
                        $obj->delete();
                    } elseif ($schedule->mode == 'modified') {
                        $obj = ListingSchedule::find($schedule->id);
                        $obj->date_start = new Carbon($schedule->date_start, config('app.timezone'));
                        $obj->date_start->setTimezone(config('app.timezone'));
                        $obj->date_end = new Carbon($schedule->date_end, config('app.timezone'));
                        $obj->date_end->setTimezone(config('app.timezone'));
                        $obj->status = $schedule->status;
                        $obj->never_expired = $schedule->never_expired;
                        $obj->save();

                        if (isset($schedule->venues)) {
                            if (count($schedule->venues) == 0) {
                                foreach ($obj->venues as $venue) {
                                    $venue->delete();
                                }
                            } else {
                                foreach ($obj->venues as $venue) {

                                    $obj_found = false;
                                    foreach ($schedule->venues as $vne) {
                                        if ($venue->venue_id == $vne->venue->id && $vne->tier_id == $venue->tier_id) {
                                            $obj_found = true;
                                        }
                                    }

                                    if (!$obj_found) {
                                        $venue->delete();
                                    }
                                }

                                foreach ($schedule->venues as $venue) {
                                    $obj_found = false;
                                    foreach ($obj->venues as $vne) {
                                        if ($vne->venue_id == $venue->venue->id && $vne->tier_id == $venue->tier_id) {
                                            $obj_found = true;
                                        }
                                    }

                                    if (!$obj_found) {
                                        $lsv = new ListingScheduleVenue();
                                        $lsv->listing_schedule_id = $obj->id;
                                        $lsv->venue_id = $venue->venue->id;
                                        $lsv->tier_id = $venue->tier_id;
                                        $lsv->save();
                                    }

                                }
                            }
                        }

                        if (isset($schedule->tags)) {
                            if (count($schedule->tags) == 0) {
                                foreach ($obj->tags as $tag) {
                                    $tag->delete();
                                }
                            } else {
                                foreach ($obj->tags as $tag) {

                                    $obj_found = false;
                                    foreach ($schedule->tags as $tg) {
                                        if ($tag->tag_id == $tg->tag->id && $tag->tier_id == $tg->tier_id) {
                                            $obj_found = true;
                                        }
                                    }

                                    if (!$obj_found) {
                                        $tag->delete();
                                    }

                                }

                                foreach ($schedule->tags as $tag) {
                                    $obj_found = false;
                                    foreach ($obj->tags as $tg) {
                                        if ($tg->tag_id == $tag->tag->id && $tag->tier_id == $tg->tier_id) {
                                            $obj_found = true;
                                        }
                                    }

                                    if (!$obj_found) {
                                        $lst = new ListingScheduleTag();
                                        $lst->listing_schedule_id = $obj->id;
                                        $lst->tag_id = $tag->tag->id;
                                        $lst->tier_id = $tag->tier_id;
                                        $lst->save();
                                    }
                                }
                            }
                        }

                        if (isset($schedule->types)) {
                            if (count($schedule->types) == 0) {
                                foreach ($obj->types as $type) {
                                    $type->delete();
                                }
                            } else {
                                foreach ($obj->types as $type) {

                                    $obj_found = false;

                                    foreach ($schedule->types as $typ) {
                                        if ($type->listing_type_id == $typ->type->id && $type->tier_id == $typ->tier_id) {
                                            $obj_found = true;
                                        }
                                    }

                                    if (!$obj_found) {
                                        $type->delete();
                                    }

                                }

                                foreach ($schedule->types as $type) {
                                    $obj_found = false;

                                    foreach ($obj->types as $typ) {
                                        if ($typ->listing_type_id == $type->type->id && $type->tier_id == $typ->tier_id) {
                                            $obj_found = true;
                                        }
                                    }

                                    if (!$obj_found) {
                                        $lst = new ListingScheduleType();
                                        $lst->listing_schedule_id = $obj->id;
                                        $lst->listing_type_id = $type->type->id;
                                        $lst->tier_id = $type->tier_id;
                                        $lst->save();
                                    }
                                }
                            }
                        }

                    } elseif ($schedule->mode == 'new') {
                        $obj = new ListingSchedule();
                        $obj->listing_id = $listing->id;
                        $obj->date_start = new Carbon($schedule->date_start, config('app.timezone'));
                        $obj->date_start->setTimezone(config('app.timezone'));
                        $obj->date_end = new Carbon($schedule->date_end, config('app.timezone'));
                        $obj->date_end->setTimezone(config('app.timezone'));
                        $obj->status = $schedule->status;
                        $obj->never_expired = $schedule->never_expired;
                        $obj->save();

                        if (isset($schedule->venues)) {

                            foreach ($schedule->venues as $venue) {
                                $lsv = new ListingScheduleVenue();
                                $lsv->listing_schedule_id = $obj->id;
                                $lsv->venue_id = $venue->venue->id;
                                $lsv->tier_id = $venue->tier_id;
                                $lsv->save();
                            }
                        }

                        if (isset($schedule->tags)) {

                            foreach ($schedule->tags as $tag) {
                                $lst = new ListingScheduleTag();
                                $lst->listing_schedule_id = $obj->id;
                                $lst->tag_id = $tag->tag->id;
                                $lst->tier_id = $tag->tier_id;
                                $lst->save();
                            }
                        }

                        if (isset($schedule->types)) {

                            foreach ($schedule->types as $type) {
                                $lst = new ListingScheduleType();
                                $lst->listing_schedule_id = $obj->id;
                                $lst->listing_type_id = $type->type->id;
                                $lst->tier_id = $type->tier_id;
                                $lst->save();
                            }
                        }
                    }

                }
            }

            DB::commit();
            return response()->json(['status' => 'ok', 'message' => 'Updating listing is successful.', 'data' => ['id' => $listing->id], 'requests' => $request->all()]);

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'requests' => $request->all()], Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Delete a specific listing
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function destroy($id)
    {
        try {
            $listing = Listing::find($id);

            if (is_null($listing)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                $od = OrderDetail::where('listing_id', $id)->first();
                if (is_null($od)) {

                    DB::beginTransaction();

                    Listing::destroy($id);

                    DB::commit();
                    return response()->json(['status' => 'ok', 'message' => 'Deleting listing is successful.']);

                } else {
                    return response()->json(['status' => 'error', 'message' => 'Not safe. Some objects used this value'], Response::HTTP_BAD_REQUEST);
                }
            }

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function search(Request $request, Listing $listing)
    {
        try {
            $listing = $listing->newQuery();

            if ($request->has('keyword')) {
                $listing->where(function ($query) use ($request) {

                    if (strpos($request->input('keyword'), ' ') !== false) {
                        $array = explode(" ", $request->input('keyword'));
                        $keyword = implode("%", $array);
                        $query->where('heading', 'like', '%' . $keyword . '%');
                        $query->orWhere('name', 'like', '%' . $keyword . '%');
                    } else {
                        $query->where('heading', 'like', '%' . $request->input('keyword') . '%');
                        $query->orWhere('name', 'like', '%' . $request->input('keyword') . '%');
                    }


                });
            }

            if ($request->has('type_id')) {
                $listing->where('listing_type_id', $request->input('type_id'));
            }

            if ($request->has('status')) {
                if ($request->input('status') != "any") {
                    $listing->where('status', $request->input('status'));
                }
            }

            if ($request->has('date_start')) {
                $temp = new Carbon($request->input('date_start'), config('app.timezone'));
                $temp->setTimezone(config('app.timezone'));
                $listing->whereDate('date_start', '=', $temp);
            }

            if ($request->has('date_end')) {
                $temp = new Carbon($request->input('date_end'), config('app.timezone'));
                $temp->setTimezone(config('app.timezone'));
                $listing->whereDate('date_end', '=', $temp);
            }

            if ($request->has('sell_date_start')) {
                $temp = new Carbon($request->input('sell_date_start'), config('app.timezone'));
                $temp->setTimezone(config('app.timezone'));
                $listing->whereDate('date_start', '=', $temp);
            }

            if ($request->has('sell_date_end')) {
                $temp = new Carbon($request->input('sell_date_end'), config('app.timezone'));
                $temp->setTimezone(config('app.timezone'));
                $listing->whereDate('date_end', '=', $temp);
            }

            $listings = $listing->with('products.product', 'venue', 'schedules.types.type', 'schedules.venues.venue', 
              'schedules.tags.tag', 'order_details.member_vouchers', 'order_details.order.member.user' )->has('type')->get();
            
            foreach ($listings as $thelisting) {

              if ($thelisting->venue_id == 0){
                $company_name = Setting::select('value')->where('key', 'company_name')->first()->value;
                $company_street_number = Setting::select('value')->where('key', 'company_street_number')->first()->value;
                $company_street_name = Setting::select('value')->where('key', 'company_street_name')->first()->value;
                $company_suburb = Setting::select('value')->where('key', 'company_suburb')->first()->value;
                $company_state = Setting::select('value')->where('key', 'company_state')->first()->value;
                $company_postcode = Setting::select('value')->where('key', 'company_postcode')->first()->value;

                $thelisting->venue_address = $company_name.", "
                  .$company_street_number." " 
                  .$company_street_name.", " 
                  .$company_suburb.", " 
                  .$company_state.", " 
                  .$company_postcode ;
              }


              $thelisting->total_ticket = $thelisting->order_details_success->sum('qty');
              $thelisting->ticket_remaining = $thelisting->max_limit == 0 ? 'Unlimited' :  ($thelisting->max_limit - $thelisting->total_ticket);            

            }


            return response()->json(['status' => 'ok', 'data' => $listings, 'request' => $request->all()]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }


    protected function changeStatus(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'status' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();

            $listing = Listing::find($request->input('id'));
            $listing->status = $request->input('status');
            $listing->save();

            DB::commit();
            return response()->json(['status' => 'ok', 'message' => 'Changing listing status is successful.', 'data' => ['id' => $listing->id], 'requests' => $request->all()]);

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'requests' => $request->all()], Response::HTTP_BAD_REQUEST);
        }
    }


    protected function countListing(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'display_id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $type = ListingType::find($request->input('display_id'));

            if (is_null($type)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            }

            $listingSchedules = DB::table('listing_schedules')
                ->whereDate('date_start', '<=', Carbon::now(config('app.timezone'))->toDateString())
                ->whereDate('date_end', '>=', Carbon::now(config('app.timezone'))->toDateString())
                ->whereExists(function ($query) use ($type, $request) {

                    if (isset($request->input('decrypted_token')->member)) {
                        if (!is_null($request->input('decrypted_token')->member->tiers()->first())) {
                            $query->select(DB::raw(1))
                                ->from('listing_schedule_listing_type')
                                ->where('listing_schedule_listing_type.tier_id', $request->input('decrypted_token')->member->tiers()->first()->id)
                                ->where('listing_schedule_listing_type.listing_type_id', $type->id)
                                ->whereNull('listing_schedule_listing_type.deleted_at')
                                ->whereRaw('listing_schedules.id = listing_schedule_listing_type.listing_schedule_id');
                        } else {
                            $query->select(DB::raw(1))
                                ->from('listing_schedule_listing_type')
                                ->where('listing_schedule_listing_type.listing_type_id', $type->id)
                                ->whereNull('listing_schedule_listing_type.deleted_at')
                                ->whereRaw('listing_schedules.id = listing_schedule_listing_type.listing_schedule_id');
                        }
                    } else {
                        $query->select(DB::raw(1))
                            ->from('listing_schedule_listing_type')
                            ->where('listing_schedule_listing_type.listing_type_id', $type->id)
                            ->whereNull('listing_schedule_listing_type.deleted_at')
                            ->whereRaw('listing_schedules.id = listing_schedule_listing_type.listing_schedule_id');
                    }
                })
                ->whereExists(function ($query) {
                    $query->select(DB::raw(1))
                        ->from('listings')
                        ->where('status', 'active')
                        ->whereNull('listings.deleted_at')
                        ->whereRaw('listings.id = listing_schedules.listing_id');
                })
                ->where('listing_schedules.status', 'active')
                ->whereNull('listing_schedules.deleted_at')
                ->select('listing_schedules.id')
                ->count();

            return response()->json(['status' => 'ok', 'data' => $listingSchedules]);

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }


    private function checkIsAValidDate($myDateString)
    {
        return (bool)strtotime($myDateString);
    }

    protected function exportListing(Request $request)
    {
        try {
            $listing = Listing::with(['order_details' => function ($query) {
                $query->where('status', 'successful');
            }])->find($request->input('listingID'));

            $dns = new DNS1D();
            $collections = array(
                'order_details' => []
            );
            foreach ($listing->order_details as $order_detail) {

                $order_detail->order->member;
                $order_detail->product;

                $data = array();
                $data['order_detail'] = $order_detail;
                $data['barcodes'] = array();
                $member_vouchers = MemberVouchers::where('order_details_id', $order_detail->id)->get();

                foreach ($member_vouchers as $i => $member_voucher) {
                    $barcode = $dns->getBarcodeHTML($member_voucher->barcode, "EAN13");

                    $data['barcodes'][] = array(
                        'voucher' => $member_voucher,
                        'image' => $barcode
                    );
                }
                $collections['order_details'][] = $data;
            }

            $pdf = App::make('dompdf.wrapper')->loadView('export.exportListing', $collections);
            $pdf->setPaper('A4', 'landscape');
            return $pdf->download('export.pdf');

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function checkListingProduct(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'listing_id' => 'required',
                'product_id' => 'required',

            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $orderDetailCount = OrderDetail::where('listing_id', $request->input('listing_id'))
                ->where('product_id', $request->input('product_id'))
                ->count();

            $result = 'safe';
            if ($orderDetailCount > 0) {
                $result = 'unsafe';
            }

            return response()->json(['status' => 'ok', 'data' => $result]);

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Role;
use Illuminate\Http\Request;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class RoleControlController extends Controller
{
    /**
     * Return all controls
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index()
    {
        try {
            $roles = Role::with('control')->all();

            return response()->json(['status' => 'ok', 'data' => $roles]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return all roles (pagination)
     *
     * @param Request $request
     * @return mixed
     */
    protected function paginateRole(Request $request, Role $role)
    {
        try {

            $obj = $role->newQuery();

            if ($request->has('keyword')) {
                $obj->where(function ($query) use ($request) {

                    if (strpos($request->input('keyword'), ' ') !== false) {
                        $array = explode(" ", $request->input('keyword'));
                        $keyword = implode("%", $array);
                        $query->where('name', 'like', '%' . $keyword . '%');
                        $query->orWhere('description', 'like', '%' . $keyword . '%');
                    } else {
                        $query->where('name', 'like', '%' . $request->input('keyword') . '%');
                        $query->orWhere('description', 'like', '%' . $request->input('keyword') . '%');
                    }

                });
            }

            if ($request->has('orderBy') && $request->has('sortBy')) {
                $obj->orderBy($request->input('orderBy'), $request->input('sortBy'));
            }

            $paginate_number = 10;
            if ($request->has('paginate_number')) {
                $paginate_number = $request->input('paginate_number');
            }

            $roles = $obj->with('control')
                ->paginate($paginate_number);

            return response()->json(['status' => 'ok', 'data' => $roles]);

        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return role detail
     *
     * @return mixed
     */
    protected function show($id)
    {
        try {
            $role = Role::with('control')->find($id);

            return response()->json(['status' => 'ok', 'data' => $role]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update role controls
     *
     * @return mixed
     */
    protected function update(Request $request, $id)
    {
        try {
            $role = Role::with('control')->find($id);

            if (is_null($role)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            }

            $role->name = $request->input('name');
            $role->description = $request->input('description');
            $role->save();

            $payload = \GuzzleHttp\json_decode($request->input('control'));

            $role->control->listing_create = (boolean)$payload->listing_create;
            $role->control->listing_read = (boolean)$payload->listing_read;
            $role->control->listing_update = (boolean)$payload->listing_update;
            $role->control->listing_delete = (boolean)$payload->listing_delete;

            $role->control->voucher_create = (boolean)$payload->voucher_create;
            $role->control->voucher_read = (boolean)$payload->voucher_read;
            $role->control->voucher_update = (boolean)$payload->voucher_update;
            $role->control->voucher_delete = (boolean)$payload->voucher_delete;

            $role->control->product_create = (boolean)$payload->product_create;
            $role->control->product_read = (boolean)$payload->product_read;
            $role->control->product_update = (boolean)$payload->product_update;
            $role->control->product_delete = (boolean)$payload->product_delete;

            $role->control->role_create = (boolean)$payload->role_create;
            $role->control->role_read = (boolean)$payload->role_read;
            $role->control->role_update = (boolean)$payload->role_update;
            $role->control->role_delete = (boolean)$payload->role_delete;

            $role->control->user_create = (boolean)$payload->user_create;
            $role->control->user_read = (boolean)$payload->user_read;
            $role->control->user_update = (boolean)$payload->user_update;
            $role->control->user_delete = (boolean)$payload->user_delete;

            $role->control->user_role_create = (boolean)$payload->user_role_create;
            $role->control->user_role_read = (boolean)$payload->user_role_read;
            $role->control->user_role_update = (boolean)$payload->user_role_update;
            $role->control->user_role_delete = (boolean)$payload->user_role_delete;

            $role->control->staff_create = (boolean)$payload->staff_create;
            $role->control->staff_read = (boolean)$payload->staff_read;
            $role->control->staff_update = (boolean)$payload->staff_update;
            $role->control->staff_delete = (boolean)$payload->staff_delete;

            $role->control->notification_create = (boolean)$payload->notification_create;
            $role->control->notification_read = (boolean)$payload->notification_read;
            $role->control->notification_update = (boolean)$payload->notification_update;
            $role->control->notification_delete = (boolean)$payload->notification_delete;

            $role->control->faq_create = (boolean)$payload->faq_create;
            $role->control->faq_read = (boolean)$payload->faq_read;
            $role->control->faq_update = (boolean)$payload->faq_update;
            $role->control->faq_delete = (boolean)$payload->faq_delete;

            $role->control->survey_create = (boolean)$payload->survey_create;
            $role->control->survey_read = (boolean)$payload->survey_read;
            $role->control->survey_update = (boolean)$payload->survey_update;
            $role->control->survey_delete = (boolean)$payload->survey_delete;

            $role->control->venue_create = (boolean)$payload->venue_create;
            $role->control->venue_read = (boolean)$payload->venue_read;
            $role->control->venue_update = (boolean)$payload->venue_update;
            $role->control->venue_delete = (boolean)$payload->venue_delete;

            $role->control->enquiry_read = (boolean)$payload->enquiry_read;
            $role->control->enquiry_update = (boolean)$payload->enquiry_update;
            $role->control->enquiry_delete = (boolean)$payload->enquiry_delete;

            $role->control->member_create = (boolean)$payload->member_create;
            $role->control->member_read = (boolean)$payload->member_read;
            $role->control->member_update = (boolean)$payload->member_update;
            $role->control->member_delete = (boolean)$payload->member_delete;

            $role->control->setting_read = (boolean)$payload->setting_read;
            $role->control->setting_update = (boolean)$payload->setting_update;

            $role->control->friend_referral_read = (boolean)$payload->friend_referral_read;

            $role->control->transaction_read = (boolean)$payload->transaction_read;
            $role->control->transaction_export = (boolean)$payload->transaction_export;

            $role->control->report_read = (boolean)$payload->report_read;
            $role->control->report_export = (boolean)$payload->report_export;

            $role->control->log_read = (boolean)$payload->log_read;
            $role->control->log_delete = (boolean)$payload->log_delete;

            $role->control->resend_confirmation = (boolean)$payload->resend_confirmation;
            $role->control->send_broadcast = (boolean)$payload->send_broadcast;

            $role->control->frontend_access = (boolean)$payload->frontend_access;
            $role->control->backend_access = (boolean)$payload->backend_access;
            $role->control->root_access = (boolean)$payload->root_access;
            $role->control->venue_management = (boolean)$payload->venue_management;

            $role->control->admin_update = (boolean)$payload->admin_update;

            $role->control->save();

            return response()->json(['status' => 'ok', 'message' => 'Updating role is successful']);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove role
     *
     * @return mixed
     */
    protected function destroy(Request $request, $id)
    {
        try {

            $role = Role::find($id);

            if (is_null($role)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                DB::beginTransaction();
                Role::destroy($id);
                DB::commit();

                return response()->json(['status' => 'ok', 'message' => 'Deleting role is successful.']);
            }

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}

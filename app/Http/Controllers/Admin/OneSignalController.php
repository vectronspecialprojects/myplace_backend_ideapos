<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Jobs\SendOneSignalNotification;
use App\Member;
use App\MemberTiers;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class OneSignalController extends Controller
{
    /**
     * Send push notification to specific user.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function send(Request $request) {
        try {

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'message' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $member_id = $request->input('id');
            $member = Member::find($member_id);
            $user = $member->user;

            $message = $request->input('message');

            $company_name = Setting::where('key', 'company_name')->first()->value;
            $data = [
                // "tags" => [["key" => "email", "relation" => "=", "value" => $user->email]],
                "filters" => [["field" => "tag", "key" => "email", "relation" => "=", "value" => $member->user->email]],
                "contents" => ["en" => $message],
                "headings" => ["en" => $company_name],
                "data" => ["category" => "member"],
                "ios_badgeType" => "Increase",
                "ios_badgeCount" => 1
            ];

            dispatch(new SendOneSignalNotification($data));

            return response()->json(['status' => 'ok', 'message'=> 'Sending notification is successful']);

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Send push notification to all.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function broadcastMessage(Request $request) {
        try {

            $validator = Validator::make($request->all(), [
                'message' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            // $members = Member::with('user')->get();

            // Log::info("OneSignalController/broadcastMessage START");
            // Log::info(Carbon::now(config('app.timezone')));

            $message = $request->input('message');
            $company_name = Setting::where('key', 'company_name')->first()->value;

            // $members = Member::with('user')->get();
            // foreach ($members as $member) {
            //     $user = $member->user;

            //     $data = [
            //         // "tags" => [["key" => "email", "relation" => "=", "value" => $user->email]],
            //         "filters" => [["field" => "tag", "key" => "email", "relation" => "=", "value" => $user->email]],
            //         "contents" => ["en" => $message],
            //         "headings" => ["en" => $company_name],
            //         "data" => ["category" => "member"],
            //         "ios_badgeType" => "Increase",
            //         "ios_badgeCount" => 1
            //     ];
            //     //Log::error($data);
            //     dispatch(new SendOneSignalNotification($data));
            // }


            // $tags[] = ["key" => "email", "relation" => "=", "value" => "admin@vectron.com.au"];
            // $tags[] = ["operator" => "OR"];

            // // REMOVE LAST OR
            // $dataNotif = [
            //     "tags" => $tags,
            //     "contents" => ["en" => $message],
            //     "headings" => ["en" => $company_name],
            //     "data" => ["category" => "member"],
            //     "ios_badgeType" => "Increase",
            //     "ios_badgeCount" => 1
            // ];

            // // Log::info($dataNotif);
            // // SEND API REQUEST TO ONESIGNAL
            // // OneSignalFacade::postNotification($data);
            // dispatch(new SendOneSignalNotification($dataNotif));
            
            // //get all member id from tier
            // $members = Member::chunk(2000, function ($members) use ( $message, $company_name ) {
                    
            //         $player_ids = [];
            //         foreach ($members as $member) {

            //             if ( !is_null($member) ){
            //                 if (!empty($member->onesignal)) {
            //                     $player_ids[] = $member->onesignal;
            //                 }
            //             }
            //         }

            //         // SEND TO ONESIGNAL
            //         $dataNotif = [
            //             "contents" => ["en" => $message],
            //             "headings" => ["en" => $company_name],
            //             "data" => ["category" => "member"],
            //             "include_player_ids" => $player_ids,
            //             "ios_badgeType" => "Increase",
            //             "ios_badgeCount" => 1
            //         ];

            //         Log::info($player_ids);
            //         Log::info($dataNotif);

            //         // $job = (new SendOneSignalNotification($dataNotif))->delay(150);
            //         // dispatch($job);
            // });
                        
            // Log::info("OneSignalController/broadcastMessage END");
            // Log::info(Carbon::now(config('app.timezone')));

            // get all tier
            $tiers = Tier::all();

            $tags = [];
            foreach ($tiers as $tier) {
                if ( !is_null($tier) ){
                    // Log::info($tier->id);                    
                    $tags[] = ["key" => "tier_id", "relation" => "=", "value" => $tier->id];
                    $tags[] = ["operator" => "OR"];
                    $tags[] = ["key" => "tier_name", "relation" => "=", "value" => $tier->name];
                    $tags[] = ["operator" => "OR"];
                }
            }

            array_pop($tags);
            $dataNotif = [
                "tags" => $tags,
                "contents" => ["en" => $message],
                "headings" => ["en" => $company_name],
                "data" => ["category" => "member"],
                "ios_badgeType" => "Increase",
                "ios_badgeCount" => 1
            ];

            // Log::info($tags);
            // Log::info($dataNotif);

            dispatch(new SendOneSignalNotification($dataNotif));


            return response()->json(['status' => 'ok', 'message'=> 'Broadcast notification is successful']);

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }
    
    /**
     * Send push notification to group.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function broadcastGroupMessage(Request $request) {
        try {

            $validator = Validator::make($request->all(), [
                'tier_id' => 'required',
                'message' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }
            
            // Log::info("OneSignalController/broadcastGroupMessage START");
            // Log::info(Carbon::now(config('app.timezone')));

            //receive the input
            $tier_id = $request->input('tier_id');
            //get the tier from venue
            //$tiers = Tier::select('id')->whereIn('venue_id', $venue_id)->pluck('id');
                        
            $message = $request->input('message');
            $company_name = Setting::where('key', 'company_name')->first()->value;
            
            // //get all member id from tier
            // $tiermembers = MemberTiers::with('member.user')
            //     ->whereIn('tier_id', $tier_id)
            //     ->chunk(100, function ($tiermembers) use ( $message, $company_name ) {
                    
            //         $tags = [];
            //         foreach ($tiermembers as $tiermember) {

            //             if ( !is_null($tiermember->member) ){
            //                 // Log::info($tiermember->id);
            //                 //get all member have tier
            //                 $member = $tiermember->member;
            //                 $user = $member->user;

            //                 $tags[] = ["key" => "email", "relation" => "=", "value" => $user->email];
            //                 $tags[] = ["operator" => "OR"];
            //             }
            //         }

            //         array_pop($tags);
            //         $dataNotif = [
            //             "tags" => $tags,
            //             "contents" => ["en" => $message],
            //             "headings" => ["en" => $company_name],
            //             "data" => ["category" => "member"],
            //             "ios_badgeType" => "Increase",
            //             "ios_badgeCount" => 1
            //         ];

            //         // Log::info($tags);
            //         // Log::info($dataNotif);

            //         dispatch(new SendOneSignalNotification($dataNotif));

            //     });

            // Log::info(Carbon::now(config('app.timezone')));
            // //get all member id from tier
            // $tiermembers = MemberTiers::with('member.user')
            //     ->whereIn('tier_id', $tier_id)
            //     ->chunk(2000, function ($tiermembers) use ( $message, $company_name ) {
                    
            //         $player_ids = [];
            //         foreach ($tiermembers as $tiermember) {

            //             if ( !is_null($tiermember->member) ){
            //                 // Log::info($tiermember->id);
            //                 //get all member have tier
            //                 $member = $tiermember->member;
            //                 if (!empty($member->onesignal)) {
            //                     $player_ids[] = $member->onesignal;
            //                 }
            //             }
            //         }

            //         // SEND TO ONESIGNAL
            //         $dataNotif = [
            //             "contents" => ["en" => $message],
            //             "headings" => ["en" => $company_name],
            //             "data" => ["category" => "member"],
            //             "include_player_ids" => $player_ids,
            //             "ios_badgeType" => "Increase",
            //             "ios_badgeCount" => 1
            //         ];

            //         Log::info($player_ids);
            //         // Log::info($dataNotif);

            //         // $job = (new SendOneSignalNotification($dataNotif))->delay(150);
            //         // dispatch($job);
            // });

            // Log::info("OneSignalController/broadcastGroupMessage END");
            // Log::info(Carbon::now(config('app.timezone')));

            // get all tier
            $tiers = Tier::whereIn('id', $tier_id)->get();

            $tags = [];
            foreach ($tiers as $tier) {
                if ( !is_null($tier) ){
                    // Log::info($tier->id);                    
                    $tags[] = ["key" => "tier_id", "relation" => "=", "value" => $tier->id];
                    $tags[] = ["operator" => "OR"];
                    $tags[] = ["key" => "tier_name", "relation" => "=", "value" => $tier->name];
                    $tags[] = ["operator" => "OR"];
                }
            }

            array_pop($tags);
            $dataNotif = [
                "tags" => $tags,
                "contents" => ["en" => $message],
                "headings" => ["en" => $company_name],
                "data" => ["category" => "member"],
                "ios_badgeType" => "Increase",
                "ios_badgeCount" => 1
            ];

            // Log::info($tags);
            // Log::info($dataNotif);

            dispatch(new SendOneSignalNotification($dataNotif));

            return response()->json(['status' => 'ok', 'message'=> 'Broadcast Group notification is successful']);

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }
    
}

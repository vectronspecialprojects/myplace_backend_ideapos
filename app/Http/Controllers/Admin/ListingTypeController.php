<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Listing;
use App\ListingType;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;


class ListingTypeController extends Controller
{
    /**
     * Return all Listing types
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(Request $request)
    {
        try {

            $types = ListingType::all();

            return response()->json(['status' => 'ok', 'data' => $types]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Create a listing type
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function store(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'name' => 'required|unique:listing_types',
                'key' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $listingType = new ListingType;
            $listingType->name = $request->input('name');
            $listingType->key = $request->input('key');
            $listingType->save();

            return response()->json(['status' => 'ok', 'message' => 'Creating listing type is successful.']);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Delete a specific listing type
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function destroy($id)
    {
        try {
            $listingType = ListingType::find($id);

            if (is_null($listingType)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                $listing = Listing::where('listing_type_id', $id)->first();
                if (is_null($listing)) {
                    ListingType::destroy($id);
                    return response()->json(['status' => 'ok', 'message' => 'Deleting type is successful.']);
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Not safe. Some objects used this value'], Response::HTTP_BAD_REQUEST);
                }
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update a listing type
     * >> for Admin
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(Request $request, $id)
    {
        try {

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'key' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $listingType = ListingType::find($id);

            if (is_null($listingType)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            }

            $listingType->name = $request->input('name');
            $listingType->key = $request->input('key');
            $listingType->save();

            return response()->json(['status' => 'ok', 'message' => 'Updating type is successful.']);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return information for a specific listing type
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show($id)
    {
        try {
            $listingType = ListingType::with('listings')->find($id);

            if (is_null($listingType)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            }

            return response()->json(['status' => 'ok', 'data' => $listingType]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Hide listing type
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function hide(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $listingType = ListingType::find($request->input('id'));

            if (is_null($listingType)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            }

            $key = $listingType->key;
            $key->hide_this = true;

            $listingType->key = $key;
            $listingType->save();

            return response()->json(['status' => 'ok', 'message' => 'Hiding type is successful.', 'data' => $listingType]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * unHide listing type
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function unHide(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'id' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $listingType = ListingType::find($request->input('id'));

            if (is_null($listingType)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            }

            $key = $listingType->key;
            $key->hide_this = false;

            $listingType->key = $key;
            $listingType->save();

            return response()->json(['status' => 'ok', 'message' => 'unHiding type is successful.', 'data' => $listingType]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Listing;
use App\ListingPivotTag;
use App\ListingTag;
use Approached\LaravelImageOptimizer\ImageOptimizer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ListingPivotTagController extends Controller
{
    /**
     * Return all ListingTag
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index()
    {
        try {
            $ListingPivotTags = ListingPivotTag::with('listing','listing_tags')->get();

            return response()->json(['status' => 'ok', 'data' => $ListingPivotTags]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }


    /**
     * Search through all ListingTag
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function search(Request $request, ListingPivotTag $ListingPivotTag)
    {
        try {
            $ListingPivotTag = $ListingPivotTag->newQuery();

            if ($request->has('keyword')) {
                $ListingPivotTag->where(function ($query) use ($request) {

                    if (strpos($request->input('keyword'), ' ') !== false) {
                        $array = explode(" ", $request->input('keyword'));
                        $keyword = implode("%", $array);
                        $query->where('name', 'like', '%' . $keyword . '%');
                    } else {
                        $query->where('name', 'like', '%' . $request->input('keyword') . '%');
                    }

                });
            }

            $ListingPivotTag = $ListingPivotTag->get();
            
            return response()->json(['status' => 'ok', 'data' => $ListingPivotTag, 'request' => $request->all()]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Create a ListingTag
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function store(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'listing_id' => 'required',
                'listing_tag_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $ListingPivotTag = new ListingPivotTag;

            if ($request->has('listing_id')) {
                $ListingPivotTag->Listing_id = $request->input('listing_id');
            }

            if ($request->has('listing_name')) {
                $ListingPivotTag->Listing_name = $request->input('listing_name');
            }
            
            if ($request->has('listing_tag_id')) {
                $ListingPivotTag->Listing_tag_id = $request->input('listing_tag_id');
            }

            if ($request->has('listing_tags_name')) {
                $ListingPivotTag->Listing_tags_name = $request->input('listing_tags_name');
            }

            if ($request->has('status')) {
                $ListingPivotTag->status = $request->input('status');
            }

            $ListingPivotTag->save();

            return response()->json(['status' => 'ok', 'message' => 'Creating ListingTag is successful.']);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Delete a specific ListingTag
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function destroy($id)
    {
        try {
            $ListingPivotTag = ListingPivotTag::find($id);

            if (is_null($ListingPivotTag)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                ListingPivotTag::destroy($id);

                return response()->json(['status' => 'ok', 'message' => 'Deleting ListingTag is successful.']);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update a Listing
     * >> for Admin
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(Request $request, $id)
    {
        try {

            $ListingPivotTag = ListingPivotTag::find($id);

            if (is_null($ListingPivotTag)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            }

            if ($request->has('listing_id')) {
                $ListingPivotTag->Listing_id = $request->input('listing_id');
            }

            if ($request->has('listing_name')) {
                $ListingPivotTag->Listing_name = $request->input('listing_name');
            }
            
            if ($request->has('listing_tag_id')) {
                $ListingPivotTag->Listing_tag_id = $request->input('listing_tag_id');
            }

            if ($request->has('listing_tags_name')) {
                $ListingPivotTag->Listing_tags_name = $request->input('listing_tags_name');
            }

            if ($request->has('status')) {
                $ListingPivotTag->status = $request->input('status');
            }

            $ListingPivotTag->save();

            return response()->json(['status' => 'ok', 'message' => 'Updating ListingTag is successful.']);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return information for specific Listing
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show($id)
    {
        try {
            $ListingPivotTag = ListingPivotTag::find($id);

            if (is_null($ListingPivotTag)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                return response()->json(['status' => 'ok', 'data' => $ListingPivotTag]);
            }
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Change status
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function changeStatus(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'active' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();

            $ListingPivotTag = ListingPivotTag::find($request->input('id'));
            $ListingPivotTag->active = $request->input('active');
            $ListingPivotTag->save();

            DB::commit();
            return response()->json(['status' => 'ok', 'message' => 'Changing ListingTag status is successful.', 'data' => ['id' => $ListingPivotTag->id], 'requests' => $request->all()]);

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'requests' => $request->all()], Response::HTTP_BAD_REQUEST);
        }
    }

}

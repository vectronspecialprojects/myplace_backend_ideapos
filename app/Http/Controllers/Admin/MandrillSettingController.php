<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\MailLog;
use App\MandrillSetting;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class MandrillSettingController extends Controller
{
    /**
     * Return all settings
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index()
    {
        try {
            $settings = MandrillSetting::all();

            return response()->json(['status' => 'ok', 'data' => $settings]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return information for specific setting
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show($id)
    {
        try {
            $setting = MandrillSetting::find($id);

            if (is_null($setting)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {
                $setting->table;

                return response()->json(['status' => 'ok', 'data' => $setting]);
            }
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update setting
     *
     * @param Request $request
     * @param $key
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(Request $request, $key)
    {
        try {
            $setting = MandrillSetting::where('key', $key)->first();

            if (is_null($setting)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            }

            $setting->value = $request->input('value');
            $setting->save();

            return response()->json(['status' => 'ok', 'data' => 'Updating setting is successful.']);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'request' => $request->all()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return all logs
     *
     * @param Request $request
     * @return mixed
     */
    protected function paginateLogs(Request $request, MailLog $ml)
    {
        try {

            $obj = $ml->newQuery();

            $paginate_number = 10;
            if ($request->has('paginate_number')) {
                $paginate_number = $request->input('paginate_number');
            }

            if ($request->has('keyword')) {
                $obj->where(function ($query) use ($request) {

                    if (strpos($request->input('keyword'), ' ') !== false) {
                        $array = explode(" ", $request->input('keyword'));
                        $keyword = implode("%", $array);
                        $query->where('mandrill_response', 'like', '%' . $keyword . '%');
                        $query->orWhere('payload', 'like', '%' . $keyword . '%');
                    } else {
                        $query->where('mandrill_response', 'like', '%' . $request->input('keyword') . '%');
                        $query->orWhere('payload', 'like', '%' . $request->input('keyword') . '%');
                    }

                });
            }

            if ($request->has('showUnsent')) {
                if (intval($request->input('showUnsent')) === 1) {
                    $obj->where('mandrill_response', 'not like', '%"status":"sent"%');
                }
            }


            $obj->orderBy('created_at', 'desc');

            $logs = $obj->paginate($paginate_number);

            return response()->json(['status' => 'ok', 'data' => $logs]);

        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}

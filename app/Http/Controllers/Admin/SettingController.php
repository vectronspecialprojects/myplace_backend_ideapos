<?php

namespace App\Http\Controllers\Admin;

use App\Email;
use App\Helpers\IGT;
use App\Helpers\MetropolisARCOdyssey;
use App\Helpers\MandrillExpress;
use App\PendingJob;
use App\Setting;
use App\SystemLog;
use App\Tier;
use App\StripeCustomer;
use Approached\LaravelImageOptimizer\ImageOptimizer;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class SettingController extends Controller
{
    /**
     * Return all settings
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(Request $request)
    {
        try {

            if ($request->has('key')) {
                $settings = Setting::where('key', $request->input('key'))->first();

            } else {
                $settings = Setting::where('key', '<>', 'unlock_key')->get();

                foreach ($settings as $setting) {
                    $setting->table;
                }

            }

            return response()->json(['status' => 'ok', 'data' => $settings]);


        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }


    /**
     * Return information for specific setting
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show($key)
    {
        try {
            $setting = Setting::where('key', $key)->first();

            if (is_null($setting)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {
                $setting->table;

                return response()->json(['status' => 'ok', 'data' => $setting]);
            }
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update setting
     *
     * @param Request $request
     * @param $key
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(Request $request, $key)
    {
        try {
            $setting = Setting::where('key', $key)->first();

            if (is_null($setting)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            }

            if ($setting->category === 'stripe') {
                if (!$request->input('decrypted_token')->roles()->first()->control->root_access) {
                    return response()->json(['status' => 'error', 'message' => 'Insufficient access: root_access'], Response::HTTP_UNAUTHORIZED);
                }
            }

            $setting->value = $request->input('value');

            $setting->save();

            return response()->json(['status' => 'ok', 'data' => 'Updating setting is successful.']);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'request' => $request->all()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update setting (multiple)
     *
     * @param Request $request
     * @param $key
     * @return \Illuminate\Http\JsonResponse
     */
    protected function multipleUpdates(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'settings' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();
            // Log::info('settings');
            // Log::info($request->input('settings'));

            $settings = \GuzzleHttp\json_decode($request->input('settings'));

            if (!is_array($settings)) {
                $settings = [$settings];
            }

            foreach ($settings as $obj) {
                // Log::info(print_r($obj, true));
                if (isset($obj->value)) {

                    if (!empty($obj->value)) {
                        $setting = Setting::where('key', $obj->key)->first();

                        if ($setting->category === 'stripe') {
                            if (!$request->input('decrypted_token')->roles()->first()->control->root_access) {
                                return response()->json(['status' => 'error', 'message' => 'Insufficient access: root_access'], Response::HTTP_UNAUTHORIZED);
                            }
                        }

                        $setting->value = $obj->value;
                        $setting->save();

                        // Log::info($setting);

                        if ($setting->key == "default_tier") {
                            DB::table('tiers')->update(array('is_default_tier' => 0));
                            DB::table('tiers')->where('id', $setting->value)->update(array('is_default_tier' => 1));
                        }

                    } else {

                        if (is_numeric($obj->value)) {
                            $setting = Setting::where('key', $obj->key)->first();
                            // Log::info($setting);

                            if ($setting->category === 'stripe') {
                                if (!$request->input('decrypted_token')->roles()->first()->control->root_access) {
                                    return response()->json(['status' => 'error', 'message' => 'Insufficient access: root_access'], Response::HTTP_UNAUTHORIZED);
                                }
                            }

                            $setting->value = $obj->value;
                            $setting->save();

                            // Log::info($setting);
                        } else {
                            // Log::info("value not set");
                            $setting = Setting::where('key', $obj->key)->first();

                            $setting->value = '';
                            $setting->save();
                        }

                        //special hacks because no need value "default_card_number_prefix"
                        if ($obj->key == "default_card_number_prefix") {
                            $setting = Setting::where('key', $obj->key)->first();

                            $setting->value = $obj->value;
                            $setting->save();
                        }
                    }

                }

                if (isset($obj->extended_value)) {
                    $setting = Setting::where('key', $obj->key)->first();

                    if ($setting->category === 'stripe') {
                        if (!$request->input('decrypted_token')->roles()->first()->control->root_access) {
                            return response()->json(['status' => 'error', 'message' => 'Insufficient access: root_access'], Response::HTTP_UNAUTHORIZED);
                        }
                    }

                    $setting->extended_value = $obj->extended_value;
                    $setting->save();
                }
            }

            DB::commit();

            return response()->json(['status' => 'ok', 'data' => 'Updating multiple settings is successful.']);
        } catch (\Exception $e) {
            DB::rollBack();

            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'request' => $request->all()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Upload image (mobile gallery)
     *
     * @param Request $request
     * @param $key
     * @return \Illuminate\Http\JsonResponse
     */
    protected function uploadImage(Request $request, ImageOptimizer $imageOptimizer)
    {
        try {
            $validator = Validator::make($request->all(), [
                'image' => 'required|file',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            if ($request->hasFile('image')) {

                if (config('bucket.s3')) {
                    $timestamp = Carbon::now(config('app.timezone'))->timestamp;
                    $picture = $request->file('image');
                    $mime = explode('/', $picture->getMimeType());
                    $type = end($mime);
                    $imageOptimizer->optimizeUploadedImageFile($picture);
                    Storage::disk('s3')->put('settings/galleries/image_' . $timestamp . '.' . $type, file_get_contents($picture), 'public');
                    $url = "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/settings/galleries/image_" . $timestamp . '.' . $type . "?" . time();

                    return response()->json(['status' => 'ok', 'message' => 'Upload successful', 'data' => $url]);

                } else {
                    $log = new SystemLog();
                    $log->type = 'setting-error';
                    $log->humanized_message = 'Setting of AWS S3 bucket not found.';
                    $log->payload = 'SettingController.php/uploadImage';
                    $log->message = 'Setting of S3 bucket not found.';
                    $log->source = 'SettingController.php';
                    $log->save();

                    return response()->json(['status' => 'error', 'message' => 'Please contact Administrator.'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                return response()->json(['status' => 'error', 'message' => 'no image file'], Response::HTTP_BAD_REQUEST);

            }
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'request' => $request->all()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Upload Friend Referral background image
     *
     * @param Request $request
     * @param $key
     * @return \Illuminate\Http\JsonResponse
     */
    protected function uploadImageFFBackground(Request $request, ImageOptimizer $imageOptimizer)
    {
        try {
            $validator = Validator::make($request->all(), [
                'image' => 'required|file',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            if ($request->hasFile('image')) {

                if (config('bucket.s3')) {
                    $timestamp = Carbon::now(config('app.timezone'))->timestamp;
                    $picture = $request->file('image');
                    $mime = explode('/', $picture->getMimeType());
                    $type = end($mime);
                    $imageOptimizer->optimizeUploadedImageFile($picture);
                    Storage::disk('s3')->put('settings/friendReferral/image_' . $timestamp . '.' . $type, file_get_contents($picture), 'public');
                    $url = "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/settings/friendReferral/image_" . $timestamp . '.' . $type . "?" . time();

                    return response()->json(['status' => 'ok', 'message' => 'Upload successful', 'data' => $url]);

                } else {
                    $log = new SystemLog();
                    $log->type = 'setting-error';
                    $log->humanized_message = 'Setting of AWS S3 bucket not found.';
                    $log->payload = 'SettingController.php/uploadImageFFBackground';
                    $log->message = 'Setting of S3 bucket not found.';
                    $log->source = 'SettingController.php';
                    $log->save();

                    return response()->json(['status' => 'error', 'message' => 'Please contact Administrator.'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                return response()->json(['status' => 'error', 'message' => 'no image file'], Response::HTTP_BAD_REQUEST);

            }
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'request' => $request->all()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Upload Invoice logo
     *
     * @param Request $request
     * @param $key
     * @return \Illuminate\Http\JsonResponse
     */
    protected function uploadInvoiceLogo(Request $request, ImageOptimizer $imageOptimizer)
    {
        try {
            $validator = Validator::make($request->all(), [
                'image' => 'required|file',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            if ($request->hasFile('image')) {

                if (config('bucket.s3')) {
                    $timestamp = Carbon::now(config('app.timezone'))->timestamp;
                    $picture = $request->file('image');
                    $mime = explode('/', $picture->getMimeType());
                    $type = end($mime);
                    $imageOptimizer->optimizeUploadedImageFile($picture);
                    Storage::disk('s3')->put('settings/invoiceLogo/image_' . $timestamp . '.' . $type, file_get_contents($picture), 'public');
                    $url = "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/settings/invoiceLogo/image_" . $timestamp . '.' . $type . "?" . time();

                    return response()->json(['status' => 'ok', 'message' => 'Upload successful', 'data' => $url]);

                } else {
                    $log = new SystemLog();
                    $log->type = 'setting-error';
                    $log->humanized_message = 'Setting of AWS S3 bucket not found.';
                    $log->payload = 'SettingController.php/uploadInvoiceLogo';
                    $log->message = 'Setting of S3 bucket not found.';
                    $log->source = 'SettingController.php';
                    $log->save();

                    return response()->json(['status' => 'error', 'message' => 'Please contact Administrator.'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                return response()->json(['status' => 'error', 'message' => 'no image file'], Response::HTTP_BAD_REQUEST);

            }
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'request' => $request->all()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Upload Invoice Header logo
     *
     * @param Request $request
     * @param $key
     * @return \Illuminate\Http\JsonResponse
     */
    protected function uploadInvoiceHeaderLogo(Request $request, ImageOptimizer $imageOptimizer)
    {
        try {
            $validator = Validator::make($request->all(), [
                'image' => 'required|file',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            if ($request->hasFile('image')) {
                if (config('bucket.s3')) {
                    $timestamp = Carbon::now(config('app.timezone'))->timestamp;
                    $picture = $request->file('image');
                    $mime = explode('/', $picture->getMimeType());
                    $type = end($mime);
                    $imageOptimizer->optimizeUploadedImageFile($picture);
                    Storage::disk('s3')->put('settings/invoiceHeaderLogo/image_' . $timestamp . '.' . $type, file_get_contents($picture), 'public');
                    $url = "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/settings/invoiceHeaderLogo/image_" . $timestamp . '.' . $type . "?" . time();

                    return response()->json(['status' => 'ok', 'message' => 'Upload successful', 'data' => $url]);

                } else {
                    $log = new SystemLog();
                    $log->type = 'setting-error';
                    $log->humanized_message = 'Setting of AWS S3 bucket not found.';
                    $log->payload = 'SettingController.php/uploadInvoiceHeaderLogo';
                    $log->message = 'Setting of S3 bucket not found.';
                    $log->source = 'SettingController.php';
                    $log->save();

                    return response()->json(['status' => 'error', 'message' => 'Please contact Administrator.'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                return response()->json(['status' => 'error', 'message' => 'no image file'], Response::HTTP_BAD_REQUEST);

            }
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'request' => $request->all()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Upload Confirmation Logo
     *
     * @param Request $request
     * @param $key
     * @return \Illuminate\Http\JsonResponse
     */
    protected function uploadConfirmationLogo(Request $request, ImageOptimizer $imageOptimizer)
    {
        try {
            $validator = Validator::make($request->all(), [
                'image' => 'required|file',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            if ($request->hasFile('image')) {
                if (config('bucket.s3')) {
                    $timestamp = Carbon::now(config('app.timezone'))->timestamp;
                    $picture = $request->file('image');
                    $mime = explode('/', $picture->getMimeType());
                    $type = end($mime);
                    $imageOptimizer->optimizeUploadedImageFile($picture);
                    Storage::disk('s3')->put('settings/confirmationLogo/image_' . $timestamp . '.' . $type, file_get_contents($picture), 'public');
                    $url = "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/settings/confirmationLogo/image_" . $timestamp . '.' . $type . "?" . time();

                    return response()->json(['status' => 'ok', 'message' => 'Upload successful', 'data' => $url]);

                } else {
                    $log = new SystemLog();
                    $log->type = 'setting-error';
                    $log->humanized_message = 'Setting of AWS S3 bucket not found.';
                    $log->payload = 'SettingController.php/uploadConfirmationLogo';
                    $log->message = 'Setting of S3 bucket not found.';
                    $log->source = 'SettingController.php';
                    $log->save();

                    return response()->json(['status' => 'error', 'message' => 'Please contact Administrator.'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                return response()->json(['status' => 'error', 'message' => 'no image file'], Response::HTTP_BAD_REQUEST);

            }
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'request' => $request->all()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Send Helpdesk email
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendHelpdeskEmail(Request $request, MandrillExpress $mx)
    {
        try {
            $validator = Validator::make($request->all(), [
                'to' => 'required',
                'from' => 'required',
                'subject' => 'required',
                'message' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $sender = $request->input('from');
            $recipient = $request->input('to');
            $subject = $request->input('subject');
            $message = $request->input('message');

            $cc = $request->has('cc') ? $request->input('cc') : '';

            $company_name = Setting::where('key', 'company_name')->first()->value;
            $company_phone = Setting::where('key', 'company_phone')->first()->value;
            $url = config('bucket.APP_URL');

            // Send Referral Email
            $data = array(
                'FROM' => $sender,
                'TO' => $recipient,
                'CC' => $cc,
                'WHEN' => date("l jS \of F Y h:i:s A"),
                'URL' => $url,
                'SUBJECT' => $subject,
                'MESSAGE' => $message,
                'PROJECT_NAME' => 'Bepoz Loyalty App',
                'VENUE' => $company_name,
                'CONTACT_NUMBER' => $company_phone
            );

            if ($mx->init()) {
                $mx->send('helpdesk', 'Vectron Support Team', $recipient, $data);
            } else {
                $pj = new PendingJob();
                $pj->payload = \GuzzleHttp\json_encode($data);
                $pj->extra_payload = \GuzzleHttp\json_encode(array(
                    'email' => $recipient,
                    'category' => 'helpdesk',
                    'name' => 'Vectron Support Team'
                ));
                $pj->queue = "email";
                $pj->save();
            }


        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'request' => $request->all()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Verify unlock key
     *
     * @param Request $request
     * @param $key
     * @return \Illuminate\Http\JsonResponse
     */
    protected function unlockKey(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'password' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $key = Setting::where('key', 'unlock_key')->first();

            if ($key->value !== $request->input('password')) {
                return response()->json(['status' => 'error', 'message' => 'Invalid Password'], Response::HTTP_BAD_REQUEST);
            }

            return response()->json(['status' => 'ok', 'message' => 'Password Confirmed']);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'request' => $request->all()], Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Clear Stripe Customer (remove saved credit card obj)
     *
     * @param Request $request
     * @param $key
     * @return \Illuminate\Http\JsonResponse
     */
    protected function clearStripeCustomers(Request $request)
    {
        try {
            StripeCustomer::whereNull('deleted_at')->delete();

            return response()->json(['status' => 'ok', 'message' => 'Clearing Stripe customer objects is successful.']);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'request' => $request->all()], Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Get Manual Pdf file
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDownload()
    {
        $file = public_path() . "/doc/manual.pdf";

        // set pdf header
        $headers = array(
            'Content-Type: application/pdf',
        );

        return response()->download($file, 'manual.pdf', $headers);
    }

    protected function refreshGamingSettingIGT()
    {
        try {
            $igt = new IGT();

            $gaming_mandatory_field = Setting::where('key', 'gaming_mandatory_field')->first();
            $allGamingField = \GuzzleHttp\json_decode($gaming_mandatory_field->extended_value);
            // Log::info("Myplace Gaming Field");

            // echo print_r($allGamingField, true);
            // foreach ($allGamingField as $data) {
            //     echo $data->key." ".$data->fieldName."<br>";
            // }

            $resultGetMandatoryFields = $igt->GetMandatoryFields();
            // echo "<br>IGT Gaming Field<br>";

            // Loop mandatory fields get from IGT
            foreach ($resultGetMandatoryFields->values as $data) {
                // echo $data->key." ".$data->label."<br>";

                $fieldName = $data->label[0] . "";

                $this->setGamingSetting($allGamingField, $fieldName, $fieldName, true);
            }

            // Log::info(print_r($allGamingField, true));

            // Log::info("Myplace Gaming Field after insert");

            // NOT IN MANDATORY FIELD LIST
            $this->setGamingSetting($allGamingField, "Address", "Address", true);
            $this->setGamingSetting($allGamingField, "Zip Code", "Zip Code", true);

            // NOT IN MANDATORY FIELD LIST, NEED SAVE MULTIVALUES GET FROM IGT
            $resultGetCountriesList = $igt->GetCountriesList();
            $this->setGamingSetting($allGamingField, "Address Country ID", "Country", true, $resultGetCountriesList);

            // NOT IN MANDATORY FIELD LIST, DEFAULT COUNTRY AUSTRALIA
            $resultGetStatesList = $igt->GetStatesList(36);
            $this->setGamingSetting($allGamingField, "Address State ID", "State", true, $resultGetStatesList, true, "gamingstatelist/{countryID}");

            $savegaming = \GuzzleHttp\json_encode($allGamingField);
            $gaming_mandatory_field->extended_value = $savegaming;
            $gaming_mandatory_field->save();

            $settings = array();
            $settings['gaming_mandatory_field'] = $gaming_mandatory_field->extended_value;

            return response()->json(['status' => 'ok', 'data' => $settings]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function setGamingSetting(&$allGamingField, $fieldName, $fieldDisplayTitle, $displayInApp, $multiValues = null,
                                        $needCall = null, $paramCall = null)
    {
        // find setting already exist in database
        $filteredItems = array_filter($allGamingField, function ($f) use ($fieldName) {
            return $f->fieldName == $fieldName;
        });

        if ($filteredItems) {
            // Log::info("update mandatory gaming field");

            foreach ($allGamingField as $data) {
                // echo $data->key." ".$data->fieldName."<br>";
                if ($data->fieldName == $fieldName) {
                    $data->fieldDisplayTitle = $fieldDisplayTitle;
                    if ($multiValues) {
                        $data->fieldType = "Dropdown";
                        $data->multiValues = $multiValues;
                    }
                }
            }
        } else {
            // Log::info("insert new mandatory gaming field");
            $key = preg_replace('/\s+/', '_', strtolower($fieldName));

            $o = new \stdClass();
            $o->id = $key;
            $o->fieldName = $fieldName;
            $o->key = $key;
            $o->fieldType = "Text";
            $o->sendBepoz = false;
            $o->bepozFieldName = "";
            $o->fieldDisplayTitle = $fieldDisplayTitle;
            $o->sendGaming = true;
            $o->GamingFieldName = $fieldDisplayTitle;
            $o->default_value = "";

            if ($multiValues) {
                $o->fieldType = "Dropdown";
                $o->multiValues = $multiValues;
            } else {
                $o->multiValues = "";
            }

            $o->active = true;
            $o->required = true;
            $o->displayInApp = $displayInApp;

            if ($needCall) {
                $o->needCall = $needCall;
            } else {
                $o->needCall = false;
            }

            if ($paramCall) {
                $o->paramCall = $paramCall;
            } else {
                $o->paramCall = "";
            }

            $o->displayOrder = "";
            $o->info = "";

            $allGamingField[] = $o;
        }
    }

    protected function refreshGamingSettingOdyssey()
    {
        try {
            $odyssey = new MetropolisARCOdyssey();

            $gaming_mandatory_field = Setting::where('key', 'gaming_mandatory_field')->first();
            $allGamingField = \GuzzleHttp\json_decode($gaming_mandatory_field->extended_value);
            // Log::info("Myplace Gaming Field");

            $resultManageToken = $odyssey->ManageToken();
            $resultGender = $odyssey->GetLookupValues($resultManageToken, "Gender");
            $resultOccupation = $odyssey->GetLookupValues($resultManageToken, "Occupation");
            $resultTitle = $odyssey->GetLookupValues($resultManageToken, "Title");
            // echo "<br>IGT Gaming Field<br>";

            $resultGenderDecode = \GuzzleHttp\json_decode($resultGender);
            $resultOccupationDecode = \GuzzleHttp\json_decode($resultOccupation);
            $resultTitleDecode = \GuzzleHttp\json_decode($resultTitle);

            // Log::info($resultGender);
            // Log::info($resultOccupation);
            // Log::info($resultTitle);

            $this->setGamingSetting($allGamingField, "Gender", "Gender", true, $resultGenderDecode->body->result);
            $this->setGamingSetting($allGamingField, "Occupation", "Occupation", true, $resultOccupationDecode->body->result);
            $this->setGamingSetting($allGamingField, "Title", "Title", true, $resultTitleDecode->body->result);

            $this->setGamingSetting($allGamingField, "AddressType", "AddressType", true);
            $this->setGamingSetting($allGamingField, "Addresses", "Street Address", true);
            $this->setGamingSetting($allGamingField, "Suburb", "Suburb", true);
            $this->setGamingSetting($allGamingField, "PostCode", "PostCode", true);
            // $this->setGamingSetting($allGamingField, "State", "State", true);

            $savegaming = \GuzzleHttp\json_encode($allGamingField);
            $gaming_mandatory_field->extended_value = $savegaming;
            $gaming_mandatory_field->save();

            $settings = array();
            $settings['gaming_mandatory_field'] = $gaming_mandatory_field->extended_value;

            return response()->json(['status' => 'ok', 'data' => $settings]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function callCustomFieldOdyssey()
    {
        try {
            $odyssey = new MetropolisARCOdyssey();

            $gaming_mandatory_field = Setting::where('key', 'gaming_mandatory_field')->first();
            $allGamingField = \GuzzleHttp\json_decode($gaming_mandatory_field->extended_value);
            // Log::info("Myplace Gaming Field");

            $resultManageToken = $odyssey->ManageToken();
            $result = $odyssey->GetCustomFields($resultManageToken);

            return response()->json(['status' => 'ok', 'data' => $result]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function restoreGamingDefault()
    {
        try {
            $gaming_mandatory_field = Setting::where('key', 'gaming_mandatory_field')->first();
            $allGamingField = \GuzzleHttp\json_decode($gaming_mandatory_field->extended_value);
            // Log::info("Myplace Gaming Field");

            // CLEAR ALL
            unset($allGamingField);
            $allGamingField = array();

            $this->setGamingSetting($allGamingField, "FirstName", "FirstName", false);
            $this->setGamingSetting($allGamingField, "LastName", "LastName", false);
            $this->setGamingSetting($allGamingField, "PreferredName", "PreferredName", true);

            $savegaming = \GuzzleHttp\json_encode($allGamingField);
            $gaming_mandatory_field->extended_value = $savegaming;
            $gaming_mandatory_field->save();

            $settings = array();
            $settings['gaming_mandatory_field'] = $gaming_mandatory_field->extended_value;

            return response()->json(['status' => 'ok', 'data' => $settings]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }


}

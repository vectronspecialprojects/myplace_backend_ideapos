<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ExportSurvey;
use App\MemberAnswer;
use App\Question;
use App\Survey;
use App\SurveyQuestion;
use App\SurveyTier;
use App\Tier;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class SurveyController extends Controller
{
    /**
     * Return all surveys
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index()
    {
        try {
            $surveys = Survey::all();

            return response()->json(['status' => 'ok', 'data' => $surveys]);
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return all surveys (pagination)
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function paginateSurvey(Request $request, Survey $survey)
    {
        try {
            $obj = $survey->newQuery();

            if ($request->has('keyword')) {

                $obj->orWhere(function ($query) use ($request) {
                    if (strpos($request->input('keyword'), ' ') !== false) {
                        $array = explode(" ", $request->input('keyword'));
                        $keyword = implode("%", $array);
                        $query->where('title', 'like', '%' . $keyword . '%');
                    } else {
                        $query->where('title', 'like', '%' . $request->input('keyword') . '%');
                    }
                });

            }

            if ($request->has('orderBy') && $request->has('sortBy')) {
                $obj->orderBy($request->input('orderBy'), $request->input('sortBy'));
            }

            $surveys = $obj->paginate(10);

            return response()->json(['status' => 'ok', 'data' => $surveys]);


        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }


    /**
     * Create a survey
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function store(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'description' => 'required',
                'status' => 'required',
                'never_expired' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();

            $survey = new Survey;
            $survey->title = $request->input('title');
            $survey->description = $request->input('description');
            $survey->status = $request->input('status');
            $survey->never_expired = (bool)$request->input('never_expired');
            $survey->extra_setting = '';
            $survey->payload = '';

            if (!(bool)$request->input('never_expired')) {
                if ($request->has('date_expiry')) {
                    $survey->date_expiry = new Carbon($request->input('date_expiry'), $request->input('date_timezone'));
                    $survey->date_expiry->setTimezone(config('app.timezone'));
                } else {
                    $survey->date_expiry = Carbon::now(config('app.timezone'));
                }
            }

            if ($request->has('point_reward')) {
                $survey->point_reward = $request->input('point_reward');
            }

            if ($request->has('reward_type')) {
                $survey->reward_type = $request->input('reward_type');
            }

            if ($request->has('voucher_setup_reward')) {
                $survey->voucher_setup_reward = $request->input('voucher_setup_reward');
            }
            
            $survey->save();

            if ($request->has('questions')) {

                $questions = \GuzzleHttp\json_decode($request->input('questions'));
                foreach ($questions as $question) {
                    $obj = Question::find($question->id);

                    if (!is_null($obj)) {
                        $survey->questions()->save($obj, ['question_order' => $question->question_order]);
                    }

                }

            }

            if ($request->has('tiers')) {

                $tiers = \GuzzleHttp\json_decode($request->input('tiers'));
                foreach ($tiers as $tier) {
                    $obj = Tier::find($tier->id);

                    if (!is_null($obj)) {
                        $survey->tiers()->save($obj);
                    }
                }

            }

            DB::commit();

            return response()->json(['status' => 'ok', 'message' => 'Creating survey is successful.']);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Delete a specific survey
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function destroy(Request $request, $id)
    {
        try {
            $survey = Survey::find($id);

            if (is_null($survey)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                $ma = MemberAnswer::where('survey_id', $id)->first();
                if (is_null($ma)) {
                    DB::beginTransaction();
                    Survey::destroy($id);
                    DB::commit();

                    return response()->json(['status' => 'ok', 'message' => 'Deleting survey is successful.']);
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Not safe. Some objects used this value'], Response::HTTP_BAD_REQUEST);

                }
            }

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update a survey
     * >> for Admin
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(Request $request, $id)
    {
        try {

            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'description' => 'required',
                'status' => 'required',
                'never_expired' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $survey = Survey::find($id);

            if (is_null($survey)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();

            if ($request->has('title')) {
                $survey->title = $request->input('title');
            }

            if ($request->has('description')) {
                $survey->description = $request->input('description');
            }

            if ($request->has('status')) {
                $survey->status = $request->input('status');
            }

            $survey->never_expired = (bool)$request->input('never_expired');

            if ($request->has('point_reward')) {
                $survey->point_reward = $request->input('point_reward');
            }

            if ($request->has('reward_type')) {
                $survey->reward_type = $request->input('reward_type');
            }

            if ($request->has('voucher_setup_reward')) {
                $survey->voucher_setup_reward = $request->input('voucher_setup_reward');
            }

            if (!(bool)$request->input('never_expired')) {
                if ($request->has('date_expiry')) {
                    $survey->date_expiry = new Carbon($request->input('date_expiry'), $request->input('date_timezone'));
                    $survey->date_expiry->setTimezone(config('app.timezone'));
                } else {
                    $survey->date_expiry = Carbon::now();
                }
            }

            $survey->save();

            // update survey tier
            if ($request->has('tiers')) {

                $tiers = \GuzzleHttp\json_decode($request->input('tiers'));
                $ids = [];

                foreach ($tiers as $tier) {

                    $ids[] = $tier->id;

                    $st = SurveyTier::where('tier_id', $tier->id)->where('survey_id', $survey->id)->first();

                    if (is_null($st)) {
                        $obj = Tier::find($tier->id);

                        if (!is_null($obj)) {
                            $survey->tiers()->save($obj);
                        }
                    }
                }

                if (!empty($ids)) {
                    $tiers = SurveyTier::whereNotIn('tier_id', $ids)->where('survey_id', $survey->id)->get();
                    foreach ($tiers as $tier) {
                        $tier->delete();
                    }
                }

                if (empty($tiers)) {
                    SurveyTier::where('survey_id', $id)->delete();
                }

            }

            // update survey question
            if ($request->has('questions')) {

                $questions = \GuzzleHttp\json_decode($request->input('questions'));
                $ids = [];

                foreach ($questions as $question) {
                    if ($question->mode == 'deleted') {
                        SurveyQuestion::where('question_id', $question->id)->where('survey_id', $survey->id)->delete();
                    } else {
                        $ids[] = $question->id;

                        $sq = SurveyQuestion::where('question_id', $question->id)->where('survey_id', $survey->id)->first();

                        if (is_null($sq)) {
                            $obj = Question::find($question->id);

                            if (!is_null($obj)) {
                                $survey->questions()->save($obj, ['question_order' => $question->question_order]);
                            }
                        } else {
                            $sq->question_order = $question->question_order;
                            $sq->save();
                        }
                    }

                }

                if (!empty($ids)) {
                    $questions = SurveyQuestion::whereNotIn('question_id', $ids)->where('survey_id', $survey->id)->get();
                    foreach ($questions as $question) {
                        $question->delete();
                    }
                }

                if (empty($questions)) {
                    SurveyQuestion::where('survey_id', $id)->delete();
                }

            }

            DB::commit();

            return response()->json(['status' => 'ok', 'message' => 'Updating survey is successful.']);

        } catch (\Exception $e) {
            Log::error($e);
            DB::rollBack();
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return information for specific survey
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show(Request $request, $id)
    {
        try {
            $survey = Survey::with('questions', 'tiers')->find($id);

            if (is_null($survey)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {
                return response()->json(['status' => 'ok', 'data' => $survey]);
            }
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return information for specific survey
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function showDetails(Request $request, $id)
    {
        try {
            $survey = Survey::with('questions.answers', 'tiers', 'survey_answers.answr')->find($id);

            if (is_null($survey)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {
                foreach ($survey->survey_answers as $answer) {
                    if (intval($answer->answer_id) != 0) {
                        $answer->answer = $answer->answr;
                    } else {
                        if (!is_null($answer->answer)) {
                            $answer->answer = \GuzzleHttp\json_decode($answer->answer);
                        } else {
                            $answer->answer = '';
                        }
                    }
                }
                return response()->json(['status' => 'ok', 'data' => $survey]);
            }
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Check survey question
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function checkQuestionSurvey(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'survey_id' => 'required',
                'question_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            // check pivot
            $ma = MemberAnswer::where('survey_id', $request->input('survey_id'))
                ->where('question_id', $request->input('question_id'))
                ->first();

            if (is_null($ma)) {
                return response()->json(['status' => 'ok', 'message' => 'Checking completed.', 'data' => 'safe']);
            } else {
                return response()->json(['status' => 'ok', 'message' => 'Checking completed.', 'data' => 'unsafe']);
            }

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Check survey
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function checkSurvey(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'survey_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            // check pivot
            $ma = MemberAnswer::where('survey_id', $request->input('survey_id'))
                ->first();

            if (is_null($ma)) {
                return response()->json(['status' => 'ok', 'message' => 'Checking completed.', 'data' => 'safe']);
            } else {
                return response()->json(['status' => 'ok', 'message' => 'Checking completed.', 'data' => 'unsafe']);
            }

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Create survey report
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function generateSurveyReport(Request $request)
    {
        try {
            if ($request->has('start_year')) {
                $start_year = intval($request->input('start_year'));
                if ($start_year <= 1000 || $start_year > 2100) {
                    return response()->json(['status' => 'error', 'message' => 'invalid year'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $start_year = date('Y');
            }

            if ($request->has('start_month')) {
                $start_month = intval($request->input('start_month'));
                if ($start_month < 1 || $start_month > 12) {
                    return response()->json(['status' => 'error', 'message' => 'invalid month'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $start_month = date('n');
            }

            if ($request->has('start_date')) {
                $start_date = intval($request->input('start_date'));
                if ($start_date < 1 || $start_date > 31) {
                    return response()->json(['status' => 'error', 'message' => 'invalid date'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $start_date = date('j');
            }

            if ($request->has('end_year')) {
                $end_year = intval($request->input('end_year'));
                if ($end_year <= 1000 || $end_year > 2100) {
                    return response()->json(['status' => 'error', 'message' => 'invalid year'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $end_year = date('Y');
            }

            if ($request->has('end_month')) {
                $end_month = intval($request->input('end_month'));
                if ($end_month < 1 || $end_month > 12) {
                    return response()->json(['status' => 'error', 'message' => 'invalid month'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $end_month = date('n');
            }

            if ($request->has('end_date')) {
                $end_date = intval($request->input('end_date'));
                if ($end_date < 1 || $end_date > 31) {
                    return response()->json(['status' => 'error', 'message' => 'invalid date'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $end_date = date('j');
            }

            $start_calendar = Carbon::createFromDate($start_year, $start_month, $start_date, config('app.timezone'))->hour(0)->minute(0)->second(0);
            $end_calendar = Carbon::createFromDate($end_year, $end_month, $end_date, config('app.timezone'))->hour(23)->minute(59)->second(59);

            $survey = Survey::with('questions.answers', 'tiers', 'survey_answers.answr', 'survey_answers.member')
                ->with(['survey_answers' => function ($query) use ($start_calendar, $end_calendar) {
                    $query->whereRaw("(answer_member.created_at >= ? AND answer_member.created_at <= ?)", [$start_calendar->toDateTimeString(), $end_calendar->toDateTimeString()]);
                }])
                ->find($request->input('survey_id'));

            if (is_null($survey)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {
                foreach ($survey->survey_answers as $answer) {
                    if (intval($answer->answer_id) != 0) {
                        $answer->answer = $answer->answr;
                    } else {
                        $answer->answer = \GuzzleHttp\json_decode($answer->answer);
                    }
                }
                return response()->json(['status' => 'ok', 'data' => $survey]);
            }
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Export survey report
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getExportSurvey(Request $request)
    {
        try {
            if ($request->has('start_year')) {
                $start_year = intval($request->input('start_year'));
                if ($start_year <= 1000 || $start_year > 2100) {
                    return response()->json(['status' => 'error', 'message' => 'invalid year'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $start_year = date('Y');
            }

            if ($request->has('start_month')) {
                $start_month = intval($request->input('start_month'));
                if ($start_month < 1 || $start_month > 12) {
                    return response()->json(['status' => 'error', 'message' => 'invalid month'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $start_month = date('n');
            }

            if ($request->has('start_date')) {
                $start_date = intval($request->input('start_date'));
                if ($start_date < 1 || $start_date > 31) {
                    return response()->json(['status' => 'error', 'message' => 'invalid date'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $start_date = date('j');
            }

            if ($request->has('end_year')) {
                $end_year = intval($request->input('end_year'));
                if ($end_year <= 1000 || $end_year > 2100) {
                    return response()->json(['status' => 'error', 'message' => 'invalid year'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $end_year = date('Y');
            }

            if ($request->has('end_month')) {
                $end_month = intval($request->input('end_month'));
                if ($end_month < 1 || $end_month > 12) {
                    return response()->json(['status' => 'error', 'message' => 'invalid month'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $end_month = date('n');
            }

            if ($request->has('end_date')) {
                $end_date = intval($request->input('end_date'));
                if ($end_date < 1 || $end_date > 31) {
                    return response()->json(['status' => 'error', 'message' => 'invalid date'], Response::HTTP_BAD_REQUEST);
                }
            } else {
                $end_date = date('j');
            }

            $start_calendar = Carbon::createFromDate($start_year, $start_month, $start_date, config('app.timezone'))->hour(0)->minute(0)->second(0);
            $end_calendar = Carbon::createFromDate($end_year, $end_month, $end_date, config('app.timezone'))->hour(23)->minute(59)->second(59);

            $survey = Survey::with('questions.answers', 'tiers', 'survey_answers.answr', 'survey_answers.member')
                ->with(['survey_answers' => function ($query) use ($start_calendar, $end_calendar) {
                    $query->whereRaw("(answer_member.created_at >= ? AND answer_member.created_at <= ?)", [$start_calendar->toDateTimeString(), $end_calendar->toDateTimeString()]);
                }])
                ->find($request->input('survey_id'));

            foreach ($survey->survey_answers as $survey_answer) {
                // Log::info($survey_answer->answer);
                if ($survey_answer->answer){
                    $survey_answer->answer = \GuzzleHttp\json_decode($survey_answer->answer, true);
                }
            }
            $survey->from = $start_calendar;
            $survey->to = $end_calendar;

            $values = $survey->toArray();
            // Log::info($values);

            $today = Carbon::now(config('app.timezone'));

            return Excel::download(new ExportSurvey($values), 'Survey '. $today.'.xlsx');

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}

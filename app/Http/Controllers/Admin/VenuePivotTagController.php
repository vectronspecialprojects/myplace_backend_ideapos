<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Venue;
use App\VenuePivotTag;
use App\VenueTag;
use Approached\LaravelImageOptimizer\ImageOptimizer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class VenuePivotTagController extends Controller
{
    /**
     * Return all VenueTag
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index()
    {
        try {
            $venuePivotTags = VenuePivotTag::with('venue','venue_tags')->get();

            return response()->json(['status' => 'ok', 'data' => $venuePivotTags]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }


    /**
     * Search through all VenueTag
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function search(Request $request, VenuePivotTag $venuePivotTag, Venue $venueTag)
    {
        try {
            $venuePivotTag = $venuePivotTag->newQuery();

            if ($request->has('keyword')) {
                $venuePivotTag->where(function ($query) use ($request) {

                    if (strpos($request->input('keyword'), ' ') !== false) {
                        $array = explode(" ", $request->input('keyword'));
                        $keyword = implode("%", $array);
                        $query->where('venue_name', 'like', '%' . $keyword . '%');
                        $query->orWhere('venue_tags_name', 'like', '%' . $keyword . '%');
                    } else {
                        $query->where('venue_name', 'like', '%' . $request->input('keyword') . '%');
                        $query->orWhere('venue_tags_name', 'like', '%' . $request->input('keyword') . '%');
                    }

                });
            }

            $venuePivotTag = $venuePivotTag->get();
            
            return response()->json(['status' => 'ok', 'data' => $venuePivotTag, 'request' => $request->all()]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Create a venueTag
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function store(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'venue_id' => 'required',
                'venue_tag_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            $venuePivotTag = new VenuePivotTag;

            if ($request->has('venue_id')) {
                $venuePivotTag->venue_id = $request->input('venue_id');
            }

            if ($request->has('venue_name')) {
                $venuePivotTag->venue_name = $request->input('venue_name');
            }
            
            if ($request->has('venue_tag_id')) {
                $venuePivotTag->venue_tag_id = $request->input('venue_tag_id');
            }

            if ($request->has('venue_tags_name')) {
                $venuePivotTag->venue_tags_name = $request->input('venue_tags_name');
            }

            if ($request->has('status')) {
                $venuePivotTag->status = $request->input('status');
            }

            $venuePivotTag->save();

            return response()->json(['status' => 'ok', 'message' => 'Creating venueTag is successful.']);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Delete a specific venueTag
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function destroy($id)
    {
        try {
            $venuePivotTag = VenuePivotTag::find($id);

            if (is_null($venuePivotTag)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                VenuePivotTag::destroy($id);

                return response()->json(['status' => 'ok', 'message' => 'Deleting venueTag is successful.']);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update a venue
     * >> for Admin
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(Request $request, $id)
    {
        try {

            $venuePivotTag = VenuePivotTag::find($id);

            if (is_null($venuePivotTag)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            }

            if ($request->has('venue_id')) {
                $venuePivotTag->venue_id = $request->input('venue_id');
            }

            if ($request->has('venue_name')) {
                $venuePivotTag->venue_name = $request->input('venue_name');
            }
            
            if ($request->has('venue_tag_id')) {
                $venuePivotTag->venue_tag_id = $request->input('venue_tag_id');
            }

            if ($request->has('venue_tags_name')) {
                $venuePivotTag->venue_tags_name = $request->input('venue_tags_name');
            }

            if ($request->has('status')) {
                $venuePivotTag->status = $request->input('status');
            }

            $venuePivotTag->save();

            return response()->json(['status' => 'ok', 'message' => 'Updating venueTag is successful.']);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return information for specific venue
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show($id)
    {
        try {
            $venuePivotTag = VenuePivotTag::find($id);

            if (is_null($venuePivotTag)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {

                return response()->json(['status' => 'ok', 'data' => $venuePivotTag]);
            }
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Change status
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function changeStatus(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'active' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            DB::beginTransaction();

            $venuePivotTag = VenuePivotTag::find($request->input('id'));
            $venuePivotTag->active = $request->input('active');
            $venuePivotTag->save();

            DB::commit();
            return response()->json(['status' => 'ok', 'message' => 'Changing venueTag status is successful.', 'data' => ['id' => $venuePivotTag->id], 'requests' => $request->all()]);

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage(), 'requests' => $request->all()], Response::HTTP_BAD_REQUEST);
        }
    }

}

<?php

namespace App\Http\Controllers\Member;

use App\FAQ;
use Illuminate\Http\Response as HttpResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class FaqController extends Controller
{
    /**
     * Return all active faqs
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index()
    {
        try {
            $faqs = FAQ::where('status', 'active')->get();

            //Display the linebreaks in the app
            foreach ( $faqs as $faq)
            {
                $faq->answer = str_replace("\n", "<br>", $faq->answer);
            }

            return response()->json(['status' => 'ok', 'data' => $faqs]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], HttpResponse::HTTP_BAD_REQUEST);
        }
    }

}

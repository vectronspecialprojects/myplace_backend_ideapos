<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\ListingType;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class ListingTypeController extends Controller
{
    /**
     * Return all Listing types
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index()
    {
        try {
            $types = ListingType::where('display', true)
                ->get();

            return response()->json(['status' => 'ok', 'data' => $types]);
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}

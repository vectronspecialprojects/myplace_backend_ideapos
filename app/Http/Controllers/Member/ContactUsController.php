<?php

namespace App\Http\Controllers\Member;

use App\ContactUs;
use App\Http\Controllers\Controller;
use App\MemberLog;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class ContactUsController extends Controller
{
    protected function store(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'method' => 'required',
                'message' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
            }

            if($request->input('method') != "email" && $request->input('method') != "phone") {
                return response()->json(['status' => 'error', 'request' => $request->all(), 'message' => 'Invalid method.'], Response::HTTP_BAD_REQUEST);
            }

            $contact_us = new ContactUs;
            $contact_us->member_id = $request->input('decrypted_token')->member->id;
            $contact_us->message = $request->input('message');
            $contact_us->method = $request->input('method');

            if($request->has('time_of_call')) {
                $contact_us->time_of_call = $request->input('time_of_call');
            }

            $contact_us->save();

            $ml = new MemberLog();
            $ml->member_id = $request->input('decrypted_token')->member->id;
            $ml->message = $request->input('decrypted_token')->member->first_name . '(' . $request->input('decrypted_token')->member->id . ') has contacted Bepoz. ';
            $ml->after = $contact_us->toJson();
            $ml->save();

            return response()->json(['status' => 'ok', 'message' => 'Your message submitted. Thank you.']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}

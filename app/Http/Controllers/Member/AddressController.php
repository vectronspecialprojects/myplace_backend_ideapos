<?php

namespace App\Http\Controllers\Member;

use App\Address;
use App\BepozJob;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class AddressController extends Controller
{
    /**
     * Return all addresses of the user (home , office & postal)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(Request $request)
    {
        try {

            $user = $request->input('decrypted_token');
            if ($user->member->member_addresses->isEmpty()) {
                return response()->json(['status' => 'error', 'message' => 'no_address'], Response::HTTP_BAD_REQUEST);
            } else {
                return response()->json(['status' => 'ok', 'data' => $user->member->member_addresses]);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }


    /**
     * Updating Address
     * >> all parameters are optional
     *
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function update(Request $request, $id)
    {
        try {

            $user = $request->input('decrypted_token');

            $address = Address::find($id);

            if ($address->member_id != $user->member->id) {
                return response()->json(['status' => 'error', 'message' => 'this_is_not_your_address'], Response::HTTP_BAD_REQUEST);
            } else {

                DB::beginTransaction();

                if ($request->has('category')) {
                    $address->category = $request->input('category');
                }

                if ($request->has('payload')) {
                    $address->payload = $request->input('payload');
                }

                $address->save();

                if (!is_null($user->member->bepoz_account_id)) {
                    $data = array();
                    $data['member_id'] = $user->member->id;

                    $job = new BepozJob();
                    $job->queue = "update-account";
                    $job->payload = \GuzzleHttp\json_encode($data);
                    $job->available_at = time();
                    $job->created_at = time();
                    $job->save();
                }

                DB::commit();
            }

            return response()->json(['status' => 'ok', 'message' => 'Updating address successful.']);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Delete address
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function destroy(Request $request, $id)
    {
        try {
            $address = Address::find($id);

            if (is_null($address)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {
                Address::destroy($id);
                return response()->json(['status' => 'ok', 'message' => 'Deleting address is successful.']);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Adding Address
     * >> if the suburb_id is provided, then no need to add new suburb data
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $user = $request->input('decrypted_token');

            $address = new Address();

            $address->member_id = $user->member->id;

            if ($request->has('category')) {
                $address->category = $request->input('category');
            }

            if ($request->has('payload')) {
                $address->payload = $request->input('payload');
            }

            $address->save();

            if (!is_null($user->member->bepoz_account_id)) {
                $data = array();
                $data['member_id'] = $user->member->id;

                $job = new BepozJob();
                $job->queue = "update-account";
                $job->payload = \GuzzleHttp\json_encode($data);
                $job->available_at = time();
                $job->created_at = time();
                $job->save();
            }

            DB::commit();

            return response()->json(['status' => 'ok', 'message' => 'Adding address successful.']);

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

}

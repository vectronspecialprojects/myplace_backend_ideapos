<?php

namespace App\Http\Controllers\Member;

ini_set('max_execution_time', 300); //5 minutes

use App\ClaimedPromotion;
use App\GiftCertificate;
use App\Jobs\RetrieveVouchers;
use App\Listing;
use App\ListingFavorite;
use App\ListingLike;
use App\Member;
use App\MemberVouchers;
use App\Order;
use App\OrderDetail;
use App\Product;
use App\PrizePromotion;
use App\PromoAccount;
use App\Setting;
use App\User;
use App\Venue;
use App\VenuePivotTag;
use App\VenueTag;
use App\VoucherSetups;
use Barryvdh\DomPDF\PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Milon\Barcode\DNS1D;

class UserVoucherController extends Controller
{
    /**
     * Retrieve member vouchers.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index(Request $request)
    {
        try {
            //dispatch(new RetrieveVouchers($request->input('decrypted_token')->member));
            //product_type_id <> 2 instead product_type_id = 1
            $vouchers = MemberVouchers::with('member.tiers.venue')
                ->where('member_id', '=', $request->input('decrypted_token')->member->id)
                ->where(function ($query) use ($request) {
                    $query->whereHas('order_detail', function ($query) use ($request) {
                        $query->where('product_type_id', '<>', 2);
                    });
                    $query->orWhere('member_vouchers.category', 'bepoz');
                    $query->orWhere('member_vouchers.category', 'shop');
                })
                ->where('redeemed', 0)
                ->where('amount_left', '<>', 0)
                ->whereRaw("((year(expire_date) = 1 OR expire_date >= ?) OR expire_date IS NULL)", [Carbon::now(config('app.timezone'))])
                ->get();
            // Log::info("member vouchers count"); Log::info($vouchers->count());
            $image = Setting::where('key', 'invoice_logo')->first()->value;
            $ios_link = Setting::where('key', 'ios_link')->first()->extended_value;
            $android_link = Setting::where('key', 'android_link')->first()->extended_value;
            $address = Setting::where('key', 'address')->first()->extended_value;
            $email = Setting::where('key', 'email')->first()->extended_value;
            $telp = Setting::where('key', 'telp')->first()->extended_value;
            $fax = Setting::where('key', 'fax')->first()->extended_value;
            $open_and_close_hours = Setting::where('key', 'open_and_close_hours')->first()->extended_value;
            $pickup_and_delivery_hours = Setting::where('key', 'pickup_and_delivery_hours')->first()->extended_value;
            $social_links = Setting::where('key', 'social_links')->first()->extended_value;
            $menu_links = Setting::where('key', 'menu_links')->first()->extended_value;
            $hide_delivery_info = Setting::where('key', 'hide_delivery_info')->first()->extended_value;
            $hide_opening_hours_info = Setting::where('key', 'hide_opening_hours_info')->first()->extended_value;

            // DUMMY VENUE
            $venueDefault = collect(
                [
                    'id' => 0,
                    'name' => 'ALL VENUES',
                    'image' => $image,
                    'ios_link' => $ios_link,
                    'android_link' => $android_link,
                    'address' => $address,
                    'email' => $email,
                    'telp' => $telp,
                    'fax' => $fax,
                    'open_and_close_hours' => $open_and_close_hours,
                    'pickup_and_delivery_hours' => $pickup_and_delivery_hours,
                    'social_links' => $social_links,
                    'menu_links' => $menu_links,
                    'hide_delivery_info' => $hide_delivery_info,
                    'hide_opening_hours_info' => $hide_opening_hours_info
                ]);

            // Log::info("CALL USER VOUCHER");
            foreach ($vouchers as $voucher) {
                // Log::info($voucher);

                $cp = ClaimedPromotion::where('member_voucher_id', $voucher->id)->first();
                if (is_null($cp)) {
                    if ($voucher->order_id === 0) {
                        $voucherSetup = VoucherSetups::find($voucher->voucher_setup_id);

                        $tc = Setting::where('key', 'invoice_logo')->first();
                        
                        $image_square = $tc->value;
                        $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)
                            ->where('status', 'active')->first();

                        if (is_null($product)) {
                            $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)->first();
                        }

                        if (is_null($product)) {
                            $order = Order::where('id', $voucher->order_id)->first();
                            if (is_null($order)){
                                $image_square = $tc->value;
                            } else {
                                $image_square = $voucher->order->listing->image_square;
                            }
                        } else {
                            $image_square = $product->image;
                        }

                        $product_name = '';
                        if (!is_null($product)) {
                            if (!is_null($product->name) || $product->name != ""){
                                $product_name = $product->name;
                            }
                        } else {
                            if ( !is_null($voucherSetup) ){
                                $product_name = $voucherSetup->name;
                            }
                        }

                        $voucher->claim_promotion = collect(
                            [
                                'listing' => [
                                    // 'heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                    'heading' => $product_name,
                                    'image_square' => $tc->value],
                                'product' => [
                                    // 'name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                    'name' => $product_name,
                                    'desc_short' => 'This voucher was issued from the till.']
                            ]);
                        // Log::info("108");

                        $order_detail = OrderDetail::with('venue')->where('id', $voucher->order_details_id)->first();

                        $venue = $order_detail->venue;

                        if (intval($order_detail->venue_id) === 0){
                            // GET VENUE FROM BEPOZ claim venue id
                            if (intval($voucher->claim_venue_id) > 0) {
                                $venue = Venue::where('bepoz_venue_id', $voucher->claim_venue_id)->first();

                                if (is_null($venue)){
                                    $venue = $venueDefault;
                                }
                            } else {
                                // DUMMY VENUE
                                $venue = $venueDefault;
                            }
                        }

                        $voucher->order_detail = collect(
                            [
                                // 'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                'product_name' => $product_name,
                                'listing' => [
                                    // 'heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                    'heading' => $product_name,
                                    'image_square' => $image_square
                                ],
                                'status' => 'successful',
                                'venue' => $venue,
                                'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name]
                            ]);

                        $voucher->img = $tc->value;

                        if (is_null($product)) {
                            //$voucher->img = $voucher->order->listing->image_square;
                            $order = Order::where('id', $voucher->order_id)->first();
                            if (is_null($order)){
                                $voucher->img = $tc->value;
                            } else {
                                $voucher->img = $voucher->order->listing->image_square;
                            }
                        } else {
                            $voucher->img = $product->image;
                        }

                    } else {
                        $tc = Setting::where('key', 'invoice_logo')->first();
                        $voucherSetup = VoucherSetups::find($voucher->voucher_setup_id);
                        
                        $image_square = $tc->value;
                        $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)
                            ->where('status', 'active')->first();

                        if (is_null($product)) {
                            $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)->first();
                        }

                        if (is_null($product)) {
                            $order = Order::where('id', $voucher->order_id)->first();
                            if (is_null($order)){
                                $image_square = $tc->value;
                            } else {
                                if (!is_null($voucher->order->listing)) {
                                    $image_square = $voucher->order->listing->image_square;
                                } else {
                                    $image_square = $tc->value;
                                }
                            }
                        } else {
                            $image_square = $product->image;                            
                        }

                        $product_name = '';
                        if (!is_null($product)) {
                            if (!is_null($product->name) || $product->name != ""){
                                $product_name = $product->name;
                            }
                        } else {
                            if ( !is_null($voucherSetup) ){
                                $product_name = $voucherSetup->name;
                            }
                        }
                        
                        $order_detail = OrderDetail::with('venue')->where('id', $voucher->order_details_id)->first();
                        
                        $venue = $order_detail->venue;

                        if (intval($order_detail->venue_id) === 0){
                            // GET VENUE FROM BEPOZ claim venue id
                            if (intval($voucher->claim_venue_id) > 0) {
                                $venue = Venue::where('bepoz_venue_id', $voucher->claim_venue_id)->first();

                                if (is_null($venue)){
                                    $venue = $venueDefault;
                                }
                            } else {
                                // DUMMY VENUE
                                $venue = $venueDefault;
                            }
                        }
                        
                        $voucher->order_detail = collect(
                            [
                                // 'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                'product_name' => $product_name,
                                'listing' => [
                                    // 'heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                    'heading' => $product_name,
                                    'image_square' => $image_square
                                ],
                                'status' => 'successful',
                                'venue' => $venue,
                                // 'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name]
                                'voucher_setup' => ['name' => $product_name]
                            ]);
                        // Log::info("246");

                        $voucher->order = collect(
                            [
                                // 'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                'product_name' => $product_name,
                                'listing' => [
                                    // 'heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                    'heading' => $product_name,
                                    'image_square' => $image_square
                                ],
                                'status' => 'successful',
                                // 'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name]
                                'voucher_setup' => ['name' => $product_name]
                            ]);

                        $voucher->order->listing = collect(
                            [
                                // 'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                'product_name' => $product_name,
                                'listing' => [
                                    // 'heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                    'heading' => $product_name,
                                    'image_square' => $image_square
                                ],
                                'status' => 'successful',
                                // 'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                                'voucher_setup' => ['name' => $product_name],
                                'image_square' => $tc->value
                            ]);

                        //$voucher->order_detail->listing;
                        //$voucher->order_detail;
                        //$voucher->order_detail->voucher_setup;

                        if (is_null($voucher->order->listing)) {
                            $tc = Setting::where('key', 'invoice_logo')->first();
                            $voucher->img = $tc->value;
                        } else {
                            //CHANGE PRIORITIES PRODUCT IMAGE FIRST
                            //$voucher->img = $voucher->order->listing->image_square;

                            $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)
                                ->where('status', 'active')->first();

                            if (is_null($product)) {
                                $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)->first();
                            }
        
                            if (is_null($product)) {
                                //$voucher->img = $voucher->order->listing->image_square;
                                $voucher->img = $tc->value;
                            } else {
                                $voucher->img = $product->image;
                            }
                        }

                        $order = Order::find($voucher->order_id);
                        if (!is_null($order)) {
                            if (!is_null($order->payload)) {
                                $payload = \GuzzleHttp\json_decode($order->payload);
                                if ($payload->transaction_type == 'friend_referral') {
                                    $tc = Setting::where('key', 'friend_referral_voucher_background')->first();
                                    $voucherSetup = VoucherSetups::find($voucher->voucher_setup_id);
                                    
                                    //$voucher->claim_promotion = collect(
                                    //    [
                                    //        'listing' => [
                                    //            'heading' => $voucher->order_detail->voucher_setup->name,
                                    //            'image_square' => $tc->value],
                                    //        'product' => [
                                    //            'name' => $voucher->order_detail->voucher_setup->name,
                                    //            'desc_short' => 'This voucher was a friend referral reward.'
                                    //        ]
                                    //    ]);

                                    $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)
                                        ->where('status', 'active')->first();

                                    if (is_null($product)) {
                                        $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)->first();
                                    }

                                    if (is_null($product)) {
                                        $product_desc = 'This voucher was a friend referral reward.';
                                    } else {
                                        // Log::info("UserVoucherController 288");
                                        // Log::info($product);
                                        $product_desc = $product->desc_short;
                                    }

                                    $voucher->claim_promotion = collect(
                                        [
                                            'listing' => [
                                                // 'heading' => $voucherSetup->name,
                                                'heading' => $product_name,
                                                'image_square' => $tc->value],
                                            'product' => [
                                                // 'name' => $voucherSetup->name,
                                                'name' => $product_name,
                                                'desc_short' => $product_desc
                                            ]

                                        ]);
                                    $voucher->img = $tc->value;
                                    // Log::info("332");
                                } elseif ($payload->transaction_type == 'place_order') {
                                    $tc = Setting::where('key', 'friend_referral_voucher_background')->first();

                                    $voucherSetup = VoucherSetups::find($voucher->voucher_setup_id);

                                    //$voucher->claim_promotion = collect(
                                    //    [
                                    //        'listing' => [
                                    //            'heading' => $voucher->order_detail->voucher_setup->name,
                                    //            'image_square' => $tc->value],
                                    //        'product' => [
                                    //            'name' => $voucher->order_detail->voucher_setup->name,
                                    //            'desc_short' => 'This voucher was a friend referral reward.'
                                    //        ]
                                    //    ]);

                                    // $name = $voucher->name;

                                    // if (!is_null($voucherSetup)) {
                                    //     $name = $voucherSetup->name;
                                    // }

                                    $voucher->claim_promotion = collect(
                                        [
                                            'listing' => [
                                                // 'heading' => $name,
                                                'heading' => $product_name,
                                                'image_square' => $tc->value],
                                            'product' => [
                                                // 'name' => $name,
                                                'name' => $product_name,
                                                'desc_short' => 'This voucher was a friend referral reward.'
                                            ]

                                        ]);
                                    // Log::info("368");
                                    if (is_null($voucher->order->listing)) {
                                        $tc = Setting::where('key', 'invoice_logo')->first();
                                        $voucher->img = $tc->value;
                                    } else {

                                        //CHANGE PRIORITIES PRODUCT IMAGE FIRST
                                        //$voucher->img = $voucher->order->listing->image_square;

                                        $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)
                                            ->where('status', 'active')->first();

                                        if (is_null($product)) {
                                            $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)->first();
                                        }

                                        if (is_null($product)) {
                                            //$voucher->img = $voucher->order->listing->image_square;
                                            $order = Order::where('id', $voucher->order_id)->first();
                                            if (is_null($order)){
                                                $voucher->img = $tc->value;
                                            } else {
                                                $voucher->img = $image_square;
                                            }
                                        } else {
                                            $voucher->img = $product->image;
                                        }

                                        
                                        
                                    }

                                } else {
                                    
                                    $tc = Setting::where('key', 'invoice_logo')->first();
                                    $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)
                                        ->where('status', 'active')->first();

                                    if (is_null($product)) {
                                        $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)->first();
                                    }
                
                                    //$voucher->claim_promotion = collect(
                                    //    [
                                    //        'listing' => [
                                    //            'heading' => $voucher->order_detail->voucher_setup->name,
                                    //            'image_square' => $tc->value],
                                    //        'product' => [
                                    //            'name' => $voucher->order_detail->voucher_setup->name,
                                    //            'desc_short' => 'No description.']
                                    //    ]);

                                    if (is_null($product)) {
                                        $product_desc = 'No description.';
                                    } else {
                                        $product_desc = $product->desc_short;
                                    }
                                    
                                    $voucher->claim_promotion = collect(
                                        [
                                            'listing' => [
                                                // 'heading' => $voucher->name,
                                                'heading' => $product_name,
                                                'image_square' => $tc->value],
                                            'product' => [
                                                // 'name' => $voucher->name,
                                                'name' => $product_name,
                                                'desc_short' => $product_desc]
                                        ]);

                                    $voucher->img = $tc->value;

                                    if (is_null($product)) {
                                        //$voucher->img = $voucher->order->listing->image_square;
                                        $order = Order::where('id', $voucher->order_id)->first();
                                        if (is_null($order)){
                                            $voucher->img = $tc->value;
                                        } else {
                                            if (!is_null($voucher->order->listing)) {
                                                $temp = \GuzzleHttp\json_decode($voucher->order->listing);
                                                //Log::info($temp->image_square);
                                                $voucher->img = $temp->image_square;
                                            } else {
                                                $voucher->img = $tc->value;
                                            }
                                        }
                                    } else {
                                        $voucher->img = $product->image;
                                    }
                                    // Log::info("448");
                                }

                            }
                        }
                    }

                } else {
                    //$voucher->order_detail->listing;
                    //$voucher->order_detail->voucher_setup;
                    $tc = Setting::where('key', 'invoice_logo')->first();
                    $voucherSetup = VoucherSetups::find($voucher->voucher_setup_id);

                    $image_square = $tc->value;
                    $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)
                        ->where('status', 'active')->first();
                        
                    if (is_null($product)) {
                        $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)->first();
                    }

                    if (is_null($product)) {
                        $order = Order::where('id', $voucher->order_id)->first();
                        if (is_null($order)){
                            $image_square = $tc->value;
                        } else {
                            $image_square = $voucher->order->listing->image_square;
                        }
                    } else {
                        $image_square = $product->image;
                    }

                    $product_name = '';
                    if (!is_null($product)) {
                        if (!is_null($product->name) || $product->name != ""){
                            $product_name = $product->name;
                        }
                    } else {
                        if ( !is_null($voucherSetup) ){
                            $product_name = $voucherSetup->name;
                        }
                    }

                    $order_detail = OrderDetail::with('venue')->where('id', $voucher->order_details_id)->first();
                    
                    $venue = $order_detail->venue;

                    if (intval($order_detail->venue_id) === 0){
                        // GET VENUE FROM BEPOZ claim venue id
                        if (intval($voucher->claim_venue_id) > 0) {
                            $venue = Venue::where('bepoz_venue_id', $voucher->claim_venue_id)->first();

                            if (is_null($venue)){
                                $venue = $venueDefault;
                            }
                        } else {
                            // DUMMY VENUE
                            $venue = $venueDefault;
                        }
                    }
                    
                    $voucher->order_detail = collect(
                        [
                            // 'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                            'product_name' => $product_name,
                            'listing' => [
                                // 'heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                'heading' => $product_name,
                                'image_square' => $image_square
                            ],
                            'status' => 'successful',
                            'venue' => $venue,
                            'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name]
                        ]);

                    $voucher->order_detail->listing = collect(
                        [
                            // 'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                            'product_name' => $product_name,
                            'listing' => [
                                // 'heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                'heading' => $product_name,
                                'image_square' => $image_square
                            ],
                            'status' => 'successful',
                            'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                            'image_square' => $tc->value
                        ]);

                    $voucher->order_detail->voucher_setup = collect(
                        [
                            'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                            'listing' => ['heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                            'status' => 'successful',
                            'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name]
                        ]);
                        
                    $voucher->claim_promotion->listing;
                    $voucher->claim_promotion->product;
                    // Log::info("542");
                    
                    if (is_null($voucher->order->listing)) {
                        //$tc = Setting::where('key', 'invoice_logo')->first();
                        $voucher->img = $tc->value;
                    } else {
                        //CHANGE PRIORITIES PRODUCT IMAGE FIRST
                        //$voucher->img = $voucher->order->listing->image_square;

                        $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)
                            ->where('status', 'active')->first();
                            
                        if (is_null($product)) {
                            $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)->first();
                        }

                        if (is_null($product)) {
                            $order = Order::where('id', $voucher->order_id)->first();
                            if (is_null($order)){
                                $voucher->img = $tc->value;
                            } else {
                                $voucher->img = $voucher->order->listing->image_square;
                            }
                        } else {
                            
                            if ( $voucher->category === 'bepoz' ){
                                $voucher->img = $product->image;
                            } else {
                                $voucher->img = $voucher->order->listing->image_square;
                            }

                        }
                        
                    }
                }

            }
            // Log::info("vouchers after foreach");
            // Log::info($vouchers);

            $rejected = $vouchers->reject(function ($value, $key) {
                // Log::info("voucher ");Log::info($value->id);
                if ($value->order_id === 0) {//Log::info("529");
                    return false;
                } else {//Log::info("531");
                    
                    $voucherSetup = VoucherSetups::where('id', $value->voucher_setup_id)->first();
                    //Log::error($voucherSetup["expiry_date"]);
                    
                    //not return inactive
                    if ($voucherSetup["inactive"] == 1) {//Log::info("537");
                        return true;
                    }

                    if (is_null($value->expire_date) ) {//Log::info("537");
                        return false;
                    }
                    
                    $date_now = Carbon::now();
                    //$expiry_date = Carbon::parse($voucherSetup["expiry_date"]);
                    $expire_date = Carbon::parse($value->expire_date);
                    $date_diff = $date_now->diffInDays($expire_date, false);

                    //Log::error($value->voucher_setup_id);
                    //Log::error($voucherSetup["expiry_date"]);
                    //Log::error($date_diff);

                    // BYPASS YEAR 1
                    if ($expire_date->year == 1){
                        // continue check the other things
                        $value->expire_date = null;
                    } else {
                        // check other date with year > 1, not return expired
                        if ($date_diff < 0) {
                            return true;
                        }
                    }

                    $order = Order::find($value->order_id);
                    if (is_null($order)) {//Log::info("559");
                        return true; // rejected
                    } else {
                        if (is_null($order->payload)) {//Log::info("559");
                            return true;
                        } else {
                            $payload = \GuzzleHttp\json_decode($order->payload);
                            if (is_null($payload)) {//Log::info("567");
                                return true;
                            } else {
                                if (isset($payload->transaction_type)) {//Log::info("570");
                                    if ($payload->transaction_type != 'place_order') {//Log::info("572");
                                        $od = OrderDetail::find($value->order_details_id);
                                        if (is_null($od)) {//Log::info("573");
                                            return true;
                                        } else {
                                            if ($payload->transaction_type == 'friend_referral') {//Log::info("576");
                                                return false;
                                            }
                                            elseif ($payload->transaction_type == 'signup_reward') {//Log::info("578");
                                                return false;
                                            }
                                            else {//Log::info("582");
                                                $listing = Listing::find($od->listing_id);
                                                
                                                if ($value->category == 'shop') {//Log::info("585");
                                                    return false;
                                                }

                                                if (is_null($listing)) {//Log::info("589");
                                                    if ($value->category == 'bepoz') {//Log::info("590");
                                                        return false;
                                                    }
                                                    if ($value->category == 'survey_reward') {//Log::info("593");
                                                        return false;
                                                    }
                                                    //Log::info("596");
                                                    return true;
                                                } else {
                                                    if (is_null($listing->type->key)) {//Log::info("599");
                                                        return true;
                                                    } else {//Log::info("601");
                                                        return false;
                                                    }
                                                }
                                            }
                                        }
                                    } else {//Log::info("607");
                                        return false; // just editted
                                    }
                                } else {//Log::info("610");
                                    return false;
                                }
                            }
                        }
                    }
                }

            });
            // Log::info("member rejected count"); Log::info($rejected->count());
            foreach ($rejected as $v) {
                // REDUCE ARRAY SIZE
                unset($v->unlimited_use);
                unset($v->maximum_discount);
                // unset($v->claim_venue_id);
                // unset($v->claim_store_id);
                unset($v->issue_date);
                // unset($v->order_detail);
                // unset($v->order);
                unset($v->category);
                unset($v->used_count);
                unset($v->used_trans_id);
                unset($v->member);
                unset($v->separators);
            }

            $hqvenue = collect([
                'id' => 0,
                'name' => 'ALL VENUES',
                'image' => $image,
                'ios_link' => $ios_link,
                'android_link' => $android_link,
                'address' => $address,
                'email' => $email,
                'telp' => $telp,
                'fax' => $fax,
                'open_and_close_hours' => $open_and_close_hours,
                'pickup_and_delivery_hours' => $pickup_and_delivery_hours,
                'social_links' => $social_links,
                'menu_links' => $menu_links,
                'hide_delivery_info' => $hide_delivery_info,
                'hide_opening_hours_info' => $hide_opening_hours_info
            ]);

            $venues = Venue::with('tier', 'venue_tags')->where('active', 1)->get();
            $venues->prepend($hqvenue);

            $venueTagAll = VenueTag::
                with(
                    ['venue_pivot_tags' => function($q) {
                        $q->orderBy('display_order');
                    }]
                )
                ->where('status', 1)
                ->orderBy('display_order')
                ->get();

            $venue_tag_bgcolor = Setting::where('key', 'venue_tag_bgcolor')->first()->value;
            $venue_tags_enable = Setting::where('key', 'venue_tags_enable')->first()->value;

            return response()->json(['status' => 'ok', 'data' => $rejected->values(), 'venues' => $venues, 
                'venue_tags' => $venueTagAll, 'venue_tags_enable' => $venue_tags_enable, 'venue_tag_bgcolor' => $venue_tag_bgcolor]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Retrieve tickets.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function showAllTickets(Request $request)
    {
        try {
            $vouchers = MemberVouchers::where('member_id', '=', $request->input('decrypted_token')->member->id)
                ->whereHas('order_detail', function ($query) use ($request) {
                    $query->where('product_type_id', '=', 2);
                })
                ->where('redeemed', 0)
                ->where('amount_left', '<>', 0)
                ->whereRaw("((year(expire_date) = 1 OR expire_date >= ?) OR expire_date IS NULL)", [Carbon::now(config('app.timezone'))])
                ->get();

            $image = Setting::where('key', 'invoice_logo')->first()->value;
            $ios_link = Setting::where('key', 'ios_link')->first()->extended_value;
            $android_link = Setting::where('key', 'android_link')->first()->extended_value;
            $address = Setting::where('key', 'address')->first()->extended_value;
            $email = Setting::where('key', 'email')->first()->extended_value;
            $telp = Setting::where('key', 'telp')->first()->extended_value;
            $fax = Setting::where('key', 'fax')->first()->extended_value;
            $open_and_close_hours = Setting::where('key', 'open_and_close_hours')->first()->extended_value;
            $pickup_and_delivery_hours = Setting::where('key', 'pickup_and_delivery_hours')->first()->extended_value;
            $social_links = Setting::where('key', 'social_links')->first()->extended_value;
            $menu_links = Setting::where('key', 'menu_links')->first()->extended_value;
            $hide_delivery_info = Setting::where('key', 'hide_delivery_info')->first()->extended_value;
            $hide_opening_hours_info = Setting::where('key', 'hide_opening_hours_info')->first()->extended_value;
    
            // DUMMY VENUE
            $venueDefault = collect(
                [
                    'id' => 0,
                    'name' => 'ALL VENUES',
                    'image' => $image,
                    'ios_link' => $ios_link,
                    'android_link' => $android_link,
                    'address' => $address,
                    'email' => $email,
                    'telp' => $telp,
                    'fax' => $fax,
                    'open_and_close_hours' => $open_and_close_hours,
                    'pickup_and_delivery_hours' => $pickup_and_delivery_hours,
                    'social_links' => $social_links,
                    'menu_links' => $menu_links,
                    'hide_delivery_info' => $hide_delivery_info,
                    'hide_opening_hours_info' => $hide_opening_hours_info
                ]);

            foreach ($vouchers as $voucher) {
                $cp = ClaimedPromotion::where('member_voucher_id', $voucher->id)->first();

                if (is_null($cp)) {

                    if ($voucher->order_id === 0) {
                        $voucherSetup = VoucherSetups::find($voucher->voucher_setup_id);

                        $tc = Setting::where('key', 'invoice_logo')->first();
                        $voucher->claim_promotion = collect(
                            [
                                'listing' => [
                                    'heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                    'image_square' => $tc->value],
                                'product' => [
                                    'name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                    'desc_short' => 'This voucher was issued from the till.']
                            ]);
                        
                        // GET VENUE FROM BEPOZ claim venue id
                        if (intval($voucher->claim_venue_id) > 0) {
                            $venue = Venue::where('bepoz_venue_id', $voucher->claim_venue_id)->first();

                            if (is_null($venue)){
                                $venue = $venueDefault;
                            }
                        } else {
                            // DUMMY VENUE
                            $venue = $venueDefault;
                        }

                        $voucher->order_detail = collect(
                            [
                                'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                'listing' => ['heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                                'status' => 'successful',
                                'venue' => $venue,
                                'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name]
                            ]);

                        $voucher->img = $tc->value;


                    } else {

                        $voucher->order_detail->listing;
                        $voucher->order_detail->voucher_setup;

                        if (intval($voucher->order_detail->venue_id) > 0){
                            $voucher->order_detail->venue;
                        } else {
                            $voucher->order_detail->venue = $venueDefault;
                        }

                        if (is_null($voucher->order->listing)) {
                            $tc = Setting::where('key', 'invoice_logo')->first();
                            $voucher->img = $tc->value;
                        } else {
                            $voucher->img = $voucher->order->listing->image_square;
                        }

                        $order = Order::find($voucher->order_id);
                        if (!is_null($order)) {

                            if (!is_null($order->payload)) {

                                $payload = \GuzzleHttp\json_decode($order->payload);
                                if ($payload->transaction_type == 'friend_referral') {
                                    $tc = Setting::where('key', 'friend_referral_voucher_background')->first();
                                    $voucher->claim_promotion = collect(
                                        [
                                            'listing' => [
                                                'heading' => $voucher->order_detail->voucher_setup->name,
                                                'image_square' => $tc->value],
                                            'product' => [
                                                'name' => $voucher->order_detail->voucher_setup->name,
                                                'desc_short' => 'This voucher was a friend referral reward.'
                                            ]

                                        ]);
                                    $voucher->img = $tc->value;


                                } elseif ($payload->transaction_type == 'place_order') {
                                    $tc = Setting::where('key', 'friend_referral_voucher_background')->first();

                                    $voucher->claim_promotion = collect(
                                        [
                                            'listing' => [
                                                'heading' => $voucher->order_detail->voucher_setup->name,
                                                'image_square' => $tc->value],
                                            'product' => [
                                                'name' => $voucher->order_detail->voucher_setup->name,
                                                'desc_short' => 'This voucher was a friend referral reward.'
                                            ]

                                        ]);

                                    if (is_null($voucher->order->listing)) {
                                        $tc = Setting::where('key', 'invoice_logo')->first();
                                        $voucher->img = $tc->value;
                                    } else {
                                        $voucher->img = $voucher->order->listing->image_square;
                                    }

                                } else {

                                    $tc = Setting::where('key', 'invoice_logo')->first();
                                    $voucher->claim_promotion = collect(
                                        [
                                            'listing' => [
                                                'heading' => $voucher->order_detail->voucher_setup->name,
                                                'image_square' => $tc->value],
                                            'product' => [
                                                'name' => $voucher->order_detail->voucher_setup->name,
                                                'desc_short' => 'No description.']
                                        ]);
                                    $voucher->img = $tc->value;
                                    // Log::error("771");
                                }

                            }
                        }
                    }

                } else {

                    $voucher->order_detail->listing;
                    $voucher->order_detail->voucher_setup;
                    $voucher->claim_promotion->listing;
                    $voucher->claim_promotion->product;

                    if (intval($voucher->order_detail->venue_id) > 0){
                        $voucher->order_detail->venue;
                    } else {
                        $voucher->order_detail->venue = $venueDefault;
                    }

                    if (is_null($voucher->order->listing)) {
                        $tc = Setting::where('key', 'invoice_logo')->first();
                        $voucher->img = $tc->value;
                    } else {
                        $voucher->img = $voucher->order->listing->image_square;
                    }
                }
            }

            $rejected = $vouchers->reject(function ($value, $key) {
                $order = Order::find($value->order_id);
                if (is_null($order)) {
                    return true; // rejected
                } else {
                    if (is_null($order->payload)) {
                        return true;
                    } else {
                        $payload = \GuzzleHttp\json_decode($order->payload);
                        if (is_null($payload)) {
                            return true;
                        } else {
                            if (isset($payload->transaction_type)) {
                                if ($payload->transaction_type == 'place_order') {
                                    $od = OrderDetail::find($value->order_details_id);
                                    if (is_null($od)) {
                                        return true;
                                    } else {
                                        $listing = Listing::find($od->listing_id);
                                        if (is_null($listing)) {
                                            return true;
                                        } else {
                                            if (is_null($listing->type->key)) {
                                                return true;
                                            } else {
                                                return false;
                                            }
                                        }
                                    }
                                } else {
                                    return true;
                                }
                            } else {
                                return true;
                            }


                        }
                    }
                }
            });

            return response()->json(['status' => 'ok', 'data' => $rejected->values()]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Retrieve stamp Card Won.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function stampCardWon(Request $request)
    {
        try {
            //dispatch(new RetrieveVouchers($request->input('decrypted_token')->member));
            //product_type_id <> 2 instead product_type_id = 1
            // Log::info($request->input('decrypted_token'));
            
            $vouchers = MemberVouchers::where('member_id', '=', $request->input('decrypted_token')->member->id)
                // ->where(function ($query) use ($request) {
                    // $query->whereHas('order_detail', function ($query) use ($request) {
                    //     $query->where('product_type_id', '<>', 2);
                    // });
                    // $query->orWhere('member_vouchers.category', 'bepoz');
                // })
                ->where('category', 'bepoz')
                ->where('bepoz_prize_promo_id', '>', 0)
                ->where('redeemed', 0)
                ->where('amount_left', '<>', 0)
                ->whereRaw("((year(expire_date) = 1 OR expire_date >= ?) OR expire_date IS NULL)", [Carbon::now(config('app.timezone'))])
                ->get();
                
            $image = Setting::where('key', 'invoice_logo')->first()->value;
            $ios_link = Setting::where('key', 'ios_link')->first()->extended_value;
            $android_link = Setting::where('key', 'android_link')->first()->extended_value;
            $address = Setting::where('key', 'address')->first()->extended_value;
            $email = Setting::where('key', 'email')->first()->extended_value;
            $telp = Setting::where('key', 'telp')->first()->extended_value;
            $fax = Setting::where('key', 'fax')->first()->extended_value;
            $open_and_close_hours = Setting::where('key', 'open_and_close_hours')->first()->extended_value;
            $pickup_and_delivery_hours = Setting::where('key', 'pickup_and_delivery_hours')->first()->extended_value;
            $social_links = Setting::where('key', 'social_links')->first()->extended_value;
            $menu_links = Setting::where('key', 'menu_links')->first()->extended_value;
            $hide_delivery_info = Setting::where('key', 'hide_delivery_info')->first()->extended_value;
            $hide_opening_hours_info = Setting::where('key', 'hide_opening_hours_info')->first()->extended_value;

            $venue = collect(
                [
                    'id' => 0,
                    'name' => 'ALL VENUES',
                    'image' => $image,
                    'ios_link' => $ios_link,
                    'android_link' => $android_link,
                    'address' => $address,
                    'email' => $email,
                    'telp' => $telp,
                    'fax' => $fax,
                    'open_and_close_hours' => $open_and_close_hours,
                    'pickup_and_delivery_hours' => $pickup_and_delivery_hours,
                    'social_links' => $social_links,
                    'menu_links' => $menu_links,
                    'hide_delivery_info' => $hide_delivery_info,
                    'hide_opening_hours_info' => $hide_opening_hours_info
                ]);

            foreach ($vouchers as $voucher) {
                //Log::info($voucher);

                $cp = ClaimedPromotion::where('member_voucher_id', $voucher->id)->first();
                if (is_null($cp)) {
                    if ($voucher->order_id === 0) {
                        $voucherSetup = VoucherSetups::find($voucher->voucher_setup_id);

                        $tc = Setting::where('key', 'invoice_logo')->first();
                        $voucher->claim_promotion = collect(
                            [
                                'listing' => [
                                    'heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                    'image_square' => $tc->value],
                                'product' => [
                                    'name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                    'desc_short' => 'This voucher was issued from the till.']
                            ]);

                        $voucher->order_detail = collect(
                            [
                                'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                'listing' => ['heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                                'status' => 'successful',
                                'venue' => $venue,
                                'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name]
                            ]);

                        $voucher->img = $tc->value;

                    } else {
                        $tc = Setting::where('key', 'invoice_logo')->first();
                        $voucherSetup = VoucherSetups::find($voucher->voucher_setup_id);
                        
                        $voucher->order_detail = collect(
                            [
                                'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                'listing' => ['heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                                'status' => 'successful',
                                'venue' => $venue,
                                'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name]
                            ]);

                        $voucher->order = collect(
                            [
                                'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                'listing' => ['heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                                'status' => 'successful',
                                'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name]
                            ]);

                        $voucher->order->listing = collect(
                            [
                                'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                'listing' => ['heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                                'status' => 'successful',
                                'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                                'image_square' => $tc->value
                            ]);

                        //$voucher->order_detail->listing;
                        //$voucher->order_detail;
                        //$voucher->order_detail->voucher_setup;

                        if (is_null($voucher->order->listing)) {
                            $tc = Setting::where('key', 'invoice_logo')->first();
                            $voucher->img = $tc->value;
                        } else {
                            //CHANGE PRIORITIES PRODUCT IMAGE FIRST
                            //$voucher->img = $voucher->order->listing->image_square;

                            $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)
                                ->where('status', 'active')->first();

                            if (is_null($product)) {
                                $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)->first();
                            }

                            if (is_null($product)) {
                                //$voucher->img = $voucher->order->listing->image_square;
                                $voucher->img = $tc->value;
                            } else {
                                $voucher->img = $product->image;
                            }
                        }


                        $order = Order::find($voucher->order_id);
                        if (!is_null($order)) {
                            if (!is_null($order->payload)) {
                                $payload = \GuzzleHttp\json_decode($order->payload);
                                if ($payload->transaction_type == 'friend_referral') {
                                    $tc = Setting::where('key', 'friend_referral_voucher_background')->first();
                                    $voucherSetup = VoucherSetups::find($voucher->voucher_setup_id);

                                    //$voucher->claim_promotion = collect(
                                    //    [
                                    //        'listing' => [
                                    //            'heading' => $voucher->order_detail->voucher_setup->name,
                                    //            'image_square' => $tc->value],
                                    //        'product' => [
                                    //            'name' => $voucher->order_detail->voucher_setup->name,
                                    //            'desc_short' => 'This voucher was a friend referral reward.'
                                    //        ]
                                    //    ]);

                                    $voucher->claim_promotion = collect(
                                        [
                                            'listing' => [
                                                'heading' => $voucherSetup->name,
                                                'image_square' => $tc->value],
                                            'product' => [
                                                'name' => $voucherSetup->name,
                                                'desc_short' => 'This voucher was a friend referral reward.'
                                            ]

                                        ]);
                                    $voucher->img = $tc->value;


                                } elseif ($payload->transaction_type == 'place_order') {
                                    $tc = Setting::where('key', 'friend_referral_voucher_background')->first();

                                    $voucherSetup = VoucherSetups::find($voucher->voucher_setup_id);

                                    //$voucher->claim_promotion = collect(
                                    //    [
                                    //        'listing' => [
                                    //            'heading' => $voucher->order_detail->voucher_setup->name,
                                    //            'image_square' => $tc->value],
                                    //        'product' => [
                                    //            'name' => $voucher->order_detail->voucher_setup->name,
                                    //            'desc_short' => 'This voucher was a friend referral reward.'
                                    //        ]
                                    //    ]);

                                    $voucher->claim_promotion = collect(
                                        [
                                            'listing' => [
                                                'heading' => $voucherSetup->name,
                                                'image_square' => $tc->value],
                                            'product' => [
                                                'name' => $voucherSetup->name,
                                                'desc_short' => 'This voucher was a friend referral reward.'
                                            ]

                                        ]);

                                    if (is_null($voucher->order->listing)) {
                                        $tc = Setting::where('key', 'invoice_logo')->first();
                                        $voucher->img = $tc->value;
                                    } else {

                                        //CHANGE PRIORITIES PRODUCT IMAGE FIRST
                                        //$voucher->img = $voucher->order->listing->image_square;

                                        $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)
                                            ->where('status', 'active')->first();

                                        if (is_null($product)) {
                                            $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)->first();
                                        }
                    
                                        if (is_null($product)) {
                                            //$voucher->img = $voucher->order->listing->image_square;
                                            $order = Order::where('id', $voucher->order_id)->first();
                                            if (is_null($order)){
                                                $voucher->img = $tc->value;
                                            } else {
                                                $voucher->img = $voucher->order->listing->image_square;
                                            }
                                        } else {
                                            $voucher->img = $product->image;
                                        }

                                        
                                        
                                    }

                                } else {
                                    
                                    $tc = Setting::where('key', 'invoice_logo')->first();

                                    
                                    //$voucher->claim_promotion = collect(
                                    //    [
                                    //        'listing' => [
                                    //            'heading' => $voucher->order_detail->voucher_setup->name,
                                    //            'image_square' => $tc->value],
                                    //        'product' => [
                                    //            'name' => $voucher->order_detail->voucher_setup->name,
                                    //            'desc_short' => 'No description.']
                                    //    ]);

                                    //Log::info($voucherSetup);
                                    $voucher->claim_promotion = collect(
                                        [
                                            'listing' => [
                                                'heading' => $voucher->name,
                                                'image_square' => $tc->value],
                                            'product' => [
                                                'name' => $voucher->name,
                                                'desc_short' => 'No description.']
                                        ]);

                                    // Log::error("1064");
                                    $voucher->img = $tc->value;

                                }

                            }
                        }
                    }

                } else {
                    //$voucher->order_detail->listing;
                    //$voucher->order_detail->voucher_setup;
                    $tc = Setting::where('key', 'invoice_logo')->first();
                    $voucherSetup = VoucherSetups::find($voucher->voucher_setup_id);

                    $voucher->order_detail = collect(
                        [
                            'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                            'listing' => ['heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                            'status' => 'successful',
                            'venue' => $venue,
                            'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name]
                        ]);

                    $voucher->order_detail->listing = collect(
                        [
                            'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                            'listing' => ['heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                            'status' => 'successful',
                            'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                            'image_square' => $tc->value
                        ]);

                    $voucher->order_detail->voucher_setup = collect(
                        [
                            'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                            'listing' => ['heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                            'status' => 'successful',
                            'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name]
                        ]);
                        
                    $voucher->claim_promotion->listing;
                    $voucher->claim_promotion->product;

                    
                    if (is_null($voucher->order->listing)) {
                        //$tc = Setting::where('key', 'invoice_logo')->first();
                        $voucher->img = $tc->value;
                    } else {
                        //CHANGE PRIORITIES PRODUCT IMAGE FIRST
                        //$voucher->img = $voucher->order->listing->image_square;

                        $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)
                            ->where('status', 'active')->first();

                        if (is_null($product)) {
                            $product = Product::where('bepoz_voucher_setup_id', $voucher->voucher_setup_id)->first();
                        }
    
                        if (is_null($product)) {
                            $order = Order::where('id', $voucher->order_id)->first();
                            if (is_null($order)){
                                $voucher->img = $tc->value;
                            } else {
                                $voucher->img = $voucher->order->listing->image_square;
                            }
                        } else {
                            $voucher->img = $product->image;
                        }
                        
                    }
                }
            }

            $rejected = $vouchers->reject(function ($value, $key) {
                if ($value->order_id === 0) {
                    return false;
                } else {
                    
                    $voucherSetup = VoucherSetups::where('id', $value->voucher_setup_id)->first();
                    //Log::error($voucherSetup["expiry_date"]);
                    
                    //not return inactive
                    if ($voucherSetup["inactive"] == 1) {
                        return true;
                    }

                    $date_now = Carbon::now();
                    //$expiry_date = Carbon::parse($voucherSetup["expiry_date"]);
                    $expire_date = Carbon::parse($value->expire_date);
                    $date_diff = $date_now->diffInDays($expire_date, false);

                    //Log::error($value->voucher_setup_id);
                    //Log::error($voucherSetup["expiry_date"]);
                    //Log::error($date_diff);

                    //not return expired
                    if ($date_diff < 0) {
                        return true;
                    }

                    $order = Order::find($value->order_id);
                    if (is_null($order)) {
                        return true; // rejected
                    } else {
                        if (is_null($order->payload)) {
                            return true;
                        } else {
                            $payload = \GuzzleHttp\json_decode($order->payload);
                            if (is_null($payload)) {
                                return true;
                            } else {
                                if (isset($payload->transaction_type)) {
                                    if ($payload->transaction_type != 'place_order') {
                                        $od = OrderDetail::find($value->order_details_id);
                                        if (is_null($od)) {
                                            return true;
                                        } else {
                                            if ($payload->transaction_type == 'friend_referral') {
                                                return false;
                                            }
                                            elseif ($payload->transaction_type == 'signup_reward') {
                                                return false;
                                            }
                                            else {
                                                $listing = Listing::find($od->listing_id);
                                                if (is_null($listing)) {
                                                    if ($value->category == 'bepoz') {
                                                        return false;
                                                    }

                                                    return true;
                                                } else {
                                                    if (is_null($listing->type->key)) {
                                                        return true;
                                                    } else {
                                                        return false;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        return false; // just editted
                                    }
                                } else {
                                    return false;
                                }
                            }
                        }
                    }
                }

            });

            return response()->json(['status' => 'ok', 'data' => $rejected->values()]);

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Return information for specific member voucher
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    protected function show(Request $request, $id)
    {
        try {
            $voucher = MemberVouchers::where('member_id', '=', $request->input('decrypted_token')->member->id)
                ->where('id', $id)
                ->where('redeemed', 0)
                ->whereRaw("((year(expire_date) = 1 OR expire_date >= ?) OR expire_date IS NULL)", [Carbon::now(config('app.timezone'))])
                ->first();

            if (is_null($voucher)) {
                return response()->json(['status' => 'error', 'message' => 'not_found'], Response::HTTP_BAD_REQUEST);
            } else {
                $cp = ClaimedPromotion::where('member_voucher_id', $voucher->id)->first();
                if (is_null($cp)) {
                    if ($voucher->order_id === 0) {
                        $voucherSetup = VoucherSetups::find($voucher->voucher_setup_id);

                        $tc = Setting::where('key', 'invoice_logo')->first();
                        $voucher->claim_promotion = collect(
                            [
                                'listing' => [
                                    'heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                    'image_square' => $tc->value],
                                'product' => [
                                    'name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                    'desc_short' => 'This voucher was issued from the till.']
                            ]);

                        $voucher->order_detail = collect(
                            [
                                'product_name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name,
                                'listing' => ['heading' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name],
                                'status' => 'successful',
                                'voucher_setup' => ['name' => is_null($voucherSetup) ? 'Voucher' : $voucherSetup->name]
                            ]);

                        $voucher->img = $tc->value;

                    } else {
                        $voucher->order_detail->listing;
                        $voucher->order_detail->voucher_setup;

                        $order = Order::find($voucher->order_id);
                        if (!is_null($order)) {
                            if (!is_null($order->payload)) {
                                $payload = \GuzzleHttp\json_decode($order->payload);
                                if ($payload->transaction_type == 'friend_referral') {
                                    $tc = Setting::where('key', 'friend_referral_voucher_background')->first();
                                    $voucher->claim_promotion = collect(
                                        [
                                            'listing' => [
                                                'heading' => $voucher->order_detail->voucher_setup->name,
                                                'image_square' => $tc->value],
                                            'product' => [
                                                'name' => $voucher->order_detail->voucher_setup->name,
                                                'desc_short' => 'This voucher was a friend referral reward.'
                                            ]

                                        ]);
                                } elseif ($payload->transaction_type == 'place_order') {
                                    $tc = Setting::where('key', 'invoice_logo')->first();
                                    $voucher->claim_promotion = collect(
                                        [
                                            'listing' => [
                                                'heading' => $voucher->order_detail->voucher_setup->name,
                                                'image_square' => $tc->value],
                                            'product' => [
                                                'name' => $voucher->order_detail->voucher_setup->name,
                                                'desc_short' => 'This voucher was a friend referral reward.'
                                            ]

                                        ]);

                                    if (is_null($voucher->order->listing)) {
                                        $tc = Setting::where('key', 'invoice_logo')->first();
                                        $voucher->img = $tc->value;
                                    } else {
                                        $voucher->img = $voucher->order->listing->image_square;
                                    }

                                } else {
                                    $tc = Setting::where('key', 'invoice_logo')->first();
                                    $voucher->claim_promotion = collect(
                                        [
                                            'listing' => [
                                                'heading' => $voucher->order_detail->voucher_setup->name,
                                                'image_square' => $tc->value],
                                            'product' => [
                                                'name' => $voucher->order_detail->voucher_setup->name,
                                                'desc_short' => 'No description.']
                                        ]);
                                }
                            }
                        }
                    }

                } else {
                    $voucher->order_detail->listing;
                    $voucher->order_detail->voucher_setup;
                    $voucher->claim_promotion->listing;
                    $voucher->claim_promotion->product;

                    if (is_null($voucher->order_detail->listing)) {
                        $tc = Setting::where('key', 'invoice_logo')->first();
                        $voucher->img = $tc->value;
                    } else {
                        $voucher->img = $voucher->order_detail->listing->image_square;
                    }
                }

                return response()->json(['status' => 'ok', 'data' => $voucher]);
            }

        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function generateGiftCertificate($token)
    {
        try {
            $gf = GiftCertificate::where('token', $token)->first();
            
            //if (is_null($gf)) {
            //    return response()->json(['status' => 'error', 'message' => 'Token invalid'], Response::HTTP_BAD_REQUEST);
            //}
            
            if (is_null($gf)) {
                $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

                return View::make('errors.error', array('company_logo' => $invoice_logo, 'message' => 'Invalid Token'));
            }

            $payload = \GuzzleHttp\json_decode($gf->payload);
            $vs = VoucherSetups::find($payload->voucher_setups_id);

            if (is_null($vs)) {
                return response()->json(['status' => 'error', 'message' => 'Missing voucher'], Response::HTTP_BAD_REQUEST);
            }

            $data = array();
            $data['gf'] = $gf;
            $data['member'] = Member::find($gf->purchaser_id);
            $data['voucher'] = $vs;

            $dns = new DNS1D();

            $barcode = $dns->getBarcodePNGPath($gf->barcode, "EAN13", 2, 90, array(0, 0, 0));

            $data['barcode'] = array(
                'image' => $barcode
            );

            // header
            $data['header_setting'] = Setting::where('key', 'use_bepoz_header_as_default_header')->first()->value;
            $data['header_image'] = Setting::where('key', 'invoice_header_logo')->first()->value;

            $venue = Venue::find($gf->order->venue_id);
            if (is_null($venue)) {
                $data['company_logo'] = Setting::where('key', 'invoice_logo')->first()->value;
                $data['venue_name'] = Setting::where('key', 'company_name')->first()->value;
                $data['venue_address'] = Setting::where('key', 'company_street_number')->first()->value . ' ' . 
                    Setting::where('key', 'company_street_name')->first()->value. ' ' . 
                    Setting::where('key', 'company_suburb')->first()->value . ' ' . 
                    Setting::where('key', 'company_postcode')->first()->value . ' ' . 
                    Setting::where('key', 'company_state')->first()->value;
                $data['venue_contact'] = Setting::where('key', 'company_phone')->first()->value;
                $data['venue_email'] = Setting::where('key', 'company_email')->first()->value;
            } else {
                $data['company_logo'] = $venue->image;
                $data['venue_name'] = $venue->name;
                $data['venue_address'] = $venue->address;
                $data['venue_contact'] = $venue->telp;
                $data['venue_email'] = $venue->email;
            }

            // $pdf = App::make('dompdf.wrapper')->loadView('pdf.new-gift-certificate', $data);
            $pdf = App::make('dompdf.wrapper')->loadView('pdf.cowch-gift-certificate', $data);
            // $pdf->setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif', 'isHtml5ParserEnabled', true]);
            return $pdf->download('gift-certificate.pdf');

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function generateTicket($token)
    {
        try {
            $order = Order::where('token', $token)->first();

            if (is_null($order)) {
                $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

                return View::make('errors.error', array('company_logo' => $invoice_logo, 'message' => 'Invalid Token'));
            }

            if ($order->voucher_status === 'in_progress') {
                $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

                return View::make('success.success', array(
                    "message" => 'We are processing your voucher. Please wait and try again.',
                    'company_logo' => $invoice_logo
                ));
            }

            $member = Member::find($order->member_id);
            $data = array();

            $qty = 0;
            foreach ($order->order_details as $order_detail) {
                $qty += intval($order_detail->qty);
            }

            $listing = Listing::find($order->listing_id);

            $venue = Venue::find($order->venue_id);
            if (is_null($venue)) {
                $data['company_logo'] = Setting::where('key', 'invoice_logo')->first()->value;
                $data['venue_name'] = Setting::where('key', 'company_name')->first()->value;
                $data['venue_address'] = Setting::where('key', 'company_street_number')->first()->value . ' ' . Setting::where('key', 'company_street_name')->first()->value;
                $data['venue_address2'] = Setting::where('key', 'company_suburb')->first()->value . ' ' . Setting::where('key', 'company_postcode')->first()->value . ' ' . Setting::where('key', 'company_state')->first()->value;
                $data['venue_contact'] = Setting::where('key', 'company_phone')->first()->value;
                $data['venue_email'] = Setting::where('key', 'company_email')->first()->value;
            } else {
                $data['company_logo'] = $venue->image;
                $data['venue_name'] = $venue->name;
                $data['venue_address'] = $venue->address;
                $data['venue_contact'] = $venue->telp;
                $data['venue_email'] = $venue->email;
            }

            $data['order_id'] = $order->id;
            $data['event_name'] = $listing->heading;
            $data['event_date'] = date('d F Y', strtotime($listing->datetime_start));
            $data['event_time'] = date('h:i A', strtotime($listing->datetime_start));

            $data['ticket_count'] = $qty;
            $data['member'] = $member;
            $data['date'] = date('d F Y', strtotime($order->created_at));
            $data['total_price'] = $order->paid_total_price;
            $data['ticket_price'] = $order->order_details->first()->unit_price;

            $data['vouchers'] = [];
            $vouchers = MemberVouchers::where('order_id', $order->id)->get();

            $dns = new DNS1D();
            foreach ($vouchers as $voucher) {
                $data['vouchers'][] = array(
                    'lookup' => $voucher->barcode,
                    'image' => $dns->getBarcodePNGPath($voucher->barcode, "EAN13", 2, 90, array(0, 0, 0))
                );
            }

            // header
            $data['header_setting'] = Setting::where('key', 'use_bepoz_header_as_default_header')->first()->value;
            $data['header_image'] = Setting::where('key', 'invoice_header_logo')->first()->value;


            $pdf = App::make('dompdf.wrapper')->loadView('pdf.ticket', $data);
            return $pdf->download('ticket.pdf');

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function generateGiftCertificateBooking($token)
    {
        try {
            //$gf = GiftCertificate::where('token', $token)->first();
            $gfs = GiftCertificate::where('token', $token)->get();
            // Log::info($gfs);
            //if (is_null($gf)) {
            //    return response()->json(['status' => 'error', 'message' => 'Token invalid'], Response::HTTP_BAD_REQUEST);
            //}
            
            if (is_null($gfs)) {
                $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

                return View::make('errors.error', array('company_logo' => $invoice_logo, 'message' => 'Invalid Token'));
            }

            //if (empty($gfs)) {
            //    $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

            //    return View::make('errors.error', array('company_logo' => $invoice_logo, 'message' => 'Invalid Token'));
            //}
            
            foreach ($gfs as $gf) {
                $payload = \GuzzleHttp\json_decode($gf->payload);
                $vs = VoucherSetups::find($payload->voucher_setups_id);
                $gf['voucher'] = $vs;

                if (is_null($vs)) {
                    return response()->json(['status' => 'error', 'message' => 'Missing voucher'], Response::HTTP_BAD_REQUEST);
                }
                
                $dns = new DNS1D();

                $barcode = $dns->getBarcodePNGPath($gf->barcode, "EAN13", 2, 90, array(0, 0, 0));
                
                $gf['barcode'] = array(
                    'image' => $barcode
                );
            }
            
            $data = array();
            $data['gfs'] = $gfs;
            $data['member'] = Member::find($gfs[0]->purchaser_id);
            
            // header
            $data['header_setting'] = Setting::where('key', 'use_bepoz_header_as_default_header')->first()->value;
            $data['header_image'] = Setting::where('key', 'invoice_header_logo')->first()->value;

            $venue = Venue::find($gf->order->venue_id);
            if (is_null($venue)) {
                $data['company_logo'] = Setting::where('key', 'invoice_logo')->first()->value;
                $data['venue_name'] = Setting::where('key', 'company_name')->first()->value;
                $data['venue_address'] = Setting::where('key', 'company_street_number')->first()->value . ' ' . Setting::where('key', 'company_street_name')->first()->value;
                $data['venue_address2'] = Setting::where('key', 'company_suburb')->first()->value . ' ' . Setting::where('key', 'company_postcode')->first()->value . ' ' . Setting::where('key', 'company_state')->first()->value;
                $data['venue_contact'] = Setting::where('key', 'company_phone')->first()->value;
                $data['venue_email'] = Setting::where('key', 'company_email')->first()->value;
            } else {
                $data['company_logo'] = $venue->image;
                $data['venue_name'] = $venue->name;
                $data['venue_address'] = $venue->address;
                $data['venue_contact'] = $venue->telp;
                $data['venue_email'] = $venue->email;
            }

            $pdf = App::make('dompdf.wrapper')->loadView('pdf.gift-certificate-booking', $data);
        //    $pdf->setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif', 'isHtml5ParserEnabled', true]);
            return $pdf->download('credit-voucher.pdf');

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    protected function generateTicketBooking($token)
    {
        try {
            $order = Order::where('token', $token)->first();

            if (is_null($order)) {
                $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

                return View::make('errors.error', array('company_logo' => $invoice_logo, 'message' => 'Invalid Token'));
            }

            if ($order->voucher_status === 'in_progress') {
                $invoice_logo = Setting::where('key', 'invoice_logo')->first()->value;

                return View::make('success.success', array(
                    "message" => 'We are processing your voucher. Please wait and try again.',
                    'company_logo' => $invoice_logo
                ));
            }

            $member = Member::find($order->member_id);
            $data = array();

            $qty = 0;
            //loop only tickets
            foreach ($order->order_details_ticket_success as $order_detail) {
                $qty += intval($order_detail->qty);
            }

            $listing = Listing::find($order->listing_id);

            $venue = Venue::find($order->venue_id);
            if (is_null($venue)) {
                $data['company_logo'] = Setting::where('key', 'invoice_logo')->first()->value;
                $data['venue_name'] = Setting::where('key', 'company_name')->first()->value;
                $data['venue_address'] = Setting::where('key', 'company_street_number')->first()->value . ' ' . Setting::where('key', 'company_street_name')->first()->value;
                $data['venue_address2'] = Setting::where('key', 'company_suburb')->first()->value . ' ' . Setting::where('key', 'company_postcode')->first()->value . ' ' . Setting::where('key', 'company_state')->first()->value;
                $data['venue_contact'] = Setting::where('key', 'company_phone')->first()->value;
                $data['venue_email'] = Setting::where('key', 'company_email')->first()->value;
            } else {
                $data['company_logo'] = $venue->image;
                $data['venue_name'] = $venue->name;
                $data['venue_address'] = $venue->address;
                $data['venue_contact'] = $venue->telp;
                $data['venue_email'] = $venue->email;
            }

            $data['order_id'] = $order->id;
            $data['event_name'] = $listing->heading;
            $data['event_date'] = date('d F Y', strtotime($listing->datetime_start));
            $data['event_time'] = date('h:i A', strtotime($listing->datetime_start));

            $data['ticket_count'] = $qty;
            $data['member'] = $member;
            $data['date'] = date('d F Y', strtotime($order->created_at));
            $data['total_price'] = $order->paid_total_price;
            $data['ticket_price'] = $order->order_details->first()->unit_price;

            $data['vouchers'] = [];
            $request = "2";
            $vouchers = MemberVouchers::with('order_detail')
              ->whereHas('order_detail', function ($q1) use ($request) {
              $q1->where('product_type_id', $request );                
            });
          
            $vouchers = $vouchers->where('order_id', $order->id)->get();

            $dns = new DNS1D();
            foreach ($vouchers as $voucher) {
                $data['vouchers'][] = array(
                    'name' => $voucher->order_detail->product_name,
                    'lookup' => $voucher->barcode,
                    'image' => $dns->getBarcodePNGPath($voucher->barcode, "EAN13", 2, 90, array(0, 0, 0))
                );
            }

            // header
            $data['header_setting'] = Setting::where('key', 'use_bepoz_header_as_default_header')->first()->value;
            $data['header_image'] = Setting::where('key', 'invoice_header_logo')->first()->value;

            $pdf = App::make('dompdf.wrapper')->loadView('pdf.ticket-booking', $data);
            return $pdf->download('ticket.pdf');
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}
<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use App\ListingFavorite;
use App\ListingLike;
use App\ListingSchedule;

class SearchListingController extends Controller
{
    
    public function search(Request $request, $query)
    {
        try {
            //$query = '%'.$request->input('query').'%';
            $query = '%'.htmlspecialchars_decode($query).'%';

            $listing_ids = DB::Select("SELECT * FROM listings WHERE status = 'active' AND CONCAT(name, comment, heading, desc_short, desc_long) like ?;",[$query]);

            if (count($listing_ids))
            {
                $listing_ids = array_map(function($x){return $x->id;}, $listing_ids);
            }


            $listingSchedules = DB::table('listing_schedules')
                ->whereDate('date_start', '<=', Carbon::now(config('app.timezone'))->toDateString())
                ->whereDate('date_end', '>=', Carbon::now(config('app.timezone'))->toDateString())
                ->whereExists(function ($query) use ($request) {

                    if (isset($request->input('decrypted_token')->member)) {
                        if (!is_null($request->input('decrypted_token')->member->tiers()->first())) {
                            $query->select(DB::raw(1))
                                ->from('listing_schedule_listing_type')
                                ->where('listing_schedule_listing_type.tier_id', $request->input('decrypted_token')->member->tiers()->first()->id)
                                ->whereNull('listing_schedule_listing_type.deleted_at')
                                ->whereRaw('listing_schedules.id = listing_schedule_listing_type.listing_schedule_id');
                        } else {
                            $query->select(DB::raw(1))
                                ->from('listing_schedule_listing_type')
                                ->whereNull('listing_schedule_listing_type.deleted_at')
                                ->whereRaw('listing_schedules.id = listing_schedule_listing_type.listing_schedule_id');
                        }
                    } else {
                        $query->select(DB::raw(1))
                            ->from('listing_schedule_listing_type')
                            ->whereNull('listing_schedule_listing_type.deleted_at')
                            ->whereRaw('listing_schedules.id = listing_schedule_listing_type.listing_schedule_id');
                    }
                })
                ->whereExists(function ($query) {
                    $query->select(DB::raw(1))
                        ->from('listings')
                        ->where('status', 'active')
                        ->whereNull('listings.deleted_at')
                        ->whereRaw('listings.id = listing_schedules.listing_id');
                })
                ->where('status', 'active')
                ->whereNull('deleted_at')
                ->select('id')
                ->get();

            if (!empty($listingSchedules)) {
                $temp = [];
                foreach ($listingSchedules as $schedule) {
                    $temp[] = $schedule->id;
                }

                $listingSchedules = ListingSchedule::with('tags.tag', 'listing.type')
                    ->join('listings', 'listing_schedules.listing_id', '=', 'listings.id')
                    ->whereIn('listing_schedules.id', $temp)
                    ->orderBy('listings.date_start', 'asc')
                    ->get(['listing_schedules.*']);

                foreach ($listingSchedules as $scheduleKey => $schedule) {
                    $schedule->types = $schedule->types()->with('type')->get(['listing_schedule_listing_type.*']);
                    $listing = $schedule->listing;

                    if (isset($request->input('decrypted_token')->member)) {

                        $favorite = ListingFavorite::where('listing_id', $listing->id)->where('member_id', $request->input('decrypted_token')->member->id)->first();

                        if (is_null($favorite)) {
                            $listing->favorite = 0;
                        } else {
                            $listing->favorite = 1;
                        }

                        $like = ListingLike::where('listing_id', $listing->id)->where('member_id', $request->input('decrypted_token')->member->id)->first();

                        if (is_null($like)) {
                            $listing->like = 0;
                        } else {
                            $listing->like = 1;
                        }

                    }

//                    if ($listing->type->repetitive == 1) {
                    if (!is_null($listing->date_end)) {
                        $time_end = Carbon::parse($listing->date_end);
                        $listing->time_end = $time_end->toTimeString();
                    }

                    if (!is_null($listing->date_start)) {
                        $time_start = Carbon::parse($listing->date_start);
                        $listing->time_start = $time_start->toTimeString();
                    }
//                    }

//                    $listing->reservations = $listing->reservations()->where('active', 1)->get();

                    $products = $listing->products()->whereExists(function ($query) {
                        $query->select(DB::raw(1))
                            ->from('products')
                            ->where('status', 'active')
                            ->whereNull('products.deleted_at')
                            ->whereRaw('products.id = listing_products.product_id');
                    })->get(['listing_products.*']);

                    foreach ($products as &$pivot) {
                        $pivot->product;

                        if ($pivot->product->is_free) {
                            $pivot->product->unit_price = 0;
                            $pivot->product->point_price = 0;
                        }

                    }

                    $listing->products = $products;
                }
            }

            $listingSchedules = $listingSchedules->filter(function($x)use($listing_ids){ return in_array($x->listing->id, $listing_ids, true);});

            return response()->json(['status' => 'ok', 'message' => $listingSchedules]);


        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage().'['.$e->getLine().']'], Response::HTTP_BAD_REQUEST);
        }
    }
}

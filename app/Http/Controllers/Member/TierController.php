<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Setting;
use App\Tier;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class TierController extends Controller
{
    /**
     * Return all tiers
     * >> for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function index()
    {
        try {
            $tiers = Tier::where('is_active', true)
                ->where('display', true)
                ->where('key', '<>', 'normal')
                ->orderBy('is_default_tier', 'desc')
                ->get();

            foreach ($tiers as $tier) {
                if (intval($tier->venue_id) > 0) {
                    $tier->venue;
                } else {
                    $tier->venue = collect(
                        [
                            'id' => 0,
                            'name' => Setting::where('key', 'company_name')->first()->value,
                            'ios_link' => Setting::where('key', 'ios_link')->first()->extended_value,
                            'android_link' => Setting::where('key', 'android_link')->first()->extended_value
                        ]);
                }
            }

            $bepozcustomfield = Setting::where('key', 'bepozcustomfield')->first();
            $temp = \GuzzleHttp\json_decode($bepozcustomfield->extended_value, true);

            // // filter need dispaly in app
            // foreach ($temp as $key => $cf) {
            //     if (!$cf['required']) {
            //         unset($temp[$key]);
            //     }
            // }

            $bepozcustomfield_enable = Setting::where('key', 'bepozcustomfield_enable')->first()->value;
            if ($bepozcustomfield_enable == "true") {
                $bepozcustomfield->extended_value = array_values($temp);
            } else {
                foreach ($temp as $key => $cf) {
                        unset($temp[$key]);
                }
            }

            $form_input_style = Setting::where('key', 'form_input_style')->first()->value;
            $form_floating_title = Setting::where('key', 'form_floating_title')->first()->value;

            return response()->json(['status' => 'ok', 'data' => $tiers, 'form_input_style' => $form_input_style, 
                'form_floating_title' => $form_floating_title, 'bepozcustomfield' => $bepozcustomfield]);
            
        } catch (\Exception $e) {
            Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

}

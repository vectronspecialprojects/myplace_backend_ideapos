<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ListingPivotTag extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'listing_pivot_tags';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $casts = [

    ];

    /**
     * Defines The tier of listingLinkTag
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */

    /**
     * Get the related listing
     * >> Define relationship
     *
     */
    public function listing()
    {
        return $this->belongsTo('App\Listing', 'listing_id');
    }

    /**
     * Get the related listingTag
     * >> Define relationship
     *
     */
    public function listing_tags()
    {
        return $this->belongsTo('App\ListingTag', 'listing_tags_id');
    }

}

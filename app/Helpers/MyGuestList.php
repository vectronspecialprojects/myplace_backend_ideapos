<?php

namespace App\Helpers;

use App\Helpers\Contracts\MyGuestListContract;

class MyGuestList implements MyGuestListContract
{
    private $apiKey = null;
    private $clientId = null;

    public function __construct()
    {
        $this->apiKey = env('MYGUESTLIST_KEY');
        $this->clientId = env('MYGUESTLIST_CLIENT_ID');
    }

    public function add_attendees($data)
    {
        $url = "https://api.myguestlist.com.au/add_attendees.php";

        $request = array(
            "apikey" =>  $this->apiKey,
            "guestlistid" => $data['guestlistid'],
            "attendees" => urlencode($data['attendees'])
        );

        return $this->callAPI($url, $request);
    }

    public function add_guestlist($data)
    {
        $url = "https://api.myguestlist.com.au/add_guestlist.php";

        $request = array(
            "apikey" =>  $this->apiKey,
            "PatronName" => urlencode($data['PatronName']),
            "PatronSurname" => urlencode($data['PatronSurname']),
            "PatronMobile" => urlencode($data['PatronMobile']),
            "Sex" => urlencode($data['Sex']),
            "PatronEmail" => urlencode($data['PatronEmail']),
            "GuestListName" => urlencode($data['GuestListName']),
            "Date" => urlencode($data['Date']), // YYYY-MM-DD
            "Time" => urlencode($data['Time']), // HH:MM:SS
            "Comments" => urlencode($data['Comments']),
            "Type" => urlencode($data['Type'])
        );

        return $this->callAPI($url, $request);
    }

    protected function callAPI($url, $data)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url."?".http_build_query($data),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        return $response;
//
//        if ($err) {
//            echo "cURL Error #:" . $err;
//        } else {
//            echo $response;
//        }
    }
}
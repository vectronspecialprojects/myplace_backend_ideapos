<?php

namespace App\Helpers\Contracts;

Interface PayWayContract
{
    public function checkNetwork();

    public function createSingleUseToken($cardNumber, $expiryDateMonth, $expiryDateYear, $cvn, $cardholderName);

    public function createCustomer($token);

    public function transaction($uuid, $customerNumber, $amount, $orderNumber);
}
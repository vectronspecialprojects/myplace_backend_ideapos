<?php

namespace App\Helpers\Contracts;

Interface MyGuestListContract
{
    public function add_guestlist($data);

    public function add_attendees($data);
}
<?php

namespace App\Helpers\Contracts;

Interface MetropolisARCOdysseyContract
{
    public function Connect();
    public function ChallengeResponse($connectResult);
    public function AddMember($sessionToken, $Gender, $Title, $FirstName, $LastName, $PreferredName, $BirthDate, $Occupation,
        $AddressType, $AddressLine1, $Suburb, $PostCode, $State);
    public function GetSessionInfo($sessionToken);
    public function ManageToken();
    public function GetLookupValues($sessionToken, $ListType);
    public function GetCustomFields($sessionToken);
    public function GetMember($sessionToken, $BadgeNo);
    public function GetMemberNames();
    public function GetMemberDetail();
    public function GetMemberAddresses();
    public function GetMemberContactChannels();
    public function GetMemberCustomFields();
    public function GetMemberPhoto();
    public function GetMembers();
    public function AddPreliminaryMember($sessionToken, $Gender, $Title, $FirstName, $LastName, $PreferredName, $BirthDate, $Occupation,
        $AddressType, $AddressLine1, $Suburb, $PostCode, $State, $EmailAddress, $Mobilephone, $venue);
    public function UpdateMemberDetail();
    public function UpdateMemberAddress();
    public function UpdateMemberContactChannels();
    public function UpdateMemberCustomFields();


}

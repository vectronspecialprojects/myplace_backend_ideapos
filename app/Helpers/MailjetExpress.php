<?php

namespace App\Helpers;

use App\Email;
use App\Helpers\Contracts\MailjetExpressContract;
use App\Helpers\Mailjet;
use App\MailLog;
use App\MandrillSetting;
use App\PendingJob;
use App\Setting;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class MailjetExpress implements MailjetExpressContract
{
    private $setting;
    private $mailjet;

    public function __construct()
    {
        require_once(app_path() . '/Helpers/Mailjet.php');
    }

    public function init()
    {
        try {

            /*
            $this->apikey = Setting::select('value')->where('key', 'mailjet_api_key')->first()->value;
            $this->secretkey = Setting::select('value')->where('key', 'mailjet_secret_key')->first()->value;


            $setting = Setting::where('key', 'mandrill_key')->first();

            if (is_null($setting)) {
                return false;
            }

            $this->setting = $setting;
            $this->mandrill = new \Mandrill($this->setting->value);
            $temp = $this->mandrill->users->ping2();

            if (isset($temp['PING'])) {
                return true;
            }

            Log::error($temp);
            return false;
            */

            $this->mailjet = new Mailjet;
            return true;

        } catch (\Exception $e) {
            Log::error($e);
            return false;
        }
    }

    public function send($category, $name, $recipient_email, $data)
    {
        try {
            $email = Email::where('uid', $category)->first();

            $template_name = MandrillSetting::where('email_id', $email->id)->where('key', 'template')->first()->value;
            $template_id = MandrillSetting::where('email_id', $email->id)->where('key', 'template_id')->first()->value;
            
            $subject = MandrillSetting::where('email_id', $email->id)->where('key', 'subject')->first();

            $mandrill_email = Setting::where('key', 'mandrill_email')->first();

            $mandrill_sender_name = Setting::where('key', 'mandrill_sender_name')->first();

            $company_name = Setting::where('key', 'company_name')->first()->value;

            $company_email = Setting::where('key', 'company_email')->first()->value;

            $template_content = array(
                "Messages" => array(
                    array(
                        "From" => array(
                            "Email" => $mandrill_email->value,
                            "Name" => $mandrill_sender_name->value
                        ),
                        "To" => array(
                            array(
                                "Email" => $recipient_email,
                                "Name" => $name
                            )
                        ),
                        "TemplateID" => (int) $template_id,
                        "TemplateLanguage" => true,
                        "Subject" => $subject->value,
                        "Variables" => $data
                    )
                )
            );

            //Log::info( \GuzzleHttp\json_encode($template_content) );

            
            //$mail = new Mailjet;
            $result = $this->mailjet->send( \GuzzleHttp\json_encode($template_content) );

            $ml = new MailLog();
            $ml->mandrill_response = \GuzzleHttp\json_encode($result);
            $ml->payload = \GuzzleHttp\json_encode($template_content);
            $ml->save();

            if (isset($result["status"])) {

                $pj = PendingJob::where('queue', 'email')
                    ->where('payload', \GuzzleHttp\json_encode($data))
                    ->where('extra_payload', \GuzzleHttp\json_encode(array(
                        'email' => $email,
                        'category' => $category,
                        'name' => $name
                    )))
                    ->first();

                if (is_null($pj)) {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $email,
                        'category' => $category,
                        'name' => $name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }

            } else {

                $pj = PendingJob::where('queue', 'email')
                    ->where('payload', \GuzzleHttp\json_encode($data))
                    ->where('extra_payload', \GuzzleHttp\json_encode(array(
                        'email' => $email,
                        'category' => $category,
                        'name' => $name
                    )))
                    ->first();

                if (!is_null($pj)) {
                    $pj->delete();
                }

            }

            $humanizedResult = "";
            if (isset($result["Messages"]["Status"])) {
                $humanizedResult = "Email sent";
            } else if ( $result === "false" ) {
                $humanizedResult = "Email not sent";
            }

            return $humanizedResult;

        } catch (\Exception $e) {
            Log::error($e);
            return false;
        }
    }
}

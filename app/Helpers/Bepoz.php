<?php

namespace App\Helpers;

use App\BepozLog;
use App\Helpers\Contracts\BepozContract;
use App\Setting;
use App\Venue;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use LSS\Array2XML;
use LSS\XML2Array;

class Bepoz implements BepozContract
{
    // variable for REST api client
    private $key;
    private $url;
    private $training;
    private $operator_id;
    private $till_id;
    private $ch;
    private $content;
    private $httpCode;
    private $xml;
    private $xmlHeader;

    /**
     * Bepoz constructor.
     */
    public function __construct()
    {
        Array2XML::init('1.0', 'UTF-8');

        $this->key = Setting::where('key', 'bepoz_mac_key')->first()->value;
        $this->url = Setting::where('key', 'bepoz_api')->first()->value;

        $till_id = Setting::where('key', 'bepoz_till_id')->first();
        $operator_id = Setting::where('key', 'bepoz_operator_id')->first();
        $training = Setting::where('key', 'bepoz_training_mode')->first();

        $this->till_id = is_null($till_id) ? null : $till_id->value;
        $this->operator_id = is_null($operator_id) ? null : $operator_id->value;
        $this->training = is_null($training) ? null : $training->value;
        $this->ch = curl_init();
    }

    public function getContent()
    {
        return $this->content;
    }

    public function getXML()
    {
        return $this->xml;
    }

    public function getXMLHeader()
    {
        return $this->xmlHeader;
    }

    public function getHttpCode()
    {
        return $this->httpCode;
    }

    public function SystemCheck()
    {
        $node = array(
            'ReqType' => "SystemCheck",
            'Data1' => '',
            'Data2' => '',
            'Data3' => '',
            'Data4' => '',
            'Data5' => '',
            'Data6' => '',
            'ClassName' => '',
            'XMLData' => ''
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));
        return $this->callAPI("SystemCheck", $encrypted, $xml);
    }

    public function Status()
    {
        $node = array(
            'ReqType' => "Status",
            'Data1' => '',
            'Data2' => '',
            'Data3' => '',
            'Data4' => '',
            'Data5' => '',
            'Data6' => '',
            'ClassName' => '',
            'XMLData' => ''
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));
        return $this->callAPI("Status", $encrypted, $xml);
    }

    //Undocumented Feature
    public function VersionGet()
    {
        $node = array(
            'ReqType' => "VersionGet",
            'Data1' => '',
            'Data2' => '',
            'Data3' => '',
            'Data4' => '',
            'Data5' => '',
            'Data6' => '',
            'ClassName' => '',
            'XMLData' => ''
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));
        return $this->callAPI("VersionGet", $encrypted, $xml);
    }

    public function ProductsSearch($data1 = null, $data2 = null, $data3 = null, $data4 = null, $data5 = null, $data6 = null)
    {
        $node = array(
            'ReqType' => "ProductsSearch",
            'Data1' => is_null($data1) ? '' : $data1,
            'Data2' => is_null($data2) ? '' : $data2,
            'Data3' => is_null($data3) ? '' : $data3,
            'Data4' => is_null($data4) ? '' : $data4,
            'Data5' => is_null($data5) ? '' : $data5,
            'Data6' => is_null($data6) ? '' : $data6,
            'ClassName' => '',
            'XMLData' => ''
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));
        return $this->callAPI("ProductsSearch", $encrypted, $xml);
    }

    public function ProductGet($data)
    {
        $node = array(
            'ReqType' => "ProductGet",
            'Data1' => $data,
            'Data2' => '',
            'Data3' => '',
            'Data4' => '',
            'Data5' => '',
            'Data6' => '',
            'ClassName' => '',
            'XMLData' => ''
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));
        return $this->callAPI("ProductGet", $encrypted, $xml);
    }

    public function ProdPriceGet($data)
    {
        $node = array(
            'ReqType' => "ProdPriceGet",
            'Data1' => $data,
            'Data2' => '',
            'Data3' => '',
            'Data4' => '',
            'Data5' => '',
            'Data6' => '',
            'ClassName' => '',
            'XMLData' => ''
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));
        return $this->callAPI("ProdPriceGet", $encrypted, $xml);
    }

    public function AccountFullGet($accountID)
    {
        $node = array(
            'ReqType' => "AccountFullGet",
            'Data1' => $accountID,
            'Data2' => '',
            'Data3' => '',
            'Data4' => '',
            'Data5' => '',
            'Data6' => '',
            'ClassName' => '',
            'XMLData' => ''
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));
        return $this->callAPI("AccountFullGet", $encrypted, $xml);
    }

    public function AccountGet($data)
    {
        $node = array(
            'ReqType' => "AccountGet",
            'Data1' => array_key_exists('AccountID', $data) ? $data['AccountID'] : '',
            'Data2' => array_key_exists('AccNumber', $data) ? $data['AccNumber'] : '',
            'Data3' => array_key_exists('CardNumber', $data) ? $data['CardNumber'] : '',
            'Data4' => array_key_exists('PhoneNumber', $data) ? $data['PhoneNumber'] : '',
            'Data5' => array_key_exists('Email', $data) ? $data['Email'] : '',
            'Data6' => '',
            'ClassName' => '',
            'XMLData' => ''
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));
        return $this->callAPI("AccountGet", $encrypted, $xml);
    }

    public function AccCustomFieldSet($data)
    {
        $node = array(
            'ReqType' => "AccCustomFieldSet",
            'Data1' => array_key_exists('AccountID', $data) ? $data['AccountID'] : '',
            'Data2' => array_key_exists('CustomFieldNumber', $data) ? $data['CustomFieldNumber'] : '',
            'Data3' => array_key_exists('DataSet', $data) ? $data['DataSet'] : '',
            'Data4' => '',
            'Data5' => '',
            'Data6' => '',
            'ClassName' => '',
            'XMLData' => ''
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));
        return $this->callAPI("AccCustomFieldSet", $encrypted, $xml);
    }

    public function AccCustomGet($accountID)
    {
        $node = array(
            'ReqType' => "AccCustomGet",
            'Data1' => $accountID,
            'Data2' => '',
            'Data3' => '',
            'Data4' => '',
            'Data5' => '',
            'Data6' => '',
            'ClassName' => '',
            'XMLData' => ''
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));
        return $this->callAPI("AccCustomGet", $encrypted, $xml);
    }

    public function CustomFieldsSet($EntityType, $EntityID, $CustomFields)
    {
        $CustomFields = array_filter($CustomFields, function ($var) {
            return !is_null($var);
        });

        $nw_array = Array2XML::createXML('ArrayOfCustomField', $CustomFields);
        $xmlData = $nw_array->saveXML();

        $node = array(
            'ReqType' => "CustomFieldsSet",
            'Data1' => $EntityType,
            'Data2' => $EntityID,
            'Data3' => '',
            'Data4' => '',
            'Data5' => '',
            'Data6' => '',
            'ClassName' => 'CustomField[]',
            'XMLData' => $xmlData
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));

        return $this->callAPI("CustomFieldsSet", $encrypted, $xml);
    }

    public function CustomFieldsMetaGet()
    {
        $node = array(
            'ReqType' => "CustomFieldsMetaGet",
            'Data1' => '',
            'Data2' => '',
            'Data3' => '',
            'Data4' => '',
            'Data5' => '',
            'Data6' => '',
            'ClassName' => '',
            'XMLData' => ''
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));
        return $this->callAPI("CustomFieldsMetaGet", $encrypted, $xml);
    }

    public function NextAccountNumber($account_group = null, $max_length = null, $range = null)
    {
        $node = array(
            'ReqType' => "NextAccountNumber",
            'Data1' => $account_group, //
            'Data2' => $max_length, //
            'Data3' => null, //NOT
            'Data4' => $range, //IN
            'Data5' => null, //
            'Data6' => null, //USE
            'ClassName' => '',
            'XMLData' => ''
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));
        return $this->callAPI("NextAccountNumber", $encrypted, $xml);
    }

    public function TestNextAccountNumber($account_group = null, $max_length = null, $range = null)
    {
        $node = array(
            'ReqType' => "TestNextAccountNumber",
            'Data1' => $account_group, //
            'Data2' => $max_length, //
            'Data3' => null, //NOT
            'Data4' => $range, //IN
            'Data5' => null, //
            'Data6' => null, //USE
            'ClassName' => '',
            'XMLData' => ''
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));
        return $this->callAPI("TestNextAccountNumber", $encrypted, $xml);
    }

    public function TestNextAccountNumberCreate($account_group = null, $max_length = null, $range = null)
    {
        $node = array(
            'ReqType' => "TestNextAccountNumberCreate",
            'Data1' => $account_group, //
            'Data2' => $max_length, //
            'Data3' => null, //NOT
            'Data4' => $range, //IN
            'Data5' => null, //
            'Data6' => null, //USE
            'ClassName' => '',
            'XMLData' => ''
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));
        return $this->callAPI("TestNextAccountNumberCreate", $encrypted, $xml);
    }

    public function AccUpdate($AccountFull, $template = null)
    {
        $AccountFull = array_filter($AccountFull, function ($var) {
            return !is_null($var);
        });

        if (array_key_exists("DateTimeCreated", $AccountFull)) {
            unset($AccountFull['DateTimeCreated']);
        }

        $nw_array = Array2XML::createXML('AccountFull', $AccountFull);
        $xmlData = $nw_array->saveXML();

        $node = array(
            'ReqType' => "AccCreateUpdate",
            'Data1' => is_null($template) ? '' : $template,
            'Data2' => '',
            'Data3' => '',
            'Data4' => '',
            'Data5' => '',
            'Data6' => '',
            'ClassName' => 'AccountFull',
            'XMLData' => $xmlData
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));
        return $this->callAPI("AccCreateUpdate", $encrypted, $xml);
    }

    public function AccCreateUpdate($accountID, $firstname, $lastname, $email, $accnumber, $cardnumber, $groupID = null, $template = null)
    {

        $nwData = array();

        if (!is_null($email)) {
            $nwData['Email1st'] = $email;
        }

        if (!is_null($lastname)) {
            $nwData['LastName'] = str_replace('"', '', $lastname);
        }

        if (!is_null($firstname)) {
            $nwData['FirstName'] = str_replace('"', '', $firstname);
        }

        if (!is_null($accnumber)) {
            $nwData['AccNumber'] = $accnumber;
        }

        if (!is_null($cardnumber)) {
            $nwData['CardNumber'] = $cardnumber;
        }

        if (!is_null($accountID)) {
            $nwData['AccountID'] = $accountID;
        }

        if (!is_null($groupID)) {
            $nwData['GroupID'] = $groupID;
            $nwData['UseGroupSettings'] = 1;
        }

        $nwData['DateTimeCreated'] = Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s');

        $nw_array = Array2XML::createXML('AccountFull', $nwData);
        $xmlData = $nw_array->saveXML();


        $node = array(
            'ReqType' => "AccCreateUpdate",
            'Data1' => $template,
            'Data2' => '',
            'Data3' => '',
            'Data4' => '',
            'Data5' => '',
            'Data6' => '',
            'ClassName' => 'AccountFull',
            'XMLData' => $xmlData
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));
        return $this->callAPI("AccCreateUpdate", $encrypted, $xml);
    }

    public function CreateAccount($AccountFull, $template = null, $autoGenerate = null, $length = null, $range = null, $ignoreEmail = null, $ignoreMobile = null)
    {
        $AccountFull = array_filter($AccountFull, function ($var) {
            return !is_null($var);
        });

        if (array_key_exists("DateTimeCreated", $AccountFull)) {
            unset($AccountFull['DateTimeCreated']);
        }

        $nw_array = Array2XML::createXML('AccountFull', $AccountFull);
        $xmlData = $nw_array->saveXML();

        $node = array(
            'ReqType' => "CreateAccount",
            'Data1' => $template ?? '',
            'Data2' => $autoGenerate,
            'Data3' => $length,
            'Data4' => $range,
            'Data5' => $ignoreEmail,
            'Data6' => $ignoreMobile,
            'ClassName' => 'AccountFull',
            'XMLData' => $xmlData
        );
        
        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));
        return $this->callAPI("CreateAccount", $encrypted, $xml);
    }

    public function UpdateAccount($AccountFull, $template = null)
    {
        $AccountFull = array_filter($AccountFull, function ($var) {
            return !is_null($var);
        });

        if (array_key_exists("DateTimeCreated", $AccountFull)) {
            unset($AccountFull['DateTimeCreated']);
        }

        $nw_array = Array2XML::createXML('AccountFull', $AccountFull);
        $xmlData = $nw_array->saveXML();

        $node = array(
            'ReqType' => "UpdateAccount",
            'Data1' => $template ?? '',
            'Data2' => 0,
            'Data3' => '',
            'Data4' => '',
            'Data5' => '',
            'Data6' => '',
            'ClassName' => 'AccountFull',
            'XMLData' => $xmlData
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));
        return $this->callAPI("UpdateAccount", $encrypted, $xml);
    }
    
    public function AccountSearch($searchField, $data)
    {
        $node = array(
            'ReqType' => "AccountsSearch",
            'Data1' => $data,
            'Data2' => $searchField,
            'Data3' => 1,
            'Data4' => '',
            'Data5' => '',
            'Data6' => '',
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));
        return $this->callAPI("AccountsSearch", $encrypted, $xml);
    }
   
    public function AccountFind($email = null, $mobile = null, $cardNo = null, $accountNo = null)
    {
        $node = array(
            'ReqType' => "AccountFind",
            'Data1' => $email,
            'Data2' => $mobile,
            'Data3' => $cardNo,
            'Data4' => $accountNo,
            'Data5' => null,
            'Data6' => null,
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));
        return $this->callAPI("AccountFind", $encrypted, $xml);
    }

    public function AccBalanceGet($accountID = null, $accountNumber = null, $cardNumber = null)
    {
        $node = array(
            'ReqType' => "AccBalanceGet",
            'Data1' => is_null($accountID) ? '' : $accountID,
            'Data2' => is_null($accountNumber) ? '' : $accountNumber,
            'Data3' => is_null($cardNumber) ? '' : $cardNumber,
            'Data4' => '',
            'Data5' => '',
            'Data6' => '1',
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));
        return $this->callAPI("AccBalanceGet", $encrypted, $xml);
    }

    public function BalanceUpdate($data)
    {
        $nw_array = Array2XML::createXML('AccBalanceUpdate', $data);
        $xmlData = $nw_array->saveXML();

        $ebet_gaming = Setting::where('key', 'ebet_gaming')->first()->value;

        $node = array(
            'ReqType' => "AccBalanceUpdate",
            'Data1' => '',
            'Data2' => '',
            'Data3' => '',
            'Data4' => '',
            'Data5' => '',
            'Data6' => $ebet_gaming,
            'ClassName' => 'AccBalanceUpdate',
            'XMLData' => $xmlData
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));
        return $this->callAPI("AccBalanceUpdate", $encrypted, $xml);
    }

    public function AccBalanceUpdate($id, $point, $status)
    {
        $nwData = array();

        $nwData['AccountID'] = $id;
        $nwData['DateTimeActivity'] = Carbon::now(config('app.timezone'))->toAtomString();
        $nwData['ShiftID'] = 0;
        $nwData['VenueID'] = 0;
        $nwData['TransactionID'] = 0;
        $nwData['GrossTurnover'] = 0;
        $nwData['NettTurnover'] = 0;
        $nwData['CostTurnover'] = 0;
        $nwData['AccountCharged'] = 0;
        $nwData['AccountPaid'] = 0;

        if ($status == "earn") {
            $nwData['PointsEarned'] = intval(floatval($point) * 100);
            $nwData['PointsRedeemed'] = 0;

        } else {
            $nwData['PointsEarned'] = 0;
            $nwData['PointsRedeemed'] = intval(floatval($point) * 100);
        }


        $nwData['JoiningFee'] = 0;
        $nwData['RenewalFee'] = 0;

        $nw_array = Array2XML::createXML('AccBalanceUpdate', $nwData);
        $xmlData = $nw_array->saveXML();

        $node = array(
            'ReqType' => "AccBalanceUpdate",
            'Data1' => '',
            'Data2' => '',
            'Data3' => '',
            'Data4' => '',
            'Data5' => '',
            'Data6' => '',
            'ClassName' => 'AccBalanceUpdate',
            'XMLData' => $xmlData
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));
        return $this->callAPI("AccBalanceUpdate", $encrypted, $xml);
    }

    public function TransactionCreate($data)
    {
        // foreach ($data as $key => &$obj) {
        //     $obj['OperatorID'] = $this->operator_id;
        //     $obj['TillID'] = $this->till_id;
        //     $obj['Training'] = $this->training;
        // }

        $nw_array = Array2XML::createXML('ArrayOfTransStdLine', ['TransStdLine' => $data]);
        $xmlData = $nw_array->saveXML();

        $node = array(
            'ReqType' => "TransactionCreate",
            'Data1' => '',
            'Data2' => '',
            'Data3' => '',
            'Data4' => '',
            'Data5' => '',
            'Data6' => '',
            'ClassName' => 'TransStdLine[]',
            'XMLData' => $xmlData
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        //Log::info("XML TransactionCreate");
        //Log::info($xml);

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));
        return $this->callAPI("TransactionCreate", $encrypted, $xml);
    }

    public function VoucherSetupGet($id)
    {
        $node = array(
            'ReqType' => "VoucherSetupGet",
            'Data1' => $id,
            'Data2' => '',
            'Data3' => '',
            'Data4' => '',
            'Data5' => '',
            'Data6' => '',
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));
        return $this->callAPI("VoucherSetupGet", $encrypted, $xml);
    }

    public function VoucherGet($id = null, $lookup = null)
    {
        $node = array(
            'ReqType' => "VoucherGet",
            'Data1' => is_null($id) ? '' : $id,
            'Data2' => is_null($lookup) ? '' : $lookup,
            'Data3' => '',
            'Data4' => '',
            'Data5' => '',
            'Data6' => '',
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));

        return $this->callAPI("VoucherGet", $encrypted, $xml);
    }

    public function VoucherIssue($voucherSetupId, $bepoz_account_id, $lookup = null, $value = null)
    {
        $node = array(
            'ReqType' => "VoucherIssue",
            'Data1' => $voucherSetupId, // bepos voucher setup id, not the cloud one
            'Data2' => is_null($bepoz_account_id) ? '' : $bepoz_account_id, // bepos account id, not the cloud one
            'Data3' => is_null($lookup) ? '' : $lookup,
            'Data4' => is_null($value) ? '' : $value,
            'Data5' => '',
            'Data6' => '',
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));

        return $this->callAPI("VoucherIssue", $encrypted, $xml);
    }

    public function IDListGet($listTypeId, $data2 = null, $data3 = null, $data4 = null)
    {
        $node = array(
            'ReqType' => "IDListGet",
            'Data1' => $listTypeId,
            'Data2' => is_null($data2) ? '' : $data2,
            'Data3' => is_null($data3) ? '' : $data3,
            'Data4' => is_null($data4) ? '' : $data4,
            'Data5' => '',
            'Data6' => '',
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $this->key, false));

        return $this->callAPI("IDListGet", $encrypted, $xml);
    }

    protected function callAPI($requestType, $mac, $xml)
    {
        // $ch = curl_init();
        $ch = $this->ch;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('mac: ' . $mac,));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_ENCODING, "gzip");
        $content = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $this->httpCode = $httpCode;
        $this->content = $content;

        if ($httpCode >= 200 && $httpCode < 300) {
            if ($content) {
                if ($this->xmlValidation($content)) {
                    $array = XML2Array::createArray($content);
                    if (is_array($array)) {
                        if (array_key_exists('ReqType', $array['Header'])) {
                            
                            $this->xmlHeader = $array['Header'];

                            if ($array['Header']['ReqType'] == "Error" || $array['Header']['ReqType'] == "ERROR") {
                                Log::error($requestType);
                                Log::error($content);
                                $result = false;
                            } else {
                                if (array_key_exists('XMLData', $array['Header'])) {
                                    $result = XML2Array::createArray($array['Header']['XMLData']);
                                } else {
                                    if ($array['Header']['ReqType'] != "VersionGet") {
                                        // Log::info($array);
                                    }
                                    $result = $array['Header'];
                                }
                            }
                        } else {
                            Log::critical($content);
                            $result = false;
                        }
                    } else {
                        Log::critical($content);
                        $result = false;
                    }
                } else {
                    Log::critical($content);
                    $result = false;
                }

            } else {
                if (intval(curl_errno($ch)) === 0 && $requestType == 'AccBalanceUpdate') {
                    $result = true;
                } else {
                    Log::critical($content);
                    Log::info(curl_errno($ch));
                    Log::error(curl_error($ch));
                    $result = false;
                }
            }
        } else {
            // $timestamp = Carbon::now(config('app.timezone'))->timestamp;
            // if ($timestamp % 3 == 0) {
            //     Log::info('Bepoz.php(536): 404 Not Found. No Bepoz Connection.');
            // }
            $result = false;
        }

        if ($requestType != 'SystemCheck' && $requestType != 'IDListGet' && $requestType != 'VoucherSetupGet') {
            $log = new BepozLog();
            $log->request_type = $requestType;
            $log->request = $xml;
            $log->response = $content;
            $log->save();
        }

        return $result;
    }

    protected function xmlValidation($input)
    {
        $result = simplexml_load_string($input, 'SimpleXmlElement', LIBXML_NOERROR + LIBXML_ERR_FATAL + LIBXML_ERR_NONE);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }


    public function VenueSystemCheck($venueID)
    {
        $venue = Venue::find($venueID);

        $node = array(
            'ReqType' => "SystemCheck",
            'Data1' => '',
            'Data2' => '',
            'Data3' => '',
            'Data4' => '',
            'Data5' => '',
            'Data6' => '',
            'ClassName' => '',
            'XMLData' => ''
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $venue->bepoz_mac_key, false));
        return $this->callAPIVenue($venue->bepoz_api, "SystemCheck", $encrypted, $xml);
    }

    public function VenueAccBalanceGet($venueID, $accountID = null, $accountNumber = null, $cardNumber = null)
    {
        $venue = Venue::find($venueID);

        $node = array(
            'ReqType' => "AccBalanceGet",
            'Data1' => is_null($accountID) ? '' : $accountID,
            'Data2' => is_null($accountNumber) ? '' : $accountNumber,
            'Data3' => is_null($cardNumber) ? '' : $cardNumber,
            'Data4' => '',
            'Data5' => '',
            'Data6' => '1', // GAMING ENABLED
        );

        $xml_array = Array2XML::createXML('Header', $node);
        $xml = $xml_array->saveXML();

        $encrypted = strtoupper(hash_hmac('sha1', $xml, $venue->bepoz_mac_key, false));
        return $this->callAPIVenue($venue->bepoz_api, "AccBalanceGet", $encrypted, $xml);
    }

    protected function callAPIVenue($url, $requestType, $mac, $xml)
    {
        // $ch = curl_init();
        $ch = $this->ch;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('mac: ' . $mac,));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);

        if ($requestType == 'AccBalanceGet') {
            curl_setopt($ch, CURLOPT_TIMEOUT, 8);
        } else {
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        }

        curl_setopt($ch, CURLOPT_ENCODING, "gzip");
        $content = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $this->httpCode = $httpCode;
        $this->content = $content;

        if ($httpCode >= 200 && $httpCode < 300) {
            if ($content) {
                if ($this->xmlValidation($content)) {
                    $array = XML2Array::createArray($content);
                    if (is_array($array)) {
                        if (array_key_exists('ReqType', $array['Header'])) {
                            $this->xmlHeader = $array['Header'];
                            // Log::info($array['Header']);

                            if ($array['Header']['ReqType'] == "Error" || $array['Header']['ReqType'] == "ERROR") {
                                Log::error($requestType);
                                Log::error($content);
                                $result = false;
                            } else {
                                if (array_key_exists('XMLData', $array['Header'])) {
                                    $result = XML2Array::createArray($array['Header']['XMLData']);
                                } else {
                                    if ($array['Header']['ReqType'] != "VersionGet") {
                                        // Log::info($array);
                                    }
                                    $result = $array['Header'];
                                }
                            }
                        } else {
                            Log::critical($content);
                            $result = false;
                        }
                    } else {
                        Log::critical($content);
                        $result = false;
                    }
                } else {
                    Log::critical($content);
                    $result = false;
                }

            } else {
                if (intval(curl_errno($ch)) === 0 && $requestType == 'AccBalanceUpdate') {
                    $result = true;
                } else {
                    Log::critical($content);
                    Log::info(curl_errno($ch));
                    Log::error(curl_error($ch));
                    $result = false;
                }
            }
        } else {
            // $timestamp = Carbon::now(config('app.timezone'))->timestamp;
            // if ($timestamp % 3 == 0) {
            //     Log::info('Bepoz.php(536): 404 Not Found. No Bepoz Connection.');
            // }
            $result = false;
        }

        if ($requestType != 'SystemCheck' && $requestType != 'IDListGet' && $requestType != 'VoucherSetupGet') {
            $log = new BepozLog();
            $log->request_type = $requestType;
            $log->request = $xml;
            $log->response = $content;
            $log->save();
        }

        return $result;
    }

    public function isCustomField($cf) {        
        $customField = false;

        switch (intval($cf->bepozFieldNum)) {
            case 1:
                $customField = true;
                break;
            case 2:
                $customField = true;
                break;
            case 3:
                $customField = true;
                break;
            case 4:
                $customField = true;
                break;
            case 5:
                $customField = true;
                break;
            case 6:
                $customField = true;
                break;
            case 7:
                $customField = true;
                break;
            case 8:
                $customField = true;
                break;
            case 9:
                $customField = true;
                break;
            case 10:
                $customField = true;
                break;
            case 101:
                $customField = true;
                break;
            case 102:
                $customField = true;
                break;
            case 103:
                $customField = true;
                break;
            case 104:
                $customField = true;
                break;
            case 105:
                $customField = true;
                break;
            case 201:
                $customField = true;
                break;
            case 202:
                $customField = true;
                break;
            case 203:
                $customField = true;
                break;
            case 204:
                $customField = true;
                break;
            case 205:
                $customField = true;
                break;
            case 301:
                $customField = true;
                break;
            case 302:
                $customField = true;
                break;
            case 303:
                $customField = true;
                break;
            case 304:
                $customField = true;
                break;
            case 305:
                $customField = true;
                break;
            case 306:
                $customField = true;
                break;
            case 307:
                $customField = true;
                break;
            case 308:
                $customField = true;
                break;
            case 309:
                $customField = true;
                break;
            case 310:
                $customField = true;
                break;
            case 311:
                $customField = true;
                break;
            case 312:
                $customField = true;
                break;
            case 313:
                $customField = true;
                break;
            case 314:
                $customField = true;
                break;
            case 315:
                $customField = true;
                break;
            case 316:
                $customField = true;
                break;
            case 317:
                $customField = true;
                break;
            case 318:
                $customField = true;
                break;
            case 319:
                $customField = true;
                break;
            case 320:
                $customField = true;
                break;
            default:
                $customField = false;
        }

        return $customField;
    }

    public function bepozAccField($number) {
        $name = '';
        switch (intval($number)) {
            case 11:
                $name = 'DoNotPost';
                break;
            case 12:
                $name = 'DoNotEmail';
                break;
            case 13:
                $name = 'DoNotSMS';
                break;
            case 14:
                $name = 'DoNotPhone';
                break;
            case 106:
                $name = 'DateBirth';
                break;
            case 321:
                $name = 'Title';
                break;
            case 322:
                $name = 'FirstName';
                break;
            case 323:
                $name = 'LastName';
                break;
            case 324:
                $name = 'Street1';
                break;
            case 325:
                $name = 'City';
                break;
            case 326:
                $name = 'State';
                break;
            case 327:
                $name = 'PCode';
                break;
            case 328:
                $name = 'Mobile';
                break;
            case 329:
                $name = 'Email1st';
                break;
            case 330:
                $name = 'Gender';
                break;
            case 331:
                $name = 'Tier';
                break;
            default:
                $name = '';
        }
        return $name;
    }

}

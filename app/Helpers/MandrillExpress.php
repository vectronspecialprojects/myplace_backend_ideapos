<?php

namespace App\Helpers;

use App\Email;
use App\Helpers\Contracts\MandrillExpressContract;
use App\MailLog;
use App\MandrillSetting;
use App\PendingJob;
use App\Setting;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class MandrillExpress implements MandrillExpressContract
{
    private $setting;
    private $mandrill;

    public function __construct()
    {
        require_once(app_path() . '/Helpers/Mandrill.php');
    }

    public function init()
    {
        try {
            $setting = Setting::where('key', 'mandrill_key')->first();

            if (is_null($setting)) {
                return false;
            }

            $this->setting = $setting;
            $this->mandrill = new \Mandrill($this->setting->value);
            $temp = $this->mandrill->users->ping2();

            if (isset($temp['PING'])) {
                return true;
            }

            Log::error($temp);
            return false;

        } catch (\Exception $e) {
            Log::error($e);
            return false;
        }
    }

    public function send($category, $name, $recipient_email, $data)
    {
        try {
            $email = Email::where('uid', $category)->first();

            $template_name = MandrillSetting::where('email_id', $email->id)->where('key', 'template')->first()->value;

            $subject = MandrillSetting::where('email_id', $email->id)->where('key', 'subject')->first();

            $mandrill_email = Setting::where('key', 'mandrill_email')->first();

            $mandrill_full_name = Setting::where('key', 'mandrill_sender_name')->first();

            $company_name = Setting::where('key', 'company_name')->first()->value;

            $company_email = Setting::where('key', 'company_email')->first()->value;

            $template_content = array(
                array(
                    "name" => "header",
                    "content" => "<h1>Header</h1>"
                ),
                array(
                    "name" => "main",
                    "content" => "<h2>Main</h2>"
                ),
                array(
                    "name" => "footer",
                    "content" => "<h3>Footer</h3>"
                ),
            );

            $message = array(
                'html' => '',
                'text' => '',
                'subject' => $subject->value,
                'from_email' => $mandrill_email->value,
                'from_name' => $mandrill_full_name->value,
                'important' => false,
                'merge' => true,
                'merge_language' => 'mailchimp',
            );

            $message['global_merge_vars'] = array(
                array(
                    'name' => 'COMPANY_YEAR',
                    'content' => date("Y")
                ),
                array(
                    'name' => 'COMPANY',
                    'content' => $company_name
                ),
                array(
                    'name' => 'CURRENT_YEAR',
                    'content' => date("Y")
                )

            );

            $message['to'] = array(
                array(
                    'email' => $recipient_email,
                    'name' => $name,
                    'type' => 'to'
                )
            );

            if (isset($data['CC'])) {
                $message['to'][] =
                    array(
                        'email' => $data['CC'],
                        'type' => 'cc'
                    );
            }

            $message['headers'] = array('Reply-To' => $company_email);

            $modified_array = array();
            foreach ($data as $key => $value) {
                $modified_array[] = array(
                    'name' => $key,
                    'content' => $value
                );
            }

            $message['merge_vars'] = array(
                array(
                    'rcpt' => $recipient_email,
                    'vars' => $modified_array
                )
            );

            if (isset($data['CC'])) {
                $message['merge_vars'][] =
                    array(
                        'rcpt' => $data['CC'],
                        'vars' => $modified_array
                    );
            }

            $result = $this->mandrill->messages->sendTemplate($template_name, $template_content, $message, false, null, Carbon::yesterday()->subWeek()->toDateTimeString());

            $ml = new MailLog();
            $ml->mandrill_response = \GuzzleHttp\json_encode($result);
            $ml->payload = \GuzzleHttp\json_encode($message);
            $ml->save();

            if (isset($result["status"])) {

                $pj = PendingJob::where('queue', 'email')
                    ->where('payload', \GuzzleHttp\json_encode($data))
                    ->where('extra_payload', \GuzzleHttp\json_encode(array(
                        'email' => $email,
                        'category' => $category,
                        'name' => $name
                    )))
                    ->first();

                if (is_null($pj)) {
                    $pj = new PendingJob();
                    $pj->payload = \GuzzleHttp\json_encode($data);
                    $pj->extra_payload = \GuzzleHttp\json_encode(array(
                        'email' => $email,
                        'category' => $category,
                        'name' => $name
                    ));
                    $pj->queue = "email";
                    $pj->save();
                }

            } else {

                $pj = PendingJob::where('queue', 'email')
                    ->where('payload', \GuzzleHttp\json_encode($data))
                    ->where('extra_payload', \GuzzleHttp\json_encode(array(
                        'email' => $email,
                        'category' => $category,
                        'name' => $name
                    )))
                    ->first();

                if (!is_null($pj)) {
                    $pj->delete();
                }

            }

            return $result;

        } catch (\Exception $e) {
            Log::error($e);
            return false;
        }
    }
}
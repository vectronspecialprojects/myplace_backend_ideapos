<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ListingSchedule extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'listing_schedules';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at', 'listing_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get the listing.
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function listing()
    {
        return $this->belongsTo('App\Listing', 'listing_id');
    }

    public function types()
    {
        return $this->hasMany('App\ListingScheduleType', 'listing_schedule_id');
    }

    public function venues()
    {
        return $this->hasMany('App\ListingScheduleVenue', 'listing_schedule_id');
    }

    public function tags()
    {
        return $this->hasMany('App\ListingScheduleTag', 'listing_schedule_id');
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($schedule) { // before delete() method call this

            foreach ($schedule->tags as $tag) {
                $tag->delete();
            }

            foreach ($schedule->types as $type) {
                $type->delete();
            }

            foreach ($schedule->venues as $venue) {
                $venue->delete();
            }

        });

    }
}

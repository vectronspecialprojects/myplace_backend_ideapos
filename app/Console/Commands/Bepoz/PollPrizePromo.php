<?php

namespace App\Console\Commands\Bepoz;

use App\Helpers\Bepoz;
use App\BepozFailedJob;
use App\BepozJob;
use App\Jobs\SendOneSignalNotification;
use App\Member;
use App\MemberLog;
use App\MemberTiers;
use App\PrizePromotion;
use App\PromoAccount;
use App\Setting;
use App\SystemLog;
use App\Tier;
use App\VoucherSetups;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;
use Carbon\Carbon;

class PollPrizePromo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'poll-prize-promo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Prize Promo from Bepoz for Linked with Stamp Card';

    /**
     * Execute the console command.
     *
     */
    public function handle(Bepoz $bepoz)
    {
        try {
            $mac = Setting::where('key', 'bepoz_mac_key')->first()->value;
            $url = Setting::where('key', 'bepoz_secondary_api')->first()->value;


            //Log::warning("poll_account_total_saved");
            $last_execution_time = Setting::where('key', 'bepoz_prize_promo_last_successful_execution_time')->first();
            $datetime = new Carbon($last_execution_time->value);
            $datetime->addHours(-1);

            //$datetime = Carbon::createFromFormat('Y-m-d H:i', '2019-01-01 00:00');

            $payload = [];
            $payload['procedure'] = "PrizePromoUpdated";
            //$payload['procedure'] = "AccountsUpdated";
            // $payload['parameters'] = [$datetime->format('Y-m-d H:i:s')];

            //Log::warning($payload);

            $post_content = \GuzzleHttp\json_encode($payload);
            $encrypted = strtoupper(hash_hmac('sha1', $post_content, $mac, false));

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('mac: ' . $encrypted, 'Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_content);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            curl_setopt($ch, CURLOPT_ENCODING, "gzip");

            $content = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            // Log::warning($content);
            // Log::warning($httpCode);


            if ($httpCode >= 200 && $httpCode < 300) {
                // pass
                if ($content) {
                    $payload = \GuzzleHttp\json_decode($content);
                    if (!$payload->error) {

                        $missingPrizePromoID = false;
                        foreach ($payload->data as $data) {
                            //Log::warning($data);
                            if (isset($data->PrizePromoID)) {

                                $prize = PrizePromotion::where('bepoz_prize_promo_id', $data->PrizePromoID)->first();
                                if (is_null($prize)) {
                                    //UPDATE PrizePromotion
                                    $prize = new PrizePromotion;

                                    $prize->name = $data->Name;
                                    $prize->bepoz_prize_promo_id = $data->PrizePromoID;
                                    $prize->needed = intval($data->Needed) / 100;
                                    $prize->inactive = intval($data->InActive);
                                    $prize->date_updated = $data->DateUpdated;
                                    $prize->bepoz_voucher_setup_id = $data->VoucherSetupID;
                                    // Log::info("VoucherSetupID ".$data->VoucherSetupID);
                                    $vs = VoucherSetups::where('bepoz_voucher_setup_id', $data->VoucherSetupID)->first();
                                    //if VoucherSetups not found then create new one
                                    if (is_null($vs)) {
                                        if ($data->VoucherSetupID != '' && intval($data->VoucherSetupID) > 0) {
                                            $voucherSetupInformation = $bepoz->VoucherSetupGet($data->VoucherSetupID);
                                            // Log::info($voucherSetupInformation);

                                            if ($voucherSetupInformation) {
                                                // Log::info("create voucher setup");
                                                DB::beginTransaction();
                                                $vs = new VoucherSetups;
                                                $vs->bepoz_voucher_setup_id = $data->VoucherSetupID;
                                                $vs->name = $voucherSetupInformation['VoucherSetup']['Name'];
                                                $vs->inactive = boolval($voucherSetupInformation['VoucherSetup']['Inactive']);
                                                $vs->expiry_type = intval($voucherSetupInformation['VoucherSetup']['ExpiryType']);
                                                $vs->expiry_number = intval($voucherSetupInformation['VoucherSetup']['ExpiryNumber']);
                                                $vs->expiry_date = Carbon::parse($voucherSetupInformation['VoucherSetup']['ExpiryDate']);
                                                $vs->voucher_type = intval($voucherSetupInformation['VoucherSetup']['VoucherType']);
                                                $vs->voucher_apply = intval($voucherSetupInformation['VoucherSetup']['VoucherApply']);
                                                $vs->save();
                                                DB::commit();
                                            } else {
                                                // Log::info("nothing voucher setup");

                                                // $log = new SystemLog();
                                                // $log->type = 'bepoz-job-error';
                                                // $log->humanized_message = 'Polling voucher setup is failed. Please check the error message';
                                                // $log->message = 'Failed to reach bepoz connection';
                                                // $log->source = 'PollPrizePromo.php';
                                                // $log->save();
                                            }                                            
                                        }
                                    }

                                    if (is_null($vs)) {
                                        $prize->voucher_setup_id = 0;
                                    } else {
                                        //load VoucherSetups again
                                        $vs = VoucherSetups::where('bepoz_voucher_setup_id', $data->VoucherSetupID)->first();
                                        $prize->voucher_setup_id = $vs->id;
                                    }

                                    $prize->promo_type = $data->PromoType;
                                    $prize->on_permanent = $data->OnPermanent;
                                    $prize->schedule_num = $data->ScheduleNum;
                                    $prize->venue_id = $data->VenueID;
                                    $prize->store_id = $data->StoreID;
                                    $prize->till_id = $data->TillID;
                                    $prize->acc_profile_id = $data->AccProfileID;
                                    $prize->image_id = $data->ImageID;
                                    $prize->promo_max = $data->PromoMax;
                                    $prize->account_max = $data->AccountMax;
                                    $prize->daily_max = $data->DailyMax;
                                    $prize->daily_acc_max = $data->DailyAccMax;
                                    $prize->transaction_max = $data->TransactionMax;
                                    $prize->promo_count = $data->PromoCount;
                                    $prize->daily_count = $data->DailyCount;
                                    $prize->daily_count_date = $data->DailyCountDate;
                                    $prize->accumulate = $data->Accumulate;
                                    $prize->flag_1 = $data->Flag_1;
                                    $prize->flag_2 = $data->Flag_2;
                                    $prize->entry_delay = $data->EntryDelay;
                                    $prize->subsequent = $data->Subsequent;
                                    $prize->req_id = $data->ReqID;
                                    $prize->req_size = $data->ReqSize;
                                    $prize->req_text = $data->ReqText;
                                    $prize->prize_type = $data->PrizeType;
                                    $prize->prize_amount = $data->PrizeAmount;
                                    $prize->display_message = $data->DisplayMessage;

                                    $prize->save();

                                } else {
                                    //UPDATE PrizePromotion
                                    $prize->name = $data->Name;
                                    $prize->bepoz_prize_promo_id = $data->PrizePromoID;
                                    $prize->needed = intval($data->Needed) / 100;
                                    $prize->inactive = intval($data->InActive);
                                    $prize->date_updated = $data->DateUpdated;
                                    $prize->bepoz_voucher_setup_id = $data->VoucherSetupID;

                                    // Log::info("VoucherSetupID ".$data->VoucherSetupID);
                                    $vs = VoucherSetups::where('bepoz_voucher_setup_id', $data->VoucherSetupID)->first();
                                    //if VoucherSetups not found then create new one
                                    if (is_null($vs)) {
                                        if ($data->VoucherSetupID != '' && intval($data->VoucherSetupID) > 0) {
                                            $voucherSetupInformation = $bepoz->VoucherSetupGet($data->VoucherSetupID);
                                            if ($voucherSetupInformation) {
                                                DB::beginTransaction();
                                                $vs = new VoucherSetups;
                                                $vs->bepoz_voucher_setup_id = $data->VoucherSetupID;
                                                $vs->name = $voucherSetupInformation['VoucherSetup']['Name'];
                                                $vs->inactive = boolval($voucherSetupInformation['VoucherSetup']['Inactive']);
                                                $vs->expiry_type = intval($voucherSetupInformation['VoucherSetup']['ExpiryType']);
                                                $vs->expiry_number = intval($voucherSetupInformation['VoucherSetup']['ExpiryNumber']);
                                                $vs->expiry_date = Carbon::parse($voucherSetupInformation['VoucherSetup']['ExpiryDate']);
                                                $vs->voucher_type = intval($voucherSetupInformation['VoucherSetup']['VoucherType']);
                                                $vs->voucher_apply = intval($voucherSetupInformation['VoucherSetup']['VoucherApply']);
                                                $vs->save();
                                                DB::commit();
                                            } else {
                                                $log = new SystemLog();
                                                $log->type = 'bepoz-job-error';
                                                $log->humanized_message = 'Polling voucher setup is failed. Please check the error message';
                                                $log->message = 'Failed to reach bepoz connection';
                                                $log->source = 'PollPrizePromo.php';
                                                $log->save();
                                            }
                                        }
                                    }

                                    if (is_null($vs)) {
                                        $prize->voucher_setup_id = 0;
                                    } else {
                                        //load VoucherSetups again
                                        $vs = VoucherSetups::where('bepoz_voucher_setup_id', $data->VoucherSetupID)->first();
                                        $prize->voucher_setup_id = $vs->id;
                                    }
                                    
                                    $prize->promo_type = $data->PromoType;
                                    $prize->on_permanent = $data->OnPermanent;
                                    $prize->schedule_num = $data->ScheduleNum;
                                    $prize->venue_id = $data->VenueID;
                                    $prize->store_id = $data->StoreID;
                                    $prize->till_id = $data->TillID;
                                    $prize->acc_profile_id = $data->AccProfileID;
                                    //$prize->image_id = $data->ImageID;
                                    $prize->promo_max = $data->PromoMax;
                                    $prize->account_max = $data->AccountMax;
                                    $prize->daily_max = $data->DailyMax;
                                    $prize->daily_acc_max = $data->DailyAccMax;
                                    $prize->transaction_max = $data->TransactionMax;
                                    $prize->promo_count = $data->PromoCount;
                                    $prize->daily_count = $data->DailyCount;
                                    $prize->daily_count_date = $data->DailyCountDate;
                                    $prize->accumulate = $data->Accumulate;
                                    $prize->flag_1 = $data->Flag_1;
                                    $prize->flag_2 = $data->Flag_2;
                                    $prize->entry_delay = $data->EntryDelay;
                                    $prize->subsequent = $data->Subsequent;
                                    $prize->req_id = $data->ReqID;
                                    $prize->req_size = $data->ReqSize;
                                    $prize->req_text = $data->ReqText;
                                    $prize->prize_type = $data->PrizeType;
                                    $prize->prize_amount = $data->PrizeAmount;
                                    $prize->display_message = $data->DisplayMessage;

                                    $prize->save();
                                }

                            } else {
                                $missingPrizePromoID = true;
                            }
                            //Log::warning($data->PrizePromoID);
                        }

                        if ($missingPrizePromoID) {
                            $log = new SystemLog();
                            $log->type = 'bepoz-job-error';
                            $log->humanized_message = 'Polling Prize Promo is failed. Please check the error message';
                            $log->message = "PrizePromoID not found, please check setting for Bepoz Prize Promo, stored procedure, or connection";
                            $log->source = 'PollPrizePromo.php';
                            $log->save();
                        }

                        //$last_execution_time->value = Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s');
                        //$last_execution_time->save();

                    }
                } else {
                    //Log::error(curl_error($ch));
                    //Log::error(curl_errno($ch));

                    $log = new SystemLog();
                    $log->type = 'bepoz-job-error';
                    $log->humanized_message = 'Polling Prize Promo is failed. Please check the error message';
                    $log->message = \GuzzleHttp\json_encode(array("error" => curl_error($ch), "error_code" => curl_errno($ch)));
                    $log->source = 'PollPrizePromo.php';
                    $log->save();
                }
            } else {
                // 404
                //Log::error($httpCode);

                $log = new SystemLog();
                $log->type = 'bepoz-job-error';
                $log->humanized_message = 'Polling Prize Promo is failed. Please check the error message';
                $log->message = \GuzzleHttp\json_encode(array("error" => 'PrizePromoUpdated ' . $httpCode . ' Not Found', "error_code" => $httpCode));
                $log->source = 'PollPrizePromo.php';
                $log->save();
            }


        } catch (\Exception $e) {

            Log::warning($e);

            $log = new SystemLog();
            $log->type = 'bepoz-job-error';
            $log->humanized_message = 'Polling Prize Promo is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'PollPrizePromo.php';
            $log->save();

        }
    }
}

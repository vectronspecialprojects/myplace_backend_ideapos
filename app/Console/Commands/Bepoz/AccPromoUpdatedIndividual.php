<?php

namespace App\Console\Commands\Bepoz;

use App\Member;
use App\PrizePromotion;
use App\PromoAccount;
use App\Setting;
use App\Venue;
use App\VenuePivotTag;
use App\VenueTag;
use App\SystemLog;
use App\Jobs\Job;
use App\Helpers\Bepoz;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class AccPromoUpdatedIndividual extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'accpromo-updated-individual {memberid?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Call Bepoz stored procedure AccPromoUpdatedIndividual with Bepoz ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Bepoz $bepoz)
    {
        try {
            // Log::info('Call Bepoz stored procedure AccPromoUpdatedIndividual with Bepoz ID');
            // Log::info($this->argument('memberid'));

            if (empty($this->argument('memberid'))) {
                // add delay to prevent duplicate
                sleep(30);
            } else {

                $member = Member::find($this->argument('memberid'));
                $user = $member->user;

                if (!is_null($user->member->bepoz_account_id)) {
                    if (intval($user->member->bepoz_account_id) > 0) {
                        try {
                            $mac = Setting::where('key', 'bepoz_mac_key')->first()->value;
                            $url = Setting::where('key', 'bepoz_secondary_api')->first()->value;

                            //Log::warning("poll_account_total_saved");
                            $last_execution_time = Setting::where('key', 'bepoz_stamp_card_last_successful_execution_time')->first();
                            $datetime = new Carbon($last_execution_time->value);
                            $datetime->subDays(300);
                            // Log::info($datetime);

                            //$datetime = Carbon::createFromFormat('Y-m-d H:i', '2019-01-01 00:00');

                            $payload = [];
                            $payload['procedure'] = "AccPromoUpdatedIndividual";
                            // $payload['parameters'] = [$datetime->format('Y-m-d H:i:s'), $user->member->bepoz_account_id];
                            $payload['parameters'] = [ $user->member->bepoz_account_id];

                            //Log::warning($payload);

                            $post_content = \GuzzleHttp\json_encode($payload);
                            $encrypted = strtoupper(hash_hmac('sha1', $post_content, $mac, false));

                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_HTTPHEADER, array('mac: ' . $encrypted, 'Content-Type: application/json'));
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_content);
                            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                            curl_setopt($ch, CURLOPT_ENCODING, "gzip");

                            $content = curl_exec($ch);
                            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                            curl_close($ch);

                            // Log::warning("AccPromoUpdatedIndividual");
                            // Log::warning($content);
                            // Log::error($httpCode);

                            
                            if ($httpCode >= 200 && $httpCode < 300) {
                                // pass
                                if ($content) {
                                    $payload = \GuzzleHttp\json_decode($content);
                                    if (!$payload->error) {
                                        //Log::warning($content);
                                        //Log::warning($payload);

                                        $missingAccountID = false;
                                        foreach ($payload->data as $data) {
                                            if (isset($data->AccountID)) {
                                                if (intval($data->AccountID) != 0){
                                                    $promo_accounts = PromoAccount::where('venue_id', $data->VenueID)
                                                        ->where('bepoz_account_id', $data->AccountID)
                                                        ->where('bepoz_prize_promo_id', $data->PrizePromoID)->first();

                                                    if ( is_null($promo_accounts) ){
                                                        $promo_accounts = new PromoAccount;
                                                    }

                                                    $needUpdate = false;

                                                    if ($promo_accounts->venue_id != $data->VenueID){
                                                        $promo_accounts->venue_id = $data->VenueID;
                                                        $needUpdate = true;
                                                    }

                                                    if ($promo_accounts->bepoz_account_id != $data->AccountID){
                                                        $promo_accounts->bepoz_account_id = $data->AccountID;
                                                        $member = Member::where('bepoz_account_id', $data->AccountID)->first();
                                                        if ( !is_null($member) ){
                                                            $promo_accounts->member_id = $member->id;
                                                        } else {
                                                            continue;
                                                        }
                                                        $needUpdate = true;
                                                    }

                                                    if ($promo_accounts->bepoz_prize_promo_id != $data->PrizePromoID){
                                                        $promo_accounts->bepoz_prize_promo_id = $data->PrizePromoID;
                                                        $prizePromotion = PrizePromotion::where('bepoz_prize_promo_id', $data->PrizePromoID)->first();
                                                        if ( !is_null($prizePromotion) ){
                                                            $promo_accounts->prize_promotion_id = $prizePromotion->id;
                                                        }
                                                        $needUpdate = true;
                                                    }

                                                    if ($promo_accounts->qty_won_total != $data->QtyWonTotal){
                                                        $promo_accounts->qty_won_total = $data->QtyWonTotal;
                                                        $needUpdate = true;
                                                    }

                                                    if ($promo_accounts->qty_last_day != $data->QtyLastDay){
                                                        $promo_accounts->qty_last_day = $data->QtyLastDay;
                                                        $needUpdate = true;
                                                    }

                                                    if ($promo_accounts->date_last_day != $data->DateLastday){
                                                        $promo_accounts->date_last_day = $data->DateLastday;
                                                        $needUpdate = true;
                                                    }

                                                    if ($promo_accounts->accumulation != intval($data->Accumulation) / 100){
                                                        $promo_accounts->accumulation = intval($data->Accumulation) / 100;
                                                        $needUpdate = true;
                                                    }

                                                    if ($needUpdate){
                                                        $promo_accounts->date_updated = $data->DateUpdated;
                                                        $promo_accounts->save();
                                                    }

                                                    //Log::warning($data->accountid);

                                                }
                                            } else {
                                                $missingAccountID = true;
                                            }
                                        }

                                        if ($missingAccountID){
                                            $log = new SystemLog();
                                            $log->type = 'bepoz-job-error';
                                            $log->humanized_message = 'Polling Stamp Card is failed. Please check the error message';
                                            $log->message = "missing Account ID";
                                            $log->source = 'PollStampCard.php';
                                            $log->save();
                                        }

                                        //$last_execution_time->value = Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s');
                                        //$last_execution_time->save();

                                    }
                                } else {
                                    //Log::error(curl_error($ch));
                                    //Log::error(curl_errno($ch));

                                    $log = new SystemLog();
                                    $log->type = 'bepoz-job-error';
                                    $log->humanized_message = 'Polling Stamp Card is failed. Please check the error message';
                                    $log->message = \GuzzleHttp\json_encode(array("error" => curl_error($ch), "error_code" => curl_errno($ch)));
                                    $log->source = 'PollStampCard.php';
                                    $log->save();
                                }
                            } else {
                                // 404
                                //Log::error($httpCode);

                                $log = new SystemLog();
                                $log->type = 'bepoz-job-error';
                                $log->humanized_message = 'Poll Stamp Card is failed. Please check the error message';
                                $log->message = \GuzzleHttp\json_encode(array("error" => 'AccPromoUpdated '.$httpCode.' Not Found', "error_code" => $httpCode));
                                $log->source = 'PollStampCard.php';
                                $log->save();
                            }
                            

                        } catch (\Exception $e) {

                            Log::warning($e);

                            $log = new SystemLog();
                            $log->type = 'bepoz-job-error';
                            $log->humanized_message = 'Polling Stamp Card is failed. Please check the error message';
                            $log->message = $e;
                            $log->source = 'PollStampCard.php';
                            $log->save();

                        }
                    } else {
                        // Log::info("bepoz_account_id 0");
                    }
                } else {
                    // Log::info("bepoz_account_id null");
                }
            }
        } catch (\Exception $e) {
            Log::error($e);
        }
    }
}

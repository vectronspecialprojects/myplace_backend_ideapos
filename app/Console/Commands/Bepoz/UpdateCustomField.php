<?php

namespace App\Console\Commands\Bepoz;

use App\Address;
use App\BepozFailedJob;
use App\BepozJob;
use App\Member;
use App\ListingType;
use App\MemberTiers;
use App\Order;
use App\OrderDetail;
use App\Setting;
use App\SystemLog;
use App\Tier;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Helpers\Bepoz;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;

class UpdateCustomField extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-bepoz-custom-field';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Bepoz Custom Field';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Bepoz $bepoz)
    {
        try {

                // Log::warning("UpdateCustomField");

                $checkBepozConnection = $bepoz->SystemCheck();

                if ($checkBepozConnection) {
                    $jobs = BepozJob::where('reserved', '=', 0)
                        ->where('queue', '=', 'update-custom-field')
                        ->chunk(100, function ($jobs) use ($bepoz) {
                            // Reserve the selected jobs first before the execution
                            foreach ($jobs as $bepoz_job) {
                                $bepoz_job->setJobUID(Uuid::generate());
                                $bepoz_job->reserve();
                            }

                            foreach ($jobs as $bepoz_job) {

                                if ($bepoz_job->attempts() < 3) {
                                    $bepoz_job->attempts = intval($bepoz_job->attempts) + 1;
                                    $bepoz_job->save();

                                    try {

                                        $data = \GuzzleHttp\json_decode($bepoz_job->payload);
                                        $member = Member::with('user')->find($data->member_id);

                                        if (!is_null($member->bepoz_account_id) && $member->bepoz_account_id !== 'null') {
                                            // Log::warning("UpdateCustomField bepoz id " .$member->bepoz_account_id);

                                            // // FEATURE FROM VARSITY, NEED MAKE GLOBAL VARIABLE
                                            // $promo_code_enable = Setting::where('key', 'promo_code_enable')->first()->value;
                                            // if ( $promo_code_enable == "true" ) {
                                            //     $this->setCustomField($bepoz, $member, "Sponsorship Code");
                                            // }

                                            // $sponsorship_code_enable = Setting::where('key', 'sponsorship_code_enable')->first()->value;
                                            // if ( $sponsorship_code_enable == "true" ) {
                                            //     $this->setCustomField($bepoz, $member, "Promo Code");
                                            // }

                                            //UPDATE CUSTOM FIELD
                                            $setting = Setting::where('key', 'bepozcustomfield')->first();
                                            $allCustomField = \GuzzleHttp\json_decode($setting->extended_value);

                                            $app_flag_id_bepoz = Setting::where('key', 'app_flag_id_bepoz')->first()->value;
                                            $app_flag_name_bepoz = Setting::where('key', 'app_flag_name_bepoz')->first()->value;

                                            $customFieldUpdate = false;
                                            $customFields = array();

                                            
                                            if ($app_flag_id_bepoz != "false") {
                                                $customFieldUpdate = true;

                                                $customFields['CustomField'][] = array(
                                                    'FieldName' => $app_flag_name_bepoz,
                                                    'FieldType' => 0,
                                                    'FieldValue' => true,
                                                    'FieldNum' => $app_flag_id_bepoz
                                                );
                                            }

                                            if (!$customFieldUpdate) {
                                                        
                                                // ALL DONE NO NEED SEND CUSTOM FIELD
                                                $bepoz_job->dispatch();
                                                
                                            } else {

                                                $EntityType = 0;// Account
                                                $EntityID = $member->bepoz_account_id;

                                                $customFieldResult = $bepoz->CustomFieldsSet($EntityType, $EntityID, $customFields);

                                                // Log::warning($customFieldResult);
                                                if ($customFieldResult) {
                                                    if (!is_null($bepoz_job->failed_job_uid)) {
                                                        $failed_job = BepozFailedJob::where('job_uid', $bepoz_job->failed_job_uid)->first();
                                                        if (!is_null($failed_job)) {
                                                            $failed_job->delete();
                                                        }
                                                    }

                                                    $bepoz_job->dispatch();
                                                } else {
                                                    $log = new SystemLog();
                                                    $log->type = 'bepoz-job-error';
                                                    $log->humanized_message = 'Updating account Custom Field is failed. Please check log.';
                                                    $log->payload = $bepoz_job->payload;
                                                    $log->message = $customFieldResult;
                                                    $log->source = 'UpdateCustomField.php';
                                                    $log->save();

                                                    // try again
                                                    $bepoz_job->free();
                                                }

                                            }

                                        } else {
                                            $bepoz_job->free();
                                        }

                                    } catch (\Exception $e) {
                                        $log = new SystemLog();
                                        $log->type = 'bepoz-job-error';
                                        $log->humanized_message = 'Updating Account Custom Field is failed. Please check the error message.';
                                        $log->message = $e;
                                        $log->payload = $bepoz_job->payload;
                                        $log->source = 'UpdateCustomField.php';
                                        $log->save();

                                        $bepoz_job->free();
                                    }

                                } else {
                                    if (is_null($bepoz_job->failed_job_uid)) {

                                        $failed_job = BepozFailedJob::where('payload', $bepoz_job->payload)
                                            ->where('queue', $bepoz_job->queue)
                                            ->first();

                                        if (is_null($failed_job)) {
                                            $failed_job = new BepozFailedJob;
                                            $failed_job->queue = $bepoz_job->queue;
                                            $failed_job->payload = $bepoz_job->payload;
                                            $failed_job->job_uid = Uuid::generate(4);
                                            $failed_job->save();
                                        }
                                    }

                                    $bepoz_job->dispatch();

                                }

                            }
                        }
                    );

                }

        } catch (\Exception $e) {

            Log::warning($e);

            $log = new SystemLog();
            $log->type = 'bepoz-job-error';
            $log->humanized_message = 'Updating account is failed. Please check the error message.';
            $log->message = $e;
            $log->source = 'UpdateCustomField.php';
            $log->save();
        }

    }

    function setCustomField($bepoz, $member, $CustomFieldKey)
    {

        $setting = Setting::where('key', 'bepozcustomfield')->first();

        $allCustomField = \GuzzleHttp\json_decode($setting->extended_value);
        $CustomFieldNumber = "0";

        foreach ($allCustomField as $data) {
            if ($data->key == $CustomFieldKey) {
                $CustomFieldNumber = $data->field;
            }
        }

        //Log::info($CustomFieldNumber);
        $dataset = "";
        if ($CustomFieldKey == "Sponsorship Code") {
            $dataset = $member->sponsorship_code;
        } else if ($CustomFieldKey == "Promo Code") {
            $dataset = $member->promo_code;
        }

        // CHECK EMPTY
        if (!empty($dataset)) {
            $param = ['AccountID' => $member->bepoz_account_id,
                'CustomFieldNumber' => $CustomFieldNumber,
                'DataSet' => $dataset
            ];

            $customFieldResult = $bepoz->AccCustomFieldSet($param);

            if ($customFieldResult) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }

        return false;
    }

}

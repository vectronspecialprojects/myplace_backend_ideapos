<?php

namespace App\Console\Commands\Bepoz;

use App\BepozFailedJob;
use App\BepozJob;
use App\Jobs\SendOneSignalNotification;
use App\Member;
use App\MemberLog;
use App\MemberTiers;
use App\Setting;
use App\SystemLog;
use App\Tier;
use Illuminate\Console\Command;
use App\Helpers\Bepoz;
use Illuminate\Support\Facades\Log;
use Webpatser\Uuid\Uuid;

class SyncImage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync-image';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Image to Bepoz Account';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Bepoz $bepoz)
    {
        try {

            $mac = Setting::where('key', 'bepoz_mac_key')->first()->value;
            $url = Setting::where('key', 'bepoz_secondary_api')->first()->value;
            $send_image_bepoz = Setting::where('key', 'send_image_bepoz')->first()->value;

            //Log::warning($send_image_bepoz);

            $checkBepozConnection = $bepoz->SystemCheck();

            //check from setting this feature enabled
            if ($send_image_bepoz == "true") {

                //Log::warning("send_image_bepoz");

                if ($checkBepozConnection) {

                    $jobs = BepozJob::where('reserved', '=', 0)
                        ->where('queue', '=', 'sync-image')
                        ->limit(20)
                        ->get();


                    if (!$jobs->isEmpty()) {

                        // Reserve the selected jobs first before the execution
                        foreach ($jobs as $bepoz_job) {
                            $bepoz_job->setJobUID(Uuid::generate());
                            $bepoz_job->reserve();
                        }

                        foreach ($jobs as $bepoz_job) {
                            //Log::warning(print_r($bepoz_job,true));


                            if ($bepoz_job->attempts() < 3) {
                                $bepoz_job->attempts = intval($bepoz_job->attempts) + 1;
                                $bepoz_job->save();

                                try {
                                    $data = \GuzzleHttp\json_decode($bepoz_job->payload);
                                    $member = Member::where('bepoz_account_id', $data->bepoz_account_id)->first();

                                    //$path = $data->profile_img;
                                    //$type = pathinfo($path, PATHINFO_EXTENSION);

                                    //Log::warning($data->profile_img);

                                    $picture = file_get_contents($data->profile_img);

                                    $base64 = base64_encode($picture);

                                    //upload image
                                    $payload = [];
                                    $payload['procedure'] = "ReceiveImage";
                                    $payload['parameters'] = [$data->bepoz_account_id, $base64];

                                    $post_content = \GuzzleHttp\json_encode($payload);
                                    $encrypted = strtoupper(hash_hmac('sha1', $post_content, $mac, false));

                                    $ch = curl_init();
                                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('mac:' . $encrypted, 'Content-Type:application/json'));
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    curl_setopt($ch, CURLOPT_URL, $url);
                                    curl_setopt($ch, CURLOPT_POST, true);
                                    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_content);
                                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                                    curl_setopt($ch, CURLOPT_TIMEOUT, 15);

                                    $content = curl_exec($ch);
                                    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                                    curl_close($ch);

                                    //get image id
                                    $ImageDataID = "";

                                    //Log::info($content);
                                    //Log::info($httpCode);
                                    //MAKE SURE RESPONSE NOT ERROR
                                    if ($httpCode >= 200 && $httpCode < 300) {

                                        //CHECK THE RESULT
                                        if ($content) {

                                            $payloadImage = \GuzzleHttp\json_decode($content);
                                            //Log::warning($payloadImage);
                                            if (!$payloadImage->error) {
                                                //Log::warning("not error");
                                                //LOOP DATA
                                                //foreach ($payload->data as $data) {
                                                //$ImageDataID = $data->SCOPE_IDENTITY."<br>";
                                                //$ImageDataID = $data->SCOPE_IDENTITY;
                                                //}
                                                //Log::warning($payloadImage->data[0]->SCOPE_IDENTITY);

                                                //$ImageDataID = $payloadImage->data[0]->ImageID;


                                                //$member->bepoz_image_id = $payloadImage->data[0]->ImageID;
                                                //$member->save();

                                                $bepoz_job->dispatch();
                                            }
                                        }
                                    }

                                } catch (\Exception $e) {
                                    $log = new SystemLog();
                                    $log->type = 'bepoz-job-error';
                                    $log->humanized_message = 'Syncing image is failed. Please check the error message.';
                                    $log->message = $e;
                                    $log->payload = $bepoz_job->payload;
                                    $log->source = 'SyncImage.php';
                                    $log->save();

                                    $bepoz_job->free();
                                }

                            } else {
                                if (is_null($bepoz_job->failed_job_uid)) {

                                    $failed_job = BepozFailedJob::where('payload', $bepoz_job->payload)
                                        ->where('queue', $bepoz_job->queue)
                                        ->first();

                                    if (is_null($failed_job)) {
                                        $failed_job = new BepozFailedJob;
                                        $failed_job->queue = $bepoz_job->queue;
                                        $failed_job->payload = $bepoz_job->payload;
                                        $failed_job->job_uid = Uuid::generate(4);
                                        $failed_job->save();
                                    }
                                }

                                // Maximum attempt log no longer needed
                                // $log = new SystemLog();
                                // $log->type = 'bepoz-job-error';
                                // $log->humanized_message = 'Syncing image is failed. Maximum attempts has been reached.';
                                // $log->source = 'SyncImage.php';
                                // $log->payload = $bepoz_job->payload;
                                // $log->save();

                                $bepoz_job->dispatch();

                            }

                        }

                    }
                }

            }


        } catch (\Exception $e) {

            Log::warning($e);

            $log = new SystemLog();
            $log->type = 'bepoz-job-error';
            $log->humanized_message = 'Syncing image is failed. Please check the error message.';
            $log->message = $e;
            $log->source = 'SyncImage.php';
            $log->save();

        }

    }


}

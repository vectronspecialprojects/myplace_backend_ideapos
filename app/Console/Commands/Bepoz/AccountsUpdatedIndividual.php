<?php

namespace App\Console\Commands\Bepoz;

use App\FriendReferral;
use App\Member;
use App\MemberTiers;
use App\MemberLog;
use App\Setting;
use App\Tier;
use App\Venue;
use App\VenuePivotTag;
use App\VenueTag;
use App\SystemLog;
use App\Jobs\Job;
use App\Helpers\Bepoz;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class AccountsUpdatedIndividual extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'accounts-updated-individual {memberid?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Call Bepoz stored procedure AccountsUpdatedIndividual with Bepoz ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Bepoz $bepoz)
    {
        try {
            // Log::info("AccountsUpdatedIndividual");
            if (empty($this->argument('memberid'))) {
                // add delay to prevent duplicate
                // sleep(30);
            } else {

                $member = Member::find($this->argument('memberid'));
                $user = $member->user;

                if (!is_null($user->member->bepoz_account_id)) {
                    if (intval($user->member->bepoz_account_id) > 0) {
                        // Log::info("AccountsUpdatedIndividual bepoz ID good");
                        try {
                            $mac = Setting::where('key', 'bepoz_mac_key')->first()->value;
                            $url = Setting::where('key', 'bepoz_secondary_api')->first()->value;

                            $payload = [];
                            $payload['procedure'] = "AccountsUpdatedIndividual";
                            // $payload['parameters'] = [$last_execution_time->value];
                            $payload['parameters'] = [$user->member->bepoz_account_id];
                            // Log::info($payload);

                            $post_content = \GuzzleHttp\json_encode($payload);
                            $encrypted = strtoupper(hash_hmac('sha1', $post_content, $mac, false));


                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_HTTPHEADER, array('mac: ' . $encrypted, 'Content-Type: application/json'));
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_content);
                            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                            curl_setopt($ch, CURLOPT_ENCODING, "gzip");

                            $content = curl_exec($ch);
                            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                            curl_close($ch);

                            // Log::error($content);
                            // Log::error($httpCode);


                            if ($httpCode >= 200 && $httpCode < 300) {
                                // pass
                                if ($content) {
                                    $payload = \GuzzleHttp\json_decode($content);
                                    if (!$payload->error) {
                                        // $last_execution_time->value = Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s');
                                        // $last_execution_time->save();

                                        foreach ($payload->data as $data) {
                                            if (!isset($data->accountid)) {
                                                continue;
                                            }

                                            $tier = Tier::where('bepoz_group_id', $data->groupid)
                                                ->where('is_active', true)
                                                ->first();

                                            
                                                if (!is_null($member)) {
                                                    if (strtolower($data->email) == strtolower($member->user->email)) {
                                                        $user = $member->user;
                                                        $cloned = $user->member->replicate();

                                                        // $member->user->email = empty($data->email) ? "guest+".Carbon::now('UTC')->timestamp."@vectron.com.au" : $data->email;
                                                        if (!$tier->dont_update_from_bepoz) {
                                                            $member->first_name = $data->firstname;
                                                            $member->last_name = $data->lastname;
                                                        }

                                                        $member->bepoz_account_number = $data->accnumber;
                                                        $member->bepoz_account_card_number = $data->cardnumber;
                                                        // $member->points = intval($data->points) / 100;
                                                        $member->phone = $data->phonehome;
                                                        $member->dob = $data->datebirth;
                                                        // $member->balance = intval($data->balance) / 100;

                                                        if (isset($data->usecalinkbal)) {
                                                            $member->bepoz_UseCAlinkBalance = $data->usecalinkbal;
                                                        }

                                                        if (isset($data->usecalinkpnts)) {
                                                            $member->bepoz_UseCALinkPoints = $data->usecalinkpnts;
                                                        }

                                                        if (intval($member->bepoz_count_visits) !== intval($data->countvisits)) {
                                                            $friend_referral = FriendReferral::where('has_made_a_purchase', false)
                                                                ->where('new_member_id', $member->id)
                                                                ->where('is_rewarded', false)
                                                                ->where('is_accepted', true)
                                                                ->where('has_joined', true)
                                                                ->where('reward_option', 'after_purchase')
                                                                ->first();

                                                            if (!is_null($friend_referral)) {
                                                                $friend_referral->has_made_a_purchase = true;
                                                                $friend_referral->made_purchase_at = Carbon::now(config('app.timezone'));
                                                                $friend_referral->save();
                                                            }
                                                        }

                                                        $member->bepoz_count_visits = $data->countvisits;

                                                        switch (intval($data->status)) {
                                                            case 0:
                                                                $member->bepoz_account_status = "OK";
                                                                break;
                                                            case 1:
                                                                $member->bepoz_account_status = "On Hold";
                                                                break;
                                                            case 2:
                                                                $member->bepoz_account_status = "Pending";
                                                                break;
                                                            case 3:
                                                                $member->bepoz_account_status = "Suspended";
                                                                break;
                                                            case 4:
                                                                $member->bepoz_account_status = "Inactive";
                                                                break;
                                                            case 5:
                                                                $member->bepoz_account_status = "Deceased";
                                                                break;
                                                            default:
                                                                $member->bepoz_account_status = "OK";
                                                        }

                                                        $member->save();

                                                        $member->user->mobile = $data->mobile;
                                                        $member->user->save();

                                                        $mt = MemberTiers::where('member_id', $member->id)->first();
                                                        $mt->date_expiry = $data->datetimeexpiry;
                                                        if (!is_null($tier)) {
                                                            $mt->tier_id = $tier->id;
                                                        }
                                                        $mt->save();

                                                        $ml = new MemberLog();
                                                        $ml->before = $cloned->toJson();
                                                        $ml->member_id = $user->member->id;
                                                        $ml->message = 'System (Bepoz) updated member ' . $member->id . ' details. ';
                                                        $ml->after = $user->member->toJson();
                                                        $ml->save();
                                                    } else {

                                                        $log = new SystemLog();
                                                        $log->type = 'bepoz-job-error';
                                                        $log->humanized_message = 'Polling account member ' . $member->id . ' is failed. Please check the error message';
                                                        $log->message = "Email from Bepoz doesn't match email from database";
                                                        $log->source = 'AccountsUpdatedIndividual.php';
                                                        $log->save();

                                                        // $this->changeBepozID($member);
                                                    }
                                                    $mt->save();

                                                    $SSOAccountGUID = $this->SSOAccountGUID($member->user->email);

                                                    // Log::info($SSOAccountGUID);
                                                    // Log::info($user->member->login_token);
                                                    if ($SSOAccountGUID == $member->login_token) {
                                                        // UPDATE SSO
                                                        $this->updateSSO($user);
                                                    }

                                                    $ml = new MemberLog();
                                                    $ml->before = $cloned->toJson();
                                                    $ml->member_id = $user->member->id;
                                                    $ml->message = 'System (Bepoz) updated member ' . $member->id . ' details. ';
                                                    $ml->after = $user->member->toJson();
                                                    $ml->save();
                                                } else {

                                                    $log = new SystemLog();
                                                    $log->type = 'bepoz-job-error';
                                                    $log->humanized_message = 'Polling account member ' . $member->id . ' is failed. Please check the error message';
                                                    $log->message = "Email from Bepoz doesn't match email from database";
                                                    $log->source = 'AccountsUpdatedIndividual.php';
                                                    $log->save();

                                                    // $this->changeBepozID($member);
                                                }
                                            

                                        }
                                    }
                                } else {
                                    // Log::error(curl_error($ch));
                                    // Log::error(curl_errno($ch));

                                    // $log = new SystemLog();
                                    // $log->type = 'bepoz-job-error';
                                    // $log->humanized_message = 'Polling account is failed. Please check the error message';
                                    // $log->message = \GuzzleHttp\json_encode(array("error" => curl_error($ch), "error_code" => curl_errno($ch)));
                                    // $log->source = 'AccountsUpdatedIndividual.php';
                                    // $log->save();
                                }
                            } else {
                                // 404
                                // Log::error($httpCode);

                                // $log = new SystemLog();
                                // $log->type = 'bepoz-job-error';
                                // $log->humanized_message = 'Polling account is failed. Please check the error message';
                                // $log->message = \GuzzleHttp\json_encode(array("error" => '404 Not Found', "error_code" => $httpCode));
                                // $log->source = 'AccountsUpdatedIndividual.php';
                                // $log->save();
                            }


                        } catch (\Exception $e) {

                            Log::warning($e);

                            $log = new SystemLog();
                            $log->type = 'bepoz-job-error';
                            $log->humanized_message = 'Polling account is failed. Please check the error message';
                            $log->message = $e;
                            $log->source = 'AccountsUpdatedIndividual.php';
                            $log->save();

                        }

                    } else {
                        // Log::info("bepoz_account_id 0");
                    }
                } else {
                    // Log::info("bepoz_account_id null");
                }
            }
        } catch (\Exception $e) {
            Log::error($e);
        }
    }

        
    /**
     * CHECK SSO GUID
     *
     * @return mixed
     */
    protected function SSOAccountGUID($email)
    {
        $your_order_api = Setting::where('key', 'your_order_api')->first()->value;
        $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

        $datacheck = array(
            'email' => $email,
            "license_key" => $your_order_key
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $your_order_api . "/checkSubAccount");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datacheck);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_ENCODING, "gzip");
        $content_check = curl_exec($ch);
        $httpCode_check = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // Log::info("542");
        // Log::info($content_check);
        // Log::info($httpCode_check);

        $payload_check = \GuzzleHttp\json_decode($content_check);
        if ($payload_check->status == "ok") {
            return $payload_check->guid;
        }

        if ($payload_check->status == "error") {
            return false;
        }

        return false;
    }


    /**
     * UPDATE SSO
     *
     * @return mixed
     */
    protected function updateSSO($user)
    {
        try {
            $your_order_api = Setting::where('key', 'your_order_api')->first()->value;
            $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

            $datasend = array(
                'given_name' => $user->member->first_name,
                'family_name' => $user->member->last_name,
                'phone_number' => $user->mobile,
                'email' => $user->email,
                'bepoz_id' => $user->member->bepoz_account_id,
                'bepoz_account_number' => $user->member->bepoz_account_number,
                'bepoz_account_card_number' => $user->member->bepoz_account_card_number,
                'guid' => $user->member->login_token,
                'license_key' => $your_order_key
            );

            // $payload = json_encode( $datasend );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $your_order_api . "/updateAccount");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            curl_setopt($ch, CURLOPT_ENCODING, "gzip");
            $content = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            // Log::warning("AuthenticationController updateSSO");
            // Log::warning($your_order_api);
            // Log::warning($payload);
            // Log::warning($content);
            // Log::warning($httpCode);

            if ($content) {
                $payload = \GuzzleHttp\json_decode($content);
                if ($payload->status == "error" && $payload->error == "user_not_found") {
                    // return response()->json(['status' => 'error', 'message' => 'Email not found'], Response::HTTP_BAD_REQUEST);
                    return false;
                }

                return true;
            } else {
                // return response()->json(['status' => 'error', 'message' => 'Unable to connect'], Response::HTTP_BAD_REQUEST);
                return false;
            }

            return false;

        } catch (\Exception $e) {
            // Log::error($e);

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}

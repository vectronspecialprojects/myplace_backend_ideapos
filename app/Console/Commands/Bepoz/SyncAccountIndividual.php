<?php

namespace App\Console\Commands\Bepoz;

use App\BepozFailedJob;
use App\BepozJob;
use App\Jobs\SendOneSignalNotification;
use App\Member;
use App\MemberLog;
use App\MemberTiers;
use App\Setting;
use App\SystemLog;
use App\Tier;
use App\Venue;
use App\VenuePivotTag;
use App\VenueTag;
use App\Helpers\Bepoz;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Webpatser\Uuid\Uuid;
use App\Jobs\Job;
use Carbon\Carbon;

class SyncAccountIndividual extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync-account-individual {memberid?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Call Bepoz stored procedure SyncAccountIndividual with Bepoz ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Bepoz $bepoz)
    {
        try {
            if (empty($this->argument('memberid'))) {
                Log::error("SyncAccountIndividual missing member id");
                // add delay to prevent duplicate
                // sleep(30);
            } else {

                $member = Member::find( $this->argument('memberid') );
                $user = $member->user;

                if (is_null($user->member->bepoz_account_id)){

                    $gaming_system_enable = Setting::where('key', 'gaming_system_enable')->first()->value;

                    if ( $gaming_system_enable == "false" ) {
                        // NORMAL LOGIC
                        $bepozcustomfield_enable = Setting::where('key', 'bepozcustomfield_enable')->first()->value;

                        $datamember = array(
                            "member_id" => $member->id,
                            "type" => 'email'
                        );
                        $data = (object) $datamember;

                        try {

                            $groupID = $member->member_tier->tier->bepoz_group_id;
                            $template = $member->tiers()->first()->bepoz_template_id;
                            $tier = $member->tiers()->first();

                            // Create or Update Bepoz Account
                            // search Bepoz Account from email
                            $result = $bepoz->accountSearch(8, $member->user->email);
                            $complete = false;
                            $test = false;

                            if ($result) {
                                Log::info("88 SyncAccountIndividual result OK");
                                if ($result['IDList']['Count'] == 0) {
                                    Log::info("90 SyncAccountIndividual IDList 0");
                                    // no account, get from create member backpanel
                                    $request = array();
                                    $request['AccountID'] = 0;

                                    // MANUAL ENTRY NOT LONGER NEEDED BECAUSE DISABLE MEMBER CREATE FROM BACKPANEL
                                    // if ($tier->manual_entry){
                                    //     //use account number set from backpanel
                                    //     $accNumber = $member->bepoz_account_number;
                                    // } else 
                                    
                                    if ( $gaming_system_enable == "true" ) {
                                        //use account number set from backpanel
                                        $accNumber = $member->bepoz_account_number;
                                    } else {
                                        $accNumber = $this->recursiveAccNumber($this->getNewAccNumber($member->member_tier->tier), $bepoz, $member->member_tier->tier);
                                    }
                                    $request['AccNumber'] = $accNumber;
                                    $request['CardNumber'] = $accNumber;
                                    $request['GroupID'] = $groupID;
                                    $request['UseGroupSettings'] = is_null($groupID) ? 0 : 1;
                                    $request['FirstName'] = $this->checkEmoji($member->first_name) ? str_replace('"', '', json_encode($member->first_name)) : $member->first_name;
                                    $request['LastName'] = $this->checkEmoji($member->last_name) ? str_replace('"', '', json_encode($member->last_name)) : $member->last_name;
                                    $request['DateBirth'] = $member->dob;
                                    $request['Mobile'] = $member->user->mobile;
                                    $request['PhoneHome'] = $member->phone;
                                    $request['Email1st'] = $member->user->email;
                                    $test = $bepoz->AccUpdate($request, $template);

                                    if (is_array($test)) {
                                        $member->bepoz_account_status = "OK";
                                        $member->bepoz_account_number = $accNumber;
                                        $member->bepoz_account_card_number = $accNumber;
                                        $member->bepoz_account_id = $test['AccountFull']['AccountID'];
                                        $complete = true;
                                    }

                                } else if ($result['IDList']['Count'] == 1) {
                                    // 1 account
                                    $temp = $bepoz->AccountFullGet($result['IDList']['ID']);
                                    Log::info("124 SyncAccountIndividual IDList 1");
                                    if ($temp['AccountFull']['Status'] == 0) { // if the status is active

                                        // acc active
                                        if (empty($temp['AccountFull']['AccNumber'])) {
                                            Log::info("129 SyncAccountIndividual IDList 1 empty AccNumber");
                                            // acc active, no acc number, get from create member backpanel
                                            $request = array();
                                            $request['AccountID'] = 0;

                                            // MANUAL ENTRY NOT LONGER NEEDED BECAUSE DISABLE MEMBER CREATE FROM BACKPANEL
                                            // if ($tier->manual_entry){
                                            //     //use account number set from backpanel
                                            //     $accNumber = $member->bepoz_account_number;
                                            // } else 
                                            
                                            if ( $gaming_system_enable == "true" ) {
                                                //use account number set from backpanel
                                                $accNumber = $member->bepoz_account_number;
                                            } else {
                                                $accNumber = $this->recursiveAccNumber($this->getNewAccNumber($member->member_tier->tier), $bepoz, $member->member_tier->tier);
                                            }
                                            $request['AccNumber'] = $accNumber;
                                            $request['CardNumber'] = $accNumber;
                                            $request['GroupID'] = $groupID;
                                            $request['UseGroupSettings'] = is_null($groupID) ? 0 : 1;
                                            $request['FirstName'] = $this->checkEmoji($member->first_name) ? str_replace('"', '', json_encode($member->first_name)) : $member->first_name;
                                            $request['LastName'] = $this->checkEmoji($member->last_name) ? str_replace('"', '', json_encode($member->last_name)) : $member->last_name;
                                            $request['DateBirth'] = $member->dob;
                                            $request['Mobile'] = $member->user->mobile;
                                            $request['PhoneHome'] = $member->phone;
                                            $request['Email1st'] = $member->user->email;
                                            $test = $bepoz->AccUpdate($request, $template);

                                            if (is_array($test)) {
                                                $member->bepoz_account_status = "OK";
                                                $member->bepoz_account_number = $accNumber;
                                                $member->bepoz_account_card_number = $accNumber;
                                                $member->bepoz_account_id = $test['AccountFull']['AccountID'];
                                                $complete = true;
                                            }

                                        } else {
                                            Log::info("162 SyncAccountIndividual there is acc number");

                                            // there is acc number
                                            $request = $temp['AccountFull'];
                                            $request['GroupID'] = $groupID;
                                            $request['UseGroupSettings'] = is_null($groupID) ? 0 : 1;
                                            $request['FirstName'] = $this->checkEmoji($member->first_name) ? str_replace('"', '', json_encode($member->first_name)) : $member->first_name;
                                            $request['LastName'] = $this->checkEmoji($member->last_name) ? str_replace('"', '', json_encode($member->last_name)) : $member->last_name;
                                            $request['DateBirth'] = $member->dob;
                                            $request['Mobile'] = $member->user->mobile;
                                            $request['PhoneHome'] = $member->phone;

                                            // EXISTING ACC NEED CHANGE CARD NUMBER
                                            $CardNumberLength = strlen($request['CardNumber']);
                                            if ($tier->force_card_range){
                                                if ($CardNumberLength == 5 || $CardNumberLength == 6){
                                                    $newCardNumber = $this->bepozNextAccountNumber($bepoz, $tier);
                                                    $request['AccNumber'] = $newCardNumber;
                                                    $request['CardNumber'] = $newCardNumber;
                                                }
                                            }

                                            unset($request['AddressID']);
                                            unset($request['CommentID']);
                                            $test = $bepoz->AccUpdate($request, $template);

                                            if (is_array($test)) {
                                                $tier = Tier::where('bepoz_group_id', $test['AccountFull']['GroupID'])
                                                    ->where('is_active', true)
                                                    ->first();

                                                $mt = MemberTiers::where('member_id', $member->id)->first();
                                                $mt->date_expiry = $test['AccountFull']['DateTimeExpiry'];
                                                if (!is_null($tier)) {
                                                    $mt->tier_id = $tier->id;
                                                }
                                                $mt->save();

                                                // $pts = $this->getBalance($bepoz, $test['AccountFull']['AccountID']);
                                                // $member->points = $pts["PointBalance"];
                                                // $member->balance = $pts["AccountBalance"];
                                                // $member->points = (intval($test['AccountFull']['PointsEarned']) - intval($test['AccountFull']['PointsRedeemed']) - intval($test['AccountFull']['PointsExpired'])) / 100;
                                                // $member->balance = $test['AccountFull']['AccountBalance'] / 100;
                                                $member->bepoz_account_number = $test['AccountFull']['AccNumber'];
                                                $member->bepoz_account_card_number = $test['AccountFull']['CardNumber'];
                                                $member->bepoz_account_id = $test['AccountFull']['AccountID'];
                                                $member->bepoz_existing_account = true;
                                                $member->bepoz_account_status = "OK";
                                                $complete = true;
                                            }

                                        }

                                    } else {
                                        Log::info("229 SyncAccountIndividual acc not active, no acc number, get from create member backpanel");
                                        // acc not active, no acc number, get from create member backpanel
                                        $request = array();
                                        $request['AccountID'] = 0;

                                        // MANUAL ENTRY NOT LONGER NEEDED BECAUSE DISABLE MEMBER CREATE FROM BACKPANEL
                                        // if ($tier->manual_entry){
                                        //     //use account number set from backpanel
                                        //     $accNumber = $member->bepoz_account_number;
                                        // } else 
                                        
                                        if ( $gaming_system_enable == "true" ) {
                                            //use account number set from backpanel
                                            $accNumber = $member->bepoz_account_number;
                                        } else {
                                            $accNumber = $this->recursiveAccNumber($this->getNewAccNumber($member->member_tier->tier), $bepoz, $member->member_tier->tier);
                                        }
                                        $request['AccNumber'] = $accNumber;
                                        $request['CardNumber'] = $accNumber;
                                        $request['GroupID'] = $groupID;
                                        $request['UseGroupSettings'] = is_null($groupID) ? 0 : 1;
                                        $request['FirstName'] = $this->checkEmoji($member->first_name) ? str_replace('"', '', json_encode($member->first_name)) : $member->first_name;
                                        $request['LastName'] = $this->checkEmoji($member->last_name) ? str_replace('"', '', json_encode($member->last_name)) : $member->last_name;
                                        $request['DateBirth'] = $member->dob;
                                        $request['Mobile'] = $member->user->mobile;
                                        $request['PhoneHome'] = $member->phone;
                                        $request['Email1st'] = $member->user->email;
                                        $test = $bepoz->AccUpdate($request, $template);

                                        if (is_array($test)) {
                                            $member->bepoz_account_status = "OK";
                                            $member->bepoz_account_number = $accNumber;
                                            $member->bepoz_account_card_number = $accNumber;
                                            $member->bepoz_account_id = $test['AccountFull']['AccountID'];
                                            $complete = true;
                                        }
                                    }

                                } else {
                                    Log::info("265 SyncAccountIndividual many acc-s");
                                    // many acc-s
                                    $created = false;
                                    foreach ($result['IDList']['ID']['int'] as $id) {
                                        $temp = $bepoz->AccountFullGet($id);
                                        Log::info("251 SyncAccountIndividual many acc-s");

                                        if ($temp['AccountFull']['Status'] == 0) { // if the status is active
                                            Log::info("254 SyncAccountIndividual many acc-s status 0");

                                            // there is active acc
                                            if (empty($temp['AccountFull']['AccNumber'])) {

                                                Log::info("258 SyncAccountIndividual many acc-s status 0 empty AccNumber");
                                                // acc active, no acc number, get from create member backpanel
                                                $request = array();
                                                $request['AccountID'] = 0;

                                                // MANUAL ENTRY NOT LONGER NEEDED BECAUSE DISABLE MEMBER CREATE FROM BACKPANEL
                                                // if ($tier->manual_entry){
                                                //     //use account number set from backpanel
                                                //     $accNumber = $member->bepoz_account_number;
                                                // } else 
                                                
                                                if ( $gaming_system_enable == "true" ) {
                                                    //use account number set from backpanel
                                                    $accNumber = $member->bepoz_account_number;
                                                } else {
                                                    $accNumber = $this->recursiveAccNumber($this->getNewAccNumber($member->member_tier->tier), $bepoz, $member->member_tier->tier);
                                                }
                                                $request['AccNumber'] = $accNumber;
                                                $request['CardNumber'] = $accNumber;
                                                $request['GroupID'] = $groupID;
                                                $request['UseGroupSettings'] = is_null($groupID) ? 0 : 1;
                                                $request['FirstName'] = $this->checkEmoji($member->first_name) ? str_replace('"', '', json_encode($member->first_name)) : $member->first_name;
                                                $request['LastName'] = $this->checkEmoji($member->last_name) ? str_replace('"', '', json_encode($member->last_name)) : $member->last_name;
                                                $request['DateBirth'] = $member->dob;
                                                $request['Mobile'] = $member->user->mobile;
                                                $request['PhoneHome'] = $member->phone;
                                                $request['Email1st'] = $member->user->email;
                                                $test = $bepoz->AccUpdate($request, $template);

                                                if (is_array($test)) {
                                                    $member->bepoz_account_status = "OK";
                                                    $member->bepoz_account_number = $accNumber;
                                                    $member->bepoz_account_card_number = $accNumber;
                                                    $member->bepoz_account_id = $test['AccountFull']['AccountID'];
                                                    $complete = true;
                                                }

                                            } else {
                                                Log::info("290 SyncAccountIndividual there is acc number");

                                                // there is acc number
                                                $request = $temp['AccountFull'];
                                                $request['GroupID'] = $groupID;
                                                $request['UseGroupSettings'] = is_null($groupID) ? 0 : 1;
                                                $request['FirstName'] = $this->checkEmoji($member->first_name) ? str_replace('"', '', json_encode($member->first_name)) : $member->first_name;
                                                $request['LastName'] = $this->checkEmoji($member->last_name) ? str_replace('"', '', json_encode($member->last_name)) : $member->last_name;
                                                $request['DateBirth'] = $member->dob;
                                                $request['Mobile'] = $member->user->mobile;
                                                $request['PhoneHome'] = $member->phone;

                                                // EXISTING ACC NEED CHANGE CARD NUMBER
                                                $CardNumberLength = strlen($request['CardNumber']);
                                                if ($tier->force_card_range){
                                                    if ($CardNumberLength == 5 || $CardNumberLength == 6){
                                                        $newCardNumber = $this->bepozNextAccountNumber($bepoz, $tier);
                                                        $request['AccNumber'] = $newCardNumber;
                                                        $request['CardNumber'] = $newCardNumber;
                                                    }
                                                }

                                                unset($request['AddressID']);
                                                unset($request['CommentID']);
                                                $result_update = $bepoz->AccUpdate($request, $template);

                                                if ($result_update) {

                                                    $tier = Tier::where('bepoz_group_id', $result_update['AccountFull']['GroupID'])
                                                        ->where('is_active', true)
                                                        ->first();

                                                    $mt = MemberTiers::where('member_id', $member->id)->first();
                                                    $mt->date_expiry = $result_update['AccountFull']['DateTimeExpiry'];
                                                    if (!is_null($tier)) {
                                                        $mt->tier_id = $tier->id;
                                                    }
                                                    $mt->save();

                                                    // $pts = $this->getBalance($bepoz, $result_update['AccountFull']['AccountID']);
                                                    // $member->points = $pts["PointBalance"];
                                                    // $member->balance = $pts["AccountBalance"];
                                                    // $member->points = (intval($result_update['AccountFull']['PointsEarned']) - intval($result_update['AccountFull']['PointsRedeemed']) - intval($test['AccountFull']['PointsExpired'])) / 100;
                                                    // $member->balance = $result_update['AccountFull']['AccountBalance'] / 100;
                                                    $member->bepoz_account_number = $result_update['AccountFull']['AccNumber'];
                                                    $member->bepoz_account_card_number = $result_update['AccountFull']['CardNumber'];
                                                    $member->bepoz_account_id = $result_update['AccountFull']['AccountID'];
                                                    $member->bepoz_existing_account = true;
                                                    $member->bepoz_account_status = "OK";
                                                    $complete = true;
                                                }

                                            }
                                            $created = true;
                                            break;
                                        } else {
                                            Log::info("342 SyncAccountIndividual account not active, no acc number, get from create member backpanel");
                                            // account not active, no acc number, get from create member backpanel
                                            $request = array();
                                            $request['AccountID'] = 0;

                                            // MANUAL ENTRY NOT LONGER NEEDED BECAUSE DISABLE MEMBER CREATE FROM BACKPANEL
                                            // if ($tier->manual_entry){
                                            //     //use account number set from backpanel
                                            //     $accNumber = $member->bepoz_account_number;
                                            // } else 
                                            
                                            if ( $gaming_system_enable == "true" ) {
                                                //use account number set from backpanel
                                                $accNumber = $member->bepoz_account_number;
                                            } else {
                                                $accNumber = $this->recursiveAccNumber($this->getNewAccNumber($member->member_tier->tier), $bepoz, $member->member_tier->tier);
                                            }
                                            $request['AccNumber'] = $accNumber;
                                            $request['CardNumber'] = $accNumber;
                                            $request['GroupID'] = $groupID;
                                            $request['UseGroupSettings'] = is_null($groupID) ? 0 : 1;
                                            $request['FirstName'] = $this->checkEmoji($member->first_name) ? str_replace('"', '', json_encode($member->first_name)) : $member->first_name;
                                            $request['LastName'] = $this->checkEmoji($member->last_name) ? str_replace('"', '', json_encode($member->last_name)) : $member->last_name;
                                            $request['DateBirth'] = $member->dob;
                                            $request['Mobile'] = $member->user->mobile;
                                            $request['PhoneHome'] = $member->phone;
                                            $request['Email1st'] = $member->user->email;
                                            $test = $bepoz->AccUpdate($request, $template);

                                            if (is_array($test)) {
                                                $member->bepoz_account_status = "OK";
                                                $member->bepoz_account_number = $accNumber;
                                                $member->bepoz_account_card_number = $accNumber;
                                                $member->bepoz_account_id = $test['AccountFull']['AccountID'];
                                                $created = true;
                                                $complete = true;
                                            }

                                            break;
                                        }
                                    }

                                    if (!$created) {

                                        Log::info("408 SyncAccountIndividual no valid account, get account number from create member backpanel");
                                        // no valid account, get account number from create member backpanel
                                        $request = array();
                                        $request['AccountID'] = 0;
                                        
                                        // MANUAL ENTRY NOT LONGER NEEDED BECAUSE DISABLE MEMBER CREATE FROM BACKPANEL
                                        // if ($tier->manual_entry){
                                        //     //use account number set from backpanel
                                        //     $accNumber = $member->bepoz_account_number;
                                        // } else 
                                        
                                        if ( $gaming_system_enable == "true" ) {
                                            //use account number set from backpanel
                                            $accNumber = $member->bepoz_account_number;
                                        } else {
                                            $accNumber = $this->recursiveAccNumber($this->getNewAccNumber($member->member_tier->tier), $bepoz, $member->member_tier->tier);
                                        }
                                        $request['AccNumber'] = $accNumber;
                                        $request['CardNumber'] = $accNumber;
                                        $request['GroupID'] = $groupID;
                                        $request['UseGroupSettings'] = is_null($groupID) ? 0 : 1;
                                        $request['FirstName'] = $this->checkEmoji($member->first_name) ? str_replace('"', '', json_encode($member->first_name)) : $member->first_name;
                                        $request['LastName'] = $this->checkEmoji($member->last_name) ? str_replace('"', '', json_encode($member->last_name)) : $member->last_name;
                                        $request['DateBirth'] = $member->dob;
                                        $request['Mobile'] = $member->user->mobile;
                                        $request['PhoneHome'] = $member->phone;
                                        $request['Email1st'] = $member->user->email;
                                        $test = $bepoz->AccUpdate($request, $template);

                                        if (is_array($test)) {
                                            $member->bepoz_account_status = "OK";
                                            $member->bepoz_account_number = $accNumber;
                                            $member->bepoz_account_card_number = $accNumber;
                                            $member->bepoz_account_id = $test['AccountFull']['AccountID'];
                                            $complete = true;
                                        }

                                    }
                                }

                                if ($complete) {
                                    $member->save();

                                    // NEED SEND BEPOZ ID TO SSO
                                    // $member = Member::find( $this->argument('memberid') );
                                    $this->sendSSO($member);

                                    if (isset($data->type)) {
                                        $option = Setting::where('key', 'reward_after_signup_option')->first();
                                        $type = Setting::where('key', 'reward_after_signup_type')->first();

                                        if (!is_null($option) && !is_null($type)) {

                                            if ($option->value == 'true' && $type->value == 'point') {
                                                $point = 0;

                                                $ml = new MemberLog();
                                                $ml->member_id = $member->id;
                                                if ($data->type === 'facebook') {
                                                    $point = Setting::where('key', 'facebook_point_reward')->first()->value;
                                                    $ml->message = "This member got point rewards from social login ($data->type)";

                                                } elseif ($data->type === 'gmail') {
                                                    $point = Setting::where('key', 'google_point_reward')->first()->value;
                                                    $ml->message = "This member got point rewards from social login ($data->type)";

                                                } elseif ($data->type === 'email') {
                                                    $point = Setting::where('key', 'reward_after_signup_point')->first()->value;
                                                    $ml->message = "This member got point rewards from signup";
                                                }


                                                $ml->after = \GuzzleHttp\json_encode([
                                                    'earn' => $point
                                                ]);
                                                $ml->save();

                                                // sync point
                                                $data = array(
                                                    "point" => abs(floatval($point)),
                                                    "id" => $member->bepoz_account_id,
                                                    "member_id" => $member->id,
                                                    "status" => 'earn'
                                                );

                                                $job = new BepozJob();
                                                $job->queue = "sync-point";
                                                $job->payload = \GuzzleHttp\json_encode($data);
                                                $job->available_at = time();
                                                $job->created_at = time();
                                                $job->save();

                                                $member->points = floatval($member->points) + floatval($point);
                                                $member->save();

                                                $company_name = Setting::where('key', 'company_name')->first()->value;

                                                $data = [
                                                    // "tags" => [["key" => "email", "relation" => "=", "value" => $member->user->email]],
                                                    "filters" => [["field" => "tag", "key" => "email", "relation" => "=", "value" => $member->user->email]],
                                                    "contents" => ["en" => "You got point rewards (from signup)! $point pts. is added into your account shortly"],
                                                    "headings" => ["en" => $company_name],
                                                    "data" => ["category" => "email"],
                                                    "ios_badgeType" => "Increase",
                                                    "ios_badgeCount" => 1
                                                ];

                                                $job = (new SendOneSignalNotification($data))->delay(360);
                                                dispatch($job);
                                            }

                                        }

                                    }

                                    if ( $bepozcustomfield_enable == "true" ) {
                                        // SEND BEPOZ CUSTOM FIELD AT ONCE
                                        $resultCustomFields = $this->customFieldsSet($member, $allCustomField, $bepoz);
                                        // Log::warning($resultCustomFields);
                                    }

                                } else {
                                    Log::error("493 SyncAccountIndividual failed, insert into cron");
                                    // if failed then insert into cron list

                                    $data = array(
                                        "member_id" => $user->member->id,
                                        "type" => 'email'
                                    );

                                    $job = new BepozJob();
                                    $job->queue = "sync-account";
                                    $job->payload = \GuzzleHttp\json_encode($data);
                                    $job->available_at = time();
                                    $job->created_at = time();
                                    $job->save();
                                }

                            } else {
                                Log::error("510 SyncAccountIndividual failed, insert into cron");
                                // if failed then insert into cron list

                                $data = array(
                                    "member_id" => $user->member->id,
                                    "type" => 'email'
                                );

                                $job = new BepozJob();
                                $job->queue = "sync-account";
                                $job->payload = \GuzzleHttp\json_encode($data);
                                $job->available_at = time();
                                $job->created_at = time();
                                $job->save();
                            }
                        } catch (\Exception $e) {
                            Log::error($e);
                        }
                    } else if ( $gaming_system_enable == "true" ) {
                        // DOING NOTHING, app will trigger matching account

                    }

                } else {
                    Log::info("bepoz_account_id exist");
                }
            }

            Log::info("call in background SyncAccountIndividual ".$this->argument('memberid'));
        } catch (\Exception $e) {
            Log::error($e);
        }
    }


    protected function getNewAccNumber($tier)
    {

        $rangeValue = "(bepoz_account_number BETWEEN '$tier->boundary_start' AND '$tier->boundary_end')";

        $member = Member::whereNotNull('bepoz_account_number')
            ->whereRaw('(CHAR_LENGTH(bepoz_account_number) >= ' . strlen($tier->boundary_start) . ' AND CHAR_LENGTH(bepoz_account_number) <= ' . strlen($tier->boundary_end) . ')')
            ->whereRaw($rangeValue)
            ->whereHas('member_tier.tier', function ($query) use ($tier) {
                $query->where('id', '=', $tier->id);
            })
            ->orderBy('id', 'desc')
            ->first();

        if (is_null($member)) {
            if (!empty($tier->boundary_start)) {
                $newACC = $tier->boundary_start;
            } else {
                $newACC = "0";
            }

            $newACC++;
        } else {
            $member->bepoz_account_number++;
            $newACC = $this->tierLimitChecker($tier, $member->bepoz_account_number);
        }

        if (!empty($tier->boundary_start)) {
            if (strlen($newACC) < strlen($tier->boundary_start)) {
                $newACC = str_pad($newACC, strlen($tier->boundary_start), '0', STR_PAD_LEFT);
            }
        }

        return $newACC;
    }

    protected
    function recursiveAccNumber($id, $bepoz, $tier)
    {
        if ($bepoz->accountSearch(4, $id)['IDList']['Count'] == 0) {
            return $id;
        } else {
            $id++;
            if (!empty($tier->boundary_start)) {
                if (strlen($id) < strlen($tier->boundary_start)) {
                    $id = str_pad($id, strlen($tier->boundary_start), '0', STR_PAD_LEFT);
                }
            }
            return $this->recursiveAccNumber($id, $bepoz, $tier);
        }
    }

    protected function tierLimitChecker($tier, $id)
    {
        // if (intval($id) >= intval($tier->boundary_start) && intval($id) <= intval($tier->boundary_end)) {
        //     return $id;
        // } else {
        //     return $tier->boundary_start;
        // }

        if (strnatcmp($id, $tier->boundary_start) >= 0 && strnatcmp($id, $tier->boundary_end) <= 0) {
            return $id;
        } else {
            return $tier->boundary_start;
        }
    }

    function checkEmoji($str)
    {
        $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
        preg_match($regexEmoticons, $str, $matches_emo);
        if (!empty($matches_emo[0])) {
            return false;
        }

        // Match Miscellaneous Symbols and Pictographs
        $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
        preg_match($regexSymbols, $str, $matches_sym);
        if (!empty($matches_sym[0])) {
            return false;
        }

        // Match Transport And Map Symbols
        $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
        preg_match($regexTransport, $str, $matches_trans);
        if (!empty($matches_trans[0])) {
            return false;
        }

        // Match Miscellaneous Symbols
        $regexMisc = '/[\x{2600}-\x{26FF}]/u';
        preg_match($regexMisc, $str, $matches_misc);
        if (!empty($matches_misc[0])) {
            return false;
        }

        // Match Dingbats
        $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
        preg_match($regexDingbats, $str, $matches_bats);
        if (!empty($matches_bats[0])) {
            return false;
        }

        return true;
    }

    function setCustomField($bepoz, $member, $CustomFieldKey) {

        $setting = Setting::where('key', 'bepozcustomfield')->first();

        $allCustomField = \GuzzleHttp\json_decode($setting->extended_value);
        $CustomFieldNumber = "0";

        foreach ($allCustomField as $data) {
            if ($data->key == $CustomFieldKey) {
                $CustomFieldNumber = $data->field;
            }
        }

        //Log::info($CustomFieldNumber);
        $dataset = "";
        // if ($CustomFieldKey == "Sponsorship Code"){
        //     $dataset = $member->sponsorship_code;
        // } else if ($CustomFieldKey == "Promo Code"){
        //     $dataset = $member->promo_code;
        // }

        $param = ['AccountID' => $member->bepoz_account_id,
            'CustomFieldNumber' => $CustomFieldNumber,
            'DataSet' => $dataset
        ];
        $customFieldResult = $bepoz->AccCustomFieldSet($param);
    }

    function sendSSO($member){

        //NEW LOGIC SEND INTEGRATION BASED ON VENUE
        $your_order_api = Setting::where('key', 'your_order_api')->first()->value;
        $your_order_key = Setting::where('key', 'your_order_key')->first()->value;

        $datasend = array(
            "email" => $member->user->email,
            "bepoz_id" => $member->bepoz_account_id,
            "bepoz_account_number" => $member->bepoz_account_number,
            "bepoz_account_card_number" => $member->bepoz_account_card_number,
            "guid" => $member->login_token,
            "license_key" => $your_order_key
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $your_order_api."/updateAccount");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datasend);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_ENCODING, "gzip");
        $content = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        // Log::warning("SyncAccount sendSSO");
        // Log::warning($content);
        // Log::warning($httpCode);

        if ($content) {
            $payload = \GuzzleHttp\json_decode($content);

            if (isset($payload->status) && isset($payload->error) ){
                if ($payload->status == "error" && $payload->error == "user_not_found") {
                    $log = new SystemLog();
                    $log->type = 'bepoz-job-error';
                    $log->humanized_message = 'Syncing account is failed. Please check the error message.';
                    $log->message = $payload->error;
                    $log->source = 'SyncAccount.php';
                    $log->save();
                }

                if ($payload->status == "error" && $payload->error == "Unrecognized License.") {
                    $log = new SystemLog();
                    $log->type = 'bepoz-job-error';
                    $log->humanized_message = 'Syncing account is failed. Please check the error message.';
                    $log->message = $payload->error;
                    $log->source = 'SyncAccount.php';
                    $log->save();
                }
            }

            // SUCCESS
            $log = new SystemLog();
            $log->type = 'bepoz-job';
            $log->humanized_message = 'Syncing account SSO is success';
            $log->message = "Email = ".$member->user->email
                ."\nBepoz ID = ".$member->bepoz_account_id
                ."\nBepoz Account Number = ".$member->bepoz_account_number
                ."\nBepoz Account Card Number = ".$member->bepoz_account_card_number;
            $log->source = 'SyncAccountIndividual.php';
            $log->save();

            $ml = new MemberLog();
            $ml->member_id = $member->id;
            $ml->message = "Syncing account SSO is success"
                ."\nEmail = ".$member->user->email
                ."\nBepoz ID = ".$member->bepoz_account_id
                ."\nBepoz Account Number = ".$member->bepoz_account_number
                ."\nBepoz Account Card Number = ".$member->bepoz_account_card_number;
            $ml->after = \GuzzleHttp\json_encode($member->user);
            $ml->save();

        } else {
            $log = new SystemLog();
            $log->type = 'bepoz-job-error';
            $log->humanized_message = 'Syncing account is failed. Please check the error message.';
            $log->message = "SSO connection problem";
            $log->source = 'SyncAccountIndividual.php';
            $log->save();
        }

    }

    private function getBalance($bepoz, $account_id)
    {
        $result = array(
            "PointBalance" => 0,
            "AccountBalance" => 0
        );

        try {
            $temp = $bepoz->AccBalanceGet($account_id);
            if ($temp) {
                $result['PointBalance'] = intval($temp['AccBalance']['PointBalance']) / 100;
                $result['AccountBalance'] = intval($temp['AccBalance']['AccountBalance']) / 100;
            }
        } catch (\Exception $exception) {

        }

        return $result;
    }

    function bepozNextAccountNumber($bepoz, $tier) {

        $account_group = $tier->bepoz_group_id;
        $max_length = strlen($tier->boundary_start);
        $range = $tier->boundary_start.",".$tier->boundary_end;

        $result = $bepoz->NextAccountNumber($account_group, $max_length, $range);

        if ($result){
            $idPad = str_pad($result['Data1'], strlen($tier->boundary_start), '0', STR_PAD_LEFT);
            return $idPad;
        }

        return false;
    }

}

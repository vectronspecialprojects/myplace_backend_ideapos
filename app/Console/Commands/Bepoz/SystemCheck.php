<?php

namespace App\Console\Commands\Bepoz;


use App\ClaimedPromotion;
use App\Helpers\Bepoz;
use App\Jobs\SendOneSignalNotification;
use App\Member;
use App\MemberVouchers;
use App\Setting;
use App\SystemLog;
use App\VoucherSetups;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;

class SystemCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'system-check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Bepoz Connection Status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle(Bepoz $bepoz)
    {
        $sleep = rand(0, 15);
        sleep($sleep);
        try {
            $checkBepozConnection = $bepoz->SystemCheck();

            if (!$checkBepozConnection) {
                $log = new SystemLog();
                $log->type = 'bepoz-connection-error';
                $log->humanized_message = 'Check Bepoz Connection Status is failed. Please check the error message';
                $log->message = 'bepoz-connection-error';
                $log->source = 'SystemCheck.php';
                $log->save();
            }

            // Log::warning("");
            // Log::warning("checkBepozConnection");
            // Log::warning($checkBepozConnection);

            $bepoz_status = Setting::where('key', 'bepoz_status')->first();
            
            // Log::warning("bepoz_status");
            // Log::warning($bepoz_status->value);

            // TRIGGER SEND MERCHANT WHEN DOWN AFTER UP, AND WHEN UP AFTER DOWN
            if ($checkBepozConnection) {
                if ( $bepoz_status->value == "true" ) {
                    // CURRENT BEPOZ OK, LAST SAVED OK
                } else {
                    // CURRENT BEPOZ OK, LAST SAVED DOWN, TRiGGER SEND
                    // Log::warning("bepoz status change from false to true");
                    $bepoz_status->value = "true";
                    $bepoz_status->save();
                    Artisan::call('inform-service-hub');
                }    
            } else {
                if ( $bepoz_status->value == "true" ) {
                    // CURRENT BEPOZ DOWN, LAST SAVED OK, TRiGGER SEND
                    // Log::warning("bepoz status change from true to false");
                    $bepoz_status->value = "false";
                    $bepoz_status->save();
                    Artisan::call('inform-service-hub');
                } else {
                    // CURRENT BEPOZ DOWN, LAST SAVED DOWN
                }
            }


        } catch (\Exception $e) {

            Log::warning($e);

            $log = new SystemLog();
            $log->type = 'bepoz-connection-error';
            $log->humanized_message = 'Check Bepoz Connection Status is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'SystemCheck.php';
            $log->save();

        }

    }

}

<?php

namespace App\Console\Commands\Bepoz;

use App\BepozFailedJob;
use App\BepozJob;
use App\Member;
use App\MemberVouchers;
use App\Order;
use App\OrderDetail;
use App\Product;
use App\Setting;
use App\SystemLog;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Helpers\Bepoz;
use App\VoucherSetups;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Webpatser\Uuid\Uuid;

class IssueTicketCreditVoucher extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'issue-ticket-credit-voucher';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Issue Ticket And Credit Voucher';

    /**
     * Execute the console command.
     *
     * @param Bepoz $bepoz
     */
    public function handle(Bepoz $bepoz)
    {
        $sleep = rand(0, 15);
        sleep($sleep);
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //                          CREATE TICKET (VOUCHER)                                               //
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        try {
            $time_issue_voucher = Setting::where('key', 'time_issue_voucher')->first()->value;
            $exploded_issue_voucher = explode(":", $time_issue_voucher);

            $issue_date = Carbon::now(config('app.timezone'));
            $issue_date->hour = $exploded_issue_voucher[0];
            $issue_date->minute = $exploded_issue_voucher[1];

            if ($issue_date->isPast()) {
                $checkBepozConnection = $bepoz->SystemCheck();

                if ($checkBepozConnection) {

                    $jobs = BepozJob::where('reserved', '=', 0)
                        ->where('queue', '=', 'issue-ticket-credit-voucher')
                        ->where(function ($q) {
                            $q->whereDate('processing_date', '<=', Carbon::now(config('app.timezone'))->toDateString());
                            $q->orWhere('processing_date', null);
                        })
                        ->chunk(100, function ($jobs) use ($bepoz) {

                            // Reserve the selected jobs first before modification
                            foreach ($jobs as $bepoz_job) {
                                $bepoz_job->setJobUID(Uuid::generate());
                                $bepoz_job->reserve();
                            }

                            foreach ($jobs as $bepoz_job) {

                                if ($bepoz_job->attempts() < 3) {
                                    $bepoz_job->attempts = intval($bepoz_job->attempts) + 1;
                                    $bepoz_job->save();

                                    DB::beginTransaction();

                                    try {
                                        $data = \GuzzleHttp\json_decode($bepoz_job->payload);
                                        $voucherSetup = VoucherSetups::find($data->voucher_setups_id);
                                        $member = Member::find($data->member_id);
                                        $order_detail = OrderDetail::find($data->order_details_id);

                                        $order = Order::find($data->order_id);
                                        $payload = \GuzzleHttp\json_decode($order->payload);
                                        $value = null;
                                        if ($payload->transaction_type == 'place_order') {
                                            if (!is_null($order_detail->product_id)) {
                                                $product = Product::find($order_detail->product_id);
                                                if (!is_null($product)) {
                                                    if ($voucherSetup->voucher_type == 10) {
                                                        $value = $product->gift_value * 100;
                                                    }
                                                }
                                            }
                                        }

                                        $result = $bepoz->VoucherIssue($voucherSetup->bepoz_voucher_setup_id, $member->bepoz_account_id, null, $value);
                                        if ($result && isset($result['Voucher'])) {
                                            $order_detail->status = "in_progress";

                                            $memberVoucher = MemberVouchers::find($data->member_voucher_id);
                                            $memberVoucher->voucher_id = $result['Voucher']['VoucherID'];
                                            $memberVoucher->lookup = $result['Voucher']['Lookup'];
                                            $memberVoucher->voucher_type = $result['Voucher']['VoucherType'];
                                            $memberVoucher->unlimited_use = filter_var($result['Voucher']['UnlimitedUse'], FILTER_VALIDATE_BOOLEAN);
                                            $memberVoucher->maximum_discount = $result['Voucher']['MaximumDiscount'];
                                            $memberVoucher->claim_venue_id = $result['Voucher']['ClaimVenueID'];
                                            $memberVoucher->claim_store_id = $result['Voucher']['ClaimStoreID'];
                                            $memberVoucher->used_count = $result['Voucher']['UsedCount'];
                                            $memberVoucher->used_value = $result['Voucher']['UsedValue'];
                                            $memberVoucher->used_trans_id = $result['Voucher']['UsedTransID'];
                                            $memberVoucher->amount_left = $result['Voucher']['AmountLeft'];
                                            $memberVoucher->amount_issued = $result['Voucher']['AmountIssued'];
                                            $memberVoucher->status = 'successful';

                                            $raw_barcode = "99" . str_repeat("0", 10 - strlen($result['Voucher']['Lookup'])) . $result['Voucher']['Lookup'];
                                            $memberVoucher->barcode = $raw_barcode . $this->check_digit($raw_barcode);
                                            $memberVoucher->issue_date = Carbon::parse($result['Voucher']['IssuedDate']);
                                            $memberVoucher->expire_date = strtotime($result['Voucher']['DateExpiry']) && !is_null($result['Voucher']['DateExpiry']) ? Carbon::parse($result['Voucher']['DateExpiry']) : null;
                                            $memberVoucher->save();

                                            if (!is_null($bepoz_job->failed_job_uid)) {
                                                $failed_job = BepozFailedJob::where('job_uid', $bepoz_job->failed_job_uid)->first();
                                                if (!is_null($failed_job)) {
                                                    $failed_job->delete();
                                                }
                                            }

                                            $countVoucher = MemberVouchers::where('order_details_id', $order_detail->id)
                                                ->where('lookup', 'NOT LIKE', '100000000')
                                                ->count();

                                            if (intval($countVoucher) == intval($order_detail->qty)) {
                                                $order_detail->status = "successful";
                                            }

                                            $order_detail->save();

                                            $bepoz_job->dispatch();

                                        } else {
                                            $log = new SystemLog();
                                            $log->type = 'bepoz-job-error';
                                            $log->humanized_message = 'Issuing voucher is failed. Please check log.';
                                            $log->payload = $bepoz_job->payload;
                                            $log->message = $result;
                                            $log->source = 'IssueVoucher.php';
                                            $log->save();

                                            $bepoz_job->free();
                                        }

                                        DB::commit();

                                    } catch (\Exception $e) {
                                        DB::rollback();

                                        $log = new SystemLog();
                                        $log->humanized_message = 'Issuing voucher is failed. Please check the error message';
                                        $log->type = 'bepoz-job-error';
                                        $log->payload = $bepoz_job->payload;
                                        $log->message = $e;
                                        $log->source = 'IssueVoucher.php';
                                        $log->save();

                                        $bepoz_job->free();

                                    }


                                } else {

                                    DB::beginTransaction();

                                    // Maximum attempt log no longer needed
                                    // $log = new SystemLog();
                                    // $log->type = 'bepoz-job-error';
                                    // $log->payload = $bepoz_job->payload;
                                    // $log->humanized_message = 'Maximum attempt of issuing voucher has been reached.';
                                    // $log->source = 'IssueVoucher.php';
                                    // $log->save();

                                    if (is_null($bepoz_job->failed_job_uid)) {

                                        $failed_job = BepozFailedJob::where('payload', $bepoz_job->payload)
                                            ->where('queue', $bepoz_job->queue)
                                            ->first();

                                        if (is_null($failed_job)) {
                                            $failed_job = new BepozFailedJob;
                                            $failed_job->queue = $bepoz_job->queue;
                                            $failed_job->payload = $bepoz_job->payload;
                                            $failed_job->job_uid = Uuid::generate(4);
                                            $failed_job->save();
                                        }
                                    }

                                    $bepoz_job->dispatch();

                                    DB::commit();
                                }
                            }
                        }
                    );

                    /*
                    $jobs = BepozJob::where('reserved', '=', 0)
                        ->where('queue', '=', 'issue-ticket-credit-voucher')
                        ->where(function ($q) {
                            $q->whereDate('processing_date', '<=', Carbon::now(config('app.timezone'))->toDateString());
                            $q->orWhere('processing_date', null);
                        })
                        ->limit(5)
                        ->get();

                    if (!$jobs->isEmpty()) {

                        // Reserve the selected jobs first before modification
                        foreach ($jobs as $bepoz_job) {
                            $bepoz_job->setJobUID(Uuid::generate());
                            $bepoz_job->reserve();
                        }

                        foreach ($jobs as $bepoz_job) {

                            if ($bepoz_job->attempts() < 3) {
                                $bepoz_job->attempts = intval($bepoz_job->attempts) + 1;
                                $bepoz_job->save();

                                DB::beginTransaction();

                                try {
                                    $data = \GuzzleHttp\json_decode($bepoz_job->payload);
                                    $voucherSetup = VoucherSetups::find($data->voucher_setups_id);
                                    $member = Member::find($data->member_id);
                                    $order_detail = OrderDetail::find($data->order_details_id);

                                    $order = Order::find($data->order_id);
                                    $payload = \GuzzleHttp\json_decode($order->payload);
                                    $value = null;
                                    if ($payload->transaction_type == 'place_order') {
                                        if (!is_null($order_detail->product_id)) {
                                            $product = Product::find($order_detail->product_id);
                                            if (!is_null($product)) {
                                                if ($voucherSetup->voucher_type == 10) {
                                                    $value = $product->gift_value * 100;
                                                }
                                            }
                                        }
                                    }

                                    $result = $bepoz->VoucherIssue($voucherSetup->bepoz_voucher_setup_id, $member->bepoz_account_id, null, $value);
                                    if ($result) {
                                        $order_detail->status = "in_progress";

                                        $memberVoucher = MemberVouchers::find($data->member_voucher_id);
                                        $memberVoucher->voucher_id = $result['Voucher']['VoucherID'];
                                        $memberVoucher->lookup = $result['Voucher']['Lookup'];
                                        $memberVoucher->voucher_type = $result['Voucher']['VoucherType'];
                                        $memberVoucher->unlimited_use = filter_var($result['Voucher']['UnlimitedUse'], FILTER_VALIDATE_BOOLEAN);
                                        $memberVoucher->maximum_discount = $result['Voucher']['MaximumDiscount'];
                                        $memberVoucher->claim_venue_id = $result['Voucher']['ClaimVenueID'];
                                        $memberVoucher->claim_store_id = $result['Voucher']['ClaimStoreID'];
                                        $memberVoucher->used_count = $result['Voucher']['UsedCount'];
                                        $memberVoucher->used_value = $result['Voucher']['UsedValue'];
                                        $memberVoucher->used_trans_id = $result['Voucher']['UsedTransID'];
                                        $memberVoucher->amount_left = $result['Voucher']['AmountLeft'];
                                        $memberVoucher->amount_issued = $result['Voucher']['AmountIssued'];
                                        $memberVoucher->status = 'successful';

                                        $raw_barcode = "99" . str_repeat("0", 10 - strlen($result['Voucher']['Lookup'])) . $result['Voucher']['Lookup'];
                                        $memberVoucher->barcode = $raw_barcode . $this->check_digit($raw_barcode);
                                        $memberVoucher->issue_date = Carbon::parse($result['Voucher']['IssuedDate']);
                                        $memberVoucher->expire_date = strtotime($result['Voucher']['DateExpiry']) && !is_null($result['Voucher']['DateExpiry']) ? Carbon::parse($result['Voucher']['DateExpiry']) : null;
                                        $memberVoucher->save();

                                        if (!is_null($bepoz_job->failed_job_uid)) {
                                            $failed_job = BepozFailedJob::where('job_uid', $bepoz_job->failed_job_uid)->first();
                                            if (!is_null($failed_job)) {
                                                $failed_job->delete();
                                            }
                                        }

                                        $countVoucher = MemberVouchers::where('order_details_id', $order_detail->id)
                                            ->where('lookup', 'NOT LIKE', '100000000')
                                            ->count();

                                        if (intval($countVoucher) == intval($order_detail->qty)) {
                                            $order_detail->status = "successful";
                                        }

                                        $order_detail->save();

                                        $bepoz_job->dispatch();

                                    } else {
                                        $log = new SystemLog();
                                        $log->type = 'bepoz-job-error';
                                        $log->humanized_message = 'Issuing voucher is failed. Please check log.';
                                        $log->payload = $bepoz_job->payload;
                                        $log->message = $result;
                                        $log->source = 'IssueVoucher.php';
                                        $log->save();

                                        $bepoz_job->free();
                                    }

                                    DB::commit();

                                } catch (\Exception $e) {
                                    DB::rollback();

                                    $log = new SystemLog();
                                    $log->humanized_message = 'Issuing voucher is failed. Please check the error message';
                                    $log->type = 'bepoz-job-error';
                                    $log->payload = $bepoz_job->payload;
                                    $log->message = $e;
                                    $log->source = 'IssueVoucher.php';
                                    $log->save();

                                    $bepoz_job->free();

                                }


                            } else {

                                DB::beginTransaction();

                                $log = new SystemLog();
                                $log->type = 'bepoz-job-error';
                                $log->payload = $bepoz_job->payload;
                                $log->humanized_message = 'Maximum attempt of issuing voucher has been reached.';
                                $log->source = 'IssueVoucher.php';
                                $log->save();

                                if (is_null($bepoz_job->failed_job_uid)) {

                                    $failed_job = BepozFailedJob::where('payload', $bepoz_job->payload)
                                        ->where('queue', $bepoz_job->queue)
                                        ->first();

                                    if (is_null($failed_job)) {
                                        $failed_job = new BepozFailedJob;
                                        $failed_job->queue = $bepoz_job->queue;
                                        $failed_job->payload = $bepoz_job->payload;
                                        $failed_job->job_uid = Uuid::generate(4);
                                        $failed_job->save();
                                    }
                                }

                                $bepoz_job->dispatch();

                                DB::commit();
                            }
                        }
                    }
                    */
                }
            }


        } catch (\Exception $e) {

            Log::warning($e);

            $log = new SystemLog();
            $log->type = 'bepoz-job-error';
            $log->humanized_message = 'Issuing voucher is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'IssueVoucher.php';
            $log->save();

        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //                          CREATE CREDIT VOUCHER                                                 //
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        try {
            $checkBepozConnection = $bepoz->SystemCheck();

            if ($checkBepozConnection) {
                //$jobs = BepozJob::where('reserved', '=', 0)
                //    ->where('queue', '=', 'issue-gift-certificate')
                //    ->where(function ($q) {
                //        $q->whereDate('processing_date', '<=', Carbon::now(config('app.timezone'))->toDateString());
                //        $q->orWhere('processing_date', null);
                //    })
                //    ->limit(5)
                //    ->get();

                $jobs = BepozJob::where('reserved', '=', 0)
                    ->where('queue', '=', 'issue-ticket-credit-voucher')
                    ->where(function ($q) {
                        $q->whereDate('processing_date', '<=', Carbon::now(config('app.timezone'))->toDateString());
                        $q->orWhere('processing_date', null);
                    })
                    ->limit(5)
                    ->get();

                if (!$jobs->isEmpty()) {

                    // Reserve the selected jobs first before modification
                    foreach ($jobs as $bepoz_job) {
                        $bepoz_job->setJobUID(Uuid::generate());
                        $bepoz_job->reserve();
                    }

                    foreach ($jobs as $bepoz_job) {

                        if ($bepoz_job->attempts() < 3) {
                            $bepoz_job->attempts = intval($bepoz_job->attempts) + 1;
                            $bepoz_job->save();

                            DB::beginTransaction();

                            try {
                                $data = \GuzzleHttp\json_decode($bepoz_job->payload);
                                $voucherSetup = VoucherSetups::find($data->voucher_setups_id);
                                $order_detail = OrderDetail::find($data->order_details_id);
                                $order = Order::find($data->order_id);
                                $member = Member::find($data->member_id);
                                $value = $data->value * 100;

                                $result = $bepoz->VoucherIssue($voucherSetup->bepoz_voucher_setup_id, $member->bepoz_account_id, null, $value);
                                if ($result && isset($result['Voucher'])) {
                                    $order_detail->status = "in_progress";

                                    $gf = GiftCertificate::find($data->gift_certificate_id);

                                    $user = User::where('email', $gf->email)->first();

                                    if (!is_null($user)) {
                                        $gf->recipient_id = $user->member->id;
                                    }

                                    $gf->lookup = $result['Voucher']['Lookup'];
                                    $raw_barcode = "99" . str_repeat("0", 10 - strlen($result['Voucher']['Lookup'])) . $result['Voucher']['Lookup'];
                                    $gf->barcode = $raw_barcode . $this->check_digit($raw_barcode);
                                    $gf->issue_date = Carbon::parse($result['Voucher']['IssuedDate']);
                                    $gf->expiry_date = strtotime($result['Voucher']['DateExpiry']) && !is_null($result['Voucher']['DateExpiry']) ? Carbon::parse($result['Voucher']['DateExpiry']) : null;
                                    $gf->token = $order->token;
                                    $gf->save();

                                    if (!is_null($user)) {

                                        $memberVoucher = new MemberVouchers();
                                        $memberVoucher->name = $order_detail->voucher_setup->name;
                                        $memberVoucher->member_id = $user->member->id;
                                        $memberVoucher->voucher_id = $result['Voucher']['VoucherID'];
                                        $memberVoucher->lookup = $result['Voucher']['Lookup'];
                                        $memberVoucher->voucher_type = $result['Voucher']['VoucherType'];
                                        $memberVoucher->unlimited_use = filter_var($result['Voucher']['UnlimitedUse'], FILTER_VALIDATE_BOOLEAN);
                                        $memberVoucher->maximum_discount = $result['Voucher']['MaximumDiscount'];
                                        $memberVoucher->claim_venue_id = $result['Voucher']['ClaimVenueID'];
                                        $memberVoucher->claim_store_id = $result['Voucher']['ClaimStoreID'];
                                        $memberVoucher->used_count = $result['Voucher']['UsedCount'];
                                        $memberVoucher->used_value = $result['Voucher']['UsedValue'];
                                        $memberVoucher->used_trans_id = $result['Voucher']['UsedTransID'];
                                        $memberVoucher->amount_left = $result['Voucher']['AmountLeft'];
                                        $memberVoucher->amount_issued = $result['Voucher']['AmountIssued'];
                                        $memberVoucher->order_id = $order->id;
                                        $memberVoucher->order_details_id = $order_detail->id;
                                        $memberVoucher->status = 'successful';
                                        $memberVoucher->category = 'gift_certificate';

                                        $raw_barcode = "99" . str_repeat("0", 10 - strlen($result['Voucher']['Lookup'])) . $result['Voucher']['Lookup'];
                                        $memberVoucher->barcode = $raw_barcode . $this->check_digit($raw_barcode);
                                        $memberVoucher->issue_date = Carbon::parse($result['Voucher']['IssuedDate']);
                                        $memberVoucher->expire_date = strtotime($result['Voucher']['DateExpiry']) && !is_null($result['Voucher']['DateExpiry']) ? Carbon::parse($result['Voucher']['DateExpiry']) : null;
                                        $memberVoucher->save();
                                    }

                                    if (!is_null($bepoz_job->failed_job_uid)) {
                                        $failed_job = BepozFailedJob::where('job_uid', $bepoz_job->failed_job_uid)->first();
                                        if (!is_null($failed_job)) {
                                            $failed_job->delete();
                                        }
                                    }

                                    $order->voucher_status = 'successful';
                                    $order_detail->status = "successful";
                                    $order_detail->save();

                                    //dispatch(new SendGiftCertificateEmail($gf, $order));


                                    $bepoz_job->dispatch();

                                } else {
                                    $log = new SystemLog();
                                    $log->type = 'bepoz-job-error';
                                    $log->humanized_message = 'Issuing gift certificate is failed. Please check log.';
                                    $log->payload = $bepoz_job->payload;
                                    $log->message = $result;
                                    $log->source = 'IssueGiftCertificate.php';
                                    $log->save();

                                    $bepoz_job->free();
                                }

                                DB::commit();

                            } catch (\Exception $e) {
                                DB::rollback();

                                $log = new SystemLog();
                                $log->humanized_message = 'Issuing gift certificate is failed. Please check the error message';
                                $log->type = 'bepoz-job-error';
                                $log->payload = $bepoz_job->payload;
                                $log->message = $e;
                                $log->source = 'IssueGiftCertificate.php';
                                $log->save();

                                $bepoz_job->free();

                            }


                        } else {

                            DB::beginTransaction();

                            // Maximum attempt log no longer needed
                            // $log = new SystemLog();
                            // $log->type = 'bepoz-job-error';
                            // $log->payload = $bepoz_job->payload;
                            // $log->humanized_message = 'Maximum attempt of issuing gift certificate has been reached.';
                            // $log->source = 'IssueGiftCertificate.php';
                            // $log->save();

                            if (is_null($bepoz_job->failed_job_uid)) {

                                $failed_job = BepozFailedJob::where('payload', $bepoz_job->payload)
                                    ->where('queue', $bepoz_job->queue)
                                    ->first();

                                if (is_null($failed_job)) {
                                    $failed_job = new BepozFailedJob;
                                    $failed_job->queue = $bepoz_job->queue;
                                    $failed_job->payload = $bepoz_job->payload;
                                    $failed_job->job_uid = Uuid::generate(4);
                                    $failed_job->save();
                                }
                            }

                            $bepoz_job->dispatch();

                            DB::commit();
                        }
                    }
                }

            }


        } catch (\Exception $e) {

            Log::warning($e);

            $log = new SystemLog();
            $log->type = 'bepoz-job-error';
            $log->humanized_message = 'Issuing gift certificate is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'IssueGiftCertificate.php';
            $log->save();

        }

    }

    /**
     * Return check digit of the barcode
     *
     * @param $lookup
     * @return int
     */
    protected function check_digit($lookup)
    {
        $sum = 0;
        $codeString = str_split($lookup);

        for ($i = 0; $i < 12; $i++) {
            if (($i % 2) == 0)
                $sum += $codeString[$i];
            else
                $sum += (3 * $codeString[$i]);
        }

        $sum = $sum % 10;

        if ($sum > 0)
            $sum = 10 - $sum;
        else
            $sum = 0;

        return $sum;
    }
}

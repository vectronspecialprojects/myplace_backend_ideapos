<?php

namespace App\Console\Commands\Cloud;

use App\Member;
use App\Notification;
use App\Order;
use App\OrderDetail;
use App\Product;
use App\Role;
use App\SystemLog;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CheckOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check-order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check order status. Set status to expired if the order is expired';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sleep = rand(0, 15);
        sleep($sleep);
        try {
            $orders = Order::where('order_status', 'pending')
                ->chunk(100, function ($orders) {
                    foreach ($orders as $order) {
                        if (Carbon::now(config('app.timezone'))->diffInMinutes(Carbon::createFromTimestamp($order->expired)) > 30) {
                            OrderDetail::where('order_id', $order->id)->update(['status' => 'expired']);

                            $order->order_status = 'expired';
                            $order->payment_status = 'expired';
                            $order->voucher_status = 'expired';
                            $order->save();
                        }
                    }
                }
                );

            /*
            $orders = Order::where('order_status', 'pending')->get();

            foreach ($orders as $order) {
                if (Carbon::now(config('app.timezone'))->diffInMinutes(Carbon::createFromTimestamp($order->expired)) > 30) {
                    OrderDetail::where('order_id', $order->id)->update(['status' => 'expired']);

                    $order->order_status = 'expired';
                    $order->payment_status = 'expired';
                    $order->voucher_status = 'expired';
                    $order->save();

                }
            }
            */
        } catch (\Exception $e) {
            Log::warning($e);

            $log = new SystemLog();
            $log->type = 'bepoz-job-error';
            $log->humanized_message = 'Checking order jobs is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'CheckOrder.php';
            $log->save();
        }

    }
}

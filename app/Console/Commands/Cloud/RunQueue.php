<?php

namespace App\Console\Commands\Cloud;

use App\Jobs\Job;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class RunQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'run-queue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            for ($i = 0; $i < 100; $i++) {
                $this->callSilent('queue:work', []);
            }

        } catch (\Exception $e) {
            Log::warning($e);
        }
    }
}

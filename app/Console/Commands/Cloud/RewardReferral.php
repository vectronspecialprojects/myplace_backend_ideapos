<?php

namespace App\Console\Commands\Cloud;

use App\BepozJob;
use App\FriendReferral;
use App\Jobs\SendOneSignalNotification;
use App\Member;
use App\MemberVouchers;
use App\Order;
use App\OrderDetail;
use App\PointLog;
use App\Setting;
use App\SystemLog;
use App\VoucherSetups;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class RewardReferral extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reward-referral';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reward Friend Referral';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sleep = rand(0, 15);
        sleep($sleep);
        try {
            $referrals = FriendReferral::where('is_rewarded', false)
                ->where('has_joined', true)
                ->where('is_accepted', true)
                ->where('is_rewarded', false)
                ->where('new_member_id', '<>', 0)
                ->chunk(50, function ($referrals) {
                    foreach ($referrals as $referral) {
                        if ($referral->reward_option == 'after_join' || ($referral->reward_option == 'after_purchase' && $referral->has_made_a_purchase == true)) {

                            if ($referral->reward == 'voucher') {
                                $voucherSetup = VoucherSetups::find($referral->voucher_setups_id);
                                if (!is_null($voucherSetup)) {
                                    $transaction_payload = array(
                                        'referral' => $referral,
                                        'transaction_type' => 'friend_referral'
                                    );

                                    $payload = \GuzzleHttp\json_encode($transaction_payload);
                                    $token = md5($payload);

                                    $Order = new Order();
                                    $Order->type = "cash";
                                    $Order->expired = time();
                                    $Order->member_id = $referral->member_id;
                                    $Order->ordered_at = Carbon::now(config('app.timezone'));
                                    $Order->voucher_status = 'successful';
                                    $Order->payment_status = 'successful';
                                    $Order->order_status = 'confirmed';
                                    $Order->payload = $payload;
                                    $Order->token = $token;
                                    $Order->ip_address = '0.0.0.0';
                                    $Order->transaction_type = 'friend_referral';
                                    $Order->save();

                                    $od = new OrderDetail();
                                    $od->voucher_name = $voucherSetup->name;
                                    $od->voucher_setups_id = $referral->voucher_setups_id;
                                    $od->product_name = $voucherSetup->name;
                                    $od->order_id = $Order->id;
                                    $od->product_type_id = 1;
                                    $od->qty = 1;
                                    $od->status = 'successful';
                                    $od->category = 'friend_referral';
                                    $od->save();

                                    $memberVoucher = new MemberVouchers();
                                    $memberVoucher->name = $od->voucher_setup->name;
                                    $memberVoucher->member_id = $referral->member_id;
                                    $memberVoucher->lookup = "100000000";
                                    $memberVoucher->barcode = "";
                                    $memberVoucher->order_id = $Order->id;
                                    $memberVoucher->order_details_id = $od->id;
                                    $memberVoucher->amount_left = 100;
                                    $memberVoucher->amount_issued = 100;
                                    $memberVoucher->voucher_setup_id = $referral->voucher_setups_id;
                                    $memberVoucher->category = 'friend_referral';
                                    $memberVoucher->save();

                                    // ----------------------

                                    // bepoz job - issue voucher
                                    $data = array(
                                        "referral_id" => $referral->id,
                                        "member_id" => $referral->member_id,
                                        "member_voucher_id" => $memberVoucher->id,
                                        "voucher_setups_id" => $referral->voucher_setups_id,
                                    );

                                    $job = new BepozJob();
                                    $job->queue = "issue-friend-referral-bepoz-voucher";
                                    $job->payload = \GuzzleHttp\json_encode($data);
                                    $job->available_at = time();
                                    $job->created_at = time();
                                    $job->save();
                                }


                            } else {
                                $member = Member::find($referral->member_id);
                                $original_point = intval($member->points);
                                $after = intval($original_point) + intval($referral->point);

                                $member->points = $after;
                                $member->save();

                                // Log
                                $pl = new PointLog();
                                $pl->member_id = $member->id;
                                $pl->points_before = $original_point;
                                $pl->points_after = floatval($after);
                                $pl->desc_short = "Point rewards gained by Referral Friend.";
                                $pl->desc_long = "Gained " . floatval($referral->point) . " points from Referral Friend #" . $referral->id;
                                $pl->save();

                                // sync point
                                $data = array(
                                    "point" => abs(floatval($referral->point)),
                                    "id" => $member->bepoz_account_id,
                                    "member_id" => $member->id,
                                    "status" => 'earn'
                                );

                                $job = new BepozJob();
                                $job->queue = "sync-point";
                                $job->payload = \GuzzleHttp\json_encode($data);
                                $job->available_at = time();
                                $job->created_at = time();
                                $job->save();

                                $company_name = Setting::where('key', 'company_name')->first()->value;

                                $data = [
                                    // "tags" => [["key" => "email", "relation" => "=", "value" => $member->user->email]],
                                    "filters" => [["field" => "tag", "key" => "email", "relation" => "=", "value" => $member->user->email]],
                                    "contents" => ["en" => "You received points ($referral->point pts) from Friend Referral."],
                                    "headings" => ["en" => $company_name],
                                    "ios_badgeType" => "Increase",
                                    "ios_badgeCount" => 1
                                ];
                                dispatch(new SendOneSignalNotification($data));
                            }

                            $referral->is_rewarded = true;
                            $referral->save();
                        }
                    }
                }
                );

            /*
            $referrals = FriendReferral::where('is_rewarded', false)
                ->where('has_joined', true)
                ->where('is_accepted', true)
                ->where('is_rewarded', false)
                ->where('new_member_id', '<>', 0)
                ->get();

            foreach ($referrals as $referral) {
                if ($referral->reward_option == 'after_join' || ($referral->reward_option == 'after_purchase' && $referral->has_made_a_purchase == true)) {

                    if ($referral->reward == 'voucher') {
                        $voucherSetup = VoucherSetups::find($referral->voucher_setups_id);
                        if (!is_null($voucherSetup)) {
                            $transaction_payload = array(
                                'referral' => $referral,
                                'transaction_type' => 'friend_referral'
                            );

                            $payload = \GuzzleHttp\json_encode($transaction_payload);
                            $token = md5($payload);

                            $Order = new Order();
                            $Order->type = "cash";
                            $Order->expired = time();
                            $Order->member_id = $referral->member_id;
                            $Order->ordered_at = Carbon::now(config('app.timezone'));
                            $Order->voucher_status = 'successful';
                            $Order->payment_status = 'successful';
                            $Order->order_status = 'confirmed';
                            $Order->payload = $payload;
                            $Order->token = $token;
                            $Order->ip_address = '0.0.0.0';
                            $Order->transaction_type = 'friend_referral';
                            $Order->save();

                            $od = new OrderDetail();
                            $od->voucher_name = $voucherSetup->name;
                            $od->voucher_setups_id = $referral->voucher_setups_id;
                            $od->product_name = $voucherSetup->name;
                            $od->order_id = $Order->id;
                            $od->product_type_id = 1;
                            $od->qty = 1;
                            $od->status = 'successful';
                            $od->category = 'friend_referral';
                            $od->save();

                            $memberVoucher = new MemberVouchers();
                            $memberVoucher->name = $od->voucher_setup->name;
                            $memberVoucher->member_id = $referral->member_id;
                            $memberVoucher->lookup = "100000000";
                            $memberVoucher->barcode = "";
                            $memberVoucher->order_id = $Order->id;
                            $memberVoucher->order_details_id = $od->id;
                            $memberVoucher->amount_left = 100;
                            $memberVoucher->amount_issued = 100;
                            $memberVoucher->voucher_setup_id = $referral->voucher_setups_id;
                            $memberVoucher->category = 'friend_referral';
                            $memberVoucher->save();

                            // ----------------------

                            // bepoz job - issue voucher
                            $data = array(
                                "referral_id" => $referral->id,
                                "member_id" => $referral->member_id,
                                "member_voucher_id" => $memberVoucher->id,
                                "voucher_setups_id" => $referral->voucher_setups_id,
                            );

                            $job = new BepozJob();
                            $job->queue = "issue-friend-referral-bepoz-voucher";
                            $job->payload = \GuzzleHttp\json_encode($data);
                            $job->available_at = time();
                            $job->created_at = time();
                            $job->save();
                        }


                    } else {
                        $member = Member::find($referral->member_id);
                        $original_point = intval($member->points);
                        $after = intval($original_point) + intval($referral->point);

                        $member->points = $after;
                        $member->save();

                        // Log
                        $pl = new PointLog();
                        $pl->member_id = $member->id;
                        $pl->points_before = $original_point;
                        $pl->points_after = floatval($after);
                        $pl->desc_short = "Point rewards gained by Referral Friend.";
                        $pl->desc_long = "Gained " . floatval($referral->point) . " points from Referral Friend #" . $referral->id;
                        $pl->save();

                        // sync point
                        $data = array(
                            "point" => abs(floatval($referral->point)),
                            "id" => $member->bepoz_account_id,
                            "member_id" => $member->id,
                            "status" => 'earn'
                        );

                        $job = new BepozJob();
                        $job->queue = "sync-point";
                        $job->payload = \GuzzleHttp\json_encode($data);
                        $job->available_at = time();
                        $job->created_at = time();
                        $job->save();

                        $company_name = Setting::where('key', 'company_name')->first()->value;

                        $data = [
                            //"tags" => [["key" => "email", "relation" => "=", "value" => $member->user->email]],
                            "filters" => [["field" => "tag", "key" => "email", "relation" => "=", "value" => $member->user->email]],
                            "contents" => ["en" => "You received points ($referral->point pts) from Friend Referral."],
                            "headings" => ["en" => $company_name],
                            "ios_badgeType" => "Increase",
                            "ios_badgeCount" => 1
                        ];
                        dispatch(new SendOneSignalNotification($data));
                    }

                    $referral->is_rewarded = true;
                    $referral->save();
                }
            }
            */

        } catch (\Exception $e) {
            Log::warning($e);
        }
    }
}

<?php

namespace App\Console\Commands\Cloud;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class BackupDB extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mysql-backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $host = config('database.connections.mysql.host');
            $database = config('database.connections.mysql.database');
            $username = config('database.connections.mysql.username');
            $password = config('database.connections.mysql.password');
            $backupPath = storage_path('backup/');

            $filename = 'backup_' . Carbon::now(config('app.timezone'))->format('d_m_Y_h_i_s');

            $dumpCommand = "mysqldump -e -f -h $host -u$username -p'$password' $database --ignore-table=$database.migrations > $backupPath$filename.sql";
            exec($dumpCommand);

            $main_backup = 'backup_full.sql';
            $duplicate = "cp $backupPath$filename.sql $backupPath$main_backup";

            exec($duplicate);

            $filename = 'backup_data';

            $dumpCommand = "mysqldump -e -f -h$host -u$username -p'$password' $database --ignore-table=$database.migrations --no-create-info > $backupPath$filename.sql";
            exec($dumpCommand);

            // delete backup older than 7 days
//            $deleteCommand = "find ". storage_path('backup/') ."backup_* -mtime +7 -exec rm {} \;";
            $deleteCommand = "find " . storage_path('backup/') . " -type f -mtime +7 -name 'backup_*' -print0 | xargs -r0 rm --";
            exec($deleteCommand);

            $this->info('Mysql backup completed!');
        } catch (\Exception $e) {
            Log::warning($e);
        }
    }
}

<?php

namespace App\Console\Commands\Cloud;

use App\Helpers\Bepoz;
use App\Helpers\Helper;
use App\Member;
use App\MyplaceCache;
use App\SystemLog;

use Carbon\Carbon;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;


class RebuildCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rebuild-cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rebuild Cache';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $sleep = rand(0, 10);
            sleep($sleep);

            try {
                $cache = MyplaceCache::where('key', 'member_ids')->first();
                if (is_null($cache)) {
                    $cache = new MyplaceCache();
                    $value = $this->refreshMember();
                    $comma_separated = implode(",", $value);
                    $cache->value = $comma_separated;
                    $cache->key = 'member_ids';
                    $cache->expiration = Carbon::now()->timestamp;
                } else {
                    // Log::warning("update cache");
                    $now = Carbon::now()->timestamp;
                    if (intval($now) > intval($cache->expiration)) {
                        // Log::warning("refresh cache");
                        $value = $this->refreshMember();
                        $comma_separated = implode(",", $value);
                        $cache->value = $comma_separated;
                        $cache->expiration = Carbon::now()->timestamp;
                    }
                }
                $cache->save();
            } catch (\Exception $e) {
                Log::warning($e);

                $log = new SystemLog();
                $log->type = 'bepoz-job-error';
                $log->humanized_message = 'Rebuild Cache is failed. Please check the error message';
                $log->message = $e;
                $log->source = 'RebuildCache.php';
                $log->save();
            }


            // NEXT LOGIC
            // sleep($sleep);

        } catch (\Exception $e) {
            Log::error($e);
        }
    }

    private function refreshMember()
    {
        $member = Member::select('bepoz_account_id')
            ->where('bepoz_account_id', '<>', '')
            ->whereNotNull('bepoz_account_id')
            ->where('bepoz_account_id', '>', '0')
            ->orderBy('bepoz_account_id', 'DESC')
            ->pluck('bepoz_account_id')->toArray();
        return $member;
    }

}

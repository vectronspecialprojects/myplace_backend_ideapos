<?php

namespace App\Console\Commands\Cloud;

use App\Listing;
use App\ListingSchedule;
use App\SystemLog;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CheckListing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check-listing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check listing status. Set status to expired if the schedule is expired';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sleep = rand(0, 15);
        sleep($sleep);
        try {
            $listingSchedules = ListingSchedule::with('listing')
                ->whereDate('date_end', '<', Carbon::now(config('app.timezone'))->toDateString())
                ->whereExists(function ($query) {
                    $query->select(DB::raw(1))
                        ->from('listings')
                        ->where('status', 'active')
                        ->whereNull('listings.deleted_at')
                        ->whereRaw('listings.id = listing_schedules.listing_id');
                })
                ->where('status', 'active')
                ->chunk(50, function ($listingSchedules) {
                    foreach ($listingSchedules as $listingSchedule) {
                        $listingSchedule->listing->status = 'inactive';
                        $listingSchedule->listing->save();

                        $listingSchedule->status = 'inactive';
                        $listingSchedule->save();
                    }
                }
                );

            $listings = Listing::whereDate('datetime_end', '<', Carbon::now(config('app.timezone'))->toDateString())
                ->whereHas('type', function ($q) {
                    $q->where('key', 'like', '%"option_specific":true%');
                    $q->where('key', 'like', '%"setting_show_time":true%');
                })
                ->where('status', 'active')
                ->chunk(50, function ($listings) {
                    foreach ($listings as $listing) {
                        $listing->status = 'inactive';
                        $listing->save();
                    }
                }
                );


            /*
            $listingSchedules = ListingSchedule::with('listing')
                ->whereDate('date_end', '<', Carbon::now(config('app.timezone'))->toDateString())
                ->whereExists(function ($query) {
                    $query->select(DB::raw(1))
                        ->from('listings')
                        ->where('status', 'active')
                        ->whereNull('listings.deleted_at')
                        ->whereRaw('listings.id = listing_schedules.listing_id');
                })
                ->where('status', 'active')
                ->get();

            foreach ($listingSchedules as $listingSchedule) {
                $listingSchedule->listing->status = 'inactive';
                $listingSchedule->listing->save();

                $listingSchedule->status = 'inactive';
                $listingSchedule->save();
            }

            $listings = Listing::whereDate('datetime_end', '<', Carbon::now(config('app.timezone'))->toDateString())
                ->whereHas('type', function ($q) {
                    $q->where('key', 'like', '%"option_specific":true%');
                    $q->where('key', 'like', '%"setting_show_time":true%');
                })
                ->where('status', 'active')
                ->get();

            foreach ($listings as $listing) {
                $listing->status = 'inactive';
                $listing->save();
            }
            */


        } catch (\Exception $e) {
            Log::warning($e);

            $log = new SystemLog();
            $log->type = 'bepoz-job-error';
            $log->humanized_message = 'Checking listing jobs is failed. Please check the error message';
            $log->message = $e;
            $log->source = 'CheckListing.php';
            $log->save();
        }

    }
}

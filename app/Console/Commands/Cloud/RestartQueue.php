<?php

namespace App\Console\Commands\Cloud;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class RestartQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'restart-queue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Restart queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            // clear all dirty cache
            // $this->callSilent('clear-compiled', []);
            // $this->callSilent('cache:clear', []);
            // $this->callSilent('route:clear', []);
            // $this->callSilent('route:cache', []);

            // $this->callSilent('optimize', []);
            // $this->callSilent('config:clear', []);
            // $this->callSilent('view:clear', []);
            // Log::warning("restart-queue");

            // restart queue if there is problem
            $this->callSilent('queue:restart', []);
//            $this->callSilent('queue:retry', []);

            // delete corrupted scheduled cron
            $deleteCommand = "find " . storage_path('framework/') . " -type f -mmin +30 -name 'schedule-*' -print0 | xargs -r0 rm --";
            exec($deleteCommand);

        } catch (\Exception $e) {
            Log::warning($e);
        }
    }
}

<?php

namespace App\Console\Commands\Cloud;

use App\User;
use App\Jobs\Job;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class RefreshOTP extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'refresh-otp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh OTP';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            User::where('otp_verified', true)->update(['otp_verified' => false]);

            // CLEAR GAMING SESSION
            $yesterday = Carbon::yesterday(config('app.timezone'))->format('Y-m-d H:i:s');
            $sql = "DELETE FROM gaming_sessions WHERE created_at <= '" . $yesterday . "'";

            $result = DB::delete($sql);
            // Log::warning("delete " . $result);

        } catch (\Exception $e) {
            Log::warning($e);
        }
    }
}

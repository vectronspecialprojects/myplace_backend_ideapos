<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        Commands\Bepoz\AccBalanceGetID::class,
        Commands\Bepoz\AccountsUpdatedIndividual::class,
        Commands\Bepoz\AccPromoUpdatedIndividual::class,
        Commands\Bepoz\VoucherUpdatedIndividual::class,
        Commands\Bepoz\SyncVoucherSetups::class,
        Commands\Bepoz\SyncAccount::class,
        Commands\Bepoz\SyncAccountIndividual::class,
        Commands\Bepoz\SyncImage::class,
        Commands\Bepoz\IssueVoucher::class,
        Commands\Bepoz\SyncPoint::class,
        Commands\Bepoz\SyncPointBalance::class,
        Commands\Cloud\CheckOrder::class,
        Commands\Cloud\CheckFailedJobs::class,
        Commands\Cloud\BackupDB::class,
        Commands\Cloud\UploadLog::class,
        Commands\Bepoz\IssueBepozPromotion::class,
        Commands\Cloud\RestartQueue::class,
        Commands\Cloud\RunQueue::class,
        Commands\Cloud\SendNotificationListing::class,
        Commands\Cloud\SendReminder::class,
        Commands\Cloud\SendReminderTicket::class,
        Commands\Cloud\SendReminderVoucher::class,
        Commands\Cloud\RewardReferral::class,
        Commands\Bepoz\IssueBepozReferralReward::class,
        Commands\Bepoz\UpdateAccount::class,
        Commands\Bepoz\UpdateBalance::class,
        Commands\Bepoz\DowngradeMember::class,
        Commands\Bepoz\CreateTransaction::class,
        Commands\Bepoz\PollAccount::class,
        Commands\Bepoz\PollAccountTotalSaved::class,
        Commands\Bepoz\PollVoucher::class,
        Commands\Bepoz\IssueGiftCertificate::class,
        Commands\Bepoz\IssueGiftCertificateBooking::class,
        Commands\Cloud\CheckListing::class,
        Commands\Cloud\PerformPendingJob::class,
        Commands\Bepoz\IssueBepozSignupReward::class,
        Commands\Bepoz\IssueBepozSurveyReward::class,
        Commands\Bepoz\PollPrizePromo::class,
        Commands\Bepoz\PollStampCard::class,
        Commands\Bepoz\SystemCheck::class,
        Commands\Bepoz\SystemCheckEmail::class,
        Commands\Cloud\RefreshOTP::class,
        Commands\Cloud\RebuildCache::class,
        Commands\Cloud\CheckReservedJobs::class,
        Commands\Cloud\ClearLog::class,
        Commands\Bepoz\UpdateCustomField::class,
        Commands\Bepoz\InformServiceHub::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->command('sync-voucher-setups')
            ->everyFiveMinutes()->withoutOverlapping();

        // //effect by setting ebet gaming
        $schedule->command('sync-account')
            ->everyMinute()->withoutOverlapping();

//        $schedule->command('sync-image')
//           ->cron('* 0-8,17-23 * * *')->withoutOverlapping();
        
        $schedule->command('issue-gift-certificate')
            ->everyMinute()->withoutOverlapping();

        $schedule->command('issue-gift-certificate-booking')
            ->everyMinute()->withoutOverlapping();

        //effect by setting ebet gaming
        $schedule->command('update-bepoz-account')
            ->everyMinute()->withoutOverlapping();

        $schedule->command('update-balance')
            ->everyMinute()->withoutOverlapping();

        $schedule->command('sync-point')
            ->everyMinute()->withoutOverlapping();

        $schedule->command('sync-point-balance')
            ->everyMinute()->withoutOverlapping();

        $schedule->command('issue-voucher')
            ->everyMinute()->withoutOverlapping();

        $schedule->command('issue-bepoz-promotion')
            ->everyMinute()->withoutOverlapping();

        $schedule->command('check-order')
            ->everyFiveMinutes()->withoutOverlapping();

        $schedule->command('check-failed-jobs')
            ->everyFiveMinutes()->withoutOverlapping();

        $schedule->command('restart-queue')
            ->everyFiveMinutes()->withoutOverlapping();

//        $schedule->command('run-queue')
//           ->everyMinute()->withoutOverlapping();

        $schedule->command('mysql-backup')
            ->dailyAt('00:00')->withoutOverlapping();

        $schedule->command('create-transaction')
            ->everyMinute()->withoutOverlapping();

        $schedule->command('reward-referral')
            ->everyFiveMinutes()->withoutOverlapping();

       $schedule->command('send-notification-listing')
           ->everyMinute()->withoutOverlapping();

//        $schedule->command('send-reminder')
//            ->dailyAt('00:00')->withoutOverlapping();

        $schedule->command('send-reminder-ticket')
            ->dailyAt('00:00')->withoutOverlapping();

        $schedule->command('send-reminder-voucher')
            ->everyMinute()->withoutOverlapping();

        $schedule->command('issue-bepoz-referral-reward')
            ->everyMinute()->withoutOverlapping();

        $schedule->command('issue-bepoz-signup-reward')
            ->everyFiveMinutes()->withoutOverlapping();

        $schedule->command('issue-bepoz-survey-reward')
            ->everyFiveMinutes()->withoutOverlapping();

        $schedule->command('poll-account')
           ->everyMinute()->withoutOverlapping();

        $schedule->command('poll-account-total-saved')
            ->everyMinute()->withoutOverlapping();
            
        $schedule->command('poll-prize-promo')
            ->everyMinute()->withoutOverlapping();
        
        // $schedule->command('poll-stamp-card')
        //     ->everyMinute()->withoutOverlapping();
        
        // $schedule->command('poll-voucher')
        //     ->everyMinute()->withoutOverlapping();

//        $schedule->command('check-listing')
//            ->everyFiveMinutes()->withoutOverlapping();

        $schedule->command('execute-pending-job')
            ->everyFiveMinutes()->withoutOverlapping();
            
        $schedule->command('system-check')
            ->everyMinute()->withoutOverlapping();

        $schedule->command('system-check-email')
            ->everyFiveMinutes()->withoutOverlapping();

        $schedule->command('refresh-otp')
            ->dailyAt('00:00')->withoutOverlapping();

        $schedule->command('rebuild-cache')
            ->everyMinute()->withoutOverlapping();

        $schedule->command('clear-log')
            ->hourly()->withoutOverlapping();

        $schedule->command('check-reserved-jobs')
            ->everyMinute()->withoutOverlapping();
        
        $schedule->command('downgrade-member')
            ->dailyAt('00:00')->withoutOverlapping();

        $schedule->command('update-bepoz-custom-field')
            ->everyMinute()->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

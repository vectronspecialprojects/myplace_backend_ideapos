<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VenuePivotTag extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'venue_pivot_tags';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $casts = [

    ];

    /**
     * Defines The tier of VenueLinkTag
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */

    /**
     * Get the related venue
     * >> Define relationship
     *
     */
    public function venue()
    {
        return $this->belongsTo('App\Venue', 'venue_id');
    }

    /**
     * Get the related venue yourorder
     * >> Define relationship
     *
     */
    public function venue_yourorder()
    {
        return $this->belongsTo('App\Venue', 'venue_id')->select('your_order_link', 'your_order_location');
    }

    /**
     * Get the related VenueTag
     * >> Define relationship
     *
     */
    public function venue_tags()
    {
        return $this->belongsTo('App\VenueTag', 'venue_tag_id');
    }

}

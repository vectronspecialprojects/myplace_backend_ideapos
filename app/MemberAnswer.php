<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MemberAnswer extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'answer_member';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public function answr()
    {
        return $this->belongsTo('App\Answer', 'answer_id');
    }

    public function answer()
    {
        return $this->belongsTo('App\Answer', 'answer_id');
    }

    public function question()
    {
        return $this->belongsTo('App\Question', 'question_id');
    }

    public function member()
    {
        return $this->belongsTo('App\Member', 'member_id');
    }

    public function survey()
    {
        return $this->belongsTo('App\Survey', 'survey_id');
    }
}

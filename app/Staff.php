<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Staff extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'staff';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id', 'created_at', 'updated_at', 'deleted_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get the member record associated with the user.
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($staff) {
            $setting = Setting::where('key', 'member_profile_img')->first();
            if (!is_null($setting)) {
                $staff->profile_img = $setting->value;
            } else {
                $staff->profile_img = "https://s3-ap-southeast-2.amazonaws.com/" . env('S3_BUCKET') . "/profile_default.png";
            }
        });
    }
}

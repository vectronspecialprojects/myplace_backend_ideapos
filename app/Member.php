<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Member extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'members';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id', 'created_at', 'updated_at', 'deleted_at', 'token'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'first_name', 'last_name', 'dob', 'bepoz_account_card_number'];

    /**
     * Get the member record associated with the user.
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * Get the member tier.
     * >> Define relationship
     *
     */
    public function member_tier()
    {
        return $this->hasOne('App\MemberTiers');
    }

    /**
     * Get the member cards.
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function member_cards()
    {
        return $this->hasMany('App\MemberCard');
    }

    /**
     * Get the member transactions
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function member_orders()
    {
        return $this->hasMany('App\Order');
    }

    /**
     * Get user addresses
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function member_addresses()
    {
        return $this->hasMany('App\Address');
    }

    /**
     * Get user issued vouchers
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function member_issued_vouchers()
    {
        return $this->hasMany('App\MemberVouchers');
    }

    /**
     * Get user claimed vouchers
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function claimed_vouchers()
    {
        return $this->hasMany('App\ClaimedPromotion');
    }

    public function referrals()
    {
        return $this->hasMany('App\FriendReferral');
    }

    /**
     * Get user tiers
     * >> Define relationship
     */
    public function tiers()
    {
        return $this->belongsToMany('App\Tier', 'member_tier', 'member_id', 'tier_id')->wherePivot('deleted_at', null)->withTimestamps();
    }

    public function favorites()
    {
        return $this->belongsToMany('App\Listing', 'favorite_listing', 'member_id', 'listing_id')->wherePivot('deleted_at', null)->withTimestamps();
    }

    public function promo_accounts()
    {
        return $this->hasMany('App\PromoAccount');
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($member) {
            if (is_null($member->profile_img)) {
                $setting = Setting::where('key', 'member_profile_img')->first();
                if (!is_null($setting)) {
                    $member->profile_img = $setting->value;
                } else {
                    $member->profile_img = "https://s3-ap-southeast-2.amazonaws.com/" . env('S3_BUCKET') . "/profile_default.png";
                }
            }

            $member->token = md5( time());
        });

        static::created(function ($member) { // before delete() method call this

        });
    }

    /**
     * Get the member current preferred venue.
     * >> Define relationship
     *
     */
    public function current_preferred_venue_full()
    {
        return $this->belongsTo('App\Venue', 'current_preferred_venue');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoleControl extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'role_controls';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'created_at', 'updated_at', 'deleted_at', 'role_id'
    ];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $casts = [
        'listing_create' => 'boolean',
        'listing_read' => 'boolean',
        'listing_update' => 'boolean',
        'listing_delete' => 'boolean',

        'setting_read' => 'boolean',
        'setting_update' => 'boolean',

        'product_create' => 'boolean',
        'product_read' => 'boolean',
        'product_update' => 'boolean',
        'product_delete' => 'boolean',

        'voucher_create' => 'boolean',
        'voucher_read' => 'boolean',
        'voucher_update' => 'boolean',
        'voucher_delete' => 'boolean',

        'survey_create' => 'boolean',
        'survey_read' => 'boolean',
        'survey_update' => 'boolean',
        'survey_delete' => 'boolean',

        'member_create' => 'boolean',
        'member_read' => 'boolean',
        'member_update' => 'boolean',
        'member_delete' => 'boolean',

        'user_create' => 'boolean',
        'user_read' => 'boolean',
        'user_update' => 'boolean',
        'user_delete' => 'boolean',

        'role_create' => 'boolean',
        'role_read' => 'boolean',
        'role_update' => 'boolean',
        'role_delete' => 'boolean',

        'user_role_create' => 'boolean',
        'user_role_read' => 'boolean',
        'user_role_update' => 'boolean',
        'user_role_delete' => 'boolean',

        'notification_create' => 'boolean',
        'notification_read' => 'boolean',
        'notification_update' => 'boolean',
        'notification_delete' => 'boolean',

        'faq_create' => 'boolean',
        'faq_read' => 'boolean',
        'faq_update' => 'boolean',
        'faq_delete' => 'boolean',

        'staff_create' => 'boolean',
        'staff_read' => 'boolean',
        'staff_update' => 'boolean',
        'staff_delete' => 'boolean',

        'enquiry_read' => 'boolean',
        'enquiry_update' => 'boolean',
        'enquiry_delete' => 'boolean',

        'frontend_access' => 'boolean',
        'root_access' => 'boolean',
        'backend_access' => 'boolean',
        'venue_management' => 'boolean',

        'report_read' => 'boolean',
        'report_export' => 'boolean',

        'transaction_read' => 'boolean',
        'transaction_export' => 'boolean',

        'venue_create' => 'boolean',
        'venue_read' => 'boolean',
        'venue_update' => 'boolean',
        'venue_delete' => 'boolean',

        'log_read' => 'boolean',
        'log_delete' => 'boolean',

        'send_broadcast' => 'boolean',
        'resend_confirmation' => 'boolean',

        'friend_referral_read' => 'boolean',

        'admin_update' => 'boolean',
    ];

    public function getListingCreateAttribute($data)
    {
        return (bool)$data;
    }

    public function getListingReadAttribute($data)
    {
        return (bool)$data;
    }

    public function getListingUpdateAttribute($data)
    {
        return (bool)$data;
    }

    public function getListingDeleteAttribute($data)
    {
        return (bool)$data;
    }

    public function getSettingReadAttribute($data)
    {
        return (bool)$data;
    }

    public function getSettingUpdateAttribute($data)
    {
        return (bool)$data;
    }

    public function getProductCreateAttribute($data)
    {
        return (bool)$data;
    }

    public function getProductReadAttribute($data)
    {
        return (bool)$data;
    }

    public function getProductUpdateAttribute($data)
    {
        return (bool)$data;
    }

    public function getProductDeleteAttribute($data)
    {
        return (bool)$data;
    }

    public function getVoucherCreateAttribute($data)
    {
        return (bool)$data;
    }

    public function getVoucherReadAttribute($data)
    {
        return (bool)$data;
    }

    public function getVoucherUpdateAttribute($data)
    {
        return (bool)$data;
    }

    public function getVoucherDeleteAttribute($data)
    {
        return (bool)$data;
    }

    public function getSurveyCreateAttribute($data)
    {
        return (bool)$data;
    }

    public function getSurveyReadAttribute($data)
    {
        return (bool)$data;
    }

    public function getSurveyUpdateAttribute($data)
    {
        return (bool)$data;
    }

    public function getSurveyDeleteAttribute($data)
    {
        return (bool)$data;
    }

    public function getMemberCreateAttribute($data)
    {
        return (bool)$data;
    }

    public function getMemberReadAttribute($data)
    {
        return (bool)$data;
    }

    public function getMemberUpdateAttribute($data)
    {
        return (bool)$data;
    }

    public function getMemberDeleteAttribute($data)
    {
        return (bool)$data;
    }

    public function getUserCreateAttribute($data)
    {
        return (bool)$data;
    }

    public function getUserReadAttribute($data)
    {
        return (bool)$data;
    }

    public function getUserUpdateAttribute($data)
    {
        return (bool)$data;
    }

    public function getUserDeleteAttribute($data)
    {
        return (bool)$data;
    }

    public function getUserRoleCreateAttribute($data)
    {
        return (bool)$data;
    }

    public function getUserRoleReadAttribute($data)
    {
        return (bool)$data;
    }

    public function getUserRoleUpdateAttribute($data)
    {
        return (bool)$data;
    }

    public function getUserRoleDeleteAttribute($data)
    {
        return (bool)$data;
    }

    public function getNotificationCreateAttribute($data)
    {
        return (bool)$data;
    }

    public function getNotificationReadAttribute($data)
    {
        return (bool)$data;
    }

    public function getNotificationUpdateAttribute($data)
    {
        return (bool)$data;
    }

    public function getNotificationDeleteAttribute($data)
    {
        return (bool)$data;
    }

    public function getFaqCreateAttribute($data)
    {
        return (bool)$data;
    }

    public function getFaqReadAttribute($data)
    {
        return (bool)$data;
    }

    public function getFaqUpdateAttribute($data)
    {
        return (bool)$data;
    }

    public function getFaqDeleteAttribute($data)
    {
        return (bool)$data;
    }

    public function getStaffCreateAttribute($data)
    {
        return (bool)$data;
    }

    public function getStaffReadAttribute($data)
    {
        return (bool)$data;
    }

    public function getStaffUpdateAttribute($data)
    {
        return (bool)$data;
    }

    public function getStaffDeleteAttribute($data)
    {
        return (bool)$data;
    }

    public function getEnquiryReadAttribute($data)
    {
        return (bool)$data;
    }

    public function getEnquiryUpdateAttribute($data)
    {
        return (bool)$data;
    }

    public function getEnquiryDeleteAttribute($data)
    {
        return (bool)$data;
    }

    public function getRootAccessAttribute($data)
    {
        return (bool)$data;
    }

    public function getFrontendAccessAttribute($data)
    {
        return (bool)$data;
    }

    public function getBackendAccessAttribute($data)
    {
        return (bool)$data;
    }

    public function getFriendReferralReadAttribute($data)
    {
        return (bool)$data;
    }

    public function getReportReadAttribute($data)
    {
        return (bool)$data;
    }

    public function getReportExportAttribute($data)
    {
        return (bool)$data;
    }

    public function getTransactionReadAttribute($data)
    {
        return (bool)$data;
    }

    public function getTransactionExportAttribute($data)
    {
        return (bool)$data;
    }

    public function getVenueManagementAttribute($data)
    {
        return (bool)$data;
    }

    public function getVenueCreateAttribute($data)
    {
        return (bool)$data;
    }

    public function getVenueReadAttribute($data)
    {
        return (bool)$data;
    }

    public function getVenueUpdateAttribute($data)
    {
        return (bool)$data;
    }

    public function getVenueDeleteAttribute($data)
    {
        return (bool)$data;
    }

    public function getLogReadAttribute($data)
    {
        return (bool)$data;
    }

    public function getLogDeleteAttribute($data)
    {
        return (bool)$data;
    }

    public function getSendBroadcastAttribute($data)
    {
        return (bool)$data;
    }

    public function getResendConfirmationAttribute($data)
    {
        return (bool)$data;
    }
    
    public function getAdminUpdateAttribute($data)
    {
        return (bool)$data;
    }

}

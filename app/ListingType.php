<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ListingType extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'listing_types';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $casts = [
        'key' => 'object',
    ];

    /**
     * Get the listings.
     * >> Define relationship
     *
     */
    public function listings()
    {
        return $this->hasMany('App\Listing', 'listing_type_id');
    }

    public function getKeyAttribute($obj)
    {
        return \GuzzleHttp\json_decode($obj);
    }
}

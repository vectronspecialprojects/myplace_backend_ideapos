<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Venue extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'venues';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $casts = [
        'active' => 'boolean',
        'allow_signup_for_existing_customer_only' => 'boolean',
        'gaming_system_point' => 'boolean',
        'hide_delivery_info' => 'boolean',
        'hide_opening_hours_info' => 'boolean'
    ];

    /**
     * Defines The tier of venue
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function tier()
    {
        return $this->hasOne('App\Tier');
    }

    public function getActiveAttribute($status)
    {
        return (bool)$status;
    }

    public function getHideDeliveryInfoAttribute($status)
    {
        return (bool)$status;
    }

    public function getHideOpeningHoursInfoAttribute($status)
    {
        return (bool)$status;
    }

    
    /**
     * Get Venue Tags
     * >> Define relationship
     */
    public function venue_tags()
    {
        return $this->belongsToMany('App\VenueTag', 'venue_pivot_tags', 'venue_id', 'venue_tag_id')->wherePivot('deleted_at', null)->withPivot('display_order')->withTimestamps();
    }

    /**
     * Get the related venue
     * >> Define relationship
     *
     */
    public function venue_pivot_tags()
    {
        return $this->hasMany('App\VenuePivotTag');
    }

}

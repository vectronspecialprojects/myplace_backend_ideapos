<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at', 'payload', 'ip_address', 'transaction_type', 'third_party_provider'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get the details
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function order_details()
    {
        return $this->hasMany('App\OrderDetail');
    }

    /**
     * Get the details
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function order_details_success()
    {
      $status = ['pending', 'in_progress', 'successful'];
      return $this->hasMany('App\OrderDetail')
        ->whereIn('status', $status);
    }
    
    /**
     * Get the details for ticketing
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function order_details_ticket_success()
    {
      $status = ['pending', 'in_progress', 'successful'];
      return $this->hasMany('App\OrderDetail')->whereIn('status', $status)->where('product_type_id', '2');
      //return $this->hasMany('App\OrderDetail')->where('product_type_id', '2');
    }

    /**
     * Get the details for ticketing
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function order_details_ticket()
    {
      return $this->hasMany('App\OrderDetail')->where('product_type_id', '2');
    }
    
    /**
     * Get the details for credit voucher
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function order_details_credit_voucher_success()
    {
      $status = ['pending', 'in_progress', 'successful'];
      return $this->hasMany('App\OrderDetail')->whereIn('status', $status)->where('product_type_id', '3');
      //return $this->hasMany('App\OrderDetail')->where('product_type_id', '3');
    }

    /**
     * Get the details for credit voucher
     * >> Define relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function order_details_credit_voucher()
    {
      return $this->hasMany('App\OrderDetail')->where('product_type_id', '3');
    }

    /**
     * Get member details
     * >> Define relationship
     *
     */
    public function member()
    {
        return $this->belongsTo('App\Member', 'member_id');
    }

    public function creditcard_transactions()
    {
        return $this->hasMany('\App\CreditCardTransaction');
    }

    public function listing()
    {
        return $this->belongsTo('App\Listing', 'listing_id');
    }

    public function venue()
    {
        return $this->belongsTo('App\Venue', 'venue_id');
    }

    public function payment_log()
    {
        return $this->hasOne('App\PaymentLog');
    }
    
    public function gift_certificate()
    {
        return $this->hasOne('App\GiftCertificate');
    }
}

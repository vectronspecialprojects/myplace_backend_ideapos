<?php

namespace App\Providers;


use App\Jobs\SendEnquiryEmail;
use App\ListingEnquiry;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Log;

class EnquiryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        ListingEnquiry::created(function ($enquiry) {

            /*
             * Send enquiry confirmation to member and admin
             */
            // Log::info("EnquiryServiceProvider");
            $data = ListingEnquiry::find($enquiry->id);
            
            // if ( $data->rating > 0 ){
            //     //feedback not need send email
            // } else {
            //     dispatch(new SendEnquiryEmail($data));
            // }
            
            dispatch(new SendEnquiryEmail($data));
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

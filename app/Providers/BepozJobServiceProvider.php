<?php

namespace App\Providers;

use App\BepozJob;
use App\Jobs\SendPusherNotification;
use App\Notification;
use App\Role;
use App\SystemLog;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class BepozJobServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        BepozJob::created(function ($job) {

//            $data = array(
//                'id' => $job->id,
//                'queue' => $job->queue,
//                'created_at' => time()
//            );
//
//            $job = (new SendPusherNotification('create-bepoz-jobs', \GuzzleHttp\json_encode($data)));
//            dispatch($job);
        });

        BepozJob::deleted(function ($job) {
//            $job = (new SendPusherNotification('delete-bepoz-jobs', $job->id));
//            dispatch($job);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

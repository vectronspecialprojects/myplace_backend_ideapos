<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ListingTag extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'listing_tags';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $casts = [

    ];

    /**
     * Defines The tier of listingTag
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */

    /**
     * Get listings
     * >> Define relationship
     */
    public function listings()
    {
        return $this->belongsToMany('App\Listing', 'listing_pivot_tags', 'listing_tag_id', 'listing_id')->wherePivot('deleted_at', null)->withPivot('display_order')->withTimestamps();
    }


    /**
     * Get the related listing Pivot Tag
     * >> Define relationship
     *
     */
    public function listing_pivot_tags()
    {
        return $this->hasMany('App\ListingPivotTag');
    }

}

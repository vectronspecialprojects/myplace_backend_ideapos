<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('sync-voucher-setups', function () {});

Artisan::command('sync-account', function () {});

Artisan::command('issue-gift-certificate', function () {});

Artisan::command('issue-gift-certificate-booking', function () {});

Artisan::command('update-bepoz-account', function () {});

Artisan::command('update-balance', function () {});

Artisan::command('sync-point', function () {});

Artisan::command('sync-point-balance', function () {});

Artisan::command('issue-voucher', function () {});

Artisan::command('issue-bepoz-promotion', function () {});

Artisan::command('check-order', function () {});

Artisan::command('check-failed-jobs', function () {});

Artisan::command('restart-queue', function () {});

Artisan::command('mysql-backup', function () {});

Artisan::command('create-transaction', function () {});

Artisan::command('reward-referral', function () {});

Artisan::command('send-notification-listing', function () {});

Artisan::command('send-reminder', function () {});

Artisan::command('send-reminder-ticket', function () {});

Artisan::command('send-reminder-voucher', function () {});

Artisan::command('issue-bepoz-referral-reward', function () {});

Artisan::command('issue-bepoz-signup-reward', function () {});

Artisan::command('issue-bepoz-survey-reward', function () {});

Artisan::command('poll-account', function () {});

Artisan::command('poll-account-total-saved', function () {});

Artisan::command('poll-prize-promo', function () {});

Artisan::command('poll-stamp-card', function () {});

Artisan::command('poll-voucher', function () {});

Artisan::command('check-listing', function () {});

Artisan::command('execute-pending-job', function () {});

Artisan::command('system-check', function () {});

Artisan::command('system-check-email', function () {});

Artisan::command('refresh-otp', function () {});





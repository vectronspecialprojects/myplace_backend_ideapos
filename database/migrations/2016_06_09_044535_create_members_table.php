<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->index();
            $table->decimal('points', 10, 2)->default(0);
            $table->decimal('balance', 10, 2)->default(0);
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('phone')->nullable();
            $table->string('profile_img')->nullable();
            $table->string('extra_img')->nullable();
            $table->string('bepoz_account_id')->nullable()->index();
            $table->string('bepoz_account_number')->nullable()->index();
            $table->string('bepoz_account_card_number')->nullable()->index();
            $table->boolean('bepoz_existing_account')->default(false);
            $table->string('bepoz_account_status')->nullable()->default('Pending');
            $table->string('bepoz_count_visits')->nullable()->default('0');

            $table->boolean('bepoz_UseCAlinkBalance')->default(false);
            $table->boolean('bepoz_UseCALinkPoints')->default(false);


            $table->date('dob')->nullable()->index();
            $table->enum('status', ['active', 'inactive'])->default('active');

            $table->string('mailing_preference')->nullable()->default('subscribed');
            $table->string('token')->nullable();

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('members');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVenuePivotTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venue_pivot_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('venue_id')->unsigned()->index();
            $table->string('venue_name')->nullable();
            $table->integer('venue_tag_id')->unsigned()->index();
            $table->string('venue_tags_name')->nullable();
            $table->bigInteger('display_order')->default(0);
            $table->boolean('status')->default(false);

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('venue_pivot_tags');
    }
}

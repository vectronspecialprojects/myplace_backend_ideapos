<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateVenuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venues', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->boolean('active')->default(true);

            /*
             * Bepoz Settings
             */
            $table->string('bepoz_api')->nullable();
            $table->string('bepoz_mac_key')->nullable();
            $table->string('bepoz_secondary_api')->nullable();
            $table->string('bepoz_till_id')->nullable();
            $table->string('bepoz_operator_id')->nullable();
            $table->string('bepoz_training_mode')->default('false')->nullable();
            $table->string('bepoz_payment_name')->nullable();
            $table->string('default_card_number_prefix')->nullable();
            $table->string('default_card_track')->nullable();

            /*
             * Individual settings
             */
            $table->text('open_and_close_hours')->nullable();
            $table->text('pickup_and_delivery_hours')->nullable();
            $table->text('social_links')->nullable();
            $table->text('address')->nullable();
            $table->text('email')->nullable();
            $table->text('telp')->nullable();
            $table->text('fax')->nullable();
            $table->text('image')->nullable();
            $table->boolean('hide_delivery_info')->default(false);
            $table->boolean('hide_opening_hours_info')->default(false);

            $table->text('android_link')->nullable();
            $table->text('ios_link')->nullable();

            $table->string('bepoz_account_last_successful_execution_time')->nullable();
            $table->string('bepoz_voucher_last_successful_execution_time')->nullable();

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('venues');
    }
}

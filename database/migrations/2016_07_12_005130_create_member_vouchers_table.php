<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateMemberVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_vouchers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('member_id')->index();
            $table->bigInteger('order_id')->default(0)->index();
            $table->bigInteger('order_details_id')->default(0)->index();
            $table->string('voucher_id')->nullable()->index();

            $table->string('name')->nullable();
            $table->integer('voucher_setup_id')->nullable();
            $table->string('voucher_type')->nullable();
            $table->boolean('unlimited_use')->default(false);
            $table->integer('maximum_discount')->default(0);
            $table->string('claim_venue_id')->nullable();
            $table->string('claim_store_id')->nullable();
            $table->integer('used_count')->default(0);
            $table->string('used_trans_id')->nullable();

            $table->string('lookup')->nullable()->index();
            $table->string('barcode')->nullable()->index();
            $table->boolean('redeemed')->default(false);
            $table->integer('amount_issued')->default(0);
            $table->integer('amount_left')->default(0);
            $table->integer('used_value')->default(0);
            $table->dateTimeTz('expire_date')->nullable();
            $table->dateTimeTz('issue_date')->nullable();
            $table->dateTimeTz('used_date')->nullable();

            $table->string('status')->nullable()->default('in_progress');
            $table->string('category')->nullable(); // treat this product as voucher or ticket

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('member_vouchers');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class UpdateTiersTableCustomFlags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tiers', function (Blueprint $table) {
            $table->string('precreated_account_custom_flag_name')->nullable()->after('venue_id');
            $table->string('precreated_account_custom_flag')->nullable()->after('venue_id');
            $table->boolean('use_precreated_account')->default(false)->after('venue_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tiers', function ($table) {
            $table->dropColumn(['precreated_account_custom_flag_name']);
            $table->dropColumn(['precreated_account_custom_flag']);
            $table->dropColumn(['use_precreated_account']);
        });
    }
}

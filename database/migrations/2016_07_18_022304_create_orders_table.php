<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type')->nullable();
            $table->string('pay_id')->nullable()->index();
            $table->string('pay_type')->nullable()->index();
            $table->string('pay_token')->nullable()->index();
            $table->string('pay_uuid')->nullable()->index();
            $table->string('paypal_id')->nullable()->index();

            $table->integer('point_reward')->nullable()->default(0); // total actual point reward if user buys using cash or mix
            $table->integer('claimable_point_reward')->nullable()->default(0); // total point rewards user can claim after payment

            $table->decimal('actual_total_price', 10, 2)->nullable()->default(0); // raw total price
            $table->decimal('actual_total_point', 10, 2)->nullable()->default(0); // raw total points

            $table->integer('redeem_point_ratio')->default(0)->nullable();

            $table->decimal('total_price', 10, 2)->default(0); // deducted total price after point ratio
            $table->decimal('total_point', 10, 2)->default(0); // used total points based on point ratio

            $table->decimal('paid_total_price', 10, 2)->default(0); // total price user paid
            $table->decimal('paid_total_point', 10, 2)->default(0); // total points user used

            $table->string('token')->index();
            $table->string('expired'); // store expire time of this order
            $table->bigInteger('member_id')->index();
            $table->ipAddress('ip_address');

            $table->dateTimeTz('ordered_at')->nullable();
            $table->dateTimeTz('confirmed_at')->nullable();
            $table->dateTimeTz('cancelled_at')->nullable();
            $table->dateTimeTz('paid_at')->nullable();

            $table->enum('order_status', ['expired', 'not_started', 'pending', 'confirmed', 'cancelled'])->default('not_started');
            $table->enum('payment_status', ['expired', 'not_started', 'pending', 'successful', 'failed', 'cancelled'])->default('not_started');
            $table->enum('voucher_status', ['expired', 'not_started', 'pending', 'in_progress', 'successful', 'failed', 'cancelled'])->default('not_started');
            $table->longText('payload');

            $table->text('comments')->nullable();
            $table->text('bepoz_transaction_ids')->nullable();
            $table->bigInteger('venue_id')->default(0);
            $table->bigInteger('listing_id')->default(0);

            $table->string('transaction_type')->nullable();
            $table->string('third_party_provider')->nullable();

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}

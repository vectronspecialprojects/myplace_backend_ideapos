<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateTiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tiers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            // $table->text('description')->nullable();
            $table->string('key')->index();
            $table->boolean('is_active')->default(true);
            // $table->boolean('use_prefix')->default(false);
            // $table->boolean('use_suffix')->default(false);
            // $table->string('prefix_for_existing_member')->nullable();
            // $table->string('suffix_for_existing_member')->nullable();
            // $table->string('prefix_for_new_member')->nullable();
            // $table->string('suffix_for_new_member')->nullable();
            $table->string('bepoz_group_id')->nullable();
            $table->string('bepoz_template_id')->nullable();
            $table->string('boundary_start')->nullable();
            $table->string('boundary_end')->nullable();

            $table->boolean('is_default_tier')->default(false); // by default, all members will go here
            // $table->string('tier_group')->nullable(); // student / corporate
            $table->boolean('is_an_expiry_tier')->default(false); // this indicates that this tier is for expired member
            $table->boolean('is_purchasable')->default(false); // to indicate that this tier is purchasable

            $table->bigInteger('venue_id')->default(0);
            // $table->boolean('is_gaming')->default(false); // CALink indicator
            // $table->boolean('use_calink_balance')->default(false);
            // $table->boolean('use_calink_points')->default(false);

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tiers');
    }
}

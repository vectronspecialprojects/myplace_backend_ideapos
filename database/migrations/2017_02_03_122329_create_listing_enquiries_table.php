<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateListingEnquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listing_enquiries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('listing_id');
            $table->bigInteger('member_id');
            $table->string('subject')->nullable();
            $table->text('message')->nullable();
            $table->text('answer')->nullable();
            $table->boolean('is_answered')->default(false);
            $table->text('comment')->nullable();
            $table->bigInteger('staff_id')->nullable();

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('listing_enquiries');
    }
}

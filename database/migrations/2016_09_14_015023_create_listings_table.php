<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name'); // for internal use
            $table->text('comment')->nullable(); // for internal use
            $table->string('heading');
            $table->string('desc_short');
            $table->text('desc_long')->nullable();
            $table->string('image_banner')->nullable();
            $table->string('image_square')->nullable();
            $table->enum('status', ['draft', 'active', 'inactive'])->default('inactive');
            $table->dateTime('datetime_start')->nullable();
            $table->dateTime('datetime_end')->nullable();
            $table->date('date_start')->nullable();
            $table->date('date_end')->nullable();
            $table->time('time_start')->nullable();
            $table->time('time_end')->nullable();
            $table->integer('listing_type_id')->index();
            $table->boolean('is_hidden')->default(false);
            $table->bigInteger('venue_id')->default(0);

            $table->longText('extra_settings')->nullable();
            $table->longText('payload')->nullable();
            $table->bigInteger('click_count')->default(0);

            $table->integer('max_limit')->default(0);
            $table->integer('max_limit_per_day')->default(0);
            $table->integer('max_limit_per_account')->default(0);
            $table->integer('max_limit_per_day_per_account')->default(0);

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('listings');
    }
}

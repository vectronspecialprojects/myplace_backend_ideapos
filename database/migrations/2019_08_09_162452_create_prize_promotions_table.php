<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreatePrizePromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prize_promotions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('bepoz_prize_promo_id')->nullable();
            $table->string('needed')->nullable();
            $table->string('listing_id')->nullable();
            $table->boolean('inactive')->default(false);
            $table->dateTime('date_updated')->nullable();
            $table->string('voucher_setup_id')->nullable();
            $table->string('bepoz_voucher_setup_id')->nullable();
            $table->string('promo_type')->nullable();
            $table->string('on_permanent')->nullable();
            $table->string('schedule_num')->nullable();
            $table->string('venue_id')->nullable();
            $table->string('store_id')->nullable();
            $table->string('till_id')->nullable();
            $table->string('acc_profile_id')->nullable();
            $table->string('image_id')->nullable();
            $table->string('promo_max')->nullable();
            $table->string('account_max')->nullable();
            $table->string('daily_max')->nullable();
            $table->string('daily_acc_max')->nullable();
            $table->string('transaction_max')->nullable();
            $table->string('promo_count')->nullable();
            $table->string('daily_count')->nullable();
            $table->string('daily_count_date')->nullable();
            $table->string('accumulate')->nullable();
            $table->string('flag_1')->nullable();
            $table->string('flag_2')->nullable();
            $table->string('entry_delay')->nullable();
            $table->string('subsequent')->nullable();
            $table->string('req_id')->nullable();
            $table->string('req_size')->nullable();
            $table->string('req_text')->nullable();
            $table->string('prize_type')->nullable();
            $table->string('prize_amount')->nullable();
            $table->string('display_message')->nullable();

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prize_promotions');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTiersTableMembershipNumber extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('tiers', function (Blueprint $table) {
            $table->integer('days')->default(0)->after('expiry_date_option');
            // $table->integer('weeks')->default(0)->after('expiry_date_option');
            $table->integer('months')->default(0)->after('expiry_date_option');
            // $table->integer('years')->default(0)->after('expiry_date_option');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tiers', function ($table) {
            $table->dropColumn(['days']);
            // $table->dropColumn(['weeks']);
            $table->dropColumn(['months']);
            // $table->dropColumn(['years']);
        });
    }
}

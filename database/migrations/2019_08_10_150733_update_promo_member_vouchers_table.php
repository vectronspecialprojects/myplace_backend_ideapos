<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class UpdatePromoMemberVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('member_vouchers', function (Blueprint $table) {
            $table->string('prize_promotion_id')->nullable()->after('voucher_setup_id');
            $table->string('bepoz_prize_promo_id')->nullable()->after('prize_promotion_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('member_vouchers', function ($table) {
            $table->dropColumn(['prize_promotion_id']);
            $table->dropColumn(['bepoz_prize_promo_id']);
        });
    }
}

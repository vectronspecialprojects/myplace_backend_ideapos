<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateTableFriendReferrals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('friend_referrals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('member_id'); //Person who sent invite
            $table->string('email'); //Email of recipient
            $table->string('token');
            $table->string('full_name')->nullable();

            $table->boolean('is_accepted')->default(false);
            $table->boolean('is_rewarded')->default(false);
            $table->boolean('has_joined')->default(false);
            $table->boolean('has_made_a_purchase')->default(false);

            $table->integer('point')->default(0);
            $table->string('reward')->nullable()->default('voucher');
            $table->string('reward_option')->nullable()->default('after_purchase'); //after_purchase / after_join
            $table->integer('voucher_setups_id')->default(0);
            $table->text('reminder_interval')->nullable();

            $table->bigInteger('new_member_id')->default(0);
            $table->string('mobile')->nullable();
            $table->string('referrers_membership')->nullable();

            $table->dateTime('expiry_date')->nullable();
            $table->dateTime('sent_at')->nullable();
            $table->dateTime('made_purchase_at')->nullable();
            $table->dateTime('issued_at')->nullable();
            $table->dateTime('accepted_at')->nullable();
            $table->dateTime('joined_at')->nullable();

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('friend_referrals');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateCreditcardTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('creditcard_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_id');

            $table->string('singleUseTokenId')->nullable();
            $table->string('guid')->nullable();

            $table->string('transactionId')->nullable();
            $table->string('receiptNumber')->nullable();
            $table->string('status')->nullable();
            $table->string('responseCode')->nullable();
            $table->text('responseText')->nullable();
            $table->string('transactionType')->nullable();
            $table->string('customerNumber')->nullable();
            $table->string('customerName')->nullable();
            $table->string('currency')->nullable();
            $table->string('paymentAmount')->nullable();
            $table->string('paymentMethod')->nullable();


            // -- credit card partial information
            $table->string('cardNumber')->nullable();
            $table->string('expiryDateMonth')->nullable();
            $table->string('expiryDateYear')->nullable();
            $table->string('cardScheme')->nullable();
            $table->string('cardholderName')->nullable();

            // -- merchant detail
            $table->string('merchantId')->nullable();
            $table->string('merchantName')->nullable();

            // -- other details
            $table->string('transactionDateTime')->nullable();
            $table->string('settlementDate')->nullable();
            $table->string('isVoidable')->nullable();
            $table->string('isRefundable')->nullable();

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('creditcard_transactions');
    }
}

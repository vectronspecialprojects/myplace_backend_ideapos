<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateClaimedPromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claimed_promotions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('listing_id')->default(0);
            $table->integer('listing_type_id')->default(0);
            $table->bigInteger('product_id')->default(0);
            $table->integer('product_type_id')->default(0);
            $table->bigInteger('member_id')->default(0);
            $table->bigInteger('tier_id')->default(0);
            $table->bigInteger('voucher_setups_id')->default(0);
            $table->bigInteger('member_voucher_id')->default(0);
            $table->bigInteger('venue_id')->default(0);

            $table->longText('voucher_lookup')->nullable();
            $table->boolean('is_used')->default(false);

            $table->enum('status', ['expired', 'not_started', 'pending', 'in_progress', 'successful', 'failed', 'cancelled'])->default('not_started');
            $table->longText('payload')->nullable(); // store any key in json format

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('claimed_promotions');
    }
}

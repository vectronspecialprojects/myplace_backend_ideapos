<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateGiftCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gift_certificates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('purchaser_id');
            $table->bigInteger('recipient_id')->default(0);
            $table->string('full_name');
            $table->string('email');
            $table->text('note')->nullable();
            $table->string('lookup')->nullable();
            $table->string('barcode')->nullable();
            $table->integer('value')->nullable();
            $table->text('payload')->nullable();
            $table->dateTime('expiry_date')->nullable();
            $table->dateTime('issue_date')->nullable();
            $table->string('token')->nullable();

            $table->bigInteger('order_id')->default(0);
            $table->bigInteger('order_details_id')->default(0);
            $table->integer('voucher_setups_id')->default(0);
            $table->integer('venue_id')->default(0);

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gift_certificates');
    }
}

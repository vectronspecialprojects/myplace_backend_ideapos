<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateMemberTierPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_tier', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('member_id')->unsigned()->index();
            $table->integer('tier_id')->unsigned()->index();
            $table->dateTime('date_expiry')->nullable();
            $table->boolean('auto_renewal')->default(false);
            $table->dateTime('date_renewal')->nullable();

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('member_tier');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateListingProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listing_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('listing_id')->unsigned()->index();
            $table->integer('product_id')->unsigned()->index();
            $table->integer('tier_id')->nullable()->index();
            $table->integer('sequence_number')->nullable();

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('listing_products');
    }
}

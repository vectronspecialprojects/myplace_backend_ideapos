<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateBepozPushedPointsLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bepoz_pushed_points_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('payload')->nullable();
            $table->boolean('has_been_processed')->default(false);
            $table->dateTime('processing_time')->nullable();

            $table->timestampTz('created_at')->useCurrent();
            $table->timestampTz('updated_at')->nullable();
            $table->timestampTz('deleted_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bepoz_pushed_points_logs');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class UpdateMemberTablePreferredVenueName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('members', function (Blueprint $table) {
            $table->string('original_preferred_venue_name')->nullable()->after('original_preferred_venue');
            $table->string('current_preferred_venue_name')->nullable()->after('current_preferred_venue');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('members', function ($table) {
            $table->dropColumn(['original_preferred_venue_name']);
            $table->dropColumn(['current_preferred_venue_name']);
        });
    }
}

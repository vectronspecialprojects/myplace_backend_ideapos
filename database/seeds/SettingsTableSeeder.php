<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createSetting('pattern', '1', 'system', 'patterns');
        $this->createSetting('bepoz_api', '103.17.223.46:9992', 'bepoz', null);
        $this->createSetting('bepoz_mac_key', 'Vectron123', 'bepoz', null);
        $this->createSetting('redeem_point_ratio', 100, 'point_ratio', null);
        $this->createSetting('reward_point_ratio', 1, 'point_ratio', null);
        $this->createSetting('facebook_point_reward', 10, 'point_reward', null);
        $this->createSetting('twitter_point_reward', 10, 'point_reward', null);
        $this->createSetting('google_point_reward', 10, 'point_reward', null);
        $this->createSetting('company_name', 'Vectron Pty Ltd', 'invoice', null);
        $this->createSetting('company_street_number', '63-71', 'invoice', null);
        $this->createSetting('company_street_name', 'Boundary Road', 'invoice', null);
        $this->createSetting('company_suburb', 'North Melbourne', 'invoice', null);
        $this->createSetting('company_state', 'VIC', 'invoice', null);
        $this->createSetting('company_postcode', '3051', 'invoice', null);
        $this->createSetting('company_phone', '(03) 9328 8222', 'invoice', null);
        $this->createSetting('company_fax', '(03) 9328 8333', 'invoice', null);
        $this->createSetting('company_email', 'admin@vecport.net', 'invoice', null);
        $this->createSetting('company_abn', '112233445566', 'invoice', null);
        $this->createSetting('ticket_max_limit', 10, 'system', null);
        $this->createSetting('admin_email', 'admin@vectron.com.au', 'admin', null, \GuzzleHttp\json_encode(['admin@vectron.com.au']));
        $this->createSetting('admin_full_name', 'Info', 'admin', null);
        $this->createSetting('product_image', 'https://s3-ap-southeast-2.amazonaws.com/bepoz-loyalty-app/bepoz-Icon.jpg', 'default_image', null);
        $this->createSetting('listing_image_square', 'https://s3-ap-southeast-2.amazonaws.com/bepoz-loyalty-app/bepoz-Icon.jpg', 'default_image', null);
        $this->createSetting('listing_image_banner', 'https://via.placeholder.com/500x160', 'default_image', null);

        // try {
        //     //$path = storage_path('../public/images/profile_default.png');
        //     $path = public_path('images/profile_default.png');

        //     if (!File::exists($path)) {
        //         abort(404);
        //     }

        //     Storage::disk('s3')->put('profile_default.png', file_get_contents($path), 'public');

        //     $profile_default_link = "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/profile_default.png?" . time();

        //     Log::warning("uploaded ".$profile_default_link);
        // }
        // catch (\Exception $e) {
        //     Log::warning($e);
        // }

        $this->createSetting('member_profile_img', "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/profile_default.png", 'default_image', null);
        $this->createSetting('invoice_logo', 'https://via.placeholder.com/300x300', 'default_image', null);
        $this->createSetting('use_invoice_logo_as_default_image', 'false', 'default_image', null);
        $this->createSetting('invoice_header_logo', 'https://via.placeholder.com/862x353', 'default_image', null);
        $this->createSetting('use_bepoz_header_as_default_header', 'true', 'default_image', null);

        $this->createSetting('pickup_email', 'admin@vectron.com.au', 'system', null, \GuzzleHttp\json_encode(['admin@vectron.com.au']));
        $this->createSetting('reservation_email', 'admin@vectron.com.au', 'system', null, \GuzzleHttp\json_encode(['admin@vectron.com.au']));
        $this->createSetting('enquiry_email', 'admin@vectron.com.au', 'system', null, \GuzzleHttp\json_encode(['admin@vectron.com.au']));
        $this->createSetting('booth_email', 'admin@vectron.com.au', 'system', null, \GuzzleHttp\json_encode(['admin@vectron.com.au']));
        $this->createSetting('signup_email', 'admin@vectron.com.au', 'system', null, \GuzzleHttp\json_encode(['admin@vectron.com.au']));
        $this->createSetting('tier_email', 'admin@vectron.com.au', 'system', null, \GuzzleHttp\json_encode(['admin@vectron.com.au']));

        $this->createSetting('app_url', 'https://www.google.com.au', 'system', null);
        $this->createSetting('minimum_display_content', 50, 'system', null);

        $this->createSetting('bepoz_till_id', '17', 'bepoz', null);
        $this->createSetting('bepoz_operator_id', '10', 'bepoz', null);
        $this->createSetting('bepoz_training_mode', 'true', 'bepoz', null);

        $this->createSetting('friend_referral_reward', 'voucher', 'friend_referral', null);
        $this->createSetting('friend_referral_reward_option', 'after_purchase', 'friend_referral', null);
        $this->createSetting('friend_referral_point', '100', 'friend_referral', null);
        $this->createSetting('friend_referral_voucher_setups_id', '0', 'friend_referral', 'voucher');
        $this->createSetting('friend_referral_reminder_interval', null, 'friend_referral', null, \GuzzleHttp\json_encode(["1", "30", "90"]));
        $this->createSetting('friend_referral_voucher_background', 'https://via.placeholder.com/150x150', 'friend_referral', null);
        $this->createSetting('friend_referral_message', 'Refer a friend who becomes a member and you\'ll receive something', 'friend_referral', null);

        $this->createSetting('default_card_number_prefix', 'EMA', 'system', null);
        $this->createSetting('default_card_track', 'no_track', 'system', null);
        $this->createSetting('guest_tier_expiry_days', (365 * 5), 'system', null);

        $this->createSetting('time_send_reminder', '00:00', 'system', null);
        $this->createSetting('time_backup', '00:00', 'system', null);
        $this->createSetting('time_downgrade_member', '00:00', 'system', null);
        $this->createSetting('time_reward_referral', '00:00', 'system', null);
        $this->createSetting('time_issue_voucher', '00:00', 'system', null);

        $this->createSetting('bepoz_secondary_api', 'apitest.vectron.com.au:9191', 'bepoz', null);
        $this->createSetting('bepoz_payment_name', 'Online Payment', 'bepoz', null);

        $now = \Carbon\Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s');
        $this->createSetting('bepoz_account_last_successful_execution_time', $now, 'bepoz', null);
        $this->createSetting('bepoz_voucher_last_successful_execution_time', $now, 'bepoz', null);
        $this->createSetting('bepoz_account_total_saved_last_successful_execution_time', $now, 'bepoz', null);

        $now2 = \Carbon\Carbon::now(config('app.timezone'))->addYear(1);
        $this->createSetting('cut_off_date', $now2->toIso8601String(), 'tier', null);

        $this->createSetting('welcome_instruction_enable', 'true', 'system', null);
        $this->createSetting('welcome_instruction_total_slots', '4', 'layout', null);
        $this->createSetting('welcome_instruction', null, 'layout', null, \GuzzleHttp\json_encode(array(
            ["id" =>  1, "title" => "Title", "content" => "Title", "image_icon" => "https://via.placeholder.com/375x305"],
            ["id" =>  2, "title" => "Title", "content" => "Title", "image_icon" => "https://via.placeholder.com/375x305"],
            ["id" =>  3, "title" => "Title", "content" => "Title", "image_icon" => "https://via.placeholder.com/375x305"],
            ["id" =>  4, "title" => "Title", "content" => "Title", "image_icon" => "https://via.placeholder.com/375x305"],
            ["id" =>  5, "title" => "Title", "content" => "Title", "image_icon" => "https://via.placeholder.com/375x305"],
            ["id" =>  6, "title" => "Title", "content" => "Title", "image_icon" => "https://via.placeholder.com/375x305"],
            ["id" =>  7, "title" => "Title", "content" => "Title", "image_icon" => "https://via.placeholder.com/375x305"],
            ["id" =>  8, "title" => "Title", "content" => "Title", "image_icon" => "https://via.placeholder.com/375x305"]          
        )));

        $this->createSetting('tab_layout_total_buttons', '5', 'layout', null);
        $this->createSetting('tab_layout', null, 'layout', null, \GuzzleHttp\json_encode(array(
            ["id" => 1, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "home", 'special_link' => '', 'state' => 'HomeNavigator'],
            ["id" => 2, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 3, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 4, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 5, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
        )));

        $this->createSetting('front_menu_total_buttons', '4', 'layout', null);
        $this->createSetting('front_menu_buttons', null, 'layout', null, \GuzzleHttp\json_encode(array(
            ["id" =>  1, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  2, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  3, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  4, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  5, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  6, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  7, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  8, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  9, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 10, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none']
        )));

        $this->createSetting('side_menu_before_login_total_buttons', '4', 'layout', null);
        $this->createSetting('side_menu_before_login_buttons', null, 'layout', null, \GuzzleHttp\json_encode(array(
            ["id" =>  1, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  2, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  3, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  4, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  5, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  6, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  7, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  8, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  9, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 10, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 11, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 12, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 13, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 14, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 15, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 16, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 17, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 18, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 19, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 20, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
        )));

        $this->createSetting('side_menu_total_buttons', '10', 'layout', null);

        $this->createSetting('side_menu_buttons', null, 'layout', null, \GuzzleHttp\json_encode(array(
            ["id" =>  1, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  2, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  3, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  4, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  5, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  6, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  7, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  8, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  9, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 10, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 11, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 12, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 13, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 14, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 15, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 16, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 17, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 18, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 19, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 20, "icon" => "", "image_icon" => "https://via.placeholder.com/125x125", "icon_selector" => "icon", "page_name" => "", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
        )));

        // $this->createSetting('side_menu_location', 'left', 'layout', null);
        $this->createSetting('side_menu_show_profile', 'false', 'layout', null);

        $this->createSetting('mandrill_key', 'DcmCkTGEtBuaWVfdBTz93A', 'key', null);

        $this->createSetting('gallery_total_slots', '8', 'layout', null);

        $this->createSetting('galleries', null, 'layout', null, \GuzzleHttp\json_encode(array(
            ["id" => 1, "url" => "https://via.placeholder.com/375x305"],
            ["id" => 2, "url" => "https://via.placeholder.com/375x305"],
            ["id" => 3, "url" => "https://via.placeholder.com/375x305"],
            ["id" => 4, "url" => "https://via.placeholder.com/375x305"],
            ["id" => 5, "url" => "https://via.placeholder.com/375x305"],
            ["id" => 6, "url" => "https://via.placeholder.com/375x305"],
            ["id" => 7, "url" => "https://via.placeholder.com/375x305"],
            ["id" => 8, "url" => "https://via.placeholder.com/375x305"],
        )));

        // $this->createSetting('community_impact_title', 'false', 'system', null);
        $this->createSetting('community_impact_content_option', 'text', 'system', null);
        $this->createSetting('community_impact_content_text', '', 'system', null);
        // $this->createSetting('community_impact_color', 'false', 'system', null);
        // $this->createSetting('community_impact_font_color', 'false', 'system', null);
        $this->createSetting('community_impact_button_option', '', 'system', null);        
        $this->createSetting('community_impact_gallery_total_slots', '8', 'layout', null);
        $this->createSetting('community_impact_gallery', null, 'layout', null, \GuzzleHttp\json_encode(array(
            ["id" => 1, "url" => "https://via.placeholder.com/335x130"],
            ["id" => 2, "url" => "https://via.placeholder.com/335x130"],
            ["id" => 3, "url" => "https://via.placeholder.com/335x130"],
            ["id" => 4, "url" => "https://via.placeholder.com/335x130"],
            ["id" => 5, "url" => "https://via.placeholder.com/335x130"],
            ["id" => 6, "url" => "https://via.placeholder.com/335x130"],
            ["id" => 7, "url" => "https://via.placeholder.com/335x130"],
            ["id" => 8, "url" => "https://via.placeholder.com/335x130"],
        )));

        $this->createSetting('profile_menu_total_buttons', '4', 'layout', null);
        $this->createSetting('profile_menu_buttons', null, 'layout', null, \GuzzleHttp\json_encode(array(
            ["id" =>  1, "icon" => "", "image_icon" => "", "page_name" => "", "icon_selector" => "icon", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  2, "icon" => "", "image_icon" => "", "page_name" => "", "icon_selector" => "icon", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  3, "icon" => "", "image_icon" => "", "page_name" => "", "icon_selector" => "icon", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  4, "icon" => "", "image_icon" => "", "page_name" => "", "icon_selector" => "icon", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  5, "icon" => "", "image_icon" => "", "page_name" => "", "icon_selector" => "icon", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  6, "icon" => "", "image_icon" => "", "page_name" => "", "icon_selector" => "icon", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  7, "icon" => "", "image_icon" => "", "page_name" => "", "icon_selector" => "icon", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  8, "icon" => "", "image_icon" => "", "page_name" => "", "icon_selector" => "icon", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" =>  9, "icon" => "", "image_icon" => "", "page_name" => "", "icon_selector" => "icon", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 10, "icon" => "", "image_icon" => "", "page_name" => "", "icon_selector" => "icon", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 11, "icon" => "", "image_icon" => "", "page_name" => "", "icon_selector" => "icon", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 12, "icon" => "", "image_icon" => "", "page_name" => "", "icon_selector" => "icon", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 13, "icon" => "", "image_icon" => "", "page_name" => "", "icon_selector" => "icon", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 14, "icon" => "", "image_icon" => "", "page_name" => "", "icon_selector" => "icon", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 15, "icon" => "", "image_icon" => "", "page_name" => "", "icon_selector" => "icon", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 16, "icon" => "", "image_icon" => "", "page_name" => "", "icon_selector" => "icon", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 17, "icon" => "", "image_icon" => "", "page_name" => "", "icon_selector" => "icon", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 18, "icon" => "", "image_icon" => "", "page_name" => "", "icon_selector" => "icon", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 19, "icon" => "", "image_icon" => "", "page_name" => "", "icon_selector" => "icon", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
            ["id" => 20, "icon" => "", "image_icon" => "", "page_name" => "", "icon_selector" => "icon", "page_layout" => 'special_view', "total" => "1", "pages" => [["listing_type_id" => "none", "title" => "Title ###"]], "special_page" => "none", 'special_link' => '', 'state' => 'none'],
        )));

        $this->createSetting('about', null, 'special', null, 'About Page');
        $this->createSetting('privacy_policy_tab_title', 'Privacy Policy', 'special', null);
        $this->createSetting('terms_and_conditions_tab_title', 'Terms and Conditions', 'special', null);
        $this->createSetting('privacy_policy', null, 'special', null, 'Legals Page');
        $this->createSetting('terms_and_conditions', null, 'special', null, 'Legals Page');
        $this->createSetting('address', null, 'special', null, '71 Boundary Road, North Melb, VIC, 3051');
        $this->createSetting('email', null, 'special', null, 'jacky@vectron.com.au');
        $this->createSetting('telp', null, 'special', null, '040404040');
        $this->createSetting('fax', null, 'special', null, '040404040');

        $this->createSetting('open_and_close_hours', null, 'special', null, \GuzzleHttp\json_encode(array(
            ["day" => "Mon", "time" => "9AM to 5PM"],
            ["day" => "Tue", "time" => "9AM to 5PM"],
            ["day" => "Wed", "time" => "9AM to 5PM"],
            ["day" => "Thu", "time" => "9AM to 5PM"],
            ["day" => "Fri", "time" => "9AM to 5PM"],
            ["day" => "Sat", "time" => "9AM to 5PM"],
            ["day" => "Sun", "time" => "9AM to 5PM"]
        )));

        $this->createSetting('pickup_and_delivery_hours', null, 'special', null, \GuzzleHttp\json_encode(array(
            ["day" => "Mon", "time" => "9AM to 5PM"],
            ["day" => "Tue", "time" => "9AM to 5PM"],
            ["day" => "Wed", "time" => "9AM to 5PM"],
            ["day" => "Thu", "time" => "9AM to 5PM"],
            ["day" => "Fri", "time" => "9AM to 5PM"],
            ["day" => "Sat", "time" => "9AM to 5PM"],
            ["day" => "Sun", "time" => "9AM to 5PM"]
        )));

        $this->createSetting('reward_multiply_rules', null, 'special', null, \GuzzleHttp\json_encode(array(
            ["dayofweek" => "0", "type" => "weekly", "multiply" => "1", "specific_date" => "2000-01-01"],
            ["dayofweek" => "1", "type" => "weekly", "multiply" => "1", "specific_date" => "2000-01-01"],
            ["dayofweek" => "2", "type" => "weekly", "multiply" => "1", "specific_date" => "2000-01-01"],
            ["dayofweek" => "3", "type" => "weekly", "multiply" => "1", "specific_date" => "2000-01-01"],
            ["dayofweek" => "4", "type" => "weekly", "multiply" => "1", "specific_date" => "2000-01-01"],
            ["dayofweek" => "5", "type" => "weekly", "multiply" => "1", "specific_date" => "2000-01-01"],
            ["dayofweek" => "6", "type" => "weekly", "multiply" => "1", "specific_date" => "2000-01-01"],
        )));

        $this->createSetting('stripe_testmode', 'true', 'stripe', null);
        $this->createSetting('stripe_payment_solution', 'stripe_connect', 'stripe', null);


        // test
        $this->createSetting('stripe_testkey', 'sk_test_N0NevtylfG84cdMJzDM1NDKO', 'stripe', null);
        $this->createSetting('stripe_test_publishable_key', 'pk_test_JISjl8FiwYJFzyDNYS5wRpvM', 'stripe', null);
        $this->createSetting('stripe_test_connected_account_id', 'acct_1CSeoMKwfAtj2lqN', 'stripe', null);
        $this->createSetting('stripe_test_application_fee_option', 'fixed', 'stripe', null);
        $this->createSetting('stripe_test_fixed_application_fee', 100, 'stripe', null);
        $this->createSetting('stripe_test_dynamic_application_fee', 2, 'stripe', null);

        // live
        $this->createSetting('stripe_key', 'sk_live_F91rzV2DFzNwq7NlCeKqpzu4', 'stripe', null);
        $this->createSetting('stripe_publishable_key', 'pk_live_qjpJ2qj4kpQkeuVHkGOr0uTq', 'stripe', null);
        $this->createSetting('stripe_connected_account_id', 'acct_1CSxvuJ64g8FwAzN', 'stripe', null);
        $this->createSetting('stripe_application_fee_option', 'fixed', 'stripe', null);
        $this->createSetting('stripe_fixed_application_fee', 100, 'stripe', null);
        $this->createSetting('stripe_dynamic_application_fee', 2, 'stripe', null);


        $this->createSetting('unlock_key', 'v3ctron$123', 'system', null);

        $this->createSetting('mandrill_email', 'no-reply@vecport.net', 'system', null);
        $this->createSetting('mandrill_sender_name', 'Vectron Systems Pty Ltd', 'system', null);

        $this->createSetting('social_links', null, 'special', null, \GuzzleHttp\json_encode(array(
            ["platform" => "facebook", "url" => "www.facebook.com"],
            ["platform" => "twitter", "url" => "www.twitter.com"],
            ["platform" => "instagram", "url" => "www.instagram.com"],
            ["platform" => "snapchat", "url" => "www.snapchat.com"],
            ["platform" => "website", "url" => "www.example.com"]
        )));

        $this->createSetting('hide_delivery_info', 'true', 'special', null);
        $this->createSetting('hide_opening_hours_info', 'true', 'special', null);

        $this->createSetting('android_link', '', 'special', null, '');
        $this->createSetting('ios_link', '', 'special', null, '');

        $this->createSetting('reward_after_signup_voucher_setups_id', '1', 'system', 'voucher');
        $this->createSetting('reward_after_signup_option', 'true', 'system', null);
        $this->createSetting('reward_after_signup_type', 'voucher', 'system', null);
        $this->createSetting('reward_after_signup_point', '100', 'system', null);


        $this->createSetting('view_member', null, 'view', null, \GuzzleHttp\json_encode(array(
            ["label" => "Name", "key" => "name", "visible" => true],
            ["label" => "Status", "key" => "status", "visible" => true],
            ["label" => "Tier", "key" => "tier", "visible" => true],
            ["label" => "Phone", "key" => "phone", "visible" => true],
            ["label" => "Points", "key" => "points", "visible" => true],
            ["label" => "DOB", "key" => "dob", "visible" => true],
            ["label" => "Balance", "key" => "balance", "visible" => true],
            ["label" => "Email", "key" => "email", "visible" => true],
            ["label" => "Date Expiry", "key" => "date_expiry", "visible" => true],
        )));

        $this->createSetting('gift_certificate', 'false', 'dashboard', null);
        $this->createSetting('ticket', 'false', 'dashboard', null);
        $this->createSetting('refer_a_friend', 'false', 'dashboard', null);
        $this->createSetting('transaction', 'false', 'dashboard', null);

        $this->createSetting('use_point', 'true', 'system', null);

        $this->createSetting('gift_certificate', 'false', 'dashboard', null);
        $this->createSetting('ticket', 'false', 'dashboard', null);
        $this->createSetting('refer_a_friend', 'false', 'dashboard', null);
        $this->createSetting('transaction', 'false', 'dashboard', null);

        $this->createSetting('use_point', 'true', 'system', null);

        $this->createSetting('sidenavigationbar', null, 'system', null, \GuzzleHttp\json_encode(array(
            ["label" => "Dashboard", "key" => "dashboard", "logo" => "material-icons  icon-Ticket",
                "path" => "/dashboard", "visible" => true],
            ["label" => "Booking Ticket", "key" => "booking_ticket", "logo" => "material-icons  icon-Ticket",
                "path" => "/bookingTicket", "visible" => true],
            ["label" => "Tickets", "key" => "tickets", "logo" => "material-icons  icon-Chimney",
                "path" => "/product", "visible" => true],
            ["label" => "Vouchers", "key" => "vouchers", "logo" => "material-icons  icon-Ticket",
                "path" => "/voucher", "visible" => true],
            ["label" => "Product", "key" => "specialProduct", "logo" => "material-icons  icon-Gift-Box", 
                "path" => "/specialProduct", "visible" => true],
            ["label" => "Members", "key" => "members", "logo" => "material-icons  icon-My-Space", 
                "path" => "/member", "visible" => true],
            ["label" => "Staff", "key" => "staff", "logo" => "material-icons  icon-Bellboy",
                "path" => "/staff", "visible" => true],
            ["label" => "Notifications", "key" => "notifications", "logo" => "material-icons  icon-Mailing-List",
                "path" => "/systemNotification", "visible" => true],
            ["label" => "Enquiries", "key" => "enquiries", "logo" => "material-icons  icon-Chat-Help3",
                "path" => "/enquiry", "visible" => true],
            ["label" => "FAQs", "key" => "faqs", "logo" => "material-icons  icon-Bulleted-List",
                "path" => "/faq", "visible" => true],
            ["label" => "Friend Referrals", "key" => "friendreferrals", "logo" => "material-icons  icon-Contact-Folder",
                "path" => "/friendReferral", "visible" => true],
            ["label" => "Transactions", "key" => "transactions", "logo" => "material-icons  icon-Cash-Pay",
                "path" => "/transaction", "visible" => true],
            ["label" => "Survey", "key" => "survey", "logo" => "material-icons  icon-Alphabet-List",
                "path" => "/survey", "visible" => true],
            ["label" => "Venues", "key" => "venues", "logo" => "material-icons  icon-Key-Hole1",
                "path" => "/venue", "visible" => true],
            ["label" => "Roles", "key" => "roles", "logo" => "material-icons  icon-Key-Hole1",
                "path" => "/role", "visible" => true],
        )));

        $this->createSetting('system_check_email', '', 'system', null, \GuzzleHttp\json_encode([]));

        $this->createSetting('email_server', 'mandrill', 'system', null, \GuzzleHttp\json_encode(['mandrill', 'mailjet']));
        $this->createSetting('mailjet_api_key', '488ae60ea361792f0bbebca61d2cf2c2', 'system', null, null);
        $this->createSetting('mailjet_secret_key', '53cc213c443133ade64a52471bfc5765', 'system', null, null);

        $this->createSetting('send_image_bepoz', 'false', 'system', null);
        $this->createSetting('reminder_ticket', 'false', 'system', null);
        $this->createSetting('reminder_ticket_interval', 'false', 'system', null, \GuzzleHttp\json_encode(["2", "10", "20"]));
        $this->createSetting('reminder_ticket_time', 'false', 'system', null, \GuzzleHttp\json_encode([]));

        $this->createSetting('menu_links', 'false', 'system', null, \GuzzleHttp\json_encode(
            array(
                ["platform" => "food", "url" => ""],
                ["platform" => "drink", "url" => ""]
            )));

        $this->createSetting('poll_account_total_saved', 'false', 'system', null);
        $this->createSetting('third_party_url', '', 'system', null);
        $this->createSetting('venue_number', 'single', 'system', null, \GuzzleHttp\json_encode(['single', 'multiple']));
        $this->createSetting('ebet_gaming', 'false', 'system', null);

        $this->createSetting('enable_otp', 'false', 'system', null);
        $this->createSetting('send_otp_sms', 'false', 'system', null);
        $this->createSetting('request_otp_all_time', 'false', 'system', null);
        $this->createSetting('sms_burst_key', '1170bba6d8b80afe58a1b117d891ecdd', 'system', null);
        $this->createSetting('sms_burst_secret', 'SpecialProjects', 'system', null);
        $this->createSetting('sms_from', 'BEPOZ', 'system', null);

        // $this->createSetting('create_member', 'false', 'system', null);

        $this->createSetting('reminder_voucher', 'false', 'system', null);
        $this->createSetting('reminder_voucher_interval', 'false', 'system', null, \GuzzleHttp\json_encode(["2", "10", "20"]));
        $this->createSetting('reminder_voucher_time', 'false', 'system', null, \GuzzleHttp\json_encode([]));

        $this->createSetting('onesignal_app_id', 'd3442f74-12c9-4e82-8822-d6273726036f', 'system', null);
        $this->createSetting('onesignal_rest_api_key', 'MGI2N2ZkM2QtMTBiZS00ZWQ1LWExYTUtNWQ0NzNlYmM3ZjQ0', 'system', null);
        $this->createSetting('onesignal_user_auth_key', 'OTk2OGVkNzYtYzgzOS00YmJjLTkwZGEtYWYwMGIxM2Y2MTcy', 'system', null);


        $this->createSetting('bepoz_prize_promo_last_successful_execution_time', $now, 'bepoz', null);
        $this->createSetting('bepoz_stamp_card_last_successful_execution_time', $now, 'bepoz', null);

        $this->createSetting('recaptcha_enable', 'false', 'system', null);
        $this->createSetting('recaptcha_site_key', '6Le7BboUAAAAAJb8AVYqQczgGESnVu2eVgY0_07U', 'system', null);
        $this->createSetting('recaptcha_server_key', '6Le7BboUAAAAADvg3lLJ1vWEk6KK1wuS_Ze7YgF4', 'system', null);

        $this->createSetting('your_order_api', '', 'system', null);
        $this->createSetting('your_order_key', '', 'system', null);
        $this->createSetting('myplace_key', '', 'system', null);

        $this->createSetting('default_tier', '1', 'system', null);

        $this->createSetting('pos_voucher_enable', 'false', 'system', null);

        $this->createSetting('stripe_integration_show', 'true', 'system', null);
        $this->createSetting('gaming_integration_show', 'false', 'system', null);
        $this->createSetting('third_party_integration_show', 'false', 'system', null);

        $this->createSetting('venue_tags_enable', 'false', 'system', null);
        $this->createSetting('venue_tag_color', '#B69B78', 'system', null);
        $this->createSetting('venue_tag_font_color', '#FF0000', 'system', null);
        $this->createSetting('venue_tag_click_color', '#FFAABB', 'system', null);
        $this->createSetting('venue_tag_click_font_color', '#FF0000', 'system', null);
        $this->createSetting('venue_tag_bgcolor', '#FFFFFF', 'system', null);

        $this->createSetting('bepozcustomfield_enable', 'false', 'system', null);
        $this->createSetting('bepozcustomfield', null, 'system', null, \GuzzleHttp\json_encode(array(
            ["displayOrder" =>  1, "id" => "first_name", "fieldName" => "First Name", "fieldType" => "Text", "bepozFieldNum" => "322", "fieldDisplayTitle" => "First Name", "required" => true, "active" => true, "displayInApp" => true, "keyboardType" => "default", "disabled" => false, "defaultValue" => "", "toggleValue" => "", "minLength" => "", "maxLength" => "", "multiValues" => ""],
            ["displayOrder" =>  2, "id" => "last_name", "fieldName" => "Last Name", "fieldType" => "Text", "bepozFieldNum" => "323", "fieldDisplayTitle" => "Last Name", "required" => true, "active" => true, "displayInApp" => true, "keyboardType" => "default", "disabled" => false, "defaultValue" => "", "toggleValue" => "", "minLength" => "", "maxLength" => "", "multiValues" => ""],
            ["displayOrder" =>  3, "id" => "email", "fieldName" => "Email", "fieldType" => "Text", "bepozFieldNum" => "334", "fieldDisplayTitle" => "Email", "required" => true, "active" => true, "displayInApp" => true, "keyboardType" => "default", "disabled" => false, "defaultValue" => "", "toggleValue" => "", "minLength" => "", "maxLength" => "", "multiValues" => ""],
            ["displayOrder" =>  4, "id" => "dob", "fieldName" => "Date of Birth", "fieldType" => "Date", "bepozFieldNum" => "206", "fieldDisplayTitle" => "Date of Birth", "required" => true, "active" => true, "displayInApp" => true, "keyboardType" => "default", "disabled" => false, "defaultValue" => "", "toggleValue" => "", "minLength" => "", "maxLength" => "", "multiValues" => ""],
            ["displayOrder" =>  5, "id" => "mobile", "fieldName" => "Mobile", "fieldType" => "Text", "bepozFieldNum" => "328", "fieldDisplayTitle" => "Mobile", "required" => true, "active" => true, "displayInApp" => true, "keyboardType" => "default", "disabled" => false, "defaultValue" => "", "toggleValue" => "", "minLength" => "", "maxLength" => "", "multiValues" => ""],
            ["displayOrder" =>  6, "id" => "password", "fieldName" => "Password", "fieldType" => "Text", "bepozFieldNum" => "332", "fieldDisplayTitle" => "Password", "required" => true, "active" => true, "displayInApp" => true, "keyboardType" => "default", "disabled" => false, "defaultValue" => "", "toggleValue" => "", "minLength" => "", "maxLength" => "", "multiValues" => ""],
        )));

        $this->createSetting('google_map_apikey', '', 'system', null);

        $this->createSetting('gaming_system_url', '', 'system', null);
        $this->createSetting('gaming_username', '', 'system', null);
        $this->createSetting('gaming_password', '', 'system', null);
        $this->createSetting('gaming_site_id', '', 'system', null);
        $this->createSetting('gaming_system', '', 'system', null, \GuzzleHttp\json_encode(array(
            ["id" => "1", "label" => "IGT", "key" => "igt", "need_url" => "yes", "need_username" => "yes", "need_password" => "yes", "need_site_id" => "yes", "info" => ""],
            ["id" => "2", "label" => "Odyssey", "key" => "odyssey", "need_url" => "yes", "need_username" => "yes", "need_password" => "yes", "need_site_id" => "yes", "info" => ""]
        )));
        $this->createSetting('gaming_system_enable', 'false', 'system', null);
        $this->createSetting('gaming_mandatory_field', 'false', 'system', null, \GuzzleHttp\json_encode(array(
            ["id" => "1", "fieldName" => "First Name", "key" => "first_name", "fieldType" => "true", "fieldDisplayTitle" => "First Name", "default_value" => false, "multiValues" => false, "active" => true, "required" => true, "displayInApp" => false, "info" => ""],
            ["id" => "2", "fieldName" => "Last Name", "key" => "last_name", "fieldType" => "true", "fieldDisplayTitle" => "Last Name", "default_value" => false, "multiValues" => false, "active" => true, "required" => true, "displayInApp" => false, "info" => ""]
        )));

        $this->createSetting('instruction_lets_get_started', null, 'special', null, 'About Page');
        $this->createSetting('instruction_resend_details', null, 'special', null, 'About Page');

        $this->createSetting('reminder_voucher_push', 'false', 'system', null);
        $this->createSetting('reminder_voucher_system', 'false', 'system', null);
        $this->createSetting('reminder_voucher_system_notification_id', '0', 'system', null);

        $this->createSetting('sms_confirmation_message', 'Please confirm your mobile with click this link ', 'system', null);
        $this->createSetting('sms_mobile_change_confirmation_message', 'Please click on this url to confirm mobile number change ', 'system', null);
        $this->createSetting('sms_resend_details_message', 'Your Account Detail', 'system', null);

        $this->createSetting('poll_account_bepoz_call_limit', '10', 'system', null);

        $this->createSetting('odyssey_url', '10.36.6.166:28108/eps/execute', 'system', null);
        $this->createSetting('odyssey_from_system', 'BePoz', 'system', null);
        $this->createSetting('odyssey_to_system', 'arc_svr', 'system', null);
        $this->createSetting('odyssey_message_id', '0', 'system', null);
        $this->createSetting('odyssey_datetime_sent', '2021-05-31T23:59:59', 'system', null);
        $this->createSetting('odyssey_command_class', 'Communication', 'system', null);
        $this->createSetting('odyssey_vendor_code', 'BePoz', 'system', null);
        $this->createSetting('odyssey_device_code', '4000', 'system', null);
        $this->createSetting('odyssey_shared_key', '29FA509104E84E57', 'system', null);
        $this->createSetting('odyssey_custom_field_id', '1', 'system', null);

        $this->createSetting('is_preffered_venue_color', 'false', 'system', null);

        // $this->createSetting('default_member_expiry_date', 'false', 'system', null);

        $this->createSetting('signup_matching_bepoz_group_venue_tier', 'false', 'system', null);


        // $this->createSetting('promo_code_enable', 'false', 'system', null);
        // $this->createSetting('sponsorship_code_enable', 'false', 'system', null);
        
        $this->createSetting('listing_tags_enable', 'false', 'system', null);
        $this->createSetting('listing_tag_color', '#B69B78', 'system', null);
        $this->createSetting('listing_tag_font_color', '#FF0000', 'system', null);
        $this->createSetting('listing_tag_click_color', '#FFAABB', 'system', null);
        $this->createSetting('listing_tag_click_font_color', '#FF0000', 'system', null);
        $this->createSetting('listing_tag_bgcolor', '#FFFFFF', 'system', null);
        
        $this->createSetting('app_flag_id_bepoz', 'false', 'system', null);
        $this->createSetting('app_flag_name_bepoz', 'false', 'system', null);

        $this->createSetting('burst_sms_integration_show', 'false', 'system', null);
        
        $this->createSetting('mobile_confirmation_enable', 'false', 'system', null);
        $this->createSetting('mobile_confirmation_successful_image', 'false', 'system', null);
        $this->createSetting('mobile_confirmation_successful_message', 'false', 'system', null);
        $this->createSetting('mobile_confirmation_unsuccessful_image', 'false', 'system', null);
        $this->createSetting('mobile_confirmation_unsuccessful_message', 'false', 'system', null);
        $this->createSetting('email_confirmation_successful_image', 'false', 'system', null);
        $this->createSetting('email_confirmation_successful_message', 'false', 'system', null);
        $this->createSetting('email_confirmation_unsuccessful_image', 'false', 'system', null);
        $this->createSetting('email_confirmation_unsuccessful_message', 'false', 'system', null);
        
        $this->createSetting('mobile_confirmation_successful_button_font_color', 'false', 'system', null);
        $this->createSetting('mobile_confirmation_successful_button_border_color', 'false', 'system', null);
        $this->createSetting('mobile_confirmation_successful_button_bg_color', 'false', 'system', null);
        $this->createSetting('mobile_confirmation_successful_font_color', 'false', 'system', null);
        $this->createSetting('mobile_confirmation_successful_bg_color', 'false', 'system', null);

        $this->createSetting('mobile_confirmation_unsuccessful_button_font_color', 'false', 'system', null);
        $this->createSetting('mobile_confirmation_unsuccessful_button_border_color', 'false', 'system', null);
        $this->createSetting('mobile_confirmation_unsuccessful_button_bg_color', 'false', 'system', null);
        $this->createSetting('mobile_confirmation_unsuccessful_font_color', 'false', 'system', null);
        $this->createSetting('mobile_confirmation_unsuccessful_bg_color', 'false', 'system', null);

        $this->createSetting('email_confirmation_successful_button_font_color', 'false', 'system', null);
        $this->createSetting('email_confirmation_successful_button_border_color', 'false', 'system', null);
        $this->createSetting('email_confirmation_successful_button_bg_color', 'false', 'system', null);
        $this->createSetting('email_confirmation_successful_font_color', 'false', 'system', null);
        $this->createSetting('email_confirmation_successful_bg_color', 'false', 'system', null);

        $this->createSetting('email_confirmation_unsuccessful_button_font_color', 'false', 'system', null);
        $this->createSetting('email_confirmation_unsuccessful_button_border_color', 'false', 'system', null);
        $this->createSetting('email_confirmation_unsuccessful_button_bg_color', 'false', 'system', null);
        $this->createSetting('email_confirmation_unsuccessful_font_color', 'false', 'system', null);
        $this->createSetting('email_confirmation_unsuccessful_bg_color', 'false', 'system', null);
        
        $this->createSetting('distance_order', 'false', 'system', null);
                
        $this->createSetting('pond_hoppers_url', '', 'system', null);        
        $this->createSetting('pond_hoppers_unique_key', '', 'system', null);
        $this->createSetting('pond_hoppers_enable', 'false', 'system', null);

        $this->createSetting('feature_app_layout_gallery_top_box', 'false', 'system', null);
        $this->createSetting('feature_app_layout_first_time_signin', 'false', 'system', null);
        $this->createSetting('feature_app_layout_community_points', 'false', 'system', null);
        $this->createSetting('feature_app_layout_connect_with_us', 'false', 'system', null);

        $this->createSetting('gaming_account_lowest_enable', 'false', 'system', null);
        
        $this->createSetting('instruction_match_email_mobile', null, 'special', null, 'About Page');
        $this->createSetting('instruction_match_bepoz_id_number', null, 'special', null, 'About Page');
        
        // $this->createSetting('should_member_expiry', 'false', 'system', null);
        $this->createSetting('member_default_tier', 'false', 'system', null, \GuzzleHttp\json_encode([]));
        $this->createSetting('enable_tier_icon_image_colour', 'false', 'system', null);
        $this->createSetting('enable_tier_prefix_suffix', 'false', 'system', null);
        $this->createSetting('enable_tier_gaming_setting', 'false', 'system', null);

        // $this->createSetting('enable_terms_condition_field', 'false', 'system', null);
        $this->createSetting('email_not_allowed_domains', 'false', 'system', null, \GuzzleHttp\json_encode([]));

        $this->createSetting('form_input_style', 'rounded', 'system', null);
        $this->createSetting('form_floating_title', 'false', 'system', null);
        
        $this->createSetting('use_legacy_design', 'false', 'system', null);
        $this->createSetting('display_venue_selection', 'false', 'system', null);

        $this->createSetting('merchant_app_hub_url', 'https://sso.vecport.net/dev72/api', 'system', null);
        $this->createSetting('myplace_backpanel_url', 'https://dev.vecport.net', 'system', null);        
        $this->createSetting('bepoz_status', 'false', 'system', null, \GuzzleHttp\json_encode([]));
        
        $this->createSetting('term_webview_enable', 'false', 'system', null);
        $this->createSetting('term_webview_url', '', 'system', null);
        $this->createSetting('policy_webview_enable', 'false', 'system', null);
        $this->createSetting('policy_webview_url', '', 'system', null);

        $this->createSetting('app_name', '', 'system', null);
        $this->createSetting('app_default_website', '', 'system', null);
        $this->createSetting('app_is_show_logo', 'true', 'system', null);
        $this->createSetting('app_account_match', 'true', 'system', null);
        $this->createSetting('app_is_show_star_bar', 'true', 'system', null);
        $this->createSetting('app_show_tier_below_name', 'true', 'system', null);
        $this->createSetting('app_show_points_below_name', 'false', 'system', null);
        $this->createSetting('app_is_show_preferred_venue', 'true', 'system', null);
        $this->createSetting('app_show_tier_name', 'true', 'system', null);
        $this->createSetting('app_on_boarding_enable', 'false', 'system', null);
        $this->createSetting('app_is_group_filter', 'false', 'system', null);
        $this->createSetting('app_is_component_shadowed', 'true', 'system', null);
        $this->createSetting('app_sub_header_align_left', 'true', 'system', null);

    }

    function createSetting($key, $value, $category, $table_name, $extended_value = null)
    {
        $setting = \App\Setting::where('key', $key)->first();
        if (is_null($setting)) {
            $setting = new \App\Setting();
            $setting->key = $key;
            $setting->value = $value;
            $setting->category = $category;
            $setting->table_name = $table_name;
            $setting->extended_value = $extended_value;
            $setting->save();
        }
    }
}

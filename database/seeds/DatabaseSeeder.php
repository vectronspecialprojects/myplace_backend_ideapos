<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(RolesTableSeeder::class);

        // $this->call(VenuesTableSeeder::class);
        $this->call(TiersTableSeeder::class);
        $this->call(TagsTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(PlatformsTableSeeder::class);
        $this->call(PatternsTableSeeder::class);

        $this->call(EmailsTableSeeder::class);
        $this->call(ListingTypesTableSeeder::class);
        $this->call(ProductTypesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        
        // Restore from backup
        // $this->call(Restore::class);
    }
}

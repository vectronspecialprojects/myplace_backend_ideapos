<?php

use Illuminate\Database\Seeder;

class ListingTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createType('Special Event',
            [
                "name" => "Special Event",
                "category" => "information",
                "hide_this" => false,
                "feature_display_schedules" => true,
                "feature_products" => true,
                "feature_vouchers" => false,
                "setting_maximum_allocation" => true,
                "setting_time_driven" => true,
                "setting_show_time" => true,
                "setting_show_long_description" => true,
                "option_purchasable" => true,
                "option_inquirable" => true,
                "option_specific" => true,
                "option_regular" => false,
                "setting_show_square_image" => true,
                "setting_show_banner_image" => true,
                "setting_show_venue_selection" => true,
                "setting_bepoz_product_id" => false,
                "setting_show_price" => false,
                "setting_show_listing_tags" => false,
                "setting_never_expired" => false
            ]
        );

        $this->createType('Regular Event',
            [
                "name" => "Regular Event",
                "category" => "information",
                "hide_this" => false,
                "feature_display_schedules" => true,
                "feature_products" => false,
                "feature_vouchers" => false,
                "setting_maximum_allocation" => false,
                "setting_time_driven" => false,
                "setting_show_time" => true,
                "setting_show_long_description" => true,
                "option_purchasable" => false,
                "option_inquirable" => true,
                "option_specific" => false,
                "option_regular" => true,
                "setting_show_square_image" => true,
                "setting_show_banner_image" => true,
                "setting_show_venue_selection" => true,
                "setting_bepoz_product_id" => false,
                "setting_show_price" => false,
                "setting_show_listing_tags" => false,
                "setting_never_expired" => true
            ]
        );

        $this->createType('Points Redeem',
            [
                "name" => "Points Redeem",
                "category" => "information",
                "hide_this" => true,
                "feature_display_schedules" => true,
                "feature_products" => false,
                "feature_vouchers" => true,
                "setting_maximum_allocation" => true,
                "setting_time_driven" => false,
                "setting_show_time" => false,
                "setting_show_long_description" => true,
                "option_purchasable" => false,
                "option_inquirable" => false,
                "option_specific" => false,
                "option_regular" => false,
                "setting_show_square_image" => true,
                "setting_show_banner_image" => false,
                "setting_show_venue_selection" => true,
                "setting_bepoz_product_id" => false,
                "setting_show_price" => false,
                "setting_show_listing_tags" => false,
                "setting_never_expired" => true
            ]
        );

        $this->createType('Promotion',
            [
                "name" => "Promotion",
                "category" => "promotion",
                "hide_this" => false,
                "feature_display_schedules" => true,
                "feature_products" => false,
                "feature_vouchers" => true,
                "setting_maximum_allocation" => true,
                "setting_time_driven" => false,
                "setting_show_time" => true,
                "setting_show_long_description" => true,
                "option_purchasable" => false,
                "option_inquirable" => false,
                "option_specific" => true,
                "option_regular" => false,
                "setting_show_square_image" => true,
                "setting_show_banner_image" => false,
                "setting_show_venue_selection" => false,
                "setting_bepoz_product_id" => false,
                "setting_show_price" => false,
                "setting_show_listing_tags" => false,
                "setting_never_expired" => true
            ]
        );

        $this->createType('Gift Certificate',
            [
                "name" => "Gift Certificate",
                "category" => "information",
                "hide_this" => false,
                "feature_display_schedules" => true,
                "feature_products" => false,
                "feature_vouchers" => true,
                "setting_maximum_allocation" => true,
                "setting_time_driven" => false,
                "setting_show_time" => false,
                "setting_show_long_description" => true,
                "option_purchasable" => true,
                "option_inquirable" => true,
                "option_specific" => false,
                "option_regular" => false,
                "setting_show_square_image" => true,
                "setting_show_banner_image" => true,
                "setting_show_venue_selection" => false,
                "setting_bepoz_product_id" => false,
                "setting_show_price" => false,
                "setting_show_listing_tags" => false,
                "setting_never_expired" => true
            ]
        );

        $this->createType('Our Team',
            [
                "name" => "Our Team",
                "category" => "information",
                "hide_this" => false,
                "feature_display_schedules" => true,
                "feature_products" => false,
                "feature_vouchers" => false,
                "setting_maximum_allocation" => false,
                "setting_time_driven" => false,
                "setting_show_time" => false,
                "setting_show_long_description" => false,
                "option_purchasable" => false,
                "option_inquirable" => false,
                "option_specific" => false,
                "option_regular" => false,
                "setting_show_square_image" => true,
                "setting_show_banner_image" => true,
                "setting_show_venue_selection" => true,
                "setting_bepoz_product_id" => false,
                "setting_show_price" => false,
                "setting_show_listing_tags" => false,
                "setting_never_expired" => false
            ]
        );

        $this->createType('Stamp Card',
            [
                "name" => "Stamp Card",
                "category" => "information",
                "hide_this" => false,
                "feature_display_schedules" => true,
                "feature_products" => false,
                "feature_vouchers" => true,
                "setting_maximum_allocation" => false,
                "setting_time_driven" => false,
                "setting_show_time" => false,
                "setting_show_long_description" => false,
                "option_purchasable" => false,
                "option_inquirable" => false,
                "option_specific" => false,
                "option_regular" => false,
                "setting_show_square_image" => true,
                "setting_show_banner_image" => true,
                "setting_show_venue_selection" => true,
                "setting_bepoz_product_id" => false,
                "setting_show_price" => false,
                "setting_show_listing_tags" => false,
                "setting_never_expired" => true
            ]
        );

        $this->createType('Stamp Card Won',
            [
                "name" => "Stamp Card Won",
                "category" => "information",
                "hide_this" => false,
                "feature_display_schedules" => true,
                "feature_products" => false,
                "feature_vouchers" => true,
                "setting_maximum_allocation" => false,
                "setting_time_driven" => false,
                "setting_show_time" => false,
                "setting_show_long_description" => false,
                "option_purchasable" => false,
                "option_inquirable" => false,
                "option_specific" => false,
                "option_regular" => false,
                "setting_show_square_image" => true,
                "setting_show_banner_image" => true,
                "setting_show_venue_selection" => true,
                "setting_bepoz_product_id" => false,
                "setting_show_price" => false,
                "setting_show_listing_tags" => false,
                "setting_never_expired" => true
            ]
        );

        $this->createType('Shops',
            [
                "name" => "Shops",
                "category" => "information",
                "hide_this" => false,
                "feature_display_schedules" => true,
                "feature_products" => false,
                "feature_vouchers" => true,
                "setting_maximum_allocation" => false,
                "setting_time_driven" => false,
                "setting_show_time" => false,
                "setting_show_long_description" => false,
                "option_purchasable" => false,
                "option_inquirable" => false,
                "option_specific" => false,
                "option_regular" => false,
                "setting_show_square_image" => true,
                "setting_show_banner_image" => true,
                "setting_show_venue_selection" => true,
                "setting_bepoz_product_id" => false,
                "setting_show_price" => false,
                "setting_show_listing_tags" => false,
                "setting_never_expired" => true
            ]
        );

        $this->createType('Membership Tiers',
            [
                "name" => "Membership Tiers",
                "category" => "information",
                "hide_this" => false,
                "feature_display_schedules" => true,
                "feature_products" => false,
                "feature_vouchers" => true,
                "setting_maximum_allocation" => false,
                "setting_time_driven" => false,
                "setting_show_time" => false,
                "setting_show_long_description" => false,
                "option_purchasable" => true,
                "option_inquirable" => true,
                "option_specific" => false,
                "option_regular" => false,
                "setting_show_square_image" => true,
                "setting_show_banner_image" => true,
                "setting_show_venue_selection" => true,
                "setting_bepoz_product_id" => false,
                "setting_show_price" => false,
                "setting_show_listing_tags" => false,
                "setting_never_expired" => true
            ]
        );

    }

    function createType($name, $key)
    {
        $lt = \App\ListingType::where('name', $name)->first();
        if (is_null($lt)) {
            $lt = new \App\ListingType();
            $lt->name = $name;
            $lt->key = $key;
            $lt->save();
        }
    }
}

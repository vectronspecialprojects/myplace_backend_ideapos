<?php

use Illuminate\Database\Seeder;

class Restore extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::unprepared(file_get_contents(storage_path('backup/') . 'backup.sql'));
    }
}

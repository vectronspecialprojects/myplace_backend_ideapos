<?php

use Illuminate\Database\Seeder;

class VenuesTableSeeder extends Seeder
{
    public function run()
    {
        $this->createVenue('Venue 1', '103.17.223.46:9992', 'Vectron123', '103.17.223.46:9991',
            17, 20, 'false', 'OnlinePayment',
            'APP', 'no_track',
            \GuzzleHttp\json_encode(array(
                ["day" => "Mon", "time" => "9AM to 5PM"],
                ["day" => "Tue", "time" => "9AM to 5PM"],
                ["day" => "Wed", "time" => "9AM to 5PM"],
                ["day" => "Thu", "time" => "9AM to 5PM"],
                ["day" => "Fri", "time" => "9AM to 5PM"],
                ["day" => "Sat", "time" => "9AM to 5PM"],
                ["day" => "Sun", "time" => "9AM to 5PM"]
            )), \GuzzleHttp\json_encode(array(
                ["day" => "Mon", "time" => "9AM to 5PM"],
                ["day" => "Tue", "time" => "9AM to 5PM"],
                ["day" => "Wed", "time" => "9AM to 5PM"],
                ["day" => "Thu", "time" => "9AM to 5PM"],
                ["day" => "Fri", "time" => "9AM to 5PM"],
                ["day" => "Sat", "time" => "9AM to 5PM"],
                ["day" => "Sun", "time" => "9AM to 5PM"]
            )), '71 Boundary Road, North Melb, VIC, 3051',
            'jacky@vectron.com.au', '040404040',
            Carbon\Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s'),
            Carbon\Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s'),
        'https://s3-ap-southeast-2.amazonaws.com/bepoz-loyalty-app/bepoz-Icon.jpg',
            \GuzzleHttp\json_encode(array(
                ["platform" => "facebook", "url" => "www.facebook.com"],
                ["platform" => "twitter", "url" => "www.twitter.com"],
                ["platform" => "instagram", "url" => "www.instagram.com"],
                ["platform" => "snapchat", "url" => "www.snapchat.com"],
                ["platform" => "website", "url" => "www.example.com"]
            ))
        );

        $this->createVenue('Venue 2', '103.17.223.46:9992', 'Vectron123', '103.17.223.46:9991',
            17, 20, 'false', 'OnlinePayment',
            'APP', 'no_track',
            \GuzzleHttp\json_encode(array(
                ["day" => "Mon", "time" => "9AM to 5PM"],
                ["day" => "Tue", "time" => "9AM to 5PM"],
                ["day" => "Wed", "time" => "9AM to 5PM"],
                ["day" => "Thu", "time" => "9AM to 5PM"],
                ["day" => "Fri", "time" => "9AM to 5PM"],
                ["day" => "Sat", "time" => "9AM to 5PM"],
                ["day" => "Sun", "time" => "9AM to 5PM"]
            )), \GuzzleHttp\json_encode(array(
                ["day" => "Mon", "time" => "9AM to 5PM"],
                ["day" => "Tue", "time" => "9AM to 5PM"],
                ["day" => "Wed", "time" => "9AM to 5PM"],
                ["day" => "Thu", "time" => "9AM to 5PM"],
                ["day" => "Fri", "time" => "9AM to 5PM"],
                ["day" => "Sat", "time" => "9AM to 5PM"],
                ["day" => "Sun", "time" => "9AM to 5PM"]
            )), '71 Boundary Road, North Melb, VIC, 3051',
            'jacky@vectron.com.au', '040404040',
            Carbon\Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s'),
            Carbon\Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s'),
        'https://s3-ap-southeast-2.amazonaws.com/bepoz-loyalty-app/bepoz-Icon.jpg',
            \GuzzleHttp\json_encode(array(
                ["platform" => "facebook", "url" => "www.facebook.com"],
                ["platform" => "twitter", "url" => "www.twitter.com"],
                ["platform" => "instagram", "url" => "www.instagram.com"],
                ["platform" => "snapchat", "url" => "www.snapchat.com"],
                ["platform" => "website", "url" => "www.example.com"]
            ))
        );

        $this->createVenue('Venue 3', '103.17.223.46:9992', 'Vectron123', '103.17.223.46:9991',
            17, 20, 'false', 'OnlinePayment',
            'APP', 'no_track',
            \GuzzleHttp\json_encode(array(
                ["day" => "Mon", "time" => "9AM to 5PM"],
                ["day" => "Tue", "time" => "9AM to 5PM"],
                ["day" => "Wed", "time" => "9AM to 5PM"],
                ["day" => "Thu", "time" => "9AM to 5PM"],
                ["day" => "Fri", "time" => "9AM to 5PM"],
                ["day" => "Sat", "time" => "9AM to 5PM"],
                ["day" => "Sun", "time" => "9AM to 5PM"]
            )), \GuzzleHttp\json_encode(array(
                ["day" => "Mon", "time" => "9AM to 5PM"],
                ["day" => "Tue", "time" => "9AM to 5PM"],
                ["day" => "Wed", "time" => "9AM to 5PM"],
                ["day" => "Thu", "time" => "9AM to 5PM"],
                ["day" => "Fri", "time" => "9AM to 5PM"],
                ["day" => "Sat", "time" => "9AM to 5PM"],
                ["day" => "Sun", "time" => "9AM to 5PM"]
            )), '71 Boundary Road, North Melb, VIC, 3051',
            'jacky@vectron.com.au', '040404040',
            Carbon\Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s'),
            Carbon\Carbon::now(config('app.timezone'))->format('Y-m-d\TH:i:s'),
        'https://s3-ap-southeast-2.amazonaws.com/bepoz-loyalty-app/bepoz-Icon.jpg',
            \GuzzleHttp\json_encode(array(
                ["platform" => "facebook", "url" => "www.facebook.com"],
                ["platform" => "twitter", "url" => "www.twitter.com"],
                ["platform" => "instagram", "url" => "www.instagram.com"],
                ["platform" => "snapchat", "url" => "www.snapchat.com"],
                ["platform" => "website", "url" => "www.example.com"]
            ))

        );
    }

    function createVenue($name, $bepoz_api, $bepoz_mac_key, $bepoz_secondary_api,
                         $bepoz_till_id, $bepoz_operator_id, $bepoz_training_mode, $bepoz_payment_name,
                         $default_card_number_prefix, $default_card_track,
                         $open_and_close_hours, $pickup_and_delivery_hours, $address, $email, $telp,
                         $bepoz_account_last_successful_execution_time, $bepoz_voucher_last_successful_execution_time,
                         $image, $social_links)
    {
        $venue = \App\Venue::where('name', $name)->first();
        if (is_null($venue)) {
            $venue = new \App\Venue();
            $venue->name = $name;

            $venue->bepoz_api = $bepoz_api;
            $venue->bepoz_mac_key = $bepoz_mac_key;
            $venue->bepoz_secondary_api = $bepoz_secondary_api;
            $venue->bepoz_till_id = $bepoz_till_id;
            $venue->bepoz_operator_id = $bepoz_operator_id;
            $venue->bepoz_training_mode = $bepoz_training_mode;
            $venue->bepoz_payment_name = $bepoz_payment_name;
            $venue->default_card_number_prefix = $default_card_number_prefix;
            $venue->default_card_track = $default_card_track;

            $venue->open_and_close_hours = $open_and_close_hours;
            $venue->pickup_and_delivery_hours = $pickup_and_delivery_hours;
            $venue->address = $address;
            $venue->email = $email;
            $venue->telp = $telp;
            $venue->bepoz_account_last_successful_execution_time = $bepoz_account_last_successful_execution_time;
            $venue->bepoz_voucher_last_successful_execution_time = $bepoz_voucher_last_successful_execution_time;

            $venue->image = $image;
            $venue->social_links = $social_links;

            $venue->save();
        }
    }
}

<?php

use Illuminate\Database\Seeder;

class TiersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tiers')->insert([
            'name' => 'Normal',
            'key' => 'normal',
            'boundary_start' => 1,
            'boundary_end' => 999999,
            'is_default_tier' => true,
            // 'tier_group' => 'normal',
            'is_purchasable' => true,
            'is_an_expiry_tier' => true
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class StaffTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_roles = \App\UserRoles::with('user')
            ->where('role_id', 1)
            ->get();

        foreach ($user_roles as $user_role) {

            $staff = \App\Staff::where('user_id', $user_role->user->id)->first();
            if (is_null($staff)) {

                $pieces = explode("@", $user_role->user->email);

                $staff = new \App\Staff();
                $staff->user_id = $user_role->user->id;
                $staff->first_name = $pieces[0];
                $staff->last_name = 'Vectron';
                $staff->profile_img = "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/profile_default.png";
                $staff->save();
            }

            $member = \App\Member::where('user_id', $user_role->user->id)->first();
            if (is_null($member)) {

                $member = new \App\Member();
                $member->user_id = $user_role->user->id;
                $member->first_name = $staff->first_name;
                $member->last_name = $staff->last_name;
                $member->profile_img = "https://s3-ap-southeast-2.amazonaws.com/" . config('bucket.s3') . "/profile_default.png";
                $member->save();

                $ss = new App\UserRoles;
                $ss->role_id = 3; // Manually add
                $ss->user_id = $user_role->user->id;
                $ss->save();

                $tier = \App\Tier::first();
                $user = \App\User::find($user_role->user->id);
                $user->member->tiers()->save($tier);

            }

        }
    }
}

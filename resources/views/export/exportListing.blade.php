<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        body {
            font-weight: 400;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }

        .underline {
            border-bottom: 1px black solid;
        }

        .underline2 {
            border-bottom: 1px rosybrown solid;
        }

        .barcode-box {
            font-size: 16px;
            font-weight: 400;
            /*min-height: 100px;*/
            /*overflow: hidden;*/
            z-index: 1;
            /*position: relative;*/
            background: rgba(255, 255, 255, 0.0);
            border-radius: 2px;
            box-sizing: border-box;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            box-shadow: 0 2px 2px 0 rgba(0,0,0,.14),0 3px 1px -2px rgba(0,0,0,.2),0 1px 5px 0 rgba(0,0,0,.12);
            padding: 5px;
            display: inline-block;
        }

        .barcode-title {
            color: #FFFFFF;
            height: 176px;
            background: black;
        }

        .barcode-container {
            display: block;
        }

        .page-break {
            /*page-break-after: always;*/
        }
    </style>
</head>

<body>
    <table>
        <tr>
            <th class="underline">#</th>
            <th class="underline">Ordered At</th>
            <th class="underline">Name</th>
            <th class="underline">Bepoz Card</th>
            <th class="underline">Product</th>
            <th class="underline">Amount Cash Paid</th>
            {{--<th class="underline">Lookup(s)</th>--}}
            <th class="underline">Paid At</th>
            <th class="underline">Comment</th>
        </tr>
        @foreach ($order_details as $key => $collection)
            <tr>
                <td>{{$collection['order_detail']->id}}</td>
                <td>{{ date("jS F, Y H:i", strtotime($collection['order_detail']->order->created_at)) }}</td>
                <td>{{$collection['order_detail']->order->member->first_name . ' '. $collection['order_detail']->order->member->last_name }}</td>
                <td>{{$collection['order_detail']->order->member->bepoz_account_card_number}}</td>
                <td>{{$collection['order_detail']->qty}}x {{$collection['order_detail']->product->name}}</td>
                <td>{{toMoneyConverter($collection['order_detail']->order->actual_total_price) }}</td>
                <td>{{ date("jS F, Y H:i", strtotime($collection['order_detail']->order->paid_at)) }}</td>
                <td>{{$collection['order_detail']->order->comments}}</td>
            </tr>

        <tr>
            <td colspan="8">
                @if(intval($key)%3==0)
                    <div class="page-break"></div>
                @endif
            </td>
        </tr>

            <tr>
                <td class="underline2" colspan="8">
                    @foreach ($collection['barcodes'] as $key1 => $barcode)
                        @if ($key1 % 4 ==0)
                            <p></p>
                        @endif
                        <div class="barcode-box">
                            <div class="barcode-container" style="padding: 10px; display: inline;">{!! $barcode['image'] !!}</div>
                            <span><small><strong>Valid Until:</strong> @if ($barcode['voucher']->expire_date == 0 || \Carbon\Carbon::parse($barcode['voucher']->expire_date)->year == 1) No expiry
                                    date. @else {{date("jS F, Y", strtotime($barcode['voucher']->expire_date)) }} @endif</small></span><br/>
                            <span><small><strong>Voucher Code:</strong> {{ $barcode['voucher']->barcode }}</small></span>
                            </div>
                    @endforeach
                </td>
            </tr>
        @endforeach

    </table>
</body>

</html>
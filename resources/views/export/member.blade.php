<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<style>
    .cell {
        border: thick solid #000000;
    }

    .cell-top {
        border-top: thick solid #000000;
    }

    .cell-bottom {
        border-bottom: thick solid #000000;
    }

    .cell-right {
        border-right: thick solid #000000;
    }

    .yellow {
        background-color: #ffff00;
    }

    .gray {
        background-color: #808080;
    }

    .light-gray {
        background-color: #a9a9a9
    }

    .orange {
        background-color: #ff7f50;
    }

    .leaf-green {
        background-color: #7cfc00;
    }

    .center {
        vertical-align: middle;
    }

    .bold {
        font-weight: bold;
    }

    .wrapped {
        wrap-text: true;
    }
</style>

<tr></tr>

<tr class="bold">
    <td class="center" height="30" colspan="8" align="center"> Members </td>
</tr>

<tr class="bold">
    <td class="cell gray center" height="20" width="25" align="center">FIRST NAME</td>
    <td class="cell gray center" height="20" width="25" align="center">LAST NAME</td>
    <td class="cell gray center" height="20" width="20" align="center">MOBILE</td>
    <td class="cell gray center" height="20" width="20" align="center">EMAIL</td>
    <td class="cell gray center" height="20" width="20" align="center">CONFIRMED</td>
    <td class="cell gray center" height="20" width="20" align="center">SOURCE</td>
    <td class="cell gray center" height="20" width="20" align="center">JOINED AT</td>
    <td class="cell gray center" height="20" width="20" align="center">MEMBER ID</td>
    <td class="cell gray center" height="20" width="20" align="center">BEPOZ ID</td>
    <td class="cell gray center" height="20" width="20" align="center">BEPOZ ACCOUNT NUMBER</td>
    <td class="cell gray center" height="20" width="20" align="center">BEPOZ ACCOUNT CARD</td>
    <td class="cell gray center" height="20" width="20" align="center">TIER</td>
</tr>

@foreach ($rows as $row)

<tr>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{$row->first_name}}</td>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{$row->last_name}}</td>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{$row->user->mobile}}</td>
    <td class="center cell-top cell-bottom" height="20" width="40" align="center">{{$row->user->email}}</td>
    <td class="center cell-top cell-bottom" height="20" width="10" align="center">{{$row->user->email_confirmation ? 'yes' : 'not yet'}}</td>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{ getSocialLoginTypes($row->user->social_logins)}}</td>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{$row->user->created_at}}</td>
    <td class="center cell-top cell-bottom" height="20" width="10" align="center">{{$row->id}}</td>
    <td class="center cell-top cell-bottom" height="20" width="10" align="center">{{$row->bepoz_account_id}}</td>
    <td class="center cell-top cell-bottom" height="20" width="30" align="center">{{$row->bepoz_account_number}}</td>
    <td class="center cell-top cell-bottom" height="20" width="30" align="center">{{$row->bepoz_account_card_number}}</td>
    <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{$row->member_tier->tier->name}}</td>

</tr>

@endforeach


</html>
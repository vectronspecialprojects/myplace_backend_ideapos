<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>

<style type="text/css" >
    .cell {
        border: thick solid #000000;
    }

    .cell-top {
        border-top: thick solid #000000;
    }

    .cell-bottom {
        border-bottom: thick solid #000000;
    }

    .cell-right {
        border-right: thick solid #000000;
    }

    .yellow {
        background-color: #ffff00;
    }

    .gray {
        background-color: #808080;
    }

    .light-gray {
        background-color: #a9a9a9
    }

    .orange {
        background-color: #ff7f50;
    }

    .leaf-green {
        background-color: #7cfc00;
    }

    .center {
        vertical-align: middle;
    }

    .bold {
        font-weight: bold;
    }

    .wrapped {
        wrap-text: true;
    }
</style>

<table class="table table-striped">
<tbody>

<tr class="bold" >
    <td class="center" height="20" style="font-weight: bold; font-size: 16px; background-color: #D3D3D3;"> 
        Survey Report
    </td>
    <td class="center" height="20" style="font-weight: bold; font-size: large; background-color: #D3D3D3;"> 
        
    </td>
    <td class="center" height="20" style="font-weight: bold; font-size: large; background-color: #D3D3D3;"> 
        
    </td>
    <td class="center" height="20" style="font-weight: bold; font-size: large; background-color: #D3D3D3;"> 
        
    </td>
    <td class="center" height="20" style="font-weight: bold; font-size: large; background-color: #D3D3D3;"> 
        
    </td>
    <td class="center" height="20" style="font-weight: bold; font-size: large; background-color: #D3D3D3;"> 
        
    </td>
    <td class="center" height="20" style="font-weight: bold; font-size: large; background-color: #D3D3D3;"> 
        
    </td>
</tr>

<tr class="bold" >
    <td class="center" height="20" style="font-weight: bold; background-color: #D3D3D3;"> 
        Survey Name
    </td>
    <td class="center" height="20" style="font-weight: bold; background-color: #D3D3D3;"> 
        {{ $values['title'] }}
    </td>
    <td class="center" height="20" style="font-weight: bold; background-color: #D3D3D3;"> 
        
    </td>
    <td class="center" height="20" style="font-weight: bold; background-color: #D3D3D3;"> 
        From
    </td>
    <td class="center" height="20" style="font-weight: bold; background-color: #D3D3D3;"> 
        {{ $values['from'] }}
    </td>
    <td class="center" height="20" style="font-weight: bold; background-color: #D3D3D3;"> 
        To
    </td>
    <td class="center" height="20" style="font-weight: bold; background-color: #D3D3D3;"> 
        {{ $values['to'] }}
    </td>
</tr>


@if(!empty($values))

    <?php
    $total_order = 0;
    ?>


    <tr class="bold">
        <td class="cell gray center" height="20" width="20"  style="background-color: #F5F5F5;">Question</td>
        <td class="cell gray center" height="20" width="20"  style="background-color: #F5F5F5;">Answer</td>
        <td class="cell gray center" height="20" width="20"  style="background-color: #F5F5F5;">Member</td>
        <td class="cell gray center" height="20" width="10"  style="background-color: #F5F5F5;"></td>
        <td class="cell gray center" height="20" width="20"  style="background-color: #F5F5F5;"></td>
        <td class="cell gray center" height="20" width="10"  style="background-color: #F5F5F5;"></td>
        <td class="cell gray center" height="20" width="20"  style="background-color: #F5F5F5;"></td>
    </tr>

    @foreach($values['questions'] as $question)

        <tr>
            <td class="center cell-top cell-bottom" height="20" width="20" >{{$question['question']}}</td>
        </tr>

        
        @foreach($question['answers'] as $answer)

        <tr>
            <td class="center cell-top cell-bottom" height="20" width="20" ></td>
            <td class="center cell-top cell-bottom" height="20" width="20" >{{$answer['answer']}}</td>
            
        </tr>

            @foreach($values['survey_answers'] as $survey_answer)
                
                @if( $question['id'] == $survey_answer['question_id'] &&
                    ($answer['id'] == $survey_answer['answer'][0]['id'] || $answer['id'] == $survey_answer['answer_id']) )
                
                    <tr>
                        <td class="center cell-top cell-bottom" height="20" width="20" ></td>
                        <td class="center cell-top cell-bottom" height="20" width="20" ></td>
                        <td class="center cell-top cell-bottom" height="20" width="20" > {{ $survey_answer['member']['first_name']}} {{$survey_answer['member']['last_name']}} ({{$survey_answer['member_id']}})</td>
                        
                    </tr>
                                
                @endif

            @endforeach
        @endforeach
    @endforeach

@endif
</tbody>
</table>

</html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>

<style type="text/css" >
    .cell {
        border: thick solid #000000;
    }

    .cell-top {
        border-top: thick solid #000000;
    }

    .cell-bottom {
        border-bottom: thick solid #000000;
    }

    .cell-right {
        border-right: thick solid #000000;
    }

    .yellow {
        background-color: #ffff00;
    }

    .gray {
        background-color: #808080;
    }

    .light-gray {
        background-color: #a9a9a9
    }

    .orange {
        background-color: #ff7f50;
    }

    .leaf-green {
        background-color: #7cfc00;
    }

    .center {
        vertical-align: middle;
    }

    .bold {
        font-weight: bold;
    }

    .wrapped {
        wrap-text: true;
    }
</style>

<table class="table table-striped">
<tbody>
<tr></tr>

<tr class="bold" >
    <td class="center" height="30" colspan="10" style="text-align: center; font-weight: bold;"> 
        Feedback &amp; Enquiries Report<br>
    </td>
</tr>


@if(!empty($values))

    <?php
    $total_order = 0;
    ?>


    <tr class="bold">
        <td class="cell gray center" height="20" width="20" align="center" style="background-color: #808080;">#</td>
        <td class="cell gray center" height="20" width="20" align="center" style="background-color: #808080;">Subject</td>
        <td class="cell gray center" height="20" width="20" align="center" style="background-color: #808080;">Listing</td>
        <td class="cell gray center" height="20" width="20" align="center" style="background-color: #808080;">Received At</td>
        <td class="cell gray center" height="20" width="20" align="center" style="background-color: #808080;">Contact</td>
        <td class="cell gray center" height="20" width="20" align="center" style="background-color: #808080;">Rating</td>
        <td class="cell gray center" height="20" width="50" align="center" style="background-color: #808080;">Message</td>
        <td class="cell gray center" height="20" width="50" align="center" style="background-color: #808080;">Member</td>
        <td class="cell gray center" height="20" width="50" align="center" style="background-color: #808080;">Email</td>
        <td class="cell gray center" height="20" width="50" align="center" style="background-color: #808080;">Mobile</td>
    </tr>

    @foreach($values as $value)

        <tr>
            <td class="center cell-top cell-bottom" height="20" width="5" align="center">{{$value['id']}}</td>
            <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{$value['subject']}}</td>
            <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{$value['listing']['heading']}} ({{$value['listing']['id']}})</td>
            <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{$value['created_at']}}</td>
            <td class="center cell-top cell-bottom" height="20" width="10" align="center">{{$value['contact_me']  === 1 ? "yes" : '' }}</td>
            <td class="center cell-top cell-bottom" height="20" width="10" align="center">{{$value['rating']}}</td>
            <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{$value['message']}}</td>
            <td class="center cell-top cell-bottom" height="20" width="30" align="center">{{$value['member']['first_name']}} {{$value['member']['last_name']}}</td>
            <td class="center cell-top cell-bottom" height="20" width="30" align="center">{{$value['member']['user']['email']}}</td>
            <td class="center cell-top cell-bottom" height="20" width="20" align="center">{{$value['member']['user']['mobile']}}</td>
        </tr>

    @endforeach


@endif
</tbody>
</table>

</html>
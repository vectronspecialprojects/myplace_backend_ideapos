<style>

    @page {
        size: A4;
    }

    .invoice-box {
        /*max-width:800px;*/
        /*margin:auto;*/
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }

    .invoice-box table {
        width: 100%;
        /*line-height:inherit;*/
        text-align: left;
    }

    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }

    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }

    .invoice-box table tr.top table td {
        padding-bottom:20px;
    }

    .invoice-box table tr.top table td.title {
        font-size:45px;
        line-height: 45px;
        color: #333;
    }

    .invoice-box table tr.information table td {
        padding-bottom:40px;
    }

    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }

    .invoice-box table tr.details td {
        padding-bottom:20px;
    }

    .invoice-box table tr.item td {
        border-bottom: 1px solid #eee;
    }

    .invoice-box table tr.item.last td {
        border-bottom: none;
    }

    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
</style>

<div class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr class="top">
            <td colspan="2">
                <table>
                    <tr>
                        <td class="title">
                            <span><strong>Date:</strong> {{ $reservation_date }}</span><br />
                            @if(is_null($image))
                                <br />
                                <img id="logo" src="./images/banner.jpg" style="width: 300px" alt="Logo">
                            @else
                                {!! $image !!}
                                <span><small>{!! $lookup !!}</small></span>
                            @endif
                        </td>

                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="information">
            <td colspan="2">
                <table>
                    <tr>
                        <td>
                            <span><strong>Booker:</strong></span><br />
                            <span>Member ID #:{{$member->bepoz_account_id}}</span><br/>
                            <span>{{$member->first_name}} {{$member->last_name}}</span><br/>
                            <span>{{$user->email}}</span>
                        </td>

                        <td>
                            <span><strong>Organiser:</strong></span><br />
                            <span>{{$company_name}}</span><br/>
                            <span>{{$company_address1}}</span><br/>
                            <span>{{$company_address2}}</span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

    </table>

    <hr />

    <table cellpadding="0" cellspacing="0">
        <tr class="information">
            <td colspan="2">
                <table>
                    <tr>
                        <td>
                            <h3><strong>Information:</strong></h3>
                            <span><strong>Reservation Date:</strong> {{$reservation_date}}</span><br />
                            <span><strong>Booth:</strong> {{$listing->name}}</span><br/>
                            <span><strong>Space:</strong> {{$listing->spaces->first()->space->name}}</span><br/>
                            <span><strong>Number of Guest:</strong> {{$member_reservation->number_of_people}}</span><br />
                            <span><strong>Occasion:</strong> <br />{{ is_null($member_reservation->occasion) || $member_reservation->occasion == "" ? "Not Provided" : $member_reservation->occasion }}</span><br />
                            <span><strong>Comments:</strong> <br />{{ is_null($order->comments) ? "Not Provided" : $order->comments }}</span>
                        </td>

                        <td>
                            <span><strong>Booked on:</strong> {{ \Carbon\Carbon::parse($order->created_at)->format('l jS \\of F Y')}}</span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

    </table>


</div>

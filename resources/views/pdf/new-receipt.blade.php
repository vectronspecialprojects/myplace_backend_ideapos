<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<HEAD>
    <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <META http-equiv="X-UA-Compatible" content="IE=8">
    <STYLE type="text/css">

        html { margin: 0px; }
        @page { margin: 0px; }
        body { margin: 0px; }

        body {margin-top: 0px;margin-left: 0px; font-family: sans-serif}

        .full-image {width: 100%;}

        .ft0{font: bold 24px 'Segoe UI';color: #67c8c7;line-height: 32px; font-family: sans-serif; text-align: center;}
        .ft1{font: bold 16px 'Segoe UI';color: #595959;line-height: 21px; font-family: sans-serif;}
        .ft2{font: 16px 'Segoe UI';color: #595959;line-height: 21px; font-family: sans-serif;}
        .ft3{font: bold 19px 'Calibri';color: #67c8c7;line-height: 23px;font-family: sans-serif;}
        .ft4{font: bold 18px 'Calibri';color: #67c8c7;line-height: 22px;font-family: sans-serif;}
        .ft5{font: bold 16px 'Calibri';color: #3f3f3f;line-height: 19px;font-family: sans-serif;}
        .ft6{font: 16px 'Calibri';color: #3f3f3f;line-height: 19px;font-family: sans-serif;}
        .ft8{font: bold 13px 'Segoe UI';color: #595959;line-height: 17px;font-family: sans-serif;}

        .p0{margin-top: 35px;}
        .p1{text-align: left;padding-left: 51px;margin-top: 35px;margin-bottom: 0px;}
        .p2{text-align: left;padding-left: 51px;margin-top: 7px;margin-bottom: 0px;}
        .p3{text-align: left;padding-left: 51px;margin-top: 8px;margin-bottom: 0px;}
        .p8{text-align: center;white-space: nowrap;}
        .p9{text-align: left;padding-left: 3px;white-space: nowrap;}
        .p12{text-align: left;white-space: nowrap;}
        .p13{text-align: center;white-space: nowrap;}

        .p14{text-align: center;margin-top: 69px;margin-bottom: 0px;}
        .p16{text-align: center;margin-top: 24px;margin-bottom: 0px;}

        .td0{padding: 0px;margin: 0px;vertical-align: bottom;}
        .td1{padding: 0px;margin: 0px;vertical-align: bottom;}
        .td2{padding: 0px;margin: 0px;vertical-align: bottom;}
        .td3{padding: 0px;margin: 0px;vertical-align: bottom;}
        .td4{padding: 0px;margin: 0px;vertical-align: bottom;}
        .td5{padding: 0px;margin: 0px;vertical-align: bottom;}

        .tdicon {text-align: center;}

        .tr0{height: 23px;}
        .tr1{height: 36px;}
        .tr2{height: 42px;}

        .t0{width: 100%;font: 16px 'Calibri';color: #3f3f3f; text-align: center;font-family: sans-serif;margin-top: 40px;}
        .t1{width:100%;font: bold 13px 'Segoe UI';color: #595959;}
        .t2{width: 100%;text-align: center;font-family: sans-serif;margin-top: 40px;}

        .icon {max-width: 50px;}
        .hr-margin-top {margin-top: 150px;}

        .badge {max-width: 100px;}
    </STYLE>
</HEAD>

<body>
<div id="page_1">
    <div id="p1dimg1">
        <img class="full-image" src="./images/header-background.png">
    </div>

    <div class="dclr"></div>
    <table class="t2">
        <tr>
            <td>
                <p class="p0 ft0">HERE IS YOUR RECEIPT</p>
            </td>
        </tr>
    </table>
    <p class="p1 ft2"><SPAN class="ft1">Purchased by: </SPAN>{{ $member->first_name .' '. $member->last_name }}</p>
    <p class="p2 ft2"><SPAN class="ft1">Purchased Number: </SPAN>{{ $order->id }}</p>
    <p class="p3 ft2"><SPAN class="ft1">Purchase Date: </SPAN>{{ $order->created_at }}</p>
    <p class="p2 ft2"><SPAN class="ft1">Payment Method: </SPAN>{{ $order->type }}</p>
    @if (isset($bepoz_transaction_ids))
        <p class="p2 ft2"><SPAN class="ft1">Trans. ID: </SPAN>{{ $bepoz_transaction_ids }}</p>
    @endif

    <table cellpadding=0 cellspacing=0 class="t0">
        <tr>
            <td class="tr0 td0"><p class="p4 ft3">QTY</p></td>
            <td class="tr0 td1"><p class="p5 ft3">Description</p></td>
            <td class="tr0 td2"><p class="p6 ft4">Unit Price</p></td>
            <td class="tr0 td3"><p class="p7 ft4">Total Price</p></td>
        </tr>
        @foreach ($order_details as $i => $order_detail)
        <tr>
            <td class="tr1 td0"><p class="p8 ft5">{{$order_detail->qty}}</p></td>
            <td class="tr1 td1"><p class="p9 ft6">{{$order_detail->product_name}}</p></td>
            <td class="tr1 td2"><p class="p10 ft6">AUD {{ toMoneyConverter(($order_detail->unit_price))}}</p></td>
            <td class="tr1 td3"><p class="p11 ft6">AUD {{ toMoneyConverter(($order_detail->qty * $order_detail->unit_price))}}</p></td>
        </tr>
        @endforeach
        <tr>
            <td class="tr2 td0"><p class="p12 ft7">&nbsp;</p></td>
            <td class="tr2 td1"><p class="p12 ft6">TOTAL</p></td>
            <td class="tr2 td2"><p class="p12 ft7">&nbsp;</p></td>
            <td class="tr2 td3"><p class="p13 ft3">AUD {{toMoneyConverter($order->actual_total_price)}}</p></td>
        </tr>
    </table>

    <hr class="hr-margin-top" />

    <table cellpadding=0 cellspacing=0 class="t1">
        <tr>
            <td class="tr3 td4"><p class="p13 ft8">FOLLOW US ON</p></td>
            <td class="tr3 td5"><p class="p13 ft8">FIND US ON</p></td>
        </tr>
        <tr>
            <td class="tdicon">
                <img class="icon" src="./images/facebook.png">
                <img class="icon" src="./images/twitter.png">
                <img class="icon" src="./images/google.png">
                <img class="icon" src="./images/Linkedin.png">
                <img class="icon" src="./images/instagram.png">
            </td>
            <td class="tdicon">
                <img class="badge" src="./images/appstore.png">
                <img class="badge" src="./images/googleplay.png">
            </td>
        </tr>
    </table>
    <p class="p14 ft2">Do you have a question? Please refer to the FAQs or call the {{ $venue_name }} Customer Service
        Centre on {{$venue_contact}}. {{ $venue_name }}, {{ $venue_address .' '. $venue_address2 }}, Australia.
    </p>
</div>
</body>
</html>

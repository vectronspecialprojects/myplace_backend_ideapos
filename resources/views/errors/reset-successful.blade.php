<!DOCTYPE html>
<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            display: table;
            font-weight: 100;
            font-family: 'Lato';
            background-color: black;
        }

        .container {
            height: 100vh;
        }

        .content {
            margin-top: 20%;
            padding: 0 10px;
        }

        .title {
            font-size: 18px;
            font-weight: 300;
            color: white;
        }

        .button {
            /*margin-top: 20px;*/
            width: 100%;
            height: 30px;
            color: white;
            background-color: black;
            font-size: 18px;
        }
        .button:disabled {
            background-color: #666;
        }

    </style>

    <script>
        function close_window() {
            var win = window.open("about:blank", "_self");
            win.close();
        }
    </script>
</head>
<body>
<div class="container">
    <div class="content row">
      <div class="col-sx-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
          <img id="logo" src="{{$company_logo}}" class="img-responsive" style="margin: 0 auto; max-width:100%;" alt="Logo">
          <h3 class="text-center" style="color: white">{!! isset($heading)?$heading:'success!' !!}</h3>
        <div class="title text-center" style="color: white">{!! isset($msg)?$msg:'Reset Successful!' !!}</div>
        <!-- <div class="title text-center" style="color: white">Please log in with your new password.</div> -->
        <div><button class="button" onclick="close_window();">CLOSE</button></div>
      </div>
    </div>
</div>
</body>
</html>

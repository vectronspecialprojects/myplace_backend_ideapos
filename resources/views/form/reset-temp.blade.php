<!DOCTYPE html>
<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            display: table;
            font-weight: 100;
            font-family: 'Lato';
            background-color: black;
        }

        .container {
            height: 100vh;
            /*text-align: center;*/
            /*display: table-cell;*/
            /*vertical-align: middle;*/
        }

        .content {
            margin-top: 20%;
            /*text-align: center;*/
            /*display: inline-block;*/
        }

        .title {
            font-size: 96px;
            white-space: nowrap;
        }

        .control-label {
            color: #eee;
            /*text-align: right;*/
            margin-bottom: 0;
        }

        .button {
            /*margin-top: 20px;*/
            width: 100%;
            height: 30px;
            color: white;
            background-color: black;
        }

        .button:disabled {
            background-color: #666;
        }

        .help-block {
            color: white;
        }

    </style>
</head>
<body>
<div class="container">
    <div class="content row">
        <div class="col-sx-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
            <img id="logo" src="{{$company_logo}}" class="img-responsive" style="margin: 0 auto; max-width:100%;"
                 alt="Logo">
            <h1 class="text-center" style="color: white; white-space: nowrap;">RESET PASSWORD</h1>
            <div class="text-center" style="color: white">Please type your new Password:</div>

            <form action="{{$url}}" method="post" id="myForm">
                <input type="hidden" id="app_token" name="app_token" value="{{$app_token}}">
                <input type="hidden" id="app_secret" name="app_secret" value="{{$app_secret}}">
                <div class="form-group">
                    <label for="password" class="control-label">Temporary Password</label>
                    <input type="password" name="temporary_password" id="temporary_password" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="password" class="control-label">New Password</label>
                    <input type="password" name="password" id="password" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="retry_password" class="control-label">Confirm Password</label>
                    <input type="password" id="retry_password" name="retry_password" class="form-control" required>
                    <strong><span id="helpRetry" class="help-block"></span></strong>
                </div>
                <input type="submit" id="sub-btn" class="button" disabled value="CHANGE PASSWORD">
            </form>

        </div>
    </div>
</div>

<script
        src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>
<script type="text/javascript">
    (function () {
        var form = $('#myForm');
        var pwInput = $('#password');
        var rePwInput = $('#retry_password');
        var btn = $('#sub-btn');
        var labelretry = $("#helpRetry");
        var strongRegex = new RegExp("^(?=.*\\d)(?=.*[A-Za-z]).{6,}$");

        pwInput.keyup(function () {
            if (form[0].checkValidity() && pwInput.val() !== '' && rePwInput.val() !== ''
                && pwInput.val() === rePwInput.val() && strongRegex.test(pwInput.val()) && strongRegex.test(rePwInput.val())) {

                btn.prop('disabled', false);
                labelretry.text("");

            }
            else {
                if (pwInput.val() === rePwInput.val()) {
                    btn.prop('disabled', true);
                    labelretry.text("Password must have at least 6 characters with at least 1 letter and 1 number");
                }
                else {
                    btn.prop('disabled', true);
                    labelretry.text("Passwords do not match");
                }
            }

        });

        rePwInput.keyup(function (e) {
            if (form[0].checkValidity() && pwInput.val() !== '' && rePwInput.val() !== ''
                && pwInput.val() === rePwInput.val() && strongRegex.test(pwInput.val()) && strongRegex.test(rePwInput.val())) {

                btn.prop('disabled', false);
                labelretry.text("");

            }
            else {
                if (pwInput.val() === rePwInput.val()) {
                    btn.prop('disabled', true);
                    labelretry.text("Password must have at least 6 characters with at least 1 letter and 1 number");
                }
                else {
                    btn.prop('disabled', true);
                    labelretry.text("Passwords do not match");
                }

            }
        });
    })();
</script>
</body>
</html>

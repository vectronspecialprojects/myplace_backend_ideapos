<!DOCTYPE html>
<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            display: table;
            font-weight: 100;
            font-family: 'Lato';
            background-color: black;
        }

        .container {
          height: 100vh;
            /*text-align: center;*/
            /*display: table-cell;*/
            /*vertical-align: middle;*/
        }

        .content {
          margin-top: 20%;
            /*text-align: center;*/
            /*display: inline-block;*/
        }

        .title {
            font-size: 96px;
            white-space: nowrap;
        }

        .control-label{
          color: #eee;
          /*text-align: right;*/
          margin-bottom: 0;
        }

        .cancel-button {
            /*margin-top: 20px;*/
            width: 100%;
            height: 30px;
            color: black;
            background-color: white;
        }

        .button {
            /*margin-top: 20px;*/
            width: 100%;
            height: 30px;
            color: white;
            background-color: black;
        }

        .button:disabled {
            background-color: #666;
        }

    </style>
</head>
<body>
<div class="container">
    <div class="content row">
      <div class="col-sx-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
          <img id="logo" src="{{$company_logo}}" class="img-responsive" style="margin: 0 auto; max-width:100%;" alt="Logo">
          <h1 class="text-center" style="color: white; white-space: nowrap;"> CONFIRMATION</h1>
        <div class="text-center" style="color: white">Do you want to UN-SUBSCRIBE from us? </div>
          <form action="{{$url}}" method="post" id="myForm">
              <input type="hidden" id="app_token" name="app_token" value="{{$app_token}}">
              <input type="hidden" id="app_secret" name="app_secret" value="{{$app_secret}}">
              <input type="button" id="cancel-btn" class="cancel-button" value="CANCEL">
              <input type="submit" id="sub-btn" class="button" value="CONFIRM">
          </form>
      </div>
    </div>
</div>

<script
        src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>
<script type="text/javascript">
    (function(){
        var cancelBTN = $('#cancel-btn');

        cancelBTN.click(function(){
            window.close();
        });
    })();
</script>
</body>
</html>
